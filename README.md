Spring4 为ExtJS5提供Restful服务
===================================
> 本项目为另一个前端项目 <http://git.oschina.net/chen4w/es> 提供后台服务。<br/>
> 本项目以SpringSide4的[QuickStart](https://github.com/springside/springside4/wiki/QuickStart)（以下简称为SSQ ）为蓝本建立，<br>
> 建立方法亦参考了相关文档：<https://github.com/springside/springside4/wiki/CreateNewProject>


整合思路
-----------------------------------
  1. SSQ本身已经是一个很简洁的Restful原型，可以完全满足提供Restful服务的要求。<br>
  	思路：Restful封装机制是现成的。
  	
  2. SSQ前端界面采用Bootstrap风格的多页面实现；ExtJS作为一个重量级RIA组件，具有调度一切前端逻辑的强烈控制欲。<br>
   思路：后端的Controller在页面之间进行调度，与ExtJS的前端调度相抵触，弃用之，只依赖返回结构化对象的Restful即可。
   
  3. SSQ集成shiro作为用户认证和权限检查组件，<br>
  	沿用之。
  
  4. SSQ在数据访问层推荐使用Spring-Data-JPA，<br>
  	不排斥这种方式，但是会增加一种适应泛型实体类基本操作的机制，在业务规则允许的前提下,不必针对每个实体类一个个编写controller、派生JPA访问接口。
  	
  5.  ExtJS通过Ext.data.proxy.Rest实现了一套restful请求规则，<br>
  	服务端需要分析其请求格式，分析其可接收的数据DTO格式，在restful请求分析和应答时百依百顺。
   
  6. 整合过程保留SSQ本身逻辑、保持其多页面界面始终可用，方便在两种页面机制之间进行比较。
  
  
实现说明
-----------------------------------
  1.  [RestService.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/rest/RestService.java) 处理Restful请求<br/>
  
  2.  [ApiController.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/web/api/ApiController.java) 定义Restful请求映射<br/>
  
  3.  为[AccountService.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/service/account/AccountService.java)增加方法：应答 /auth 登录请求，并且在applicationContext-shiro.xml中设置 /auth/** = anon
  
  4.  引入泛型DAO [BaseJpaDao.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/dao/BaseJpaDao.java)<br>
   	 [ApiController.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/web/api/ApiController.java) 中定义对泛型entity的rest操作映射： /rest/{entity}/* ,method = RequestMethod.GET 、PUT、POST。
   	  
  5.   [ExtReq.java] (http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/filter/ExtReq.java) 负责分析ExtJS的grid filter请求，将其转化为JPA请求。[ResultDTO.java](http://git.oschina.net/chen4w/se/blob/master/src/main/java/net/bat/dto/ResultDTO.java) 封装返回结果。