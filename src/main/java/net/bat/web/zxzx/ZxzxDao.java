package net.bat.web.zxzx;

import java.util.List;

import net.bat.dto.MessageDTO;
import net.bat.entity.Message;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ZxzxDao extends PagingAndSortingRepository<Message, Long>, JpaSpecificationExecutor<Message> {
	@Query("select o from Message o where o.pid=?1 and bpublish=1 order by id")
	Page<Message> findByPid(Integer pid, Pageable pageRequest);
	
	@Query("select o from Message o where o.pid=0 and o.jid=?1 and bpublish=1 order by id")
	Page<Message> findByJid(Integer jid, Pageable pageRequest);

	@Query("select new net.bat.dto.MessageDTO(m.title, m.content, m.dtCreate, m.uid, u.roles, u.name) from Message m,User u where m.pid=?1 and u.id=m.uid and m.bpublish=1 order by m.id")
	List<MessageDTO> findByPid(Integer pid);

	Message findById(Long id);
}
