package net.bat.web.zxzx;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import net.bat.dto.MessageDTO;
import net.bat.entity.Message;
import net.bat.rest.RestService;
import net.bat.service.account.ShiroDbRealm.ShiroUser;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ZxzxController {
	private static final String PAGE_SIZE = "5";
	@Autowired
	private ZxzxService service;
	@Autowired
	private RestService restService;

	public static final String EN_MESSAGE = "Message";

	@RequestMapping(value = "/zxzx", method = RequestMethod.GET)
	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
			ServletRequest request) {
		
		//代理机构ID
        String jid_str = request.getParameter("jid")==null ? "0" : request.getParameter("jid").toString();    
        int jid = Integer.parseInt(jid_str);
		Page<Message> objs = service.getJidMessage(jid, pageNumber, pageSize, sortType);
		model.addAttribute("objs", objs);
		model.addAttribute("jid",jid);
		long pid = 0;
		if (objs.getContent().size() > 0) {
			Message mo = objs.getContent().get(0);
			pid = mo.getId();
		}
		model.addAttribute("pid", pid);
		return "zxzx/list";
	}

	@RequestMapping(value = "/zxzx/detail/{id}", method = RequestMethod.GET)
	public String showDetail(@PathVariable("id") Long id, Model model) {
		model.addAttribute("id", id);
		int pid = id.intValue();
		Message pobj = service.findMessage(pid);
		if (pid > 0) {
			List<MessageDTO> objs = service.getMessage(pid);
			model.addAttribute("objs", objs);
		}
		model.addAttribute("pobj", pobj);
		return "zxzx/detail";
	}

	@RequestMapping(value = "/rest/Message/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public <T> T updateEntity(@PathVariable Long id, @RequestBody Map<String, Object> rmap) throws Exception {
		rmap.put("uid", getCurrentUserId());
		rmap.put("dtCreate", new Date());
		return (T) restService.updateEntity(EN_MESSAGE, id, rmap);
	}

	@RequestMapping(value = "/rest/Message", method = RequestMethod.POST)
	@ResponseBody
	public <T> T updateEntity(@RequestBody Map<String, Object> rmap) throws Exception {
		rmap.put("uid", getCurrentUserId());
		rmap.put("dtCreate", new Date());
		return (T) restService.updateEntity(EN_MESSAGE, rmap);
	}

	@RequestMapping(value = "/zxzx/create", method = RequestMethod.POST)
	@ResponseBody
	public Message create(@RequestBody Map<String, Object> rmap) {
		Message m = new Message();
		m.setUid(getCurrentUserId().intValue());
		m.setDtCreate(new Date());
		m.setTitle(((String) rmap.get("val_title")));
		m.setContent(((String) rmap.get("val_content")));
		String jid = rmap.get("val_jid") == null ? "0" : (String) rmap.get("val_jid");
		m.setJid(Integer.parseInt(jid));
		service.saveMessage(m);
		return m;
	}

	private Long getCurrentUserId() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		if (user == null) {
			return new Long(0);
		}
		return user.id;
	}

}
