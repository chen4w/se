package net.bat.web.grzx;

import java.util.List;

import net.bat.entity.Bookmark;
import net.bat.entity.Message;
import net.bat.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
@Component
@Transactional
public class GrzxService {
	
	@Autowired
	private GrzxDao gd;
	
	@Autowired
	private GrzxMessDao gmd;
	
	@Autowired
	private GrzxBookDao gbd;
	
	/**
	 * 按照ID查询
	 * @param id
	 * @return
	 */
	public User getUser(Long id) {
		
		return gd.findOne(id);
		
	}
	
	//通过ID查询对应的Message表
	
	
	
	public List<Message> getMessage(int uid){
		List<Message> mes= gmd.findMesByUid(uid);
		return mes;
	}
	
	public List<Bookmark> getBook(int uid){
		List<Bookmark> boo=gbd.findBookByUid(uid);
		return boo;
	}

	public List<Message> getpMessage(int pid){
		List<Message> mes= gmd.findMesByPid(pid);
		return mes;
	}
	
	public List<Message> getMessages(Long id){
		List<Message> mes= gmd.findMesById(id);
		return mes;
	}
	
}
