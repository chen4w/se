package net.bat.web.grzx;

import java.util.List;

import net.bat.entity.Message;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;


//查询留言表的接口
public interface GrzxMessDao extends PagingAndSortingRepository<Message, Long>, JpaSpecificationExecutor<Message>{
				List<Message> findMesByUid(int uid);
				List<Message> findMesByPid(int pid);
				List<Message> findMesById(Long id);
				
}
