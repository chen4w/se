package net.bat.web.grzx;

import net.bat.entity.User;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * 用户id唯一，按照用户id查询
 * @author zmh
 *
 */
public interface GrzxDao extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User>{

	

}
