package net.bat.web.grzx;

import java.util.List;

import net.bat.entity.Bookmark;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;


//查询收藏表的接口
public interface GrzxBookDao extends PagingAndSortingRepository<Bookmark, Long>, JpaSpecificationExecutor<Bookmark>{
	   List<Bookmark> findBookByUid(int uid);

}

