package net.bat.web.aboutus;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AboutUsController {

	@RequestMapping(value = "/aboutus/gywm", method = RequestMethod.GET)
	public String gotoGywm(){
		return "aboutus/gywm";
	}
	
	@RequestMapping(value = "/aboutus/lxwm", method = RequestMethod.GET)
	public String gotoLxwm(){
		return "aboutus/lxwm";
	}
	
	@RequestMapping(value = "/aboutus/wzsm", method = RequestMethod.GET)
	public String gotoWzsm(){
		return "aboutus/wzsm";
	}
}
