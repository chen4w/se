package net.bat.web.dljg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springside.modules.persistence.SearchFilter;

import com.google.common.collect.Lists;

import net.bat.entity.SDljg;

public class DljgSpecification<T>{
	
	public static <T> Specification<T> Search( final List<SDljg> list,final Class<T> entityClazz,final Map<String,Object> eparas) {
		return new Specification<T>() {
		@Override
		public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
			// TODO Auto-generated method stub
			Path expression = root.get("jgdm");
			Set<String> keys = eparas.keySet();
			List<Predicate> predicates = Lists.newArrayList();
			for(SDljg  s : list){
				predicates.add(cb.equal(expression, s.getJgdm()));
			}
			Predicate s = cb.or(predicates.toArray(new Predicate[predicates.size()]));
			if(!keys.isEmpty()){
			Predicate[] predicatess = new Predicate[keys.size() + 1];
			for(int i = 0 ; i < keys.size() ; i++){
				if(eparas.containsKey("dy")){
					Path expression1 = root.get("dy");
					String value = (String) eparas.get("dy");
					String[] dys = value.split("_");
					List<Predicate> exp = Lists.newArrayList();
					for(String dy : dys){
						exp.add(cb.like(expression1, "%" + dy + "%"));
					}
					predicatess[i] =cb.or(exp.toArray(new Predicate[exp.size()]));
				}
				if(eparas.containsKey("sl_rs")){
					Path expression1 = root.get("dlrrs");
					String value = (String) eparas.get("sl_rs");
					if(value.equals("5lt")){
						predicatess[i] = cb.lessThan(expression1, 5);
					}
					if(value.equals("5-10")){
						predicatess[i] = cb.between(expression1, 5, 10);
					}
					if(value.equals("10-15")){
						predicatess[i] = cb.between(expression1, 10, 15);
					}
					if(value.equals("15-20")){
						predicatess[i] = cb.between(expression1, 15, 20);
					}
					if(value.equals("20-40")){
						predicatess[i] = cb.between(expression1, 20, 40);
					}
					if(value.equals("40gt")){
						predicatess[i] = cb.greaterThan(expression1, 40);
					}
					
				}
				if(eparas.containsKey("sl_slnx")){
					Path expression1 = root.get("dtSlsj");
					String value = (String) eparas.get("sl_slnx");
					if(value.equals("5lt")){
						Calendar calendar = Calendar.getInstance(); 
						Date date = calendar.getTime();
						int year = calendar.get(Calendar.YEAR);
						calendar.set(year - 5, 0, 0);
						predicatess[i] = cb.between(expression1, calendar.getTime(), new Date());
					}
					if(value.equals("5-10")){
						Calendar firstcalendar = Calendar.getInstance(); 
						Calendar secondcalendar = Calendar.getInstance(); 
						int year = firstcalendar.get(Calendar.YEAR);
						firstcalendar.set(year - 10, 0, 0);
						secondcalendar.set(year - 5, 12, 31);
						predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
					}
					if(value.equals("10-15")){
						Calendar firstcalendar = Calendar.getInstance(); 
						Calendar secondcalendar = Calendar.getInstance(); 
						int year = firstcalendar.get(Calendar.YEAR);
						firstcalendar.set(year - 15, 0, 0);
						secondcalendar.set(year - 10, 12, 31);
						predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
					}
					if(value.equals("15-20")){
						Calendar firstcalendar = Calendar.getInstance(); 
						Calendar secondcalendar = Calendar.getInstance(); 
						int year = firstcalendar.get(Calendar.YEAR);
						firstcalendar.set(year - 20, 0, 0);
						secondcalendar.set(year - 15, 12, 31);
						predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
					}
					if(value.equals("20-40")){
						Calendar firstcalendar = Calendar.getInstance(); 
						Calendar secondcalendar = Calendar.getInstance(); 
						int year = firstcalendar.get(Calendar.YEAR);
						firstcalendar.set(year - 40, 0, 0);
						secondcalendar.set(year - 20, 12, 31);
						predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
					}
					if(value.equals("40gt")){
						Calendar calendar = Calendar.getInstance(); 
						Date date = calendar.getTime();
						int year = calendar.get(Calendar.YEAR);
						calendar.set(year - 40, 12, 31);
						predicatess[i] = cb.lessThan(expression1, calendar.getTime());
					}
					
				}
				
				if(eparas.containsKey("sl_fwlx")){
					Path expression1 = null;
					String value = (String) eparas.get("sl_fwlx");
					if(value.equals("zlsq")){
						 expression1 = root.get("zlsq");
					}
					if(value.equals("zlfs")){
						expression1 = root.get("zlfs");
					}
					if(value.equals("zlwx")){
						 expression1 = root.get("zlwx"); 
					}
					if(value.equals("zlxzss")){
						 expression1 = root.get("xzss");
					}
					if(value.equals("zlmsss")){
						 expression1 = root.get("msss");
					}
					if(value.equals("zlqsjf")){
						 expression1 = root.get("zlqsjf");
					}
					if(value.equals("zljs")){
						 expression1 = root.get("zljs");
					}
					if(value.equals("zlfxpy")){
						 expression1 = root.get("zlfxpj");
					}
					if(value.equals("zlyj")){
						 expression1 = root.get("zlyj");
					}
					if(value.equals("zlyy")){
						 expression1 = root.get("zlyy");
					}
					if(value.equals("qyzscqgb")){
						 expression1 = root.get("zzcqgb");
					}
					predicatess[i] = cb.greaterThan(expression1, "0");
				}
				if(eparas.containsKey("name")){
					Path expression1 = root.get("mc");
					String value = (String) eparas.get("name");
					System.out.println(value);
					predicatess[i] = cb.like(expression1, "%" + value + "%");
				}
			}
				predicatess[keys.size()] = s;
				return cb.and(predicatess);
			}
			
			return s;
		}
		};
	}

}
