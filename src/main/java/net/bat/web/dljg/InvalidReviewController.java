/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.web.dljg;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import net.bat.dao.UserDAO;
import net.bat.entity.EFswx;
/**
 * Dljg管理的Controller, 使用Restful风格的Urls:
 * 
 * List page : GET /dljg/
 *   
 * @author calvin
 */
@Controller
public class InvalidReviewController {

	private static final String PAGE_SIZE = "10";

	private static Map<String, String> sortTypes = Maps.newLinkedHashMap();
	static {
		sortTypes.put("auto", "自动");
		sortTypes.put("mc", "名称");
		sortTypes.put("jgdm", "机构代码");
	}

	@Autowired
	private DljgService dljgService;
	@Autowired
	private InvalidReviewService invalidreviewservice;
	@Autowired
	private UserDAO dao;
	
	@RequestMapping(value = "/leader", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String>  leaderNam(@RequestParam(value = "name", defaultValue = "sdf")  String name) {
		System.out.println(name);
		String jsonContent = "{\"zzxm\":\"200\"}"; 
		ObjectMapper mapper = new ObjectMapper();  
		Map map = new HashMap();  
		map.put("file", "file");
		EFswx s = null;
		try {
			s = new ObjectMapper().readValue(jsonContent, EFswx.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String content = null;
		try {
			content = mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
         System.out.println(content);  
       
         return map;
	}
	
	@RequestMapping(value = "/invalidreview", method = RequestMethod.GET)
	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
			ServletRequest request) {
		Map<String,Object> eparams = new HashMap<String,Object>();
		handleEDParameter(eparams,request);
		model.addAttribute("sortType", sortType);
		model.addAttribute("sortTypes", sortTypes);
		model.addAttribute("sortAsc", sortAsc);
		// 将搜索条件编码成字符串，用于排序，分页的URL
		Map<String, String[]> pmap = request.getParameterMap();
		model.addAttribute("pmap", pmap);
		Map<String, String> search = new HashMap<String,String>();
		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
		model.addAttribute("map", search);
		if(!search.isEmpty()){
			model.addAttribute("divFlag", true);
		}else{
			model.addAttribute("divFlag", false);
		}
		return "dljg/invalidreview";
	}
	

	void handleEDParameter(Map<String, Object> searchParams,ServletRequest request){
		Map<String,String[]> allParas = request.getParameterMap();
		Set<String> list = allParas.keySet();
		for(String key : list){
			if(key.contains("sl_ajlx")){
				searchParams.put("sl_ajlx", allParas.get(key)[0]);
			}else if(key.contains("sl_jdnf")){
				searchParams.put("sl_jdnf", allParas.get(key)[0]);
			}else if (key.contains("sl_flsy_fs")){
				searchParams.put("sl_flsy_fs", allParas.get(key)[0]);
			}else if (key.contains("sl_flsy_wx")){
				searchParams.put("sl_flsy_wx", allParas.get(key)[0]);
			}else if (key.contains("sl_zllx")){
				searchParams.put("sl_zllx", allParas.get(key)[0]);
			}else if(key.contains("sl_jsly")){
				searchParams.put("sl_jsly", allParas.get(key)[0]);
			}else if(key.contains("sl_jdjl_fs")){
				searchParams.put("sl_jdjl_fs", allParas.get(key)[0]);
			}else if(key.contains("sl_jdjl_wx")){
				searchParams.put("sl_jdjl_wx", allParas.get(key)[0]);
			}else if(key.contains("sl_examinerName")){
				searchParams.put("sl_examinerName", allParas.get(key)[0]);
			}else if(key.contains("sl_leaderName")){
				searchParams.put("sl_leaderName", allParas.get(key)[0]);
			}else if(key.contains("sl_dljgs")){
				searchParams.put("sl_dljgs", allParas.get(key)[0]);
			}else if(key.contains("sl_dlrs")){
				searchParams.put("sl_dlrs", allParas.get(key)[0]);
			}
		}
	}
	
	
	
	
	public void arrangeParams(ServletRequest request,Map<String, String> searchParams){
	    Map<String, String[]> map = request.getParameterMap();  
        Set<Entry<String, String[]>> set = map.entrySet();  
        Iterator<Entry<String, String[]>> it = set.iterator();  
        while (it.hasNext()) {  
            Entry<String, String[]> entry = it.next();  
            String key = entry.getKey(); 
            String value = entry.getValue()[0];
        	if(key.contains("sl_ajlx")){
        		searchParams.put("sl_ajlx", value);
        	}
        	if(key.contains("sl_jdnf")){
        		searchParams.put("sl_jdnf", value);
        	}
        	if(key.contains("sl_flsy_fs")){
        		searchParams.put("sl_flsy_fs", value);
        	}
        	if(key.contains("sl_flsy_wx")){
        		searchParams.put("sl_flsy_wx", value);
        	}
        	if(key.contains("sl_zllx")){
        		searchParams.put("sl_zllx", value);
        	}
        	if(key.contains("sl_jsly")){
        		searchParams.put("sl_jsly", value);
        	}
        	if(key.contains("sl_jdjl_fs")){
				searchParams.put("sl_jdjl_fs", value);
			}
        	if(key.contains("sl_jdjl_wx")){
				searchParams.put("sl_jdjl_wx", value);
			}
        }  
	}
	
}
