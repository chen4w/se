/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.web.dljg;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.web.Servlets;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import net.bat.dao.UserDAO;
import net.bat.dto.EDljgVO;
import net.bat.entity.Attach;
import net.bat.entity.EDljg;
import net.bat.entity.EKh;

/**
 * Dljg管理的Controller, 使用Restful风格的Urls:
 * 
 * List page : GET /dljg/
 *   
 * @author calvin
 */
@Controller
public class DljgController {

	private static final String PAGE_SIZE = "10";

	private static Map<String, String> sortTypes = Maps.newLinkedHashMap();
	static {
		sortTypes.put("auto", "自动");
		sortTypes.put("mc", "名称");
		sortTypes.put("jgdm", "机构代码");
	}

	@Autowired
	private DljgService dljgService;
	
	@Autowired
	private UserDAO dao;
	
	private int chinaMax=0,worldMax=0; //应用于代理机构详细页--客户分布热点图数量
	
//	@RequestMapping(value = "/dljg", method = RequestMethod.GET)
//	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
//			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
//			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
//			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
//			ServletRequest request) {
//		String city = request.getParameter("dy");
//		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
//		Map<String,Object> eparams = new HashMap<String,Object>();
//		stitchingParameter(searchParams,request); // 处理参数
//		handleEDParameter(eparams,request);
//		//c4w 
//		//Map<String,Object> dljg = dljgService.getDljg(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
//		//Page<EDljg> EDljg = (Page<EDljg>) dljg.get("EDljg"); 
//		Page<EDljgDTO> objs =  dljgService.getDljgPage(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
//		List<EDljgDTO> edd = objs.getContent();
//		Calendar calendar = Calendar.getInstance(); 
//		int year = calendar.get(Calendar.YEAR);
//		for(EDljgDTO s : edd){
//			Date t = s.getDtSlsj();
//			String nx = null;
//			if(t != null){
//			calendar.setTime(t);
//			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
//			}
//			s.setDate(nx);
//		}
//		model.addAttribute("objs", objs);
//		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
//		model.addAttribute("sortType", sortType);
//		model.addAttribute("sortTypes", sortTypes);
//		model.addAttribute("sortAsc", sortAsc);
//		// 将搜索条件编码成字符串，用于排序，分页的URL
//		Map<String, String[]> pmap = request.getParameterMap();
//		model.addAttribute("pmap", pmap);
//		Map<String, String> search = new HashMap<String,String>();
//		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
//		model.addAttribute("map", search);
//		if(search.containsKey("sl_fwlx")){//"zlfs","zlwx"
//			if(search.get("sl_fwlx").equals("zlfs")){
//				model.addAttribute("fsflag", true);
//			}
//			if(search.get("sl_fwlx").equals("zlwx")){
//				model.addAttribute("wxflag", true);
//			}
//		}
//		if(!search.isEmpty()){
//			model.addAttribute("divFlag", true);
//		}else{
//			model.addAttribute("divFlag", false);
//		}
//		model.addAttribute("city", city);
//		String temp = arrangeSearchParams(request,searchParams);    //处理参数
//		model.addAttribute("searchParams", temp);//Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
//		System.out.println(Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
//		return "dljg/list";
//	}
	
	
	@RequestMapping(value = "/dljg", method = RequestMethod.GET)
	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
			ServletRequest request) {
		String city = request.getParameter("dy");   
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String,Object> eparams = new HashMap<String,Object>();
		stitchingParameter(searchParams,request); // 处理参数
		handleEDParameter(eparams,request);
		Page<EDljgVO> objs =  dljgService.getDljgPage(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
		List<EDljgVO> edd = objs.getContent();
		Calendar calendar = Calendar.getInstance(); 
		int year = calendar.get(Calendar.YEAR);
		for(EDljgVO s : edd){
			Date t = s.getDtSlsj();
			String nx = null;
			if(t != null){
			calendar.setTime(t);
			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
			}
			s.setDate(nx);
		}
		model.addAttribute("objs", objs);
		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
		model.addAttribute("sortType", sortType);
		model.addAttribute("sortTypes", sortTypes);
		model.addAttribute("sortAsc", sortAsc);
		// 将搜索条件编码成字符串，用于排序，分页的URL
		Map<String, String[]> pmap = request.getParameterMap();
		model.addAttribute("pmap", pmap);
		Map<String, String> search = new HashMap<String,String>();
		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
		model.addAttribute("map", search);
		if(search.containsKey("sl_fwlx")){//"zlfs","zlwx"
			/*if(search.get("sl_fwlx").equals("zlfs")){
				model.addAttribute("fsflag", true);
			}
			if(search.get("sl_fwlx").equals("zlwx")){
				model.addAttribute("wxflag", true);
			}
			if(search.get("sl_fwlx").equals("zlxzss")){
				model.addAttribute("xsflag", true);
			}
			if(search.get("sl_fwlx").equals("zlmsss")){
				model.addAttribute("msflag", true);
			}*/
			model.addAttribute("sl_fwlx", search.get("sl_fwlx"));
		}
		if(!search.isEmpty()){
			model.addAttribute("divFlag", true);
		}else{
			model.addAttribute("divFlag", false);
		}
		model.addAttribute("city", city);
		String temp = arrangeSearchParams(request,searchParams);    //处理参数
		model.addAttribute("searchParams", temp);//Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
		//System.out.println(Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
		return "dljg/list";
	}
	
	

	
//	@RequestMapping(value = "/technology", method = RequestMethod.GET)
//	public String technicallist(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
//			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
//			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
//			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
//			ServletRequest request) {
//		String flfs = request.getParameter("flfs");
//		String flh = request.getParameter("flh");
//		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
//		Map<String,Object> eparams = new HashMap<String,Object>();
//		stitchingParameter(searchParams,request); // 处理参数
//		handleEDParameter(eparams,request);
//		Page<EDljgDTO> objs =  dljgService.getDljgPageForJsly(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc,flfs,flh);
//		List<EDljgDTO> edd = objs.getContent();
//		Calendar calendar = Calendar.getInstance(); 
//		int year = calendar.get(Calendar.YEAR);
//		for(EDljgDTO s : edd){
//			Date t = s.getDtSlsj();
//			String nx = null;
//			if(t != null){
//			calendar.setTime(t);
//			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
//			}
//			s.setDate(nx);
//		}
//		model.addAttribute("objs", objs);
//		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
//		model.addAttribute("flfs", flfs);
//		model.addAttribute("flh", flh);
//		model.addAttribute("sortType", sortType);
//		model.addAttribute("sortTypes", sortTypes);
//		model.addAttribute("sortAsc", sortAsc);
//		// 将搜索条件编码成字符串，用于排序，分页的URL
//		Map<String, String> search = new HashMap<String,String>();
//		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
//		model.addAttribute("map", search);
//		model.addAttribute("sl_jsly", search.get("sl_jsly"));
//		if(search.containsKey("sl_fwlx")){//"zlfs","zlwx"
//			if(search.get("sl_fwlx").equals("zlfs")){
//				model.addAttribute("fsflag", true);
//			}
//			if(search.get("sl_fwlx").equals("zlwx")){
//				model.addAttribute("wxflag", true);
//			}
//		}
//		if(!search.isEmpty()){
//			model.addAttribute("divFlag", true);
//		}else{
//			model.addAttribute("divFlag", false);
//		}
//		String temp = arrangeSearchParams(request,searchParams);    //处理参数
//		model.addAttribute("searchParams", temp);//Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
//		Map<String, String[]> pmap = request.getParameterMap();
//		model.addAttribute("pmap", pmap);
//		Set<String> keys = pmap.keySet();
//		if(!keys.contains("dy") && !keys.contains("sl_rs") && !keys.contains("sl_slnx") && !keys.contains("sl_jsly")){
//			return "redirect:/dljg";
//		}
//		return "dljg/technicallist";
//	}
	
	
	@RequestMapping(value = "/technology", method = RequestMethod.GET)
	public String technicallist(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
			ServletRequest request) {
		Map<String, String[]> pmap = request.getParameterMap();
		Set<String> keys = pmap.keySet();
		if(!keys.contains("sl_jsly")){
			return "redirect:/dljg";
		}
		String flfs = request.getParameter("flfs");
		String flh = request.getParameter("flh");
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String,Object> eparams = new HashMap<String,Object>();
		stitchingParameter(searchParams,request); // 处理参数
		handleEDParameter(eparams,request);
		Page<EDljgVO> objs =  dljgService.getDljgPageForJsly(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc,flfs,flh);
		List<EDljgVO> edd = objs.getContent();
		Calendar calendar = Calendar.getInstance(); 
		int year = calendar.get(Calendar.YEAR);
		for(EDljgVO s : edd){
			Date t = s.getDtSlsj();
			String nx = null;
			if(t != null){
			calendar.setTime(t);
			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
			}
			s.setDate(nx);
		}
		model.addAttribute("objs", objs);
		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
		model.addAttribute("flfs", flfs);
		model.addAttribute("flh", flh);
		model.addAttribute("sortType", sortType);
		model.addAttribute("sortTypes", sortTypes);
		model.addAttribute("sortAsc", sortAsc);
		// 将搜索条件编码成字符串，用于排序，分页的URL
		Map<String, String> search = new HashMap<String,String>();
		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
		model.addAttribute("map", search);
		model.addAttribute("sl_jsly", search.get("sl_jsly"));
		if(search.containsKey("sl_fwlx")){//"zlfs","zlwx"
			if(search.get("sl_fwlx").equals("zlfs")){
				model.addAttribute("fsflag", true);
			}
			if(search.get("sl_fwlx").equals("zlwx")){
				model.addAttribute("wxflag", true);
			}
		}
		if(!search.isEmpty()){
			model.addAttribute("divFlag", true);
		}else{
			model.addAttribute("divFlag", false);
		}
		String temp = arrangeSearchParams(request,searchParams);    //处理参数
		model.addAttribute("searchParams", temp);//Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
		
		model.addAttribute("pmap", pmap);
		return "dljg/technicallist";
	}
	
	@RequestMapping(value = "/dljg/detail/{id}", method = RequestMethod.GET)
	public String showDetail(@PathVariable("id") Long id, Model model) {
		EDljg obj = dljgService.getDljg(id);
		long eid = obj.getEid();
		
		//总代理量(通过s_dljg表中的pid=id,fl=100000查找)
		int dll = dljgService.getZdll(id);
		model.addAttribute("dll", dll);
		
		//代理人人数
		int dlrrs = obj.getDlrrs();
				
		//注：在e_dljg加 eid字段，用于存e_dljg_sh的主键值，eid是Edljg中的eid,目前缺失该字段
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月");//yyyy-MM-dd HH:mm:ss
		
		//设立时间
		if(obj.getDtSlsj() != null){
			String slsj = df.format(obj.getDtSlsj());
			model.addAttribute("slsj", slsj);
		}		
		
		//奖惩状态
		String jczt = obj.getJczt()==0 ? "无" : "有";
		
		//公司简介
		String gsjs = obj.getGsjs();
		if(null != gsjs && gsjs.length() >= 270){
			gsjs =gsjs.substring(0, 267) + "......" + "<a class='dljg_gsjs' href='javascript: dljgInfo();'>【详情】</a>";
		}
		model.addAttribute("gsjs", gsjs);
		model.addAttribute("obj", obj);		
		model.addAttribute("jczt", jczt);		
		model.addAttribute("url_fopen", dljgService.getUrlFopen());
		model.addAttribute("url_dljg_attach", dljgService.getUrlDljgAttach());
		
		//图片附件
		/*delete by zhangqiuyi 426暂时屏蔽机构上传的简介图片。原方法未区分附件类型或名称，待改进*/
		List<Attach> ls = dljgService.getEDljgjjAttachs(eid);
		if (ls != null) {
			model.addAttribute("robjs", ls.toArray());
		}
		
		/* 公司荣誉附图  */
		List<Attach> honorls = dljgService.getEDljgryAttachs(eid);
		if (honorls != null) {
			model.addAttribute("honorarr", honorls.toArray());
		}
		
		//近五年
		Calendar cl = Calendar.getInstance(); 
		int jinnian = cl.get(Calendar.YEAR);//今年
		int[] year5 = new int[]{jinnian-5,jinnian-4,jinnian-3,jinnian-2,jinnian-1};
		model.addAttribute("year5", year5);
		
		//客户分布图区-响应数据
		
		List<Map<String,Object>>  chinaList = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>>  worldList = new ArrayList<Map<String,Object>>();
		getEdljgKhdata(id,chinaList,worldList);		
		String china_json = null,world_json=null;
		try {
			china_json = new ObjectMapper().writeValueAsString(chinaList);
			world_json = new ObjectMapper().writeValueAsString(worldList);
		} catch (JsonProcessingException e) {
			china_json = "[]";
			world_json = "[]";
		}
		model.addAttribute("chinaJson", china_json);
		model.addAttribute("worldJson", world_json);
		model.addAttribute("chinaMax", chinaMax);
		model.addAttribute("worldMax", worldMax);
		
		//代理案量展示区-响应数据
		Map<String,Object> firstMap = dljgService.getTotalDljgData(id,jinnian);
		model.addAttribute("firstMap", firstMap);
		Map<String,Object> secondMap = dljgService.getTotalAjDljgData(id,jinnian);
		model.addAttribute("secondMap", secondMap);
		Map<String,Object> thirdMap = dljgService.getSDljgAjslInfo(id);
		model.addAttribute("thirdMap", thirdMap);
		Map<String,Object> fourMap = dljgService.getSDljgFswxInfo(id);
		model.addAttribute("fourMap", fourMap);
		
		//人员结构展示区分页-响应数据
		Map<String,Object> fiveMap = dljgService.getEDlrdata(id,dlrrs,jinnian);
		model.addAttribute("fiveMap", fiveMap);
		
		//技术领域展示区分页-响应数据
		//发明实用新型
		List<String> jslyxx = new ArrayList<String>();
		//List<Map<String,Object>>  xxMap = getJslyXxTotalData(id,jslyxx);
		List<Map<String,Object>>  xxMap = getJslyTotalData(id, 1, jslyxx);
		String xxmc_json = null;
		String xx_json = null;
		try {
			xxmc_json = new ObjectMapper().writeValueAsString(jslyxx);
			xx_json = new ObjectMapper().writeValueAsString(xxMap);
		} catch (JsonProcessingException e) {
			xxmc_json = "[]";
			xx_json = "[]";
		}
		model.addAttribute("xxmc_json", xxmc_json);
		model.addAttribute("xx_json", xx_json);
		
		//外观
		List<String> jslywg = new ArrayList<String>();
		//List<Map<String,Object>>  wgMap = getJslyWgTotalData(id,jslywg);
		List<Map<String,Object>>  wgMap = getJslyTotalData(id, 2, jslywg);
		String wgmc_json = null;
		String wg_json = null;
		try {
			wgmc_json = new ObjectMapper().writeValueAsString(jslywg);
			wg_json = new ObjectMapper().writeValueAsString(wgMap);
		} catch (JsonProcessingException e) {
			wgmc_json = "[]";
			wg_json = "[]";
		}
		model.addAttribute("wgmc_json", wgmc_json);
		model.addAttribute("wg_json", wg_json);
		
		//国民经济产业分类
		List<String> jslyEconomy = new ArrayList<String>();
		List<Map<String,Object>>  economyMap = getJslyTotalData(id, 3, jslyEconomy);
		String economyLegend_json = null;
		String economyData_json = null;
		try {
			economyLegend_json = new ObjectMapper().writeValueAsString(jslyEconomy);
			economyData_json = new ObjectMapper().writeValueAsString(economyMap);
		} catch (JsonProcessingException e) {
			economyLegend_json = "[]";
			economyData_json = "[]";
		}
		model.addAttribute("economyLegend_json", economyLegend_json);
		model.addAttribute("economyData_json", economyData_json);
				
				
		model.addAttribute("action", "update");
		return "dljg/detail";
	}
	
	/*
	 * 代理机构详细页
	 * 内容使用
	 */
	@RequestMapping(value = "/dljg/form/{id}", method = RequestMethod.GET)
	public String showForm(@PathVariable("id") Long id, Model model) {
		EDljg obj = dljgService.getDljg(id);
		long eid = obj.getEid();
		
		//总代理量(通过s_dljg表中的pid=id,fl=100000查找)
		int dll = dljgService.getZdll(id);
		model.addAttribute("dll", dll);
		
		//代理人人数
		int dlrrs = obj.getDlrrs();
				
		//注：在e_dljg加 eid字段，用于存e_dljg_sh的主键值，eid是Edljg中的eid,目前缺失该字段
		SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月");//yyyy-MM-dd HH:mm:ss
		
		//设立时间
		if(obj.getDtSlsj() != null){
			String slsj = df.format(obj.getDtSlsj());
			model.addAttribute("slsj", slsj);
		}		
		
		//奖惩状态
		String jczt = obj.getJczt()==0 ? "无" : "有";
		model.addAttribute("obj", obj);		
		model.addAttribute("jczt", jczt);		
		model.addAttribute("url_fopen", dljgService.getUrlFopen());
		
		//图片附件
		//delete by zhangqiuyi 426暂时屏蔽机构上传的简介图片。原方法未区分附件类型或名称，待改进
		List<Attach> ls = dljgService.getEDljgjjAttachs(eid);
		if (ls != null) {
			model.addAttribute("robjs", ls.toArray());
		}
		
		//近五年
		Calendar cl = Calendar.getInstance(); 
		int jinnian = cl.get(Calendar.YEAR);//今年
		int[] year5 = new int[]{jinnian-5,jinnian-4,jinnian-3,jinnian-2,jinnian-1};
		model.addAttribute("year5", year5);
		
		//客户分布图区-响应数据
		List<Map<String,Object>>  chinaList = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>>  worldList = new ArrayList<Map<String,Object>>();
		getEdljgKhdata(id,chinaList,worldList);		
		String china_json = null,world_json=null;
		try {
			china_json = new ObjectMapper().writeValueAsString(chinaList);
			world_json = new ObjectMapper().writeValueAsString(worldList);
		} catch (JsonProcessingException e) {
			china_json = "[]";
			world_json = "[]";
		}
		model.addAttribute("chinaJson", china_json);
		model.addAttribute("worldJson", world_json);
		model.addAttribute("chinaMax", chinaMax);
		model.addAttribute("worldMax", worldMax);
		
		//代理案量展示区-响应数据
		Map<String,Object> firstMap = dljgService.getTotalDljgData(id,jinnian);
		model.addAttribute("firstMap", firstMap);
		Map<String,Object> secondMap = dljgService.getTotalAjDljgData(id,jinnian);
		model.addAttribute("secondMap", secondMap);
		Map<String,Object> thirdMap = dljgService.getSDljgAjslInfo(id);
		model.addAttribute("thirdMap", thirdMap);
		Map<String,Object> fourMap = dljgService.getSDljgFswxInfo(id);
		model.addAttribute("fourMap", fourMap);
		
		//人员结构展示区分页-响应数据
		Map<String,Object> fiveMap = dljgService.getEDlrdata(id,dlrrs,jinnian);
		model.addAttribute("fiveMap", fiveMap);
		
		//技术领域展示区分页-响应数据
		//发明实用新型
		List<String> jslyxx = new ArrayList<String>();
		//List<Map<String,Object>>  xxMap = getJslyXxTotalData(id,jslyxx);
		List<Map<String,Object>>  xxMap = getJslyTotalData(id, 1, jslyxx);
		String xxmc_json = null;
		String xx_json = null;
		try {
			xxmc_json = new ObjectMapper().writeValueAsString(jslyxx);
			xx_json = new ObjectMapper().writeValueAsString(xxMap);
		} catch (JsonProcessingException e) {
			xxmc_json = "[]";
			xx_json = "[]";
		}
		model.addAttribute("xxmc_json", xxmc_json);
		model.addAttribute("xx_json", xx_json);
		
		//外观
		List<String> jslywg = new ArrayList<String>();
		//List<Map<String,Object>>  wgMap = getJslyWgTotalData(id,jslywg);
		List<Map<String,Object>>  wgMap = getJslyTotalData(id, 2, jslywg);
		String wgmc_json = null;
		String wg_json = null;
		try {
			wgmc_json = new ObjectMapper().writeValueAsString(jslywg);
			wg_json = new ObjectMapper().writeValueAsString(wgMap);
		} catch (JsonProcessingException e) {
			wgmc_json = "[]";
			wg_json = "[]";
		}
		model.addAttribute("wgmc_json", wgmc_json);
		model.addAttribute("wg_json", wg_json);
		
		//国民经济产业分类
		List<String> jslyEconomy = new ArrayList<String>();
		List<Map<String,Object>>  economyMap = getJslyTotalData(id, 3, jslyEconomy);
		String economyLegend_json = null;
		String economyData_json = null;
		try {
			economyLegend_json = new ObjectMapper().writeValueAsString(jslyEconomy);
			economyData_json = new ObjectMapper().writeValueAsString(economyMap);
		} catch (JsonProcessingException e) {
			economyLegend_json = "[]";
			economyData_json = "[]";
		}
		model.addAttribute("economyLegend_json", economyLegend_json);
		model.addAttribute("economyData_json", economyData_json);
				
				
		model.addAttribute("action", "update");
		return "dljg/form";
	}

	@RequestMapping(value = "/rest/EDljgSh/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public <T> T updateEntity(@PathVariable Long id, @RequestBody Map<String, Object> rmap) throws Exception {
		return (T) dljgService.update(id, rmap);
	}
	/**
	 * 代理机构保存
	 * @param entity
	 * @param id
	 * @param rmap
	 * @return
	 * @throws Exception
	 * @author wangshuxin
	 * 2016-04-20
	 */
	@RequestMapping(value = "/rest/EDljg/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public <T> T updateEDljg(@PathVariable Long id, @RequestBody Map<String, Object> rmap)
			throws Exception {
		return (T) dljgService.updateEDljg(id, rmap);
	}
	/**
	 * 所有RequestMapping方法调用前的Model准备方法, 实现Struts2 Preparable二次部分绑定的效果,先根据form的id从数据库查出Task对象,再把Form提交的内容绑定到该对象上。
	 * 因为仅update()方法的form中有id属性，因此仅在update时实际执行.
	 */
	@ModelAttribute
	public void getTask(@RequestParam(value = "id", defaultValue = "-1") Long id, Model model) {
		if (id != -1) {
			model.addAttribute("obj", dljgService.getDljg(id));
		}
	}
	
	void handleEDParameter(Map<String, Object> searchParams,ServletRequest request){
		Map<String,String[]> allParas = request.getParameterMap();
		Set<String> list = allParas.keySet();
		for(String key : list){
			if(key.contains("sl_rs")){
				searchParams.put("sl_rs", allParas.get(key)[0]);
			}else if(key.contains("sl_slnx")){
				searchParams.put("sl_slnx", allParas.get(key)[0]);
			}else if (key.contains("sl_fwlx")){
				searchParams.put("sl_fwlx", allParas.get(key)[0]);
			}else if (key.contains("dy")){
				searchParams.put("dy", allParas.get(key)[0]);
			}else if (key.contains("name")){
				if(allParas.get(key)[0] != null && !"".equals(allParas.get(key)[0].trim())){
					searchParams.put("name", allParas.get(key)[0]);
				}
				
			}else if(key.contains("sl_pjqlx")){
				searchParams.put("sl_pjqlx", allParas.get(key)[0]);
			}else if(key.contains("sl_smsys")){
				searchParams.put("sl_smsys", allParas.get(key)[0]);
			}else if(key.contains("sl_dll")){
				searchParams.put("sl_dll", allParas.get(key)[0]);
			}
		}
	}
	
	void stitchingParameter(Map<String, Object> searchParams,ServletRequest request){
		Map<String,String[]> allParas = request.getParameterMap();
		Set<String> list = allParas.keySet();
		StringBuffer sbf = new StringBuffer();         //s_dljg标识位 (字段对应fl)
		searchParams.clear();
		if(list.isEmpty()){
			sbf.append("100000");
		}
		for(String key : allParas.keySet()){
			if(!list.contains("sl_pjqlx") && !list.contains("search_sl_pjqlx") && 
					!list.contains("search_sl_smsys") && !list.contains("sl_smsys")){
				sbf.append("100");
				break;
			}
			if(key.contains("sl_pjqlx")){
				sbf.append("105");
				searchParams.put("sl_pjqlx", "sl_pjqlx");
			}else if(key.contains("sl_smsys")){
				sbf.append("106");
				searchParams.put("sl_smsys", "sl_smsys");
			}
		}
		for(String key : allParas.keySet()){
			if(! list.contains("sl_zllx") && !list.contains("search_sl_zllx")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_zllx")){
				String[] value = allParas.get(key);
			     if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
		for(String key : allParas.keySet()){
			if(! list.contains("sl_ywlx") && !list.contains("search_sl_ywlx")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_ywlx")){
				String[] value = allParas.get(key);
			    if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
		for(String key : allParas.keySet()){
			if(! list.contains("sl_jsly") && !list.contains("search_sl_jsly")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_jsly")){
				String[] value = allParas.get(key);
			    if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
		
			searchParams.put("EQ_fl", sbf.toString());
		
			
	
	}
	
	
	public String arrangeSearchParams(ServletRequest request,Map<String, Object> searchParams){
	    Map<String, String[]> map = request.getParameterMap();
	    StringBuffer str = new StringBuffer();
        Set<Entry<String, String[]>> set = map.entrySet();  
        Iterator<Entry<String, String[]>> it = set.iterator();  
        while (it.hasNext()) {  
            Entry<String, String[]> entry = it.next(); 
            String key = entry.getKey();  
            String value = entry.getValue()[0];
        	if(key.contains("sl_pjqlx")){
        		searchParams.put("sl_pjqlx", value);
        	}
        	if(key.contains("sl_smsys")){
        		searchParams.put("sl_smsys", value);
        	}
        	if(key.contains("sl_zllx")){
        		searchParams.put("sl_zllx", value);
        	}
        	if(key.contains("sl_ywlx")){
        		searchParams.put("sl_ywlx", value);
        	}
        	if(key.contains("sl_jsly")){
        		searchParams.put("sl_jsly", value);
        	}
        	if(key.contains("sl_rs")){
				searchParams.put("sl_rs", value);
			}
        	if(key.contains("sl_slnx")){
				searchParams.put("sl_slnx", value);
			}
        	if (key.contains("sl_fwlx")){
				searchParams.put("sl_fwlx", value);
			}
        	if(key.contains("sl_ywlx")){
				searchParams.put("sl_ywlx", value);
			}
        	if(key.contains("sl_dll")){
				searchParams.put("sl_dll", value);
			}
        	if(key.contains("dy")){
				searchParams.put("dy", value);
			}
        	if(key.contains("name")){
        		if( value.trim() != null && !"".equals( value.trim()) ){
        			searchParams.put("name", value.trim());
        		}
			}
        	if(key.contains("flfs")){
				searchParams.put("flfs", value);
			}
        	if(key.contains("flh")){
				searchParams.put("flh", value);
			}
        }  
        for(String s : searchParams.keySet()){
        	if(s.equals("EQ_fl")){
        		str.append("search_" + s + "=" + searchParams.get(s) +"&");
        	}else{
        		str.append(s + "=" + searchParams.get(s) +"&");
        	}
        	
        }
        return str.toString().substring(0,str.toString().length() -1);
	}
	public void arrangeParams(ServletRequest request,Map<String, String> searchParams){
	    Map<String, String[]> map = request.getParameterMap();  
        Set<Entry<String, String[]>> set = map.entrySet();  
        Iterator<Entry<String, String[]>> it = set.iterator();  
        while (it.hasNext()) {  
            Entry<String, String[]> entry = it.next();  
            String key = entry.getKey(); 
            String value = entry.getValue()[0];
        	if(key.contains("sl_pjqlx")){
        		searchParams.put("sl_pjqlx", value);
        	}
        	if(key.contains("sl_smsys")){
        		searchParams.put("sl_smsys", value);
        	}
        	if(key.contains("sl_zllx")){
        		searchParams.put("sl_zllx", value);
        	}
        	if(key.contains("sl_ywlx")){
        		searchParams.put("sl_ywlx", value);
        	}
        	if(key.contains("sl_jsly")){
        		searchParams.put("sl_jsly", value);
        	}
        	if(key.contains("sl_rs")){
				searchParams.put("sl_rs", value);
			}
        	if(key.contains("sl_slnx")){
				searchParams.put("sl_slnx", value);
			}
        	if (key.contains("sl_fwlx")){
				searchParams.put("sl_fwlx", value);
			}
        	if(key.contains("sl_ywlx")){
				searchParams.put("sl_ywlx", value);
			}
        	if(key.contains("sl_dll")){
				searchParams.put("sl_dll", value);
			}
        	if(key.contains("flfs")){
        		searchParams.put("flfs", value);
        	}
        	if(key.contains("flh")){
        		searchParams.put("flh", value);
        	}
        	if(key.contains("dy")){
        		searchParams.put("dy", value);
        	}
        	if(key.contains("name")){
        		if(value.trim() != null && !"".equals(value.trim()) ){
        			searchParams.put("name", value.trim());
        		}
        		
        	}
        }  
	}
	/**
     * 代理结构详细页-技术领域展示区-- 新型
     * 
     */
	public List<Map<String,Object>> getJslyXxTotalData(long id,List xx){
		List<Map<String,Object>>  finalList = new ArrayList<Map<String,Object>>();
		String sql = " select s.zb,s.jsly,s.sl,d.mc from SJsly s, DJslyfl d where s.pid=? and s.zb =1  and s.jsly = d.flh ORDER BY s.sl DESC ";
		List<Object[]> objList = dao.query(0, 10, sql, new Object[] { (int) id }, null);
		int size = objList.size();
		for(int i=0; i < size;i++){
			Map<String,Object> map1 = new HashMap<String,Object>();
			Object[] obj = objList.get(i);
			String mc = (String) obj[3];
			int sl = (Integer)obj[2];
			List slList = new ArrayList();
			slList.add(sl);
			xx.add(mc);
			map1.put("name", mc);
			map1.put("type", "bar");
			map1.put("data", slList);
			finalList.add(map1);
//			if(i==9){
//				break;
//			}			
		}
		return finalList;
	}
	
	/**
     * 代理结构详细页-技术领域展示区--外观
     * 
     */
	public List<Map<String,Object>> getJslyWgTotalData(long id,List wg){
		List<Map<String,Object>>  finalList = new ArrayList<Map<String,Object>>();
		String sql = " select s.zb,s.jsly,s.sl,d.mc from SJsly s, DJslyfl d where s.pid=? and s.zb =2  and s.jsly = d.flh ORDER BY s.sl DESC ";
		List<Object[]> objList = dao.query(0, 10, sql, new Object[] { (int) id }, null);
		int size = objList.size();
		for(int i=0; i < size;i++){
			Map<String,Object> map1 = new HashMap<String,Object>();
			Object[] obj = objList.get(i);
			String mc = (String) obj[3];
			int sl = (Integer)obj[2];
			List slList = new ArrayList();
			slList.add(sl);
			wg.add(mc);
			map1.put("name", mc);
			map1.put("type", "bar");
			map1.put("data", slList);
			finalList.add(map1);
//			if(i==9){
//				break;
//			}			
		}
		return finalList;
	}
	
	/**
     * 代理结构详细页-技术领域展示区--通用方法
     * add by zhangqiuyi
     */
	public List<Map<String,Object>> getJslyTotalData(long id, int type, List lst){
		List<Map<String,Object>>  finalList = new ArrayList<Map<String,Object>>();
		String sql = " select s.zb,s.jsly,s.sl,d.mc from SJsly s, DJslyfl d where s.pid=? and s.zb =?  and s.jsly = d.flh ORDER BY s.sl DESC ";
		List<Object[]> objList = dao.query(0, 10, sql, new Object[] { (int) id, type }, null);
		int size = objList.size();
		for(int i=0; i < size;i++){
			Map<String,Object> map1 = new HashMap<String,Object>();
			Object[] obj = objList.get(i);
			String mc = (String) obj[3];
			int sl = (Integer)obj[2];
			String code = (String) obj[1];
			List slList = new ArrayList();
			slList.add(sl);
			lst.add("(" + code + ")" + mc);
			map1.put("name", "(" + code + ")" + mc);
			map1.put("type", "bar");
			map1.put("data", slList);
			finalList.add(map1);
		}
		return finalList;
	}
	
	/*
	 * 获取代理结构详细信息页-客户分布热点图数据
	 */
	public void getEdljgKhdata(long id, List<Map<String,Object>> chinaList, List<Map<String,Object>> worldList){
		int i=0,ii=0,j=0;
		List<EKh> objList = dao.queryByWhere(EKh.class, " pid = ? and dy is not NULL ORDER BY gnw,sl DESC ", new Object[] { (int) id });
		for(EKh obj : objList){
			Map<String,Object> chinamap = new HashMap<String,Object>();
			Map<String,Object> worldmap = new HashMap<String,Object>();
			int gnw = obj.getGnw();
			String dy = obj.getDy();
			int sl = obj.getSl();
			if(gnw == 1 && i==0){
				chinaMax = sl;
				i++;
			}
			else if (gnw == 3 && ii==0){
				chinaMax = chinaMax > sl? chinaMax : sl;
				ii++;
			}
			else if (gnw == 2 && j==0){
				worldMax = sl;
				j++;
			}
				
			if(gnw == 1 || gnw == 3){
				chinamap.put("name", dy);
				chinamap.put("value", sl);
				chinaList.add(chinamap);
			}else{
				worldmap.put("name", dy);
				worldmap.put("value", sl);
				worldList.add(worldmap);
			}
		}		
	}
}
