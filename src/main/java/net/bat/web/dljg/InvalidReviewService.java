package net.bat.web.dljg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import net.bat.dao.QueryResult;
import net.bat.dao.UserDAO;
import net.bat.entity.InvalidReview;
//Spring Bean的标识.
@Component
// 类中所有public函数都纳入事务管理的标识.
@Transactional
public class InvalidReviewService {
	@Autowired
	private UserDAO dao;
	@Autowired
	private SDljgDao sdljgDao;

	public Page<InvalidReview> getInvalidReviewPage(Map<String,Object> eparas, 
			int pageNumber, int pageSize, String sortType,boolean bAsc) {
		PageRequest pageRequest = buildPageRequest(pageNumber, pageSize, sortType, bAsc);
		QueryResult<InvalidReview> qr = new QueryResult<InvalidReview>();//dao.getScrollDataForOn(sql.toString(),count,where.toString(),(pageNumber-1)*pageSize, pageSize,condition.toString(),orderby);
		List<InvalidReview> list= new ArrayList<InvalidReview>();
		qr.setResultlist(list);
		qr.setTotalrecord(0l);
		Page p = new PageImpl(qr.getResultlist(), pageRequest, qr.getTotalrecord());
		return p;
	}
	private PageRequest buildPageRequest(int pageNumber, int pagzSize, String sortType, boolean bAsc) {
		Sort sort = null;
		if ("auto".equals(sortType)) {
			sort = new Sort(Direction.DESC, "id");
		} else {
			if (bAsc) {
				sort = new Sort(Direction.ASC, sortType);
			} else {
				sort = new Sort(Direction.DESC, sortType);
			}

		}
		return new PageRequest(pageNumber - 1, pagzSize, sort);
	}
}
