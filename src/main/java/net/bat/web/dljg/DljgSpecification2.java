package net.bat.web.dljg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springside.modules.persistence.SearchFilter;

import com.google.common.collect.Lists;

import net.bat.entity.EDljg;
import net.bat.entity.SDljg;

public class DljgSpecification2<T>{
	
	public static <T> Specification<T> Search( final List<EDljg> list,final Class<T> entityClazz,final Map<String,Object> eparas) {
		return new Specification<T>() {
		@Override
		public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
			// TODO Auto-generated method stub
			Path expression = root.get("jgdm");
			Set<String> keys = eparas.keySet();
			List<Predicate> predicates = Lists.newArrayList();
			for(EDljg  s : list){
				predicates.add(cb.equal(expression, s.getJgdm()));
			}
			Predicate s = cb.or(predicates.toArray(new Predicate[predicates.size()]));
			return s;
		}
		};
	}

}
