
package net.bat.web.dljg;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import net.bat.entity.EDljg;

public interface DljgIdDao extends PagingAndSortingRepository<EDljg, Long>, JpaSpecificationExecutor<EDljg> {
      
	EDljg findByJgdm(String jgdm);
}

