package net.bat.web.dljg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import net.bat.entity.EDljg;

//Spring Bean的标识.
@Component
//类中所有public函数都纳入事务管理的标识.
@Transactional
public class DljgIdService {

	@Autowired
	private DljgIdDao dljgIdDao;
	//根据代理机构代码查询到id作为pid
	public EDljg getDljgBj(String jgdm){
		return dljgIdDao.findByJgdm(jgdm);
		
	}
}
