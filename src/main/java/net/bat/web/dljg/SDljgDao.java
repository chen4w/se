/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.web.dljg;

import net.bat.entity.SDljg;


import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SDljgDao extends PagingAndSortingRepository<SDljg, Long>, JpaSpecificationExecutor<SDljg> {

}
