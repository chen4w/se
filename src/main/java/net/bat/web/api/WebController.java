package net.bat.web.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.bat.service.FileService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class WebController {
	@Autowired
	private FileService fileService;

	@RequiresPermissions("*:update")
	@RequestMapping(value = "/api/filesort", method = RequestMethod.POST)
	@ResponseBody
	public void dragDropSort(@RequestBody List<Long> newOrder) {
		fileService.dragDropSort(newOrder);
	}

	// 文件下载
	@RequiresPermissions("*:view")
	@RequestMapping(value = "/fopen", method = RequestMethod.GET)
	public void openfile(@RequestParam(value = "fp", required = true) String fp, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		fileService.onOpenFile(fp, request, response);
	}
	
	@RequestMapping(value = "/fopen/{fp}", method = RequestMethod.GET)
	public void openfile2(@PathVariable String fp, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		fileService.onOpenFile(fp, request, response);
	}

	// 处理文件上传
	@RequiresPermissions("*:create")
	@RequestMapping(value = "/fupload", method = RequestMethod.POST)
	public void fileUpload(@RequestParam MultipartFile[] files, HttpServletRequest request, HttpServletResponse response)
			throws IllegalStateException, IOException {
		fileService.onFileUpload(files, request, response);
	}

}
