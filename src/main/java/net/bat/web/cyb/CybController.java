package net.bat.web.cyb;

import java.util.HashMap;
import java.util.Map;

import net.bat.dao.UserDAO;
import net.bat.entity.EDljg;
import net.bat.entity.User;
import net.bat.service.account.ShiroDbRealm.ShiroUser;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CybController {
	@Autowired
	private UserDAO dao;
	
	@RequestMapping(value = "/cyb/hasdljgds/{jgid}", method = RequestMethod.GET)
	@ResponseBody
	//@ModelAttribute
	public Map<String,String> hasDljgDs(@PathVariable("jgid") Long jgid,Model model){
		Map<String,String> result = new HashMap<String,String>();
		//判断代理机构是否开通电商
		EDljg dljg = dao.find(EDljg.class, jgid);
		if(dljg.getDs()!=1){
			result.put("status", "no");
			result.put("msg", "功能暂未开通，敬请期待");
		}else{
			result.put("status", "yes");
		}
		return result;
	}  
	
	@RequestMapping(value = "/cyb/dljg/{jgid}", method = RequestMethod.GET)
	@ResponseBody
	//@ModelAttribute
	public Map<String,String> loginCYB_Dljg(@PathVariable("jgid") Long jgid,Model model){
		Map<String,String> result = new HashMap<String,String>();
		EDljg dljg = dao.find(EDljg.class, jgid);
		
		//获得当前登录的用户
		User user = getCurrentUser();
		if(user==null){
			result.put("status", "failed");
			result.put("msg", "请登录后进入店铺");
			return result;
		}
		//注册创意宝
		boolean regist_status = CybService.regist(user);
		String loginUrl = null;
		if(regist_status){//注册成功
			//获得登录链接
			loginUrl = CybService.login(dljg.getJgdm(),user.getId());
			result.put("status", "succ");
			result.put("url", loginUrl);
		}else{
			result.put("status", "failed");
			result.put("msg", "店铺登录失败,请联系管理员");
		}
		return result;
	}  
	
	@RequestMapping(value = "/cyb/user", method = RequestMethod.GET)
	@ResponseBody
	//@ModelAttribute
	public Map<String,String> loginCYB_User(Model model){
		Map<String,String> result = new HashMap<String,String>();
		//获得当前登录的用户
		User user = getCurrentUser();
		if(user==null){
			result.put("status", "failed");
			result.put("msg", "请登录后进入店铺");
			return result;
		}
		//注册创意宝
		boolean regist_status = CybService.regist(user);
		String loginUrl = null;
		if(regist_status){//注册成功
			//获得登录链接
			loginUrl = CybService.login(user.getId());
			result.put("status", "succ");
			result.put("url", loginUrl);
		}else{
			result.put("status", "failed");
			result.put("msg", "店铺登录失败,请联系管理员");
		}
		return result;
	}  
	
	private User getCurrentUser() {
		ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		User user = dao.findByWhere(User.class,"loginName=?",new Object[]{shiroUser.loginName});
		return user;
	}
}
