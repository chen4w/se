package net.bat.web.fswx;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.bat.dao.UserDAO;
import net.bat.dto.FswxDTO;
import net.bat.entity.SFswx;
import net.bat.util.DocConverter;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.utils.PropertiesLoader;

/**
 * 复审无效
 * 
 * @author wangshuxin 2016-04-19
 */
@Controller
public class FswxController {
	private static final String PAGE_SIZE = "10";

	@Autowired
	private FswxService fswxService;

	@Autowired
	private UserDAO dao;
	
	protected static PropertiesLoader propertiesLoader = new PropertiesLoader("classpath:/application.properties");

	@RequestMapping(value = "/fswx/dljg/{jgid}/{fl}", method = RequestMethod.GET)
	public String listByDljg(
			@PathVariable("jgid") Long jgid,
			@PathVariable("fl") Long fl,
			@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,
			Model model, ServletRequest request) {
		Map<String, Object> searchParams = new HashMap<String, Object>();
		searchParams.put("jgid", jgid);
		searchParams.put("fl", fl);
		Page<SFswx> objs = fswxService.getDljgFswxPage(searchParams, null,pageNumber, pageSize, sortType, sortAsc);

		model.addAttribute("objs", objs);
		return "fswx/list";
	}

	@RequestMapping(value = "/fswx/dlr/{dlrid}/{fl}", method = RequestMethod.GET)
	public String listByDlr(
			@PathVariable("dlrid") Long dlrid,
			@PathVariable("fl") Long fl,
			@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,
			Model model, ServletRequest request) {
		Map<String, Object> searchParams = new HashMap<String, Object>();
		searchParams.put("dlrid", dlrid);
		searchParams.put("fl", fl);
		Page<SFswx> objs = fswxService.getDlrFswxPage(searchParams, null,
				pageNumber, pageSize, sortType, sortAsc);

		model.addAttribute("objs", objs);
		return "fswx/list";
	}

	@RequestMapping(value = "/fswx/view/{decisionNo}/{fl}", method = RequestMethod.GET)
	public String viewPDF(@PathVariable("decisionNo") String decisionNo,@PathVariable("fl") Integer fl,
			Model model, HttpServletRequest request, HttpServletResponse response) {
		try{
			System.out.println("decisionNo"+decisionNo);
			String pdf_filepath = fswxService.getFswxFilePath(decisionNo,fl);//"D:\\Work\\upload\\F1999\\F1043.swf"
			
			boolean hasFile = true;
			if(pdf_filepath==null || "".equals(pdf_filepath))
				hasFile = false;
			else{
				String base_path = propertiesLoader.getProperty("FSWX_PDF_PATH");
				System.out.println("pdf文件相对路径="+pdf_filepath);
				System.out.println("pdf文件路径="+base_path+pdf_filepath);
				File pdfFile = new File(base_path+pdf_filepath);
				if(!pdfFile.exists())
					hasFile = false;
			}
			
			if(!hasFile){
				response.sendError(404);
				return null;
			}
			model.addAttribute("decisionNo", decisionNo);
			model.addAttribute("fl", fl);
		}catch(Exception e){
			e.printStackTrace();
		}
		return "fswx/viewfswx";
	}

	@RequestMapping(value = "/fswx/getswf/{decisionNo}/{fl}", method = RequestMethod.GET)
	public @ResponseBody void downloadFiles(@PathVariable("decisionNo") String decisionNo,@PathVariable("fl") Integer fl, HttpServletRequest request, HttpServletResponse response) {
		try{
			System.out.println("decisionNo"+decisionNo);
			String pdf_filepath = fswxService.getFswxFilePath(decisionNo,fl);//"D:\\Work\\upload\\F1999\\F1043.swf"
			String base_path = propertiesLoader.getProperty("FSWX_PDF_PATH");
			DocConverter convert = new DocConverter(base_path+pdf_filepath);
			convert.setEnvironment(propertiesLoader.getInteger("ENVIRONMENT"));
			boolean result = convert.conver();
			if(!result){
				return;
			}
			File downloadFile = new File(convert.getswfPath());
			FileInputStream inputStream = null;
			OutputStream outStream = null;

			try {
				inputStream = new FileInputStream(downloadFile);

				response.setContentLength((int) downloadFile.length());
				response.setContentType("application/x-shockwave-flash");

				// Write response
				outStream = response.getOutputStream();
				IOUtils.copy(inputStream, outStream);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != inputStream)
						inputStream.close();
					if (null != inputStream)
						outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
