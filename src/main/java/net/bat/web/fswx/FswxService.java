package net.bat.web.fswx;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.bat.dao.QueryResult;
import net.bat.dao.UserDAO;
import net.bat.dto.EDljgDTO;
import net.bat.dto.FswxDTO;
import net.bat.entity.FswxFile;
import net.bat.entity.SFswx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
/**
 * 复审无效
 * @author wangshuxin
 * 2016-04-19
 */
//Spring Bean的标识.
@Component
//类中所有public函数都纳入事务管理的标识.
@Transactional
public class FswxService {
	@Autowired
	private UserDAO dao;
	
	public Page<SFswx> getDljgFswxPage(Map<String, Object> searchParams,Map<String,Object> eparas, 
			int pageNumber, int pageSize, String sortType,boolean bAsc) {
		PageRequest pageRequest = buildPageRequest(pageNumber, pageSize, sortType, bAsc);
		String selectjpql = "select sfswx ";
		String fromjpql = "from SFswx sfswx ";
		String selectjpql_cout="select count(*) ";
		//TODO 构造where条件和参数值
		String wherejpql = "sfswx.pidDljg=(?1) and sfswx.fl = (?2) "; 
		List<Object> queryParams = new ArrayList<Object>();
		queryParams.add(Integer.parseInt(String.valueOf(searchParams.get("jgid"))));
		queryParams.add(Integer.parseInt(String.valueOf(searchParams.get("fl"))));
		String orderby=null;
		if(sortType!=null){
			if(sortType.equals("auto")){
				orderby = "sfswx.jdr desc";
			}else{
				if(bAsc){
					orderby = sortType;
				}else{
					orderby = sortType +" desc";
				}
			}
		}
		QueryResult<Map> qr = dao.getScrollData(selectjpql+fromjpql, selectjpql_cout+fromjpql, 
				(pageNumber-1)*pageSize, pageSize, wherejpql, queryParams.toArray(), orderby);
		Page p = new PageImpl(qr.getResultlist(), pageRequest, qr.getTotalrecord());
		return p;
	}
	
	public Page<SFswx> getDlrFswxPage(Map<String, Object> searchParams,Map<String,Object> eparas, 
			int pageNumber, int pageSize, String sortType,boolean bAsc) {
		PageRequest pageRequest = buildPageRequest(pageNumber, pageSize, sortType, bAsc);
		String selectjpql = "select sfswx ";
		String fromjpql = "from SFswx sfswx ";
		String selectjpql_cout="select count(*) ";
		//TODO 构造where条件和参数值
		String wherejpql = "sfswx.pidDlr=(?1) and sfswx.fl = (?2) "; 
		List<Object> queryParams = new ArrayList<Object>();
		queryParams.add(Integer.parseInt(String.valueOf(searchParams.get("dlrid"))));
		queryParams.add(Integer.parseInt(String.valueOf(searchParams.get("fl"))));
		String orderby=null;
		if(sortType!=null){
			if(sortType.equals("auto")){
				orderby = "sfswx.jdr desc";
			}else{
				if(bAsc){
					orderby = sortType;
				}else{
					orderby = sortType +" desc";
				}
			}
		}
		QueryResult<Map> qr = dao.getScrollData(selectjpql+fromjpql, selectjpql_cout+fromjpql, 
				(pageNumber-1)*pageSize, pageSize, wherejpql, queryParams.toArray(), orderby);
		Page p = new PageImpl(qr.getResultlist(), pageRequest, qr.getTotalrecord());
		return p;
	}
	/**
	 * 创建分页请求.
	 */
	private PageRequest buildPageRequest(int pageNumber, int pagzSize, String sortType, boolean bAsc) {
		Sort sort = null;
		if ("auto".equals(sortType)) {
			sort = new Sort(Direction.DESC, "id");
		} else {
			if (bAsc) {
				sort = new Sort(Direction.ASC, sortType);
			} else {
				sort = new Sort(Direction.DESC, sortType);
			}

		}

		return new PageRequest(pageNumber - 1, pagzSize, sort);
	}
	
	public String getFswxFilePath(String jdh,Integer fl){
		String where = "jdh=? and fl=? ";
		List<FswxFile> list = dao.queryByWhere(FswxFile.class, where, new Object[]{jdh,fl});
		if(list!=null && list.size()>0)
			return list.get(0).getPath();
		return null;
	}
}
