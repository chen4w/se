/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.web.index;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import net.bat.dao.UserDAO;
import net.bat.entity.DJslyfl;
import net.bat.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

	@Autowired
	private UserDAO dao;

	@RequestMapping(value = "/index",method = RequestMethod.GET)
	public String list( Model model,ServletRequest request) {
		String ctx = request.getServletContext().getContextPath();
		
		//分类方式1：ipc；2：洛迦诺  3:国民经济
		//ipc分类子目录、子目录叶节点
		Map<String,String> leafmap = new  HashMap<String,String>();
		String ipcsub1 = getJslyIpcLstData(ctx,1,leafmap,null);	
		
		//洛迦诺分类叶节点(没有子目录)
		String jsflleaf2 = getJslyLstData(ctx,2,null);
		
		//国民经济产业分类子目录、子目录叶节点
		Map<String,String> leafmap2 = new  HashMap<String,String>();
		String jsflsub3 =  getJslyEcomLstData(ctx,3,leafmap2,null);	
     
		
		model.addAttribute("ipcsub1", ipcsub1);
		model.addAttribute("obj1", leafmap);
		model.addAttribute("jsflleaf2", jsflleaf2);
		model.addAttribute("jsflsub3", jsflsub3);	
		model.addAttribute("obj2", leafmap2);
		return "index/home";
	}
	
	/**
	 * Ajax请求技术类型内容检索。
	 */
	@RequestMapping(value = "/index/searchJsfl", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> searchJsfl(String ctx,int flfs,String content) {	
		Map<String,Object> map = new HashMap<String,Object>();
		//分类方式1：ipc；2：洛迦诺  3:国民经济
		
		//ipc分类子目录、子目录叶节点
		if(flfs == 1){
			Map<String,String> leafmap = new  HashMap<String,String>();
			String ipcsub1 = getJslyIpcLstData(ctx,1,leafmap,content);	
			map.put("ipcsub1", ipcsub1);
			map.put("obj1", leafmap);
		}
		
		//洛迦诺分类叶节点(没有子目录)
		if(flfs == 2){
		    String jsflleaf2 = getJslyLstData(ctx,2,content);
		    map.put("jsflleaf2", jsflleaf2);
		}
		//国民经济产业分类子目录、子目录叶节点
		if(flfs == 3){	    
			Map<String,String> leafmap2 = new  HashMap<String,String>();
			String jsflsub3 =  getJslyEcomLstData(ctx,3,leafmap2,content);	
			map.put("jsflsub3", jsflsub3);
			map.put("obj2", leafmap2);
		
		}
		return map;
	}
   
	/**
     * 首页--技术类型ipc分类列表数据
     * http://localhost:8080/technology?sl_jsly=1&flfs=1&flh=A23
     * 子目录格式：<tr><td><a href="javascript:jsflleafcnt3('hello world')">A 农业</a></td></tr>
     * 叶节点格式：<tr><td><a href="$(ctx)/dljg?flfs=&flh=">专利申请</a></td><td><a href="$(ctx)/dljg?"> A01 农业；林业；畜牧业；狩猎；诱捕；捕鱼</a></td></tr>
     */
	public String getJslyIpcLstData(String ctx,int id,Map<String,String> map,String filtCnt){
		String wherecondit = "";
		if(null != filtCnt && !"".equals(filtCnt.trim())){
			wherecondit = " flfs=? and (LENGTH(flh)=1 or mc like '%"+filtCnt+"%') ";
		}else{
			wherecondit = " flfs=? ";
		}
		List<DJslyfl> objList = dao.queryByWhere(DJslyfl.class,wherecondit,new Object[] { (int) id });	
		StringBuffer sub1 = new StringBuffer();
		StringBuffer leaf1 = new StringBuffer();		
		boolean leaf = true;
		String presioussubflh = null;
		String subflh = null;
		String submc = "";
		int count = 0;
		for(DJslyfl obj : objList){
			int flfs = obj.getFlfs();
			String flh = obj.getFlh();
			String mc = obj.getMc();	
			count++;
			if(flh.length() == 1){
				subflh = flh;
				submc = mc;
				sub1.append("<tr><td><a href=\"javascript: jsflleafcnt1(");
				sub1.append("'");
				sub1.append("ipc");
				sub1.append(subflh);
				sub1.append("')\">");
				sub1.append(subflh);
				sub1.append(" ");
				sub1.append(submc);
				sub1.append("</a></td></tr>");	
				if(!leaf){
				   leaf1.append("</tr>");	
				   leaf = true;
				}
				if(count>1)
				    map.put("ipc"+presioussubflh, leaf1.toString());
				leaf1 = new StringBuffer();
				presioussubflh = subflh;
			}
			if(null !=subflh && flh.indexOf(subflh) !=-1 && flh.length() > 1){
				if(leaf){
				   leaf1.append("<tr>");
				}
				leaf1.append("<td><a href='"+ctx+"/technology?sl_jsly=1&flfs=1&flh="+flh+"'>");
				leaf1.append(flh);
				leaf1.append(" ");
				leaf1.append(mc);
				leaf1.append("</a></td>");
				if(!leaf){
				   leaf1.append("</tr>");	
				   leaf = true;
				}else{
					leaf = false;
				}
			}
		}
		//最后索引
		if(!leaf){
			leaf = true;
			leaf1.append("</tr>");	
		    map.put("ipc"+presioussubflh, leaf1.toString());			   
		}else{
			map.put("ipc"+presioussubflh, leaf1.toString());
		}
		return sub1.toString();
	}
	
	/**
     * 首页--技术类型洛迦诺分类列表数据
     * 子目录格式：<tr><td><a href="javascript:jsflleafcnt3('hello world')">A 农业</a></td></tr>
     * 叶节点格式：<tr><td><a href="$(ctx)/dljg?flfs=&flh=">专利申请</a></td><td><a href="$(ctx)/dljg?"> A01 农业；林业；畜牧业；狩猎；诱捕；捕鱼</a></td></tr>
     */
	public String getJslyLstData(String ctx,int id,String filtCnt){
		String wherecondit = "";
		if(null != filtCnt && !"".equals(filtCnt.trim())){
			wherecondit = " flfs=? and mc like '%"+filtCnt+"%' ";
		}else{
			wherecondit = " flfs=? ";
		}
				
		List<DJslyfl> objList = dao.queryByWhere(DJslyfl.class,wherecondit,new Object[] { (int) id });	
		StringBuffer leaf2 = new StringBuffer();		
		boolean leaf = true;
		for(DJslyfl obj : objList){
			int flfs = obj.getFlfs();
			String flh = obj.getFlh();
			String mc = obj.getMc();			
			if(leaf){
			   leaf2.append("<tr>");
			}
			leaf2.append("<td><a href='"+ctx+"/technology?sl_jsly=2&flfs=2&flh="+flh+"'>");
			leaf2.append(flh);
			leaf2.append(" ");
			leaf2.append(mc);
			leaf2.append("</a></td>");
			if(!leaf){
			   leaf2.append("</tr>");	
			   leaf = true;
			}else{
				leaf = false;
			}
		}
		if(!leaf){
		   leaf2.append("</tr>");	
		   leaf = true;
		}	
		return leaf2.toString();
	}
	
	/**
     * 首页--技术类型国民经济分类列表数据
     * http://localhost:8080/technology?sl_jsly=1&flfs=1&flh=A23
     * 子目录格式：<tr><td><a href="javascript:jsflleafcnt3('hello world')">A 农业</a></td></tr>
     * 叶节点格式：<tr><td><a href="$(ctx)/dljg?flfs=&flh=">专利申请</a></td><td><a href="$(ctx)/dljg?"> A01 农业；林业；畜牧业；狩猎；诱捕；捕鱼</a></td></tr>
     */
	public String getJslyEcomLstData(String ctx,int id,Map<String,String> map,String filtCnt){
		String wherecondit = "";
		if(null != filtCnt && !"".equals(filtCnt.trim())){
			wherecondit = " flfs=? and (LENGTH(flh)=1 or mc like '%"+filtCnt+"%') ";
		}else{
			wherecondit = " flfs=? ";
		}
		List<DJslyfl> objList = dao.queryByWhere(DJslyfl.class,wherecondit,new Object[] { (int) id });	
		StringBuffer sub1 = new StringBuffer();
		StringBuffer leaf1 = new StringBuffer();		
		boolean leaf = true;
		String presioussubflh = null;
		String subflh = null;
		String submc = "";
		int count = 0;
		for(DJslyfl obj : objList){
			int flfs = obj.getFlfs();
			String flh = obj.getFlh();
			String mc = obj.getMc();	
			count++;
			if(flh.length() == 1){
				subflh = flh;
				submc = mc;
				sub1.append("<tr><td><a href=\"javascript: jsflleafcnt3(");
				sub1.append("'");
				sub1.append("ecom");
				sub1.append(subflh);
				sub1.append("')\">");
				sub1.append(subflh);
				sub1.append(" ");
				sub1.append(submc);
				sub1.append("</a></td></tr>");
				if(!leaf){
				   leaf1.append("</tr>");	
				   leaf = true;
				}
				if(count>1)
				    map.put("ecom"+presioussubflh, leaf1.toString());
				leaf1 = new StringBuffer();
				presioussubflh = subflh;
				
			}
			if(null !=subflh && flh.indexOf(subflh) !=-1 && flh.length() > 1){
				if(leaf){
				   leaf1.append("<tr>");
				}
				leaf1.append("<td><a href='"+ctx+"/technology?sl_jsly=3&flfs=3&flh="+flh+"'>");
				leaf1.append(flh);
				leaf1.append(" ");
				leaf1.append(mc);
				leaf1.append("</a></td>");
				if(!leaf){
				   leaf1.append("</tr>");	
				   leaf = true;
				}else{
					leaf = false;
				}
			}
		}
		//最后索引
		if(!leaf){
			leaf = true;
			leaf1.append("</tr>");	
		    map.put("ecom"+presioussubflh, leaf1.toString());			   
		}else{
			map.put("ecom"+presioussubflh, leaf1.toString());
		}
		return sub1.toString();
	}

}
