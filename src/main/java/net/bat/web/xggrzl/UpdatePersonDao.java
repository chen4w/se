package net.bat.web.xggrzl;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import net.bat.entity.User;
/**
 * 登录名唯一，按照登录名查询
 * @author lh
 *
 */
public interface UpdatePersonDao extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User>{
	User findByLoginName(String loginName);
}
