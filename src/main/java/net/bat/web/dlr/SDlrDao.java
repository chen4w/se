/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.web.dlr;

import net.bat.entity.SDlr;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SDlrDao extends PagingAndSortingRepository<SDlr, Long>, JpaSpecificationExecutor<SDlr> {

}
