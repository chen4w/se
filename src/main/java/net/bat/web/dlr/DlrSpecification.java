package net.bat.web.dlr;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.google.common.collect.Lists;

import net.bat.entity.SDlr;

public class DlrSpecification<T>{
	
	public static <T> Specification<T> Search( final List<SDlr> list,final Class<T> entityClazz,final Map<String,Object> eparas) {
		return new Specification<T>() {
		@Override
		public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
			// TODO Auto-generated method stub
			Path expression = root.get("id");
			Set<String> keys = eparas.keySet();
			List<Predicate> predicates = Lists.newArrayList();
			for(SDlr  s : list){
				if(s.getPid() != null && !"".equals(s.getPid())){
					predicates.add(cb.equal(expression, s.getPid()));

				}
							}
			Predicate s = cb.or(predicates.toArray(new Predicate[predicates.size()]));
			if(!keys.isEmpty()){
				Predicate[] predicatess = new Predicate[keys.size() + 1];
				
				
				for(int i = 0 ; i < keys.size() ; i++){
										
					if(eparas.containsKey("dy")){
						Path expression1 = root.get("szcs");
						String value = (String) eparas.get("dy");
						String[] dys = value.split("_");
						List<Predicate> exp = Lists.newArrayList();
						for(String dy : dys){
							exp.add(cb.like(expression1, "%" + dy + "%"));
						}
						predicatess[i] =cb.or(exp.toArray(new Predicate[exp.size()]));
					}
					if(eparas.containsKey("sl_zynx")){
						Path expression1 = root.get("dtGzjy");
						String value = (String) eparas.get("sl_zynx");
						if(value.equals("3lt")){
							Calendar calendar = Calendar.getInstance(); 
							Date date = calendar.getTime();
							int year = calendar.get(Calendar.YEAR);
							calendar.set(year - 3, 0, 0);
							predicatess[i] = cb.between(expression1, calendar.getTime(), new Date());
						}
						if(value.equals("3-5")){
							Calendar firstcalendar = Calendar.getInstance(); 
							Calendar secondcalendar = Calendar.getInstance(); 
							int year = firstcalendar.get(Calendar.YEAR);
							firstcalendar.set(year - 5, 0, 0);
							secondcalendar.set(year - 3, 12, 31);
							predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
						}
						if(value.equals("5-10")){
							Calendar firstcalendar = Calendar.getInstance(); 
							Calendar secondcalendar = Calendar.getInstance(); 
							int year = firstcalendar.get(Calendar.YEAR);
							firstcalendar.set(year - 10, 0, 0);
							secondcalendar.set(year - 5, 12, 31);
							predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
						}
						if(value.equals("10-20")){
							Calendar firstcalendar = Calendar.getInstance(); 
							Calendar secondcalendar = Calendar.getInstance(); 
							int year = firstcalendar.get(Calendar.YEAR);
							firstcalendar.set(year - 20, 0, 0);
							secondcalendar.set(year - 10, 12, 31);
							predicatess[i] = cb.between(expression1, firstcalendar.getTime(), secondcalendar.getTime());
						}
						if(value.equals("20gt")){
							Calendar calendar = Calendar.getInstance(); 
							Date date = calendar.getTime();
							int year = calendar.get(Calendar.YEAR);
							calendar.set(year - 20, 12, 31);
							predicatess[i] = cb.lessThan(expression1, calendar.getTime());
						}
						
					}
					
					if(eparas.containsKey("sl_zz")){
						Path expression1 = root.get("zz");
						String value = (String) eparas.get("sl_zz");
						predicatess[i] = cb.equal(expression1, value);
					}
					String[] sp3 = new String[]{"英语","日语","韩语","德语","法语","其他"};
					String[] sv3 = new String[]{"1","2","3","4","5","6"};
					if(eparas.containsKey("sl_gzwy")){
						Path expression1 = root.get("wynl");
						String value = (String) eparas.get("sl_gzwy");
						if(value.equals("1")){
							predicatess[i] = cb.like(expression1, "英语");
						}
						if(value.equals("2")){
							predicatess[i] = cb.like(expression1, "日语");
						}
						if(value.equals("3")){
							predicatess[i] = cb.like(expression1, "韩语");
						}
						if(value.equals("4")){
							predicatess[i] = cb.like(expression1, "德语");
						}
						if(value.equals("5")){
							predicatess[i] = cb.like(expression1, "法语");
						}
					}
					
					if(eparas.containsKey("sl_fwlx")){
						Path expression1 = null;
						String value = (String) eparas.get("sl_fwlx");
						if(value.equals("zlsq")){
							 expression1 = root.get("zlsq");
						}
						if(value.equals("zlfs")){
							expression1 = root.get("zlfs");
						}
						if(value.equals("zlwx")){
							 expression1 = root.get("zlxgwx"); 
						}
						if(value.equals("zlxzss")){
							 expression1 = root.get("xzss");
						}
						if(value.equals("zlmsss")){
							 expression1 = root.get("msss");
						}
						if(value.equals("zlqsjf")){
							 expression1 = root.get("zlqsjf");
						}
						if(value.equals("zljs")){
							 expression1 = root.get("zljs");
						}
						if(value.equals("zlfxpy")){
							 expression1 = root.get("zlfxpj");
						}
						if(value.equals("zlyj")){
							 expression1 = root.get("zlyj");
						}
						if(value.equals("zlyy")){
							 expression1 = root.get("zlyy");
						}
						if(value.equals("zscqgb")){
							 expression1 = root.get("zscqgb");
						}
						predicatess[i] = cb.greaterThan(expression1, "0");
					}
					if(eparas.containsKey("name")){
						Path expression1 = root.get("szcs");
						String value = (String) eparas.get("name");
						System.out.println(value);
						predicatess[i] = cb.like(expression1, "%" + value + "%");
					}
//					if(eparas.containsKey("wynl")){
//						Path expression1 = root.get("zz");
//						String value = (String) eparas.get("wynl");
//						if(value.equals("5lt")){
//							predicatess[i] = cb.lessThan(expression1, 5);
//						}
//						if(value.equals("5-10")){
//							predicatess[i] = cb.between(expression1, 5, 10);
//						}
//						if(value.equals("10-15")){
//							predicatess[i] = cb.between(expression1, 10, 15);
//						}
//						if(value.equals("15-20")){
//							predicatess[i] = cb.between(expression1, 15, 20);
//						}
//						if(value.equals("20-40")){
//							predicatess[i] = cb.between(expression1, 20, 40);
//						}
//						if(value.equals("40gt")){
//							predicatess[i] = cb.greaterThan(expression1, 40);
//						}
//						
//					}

				}
				predicatess[keys.size()] = s;
				return cb.and(predicatess);
			}
			return s;
		   }
		};
	}

}
