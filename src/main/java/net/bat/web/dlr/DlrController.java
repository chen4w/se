package net.bat.web.dlr;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.web.Servlets;

import com.google.common.collect.Maps;

import net.bat.dto.EDlrVO;
import net.bat.dto.LawsuitsDlrVO;
import net.bat.entity.EDljg;
import net.bat.entity.EDlr;
import net.bat.entity.SFswx;
import net.bat.service.dlr.DlrService;
import net.bat.web.dljg.DljgService;

@Controller
public class DlrController {
	
	private static final String PAGE_SIZE = "10";
	private static Map<String, String> sortTypes = Maps.newLinkedHashMap();
	static {
		sortTypes.put("auto", "自动");
		sortTypes.put("title", "标题");
	}

	@Autowired
	private DljgService dljgService;
	
	
	@Autowired
	private DlrService dlrService;
	
	@RequestMapping(value = "/dlr", method = RequestMethod.GET)
	public String List(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,Model model,
			ServletRequest request){
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String,Object> eparams = new HashMap<String,Object>();
		stitchingParameter(searchParams,request); // 处理参数
		handleEDRParameter(eparams,request);
		Page<EDlrVO> EDlr = dlrService.getDlrPage(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
		//Page<EDlr> objs = dlrService.getDlr(searchParams, pageNumber, pageSize, sortType,sortAsc);            
		List<EDlrVO> edd = EDlr.getContent();
		Calendar calendar = Calendar.getInstance(); 
		int year = calendar.get(Calendar.YEAR);
		for(EDlrVO s : edd){
			Date t = s.getDtGzjy();
			String nx = null;
			if(t != null){
			calendar.setTime(t);
			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
			}
			s.setTt(nx);
		}
		
		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
		model.addAttribute("objs", EDlr);
		model.addAttribute("sortType", sortType);
		model.addAttribute("sortTypes", sortTypes);
		model.addAttribute("sortAsc", sortAsc);
		// 将搜索条件编码成字符串，用于排序，分页的URL
		Map<String, String[]> pmap = request.getParameterMap();
		model.addAttribute("pmap", pmap);
		Map<String, String> search = new HashMap<String,String>();
		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
		model.addAttribute("map", search);
		if(search.containsKey("sl_fwlx")){//"zlfs","zlwx"
			/*if(search.get("sl_fwlx").equals("zlfs")){
				model.addAttribute("fsflag", true);
			}
			if(search.get("sl_fwlx").equals("zlwx")){
				model.addAttribute("wxflag", true);
			}
			if(search.get("sl_fwlx").equals("zlxzss")){
				model.addAttribute("xsflag", true);
			}
			if(search.get("sl_fwlx").equals("zlmsss")){
				model.addAttribute("msflag", true);
			}*/
			model.addAttribute("sl_fwlx", search.get("sl_fwlx"));
		}
		if(!search.isEmpty()){
			model.addAttribute("divFlag", true);
		}else{
			model.addAttribute("divFlag", false);
		}
		//arrangeSearchParams(request,searchParams);    //处理参数
		//searchParams.put("sortAsc", sortAsc);
		String temp = arrangeSearchParams(request,searchParams);    //处理参数
		model.addAttribute("searchParams", temp);
		System.out.println(Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
		return "dlr/list";
	}
	
//	@RequestMapping(value = "/lawsuitsdlr", method = RequestMethod.GET)
//	public String lawsuitsList(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
//			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
//			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
//			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,Model model,
//			ServletRequest request){
//		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
//		Map<String,Object> eparams = new HashMap<String,Object>();
//		stitchingParameter(searchParams,request); // 处理参数
//		handleEDRParameter(eparams,request);
//		Page<LawsuitsDlrDTO> EDlr = dlrService.getLawSuitsDlrPage(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
//		//Page<EDlr> objs = dlrService.getDlr(searchParams, pageNumber, pageSize, sortType,sortAsc);            
//		List<LawsuitsDlrDTO> edd = EDlr.getContent();
//		Calendar calendar = Calendar.getInstance(); 
//		Date date = calendar.getTime();
//		int year = calendar.get(Calendar.YEAR);
//		for(LawsuitsDlrDTO s : edd){
//			Date t = s.getDtGzjy();
//			String nx = null;
//			if(t != null){
//			calendar.setTime(t);
//			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
//			}
//			s.setTt(nx);
//		}
//		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
//		model.addAttribute("objs", EDlr);
//		model.addAttribute("sortType", sortType);
//		model.addAttribute("sortTypes", sortTypes);
//		model.addAttribute("sortAsc", sortAsc);
//		// 将搜索条件编码成字符串，用于排序，分页的URL
//		Map<String, String[]> pmap = request.getParameterMap();
//		model.addAttribute("pmap", pmap);
//		Map<String, String> search = new HashMap<String,String>();
//		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
//		model.addAttribute("map", search);
//		if(!search.isEmpty()){
//			model.addAttribute("divFlag", true);
//		}else{
//			model.addAttribute("divFlag", false);
//		}
//		//arrangeSearchParams(request,searchParams);    //处理参数
//		String temp = arrangeSearchParams(request,searchParams);    //处理参数
//		model.addAttribute("searchParams", temp);
//		return "dlr/susongdailiren";
//	}
	
	
	@RequestMapping(value = "/lawsuitsdlr", method = RequestMethod.GET)
	public String lawsuitsList(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,Model model,
			ServletRequest request){
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String,Object> eparams = new HashMap<String,Object>();
		stitchingParameter(searchParams,request); // 处理参数
		handleEDRParameter(eparams,request);
		Page<LawsuitsDlrVO> EDlr = dlrService.getLawSuitsDlrPage(searchParams,eparams,pageNumber, pageSize, sortType, sortAsc);
		//Page<EDlr> objs = dlrService.getDlr(searchParams, pageNumber, pageSize, sortType,sortAsc);            
		List<LawsuitsDlrVO> edd = EDlr.getContent();
		Calendar calendar = Calendar.getInstance(); 
		Date date = calendar.getTime();
		int year = calendar.get(Calendar.YEAR);
		for(LawsuitsDlrVO s : edd){
			Date t = s.getDtGzjy();
			String nx = null;
			if(t != null){
			calendar.setTime(t);
			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
			}
			s.setTt(nx);
		}
		model.addAttribute("sname", request.getParameter("name") != null ? request.getParameter("name").trim() : null);
		model.addAttribute("objs", EDlr);
		model.addAttribute("sortType", sortType);
		model.addAttribute("sortTypes", sortTypes);
		model.addAttribute("sortAsc", sortAsc);
		// 将搜索条件编码成字符串，用于排序，分页的URL
		Map<String, String[]> pmap = request.getParameterMap();
		model.addAttribute("pmap", pmap);
		Map<String, String> search = new HashMap<String,String>();
		arrangeParams(request,search);         //处理参数使得页面标签显示全部结果
		model.addAttribute("map", search);
		if(!search.isEmpty()){
			model.addAttribute("divFlag", true);
		}else{
			model.addAttribute("divFlag", false);
		}
		//arrangeSearchParams(request,searchParams);    //处理参数
		String temp = arrangeSearchParams(request,searchParams);    //处理参数
		model.addAttribute("searchParams", temp);
		System.out.println(Servlets.encodeParameterStringWithPrefix(searchParams, "search_"));
		return "dlr/susongdailiren";
	}
	
	
	
	@RequestMapping(value = "/dlr/detail/{id}", method = RequestMethod.GET)
	public String showDetail(@PathVariable("id") Long id, Model model) {
		EDlr obj = dlrService.getDlr(id);	
		int pid = obj.getPid();//代理人表主键
		model.addAttribute("gzjy",getGznx(obj.getDtGzjy()));//工作年限
		model.addAttribute("dlrzg", convertZz(obj.getZz()));//代理人资格
		model.addAttribute("jcjl", obj.getBcjjl()==0 ? "无" : "有");//奖惩记录
		model.addAttribute("url_dlr_attach", dlrService.getUrlDlrAttach());//奖惩记录连接地址
		
		// modify by zhangqiuyi
		//model.addAttribute("ljrw", convertLjrw(obj.getLjrw()));//协会领军高层次人才
		if (!convertLjrw(obj.getLjrw()).equals("暂无"))
			model.addAttribute("ljrw", convertLjrw(obj.getLjrw()));//协会领军高层次人才
		//end modify
		model.addAttribute("obj", obj);
		// add by zhangqiuyi
		//model.addAttribute("qtshzw", obj.getQtshzw() == "" ? "无" : obj.getQtshzw());
		if (obj.getQtshzw() != null && !obj.getQtshzw().trim().equals(""))
			model.addAttribute("qtshzw", obj.getQtshzw());
		else
			model.addAttribute("qtshzw", "无");
		//model.addAttribute("dlrxhzc", obj.getDlrxhzc() == "" ? "会员" : obj.getDlrxhzc());
		if (obj.getDlrxhzc() != null && !obj.getDlrxhzc().trim().equals(""))
			model.addAttribute("dlrxhzc", obj.getDlrxhzc());
		else
			model.addAttribute("dlrxhzc", "会员");
		/*model.addAttribute("gzjrc", obj.getGzjrc() == null ? "暂无" : obj.getGzjrc());
		model.addAttribute("gjjlqk", obj.getGjjlqk() == null ? "暂无" : obj.getGjjlqk());
		model.addAttribute("sbjjlqk", obj.getSbjjlqk() == null ? "暂无" : obj.getSbjjlqk());
		model.addAttribute("fbxslw", obj.getFbxslw() == null ? "暂无" : obj.getFbxslw());
		model.addAttribute("cbxszz", obj.getCbxszz() == null ? "暂无" : obj.getCbxszz());
		model.addAttribute("cdzdxm", obj.getCdzdxm() == null ? "暂无" : obj.getCdzdxm());*/
		if (obj.getGzjrc() != null && !obj.getGzjrc().trim().equals(""))
			model.addAttribute("gzjrc", obj.getGzjrc());
		if (obj.getGjjlqk() != null && !obj.getGjjlqk().trim().equals(""))
			model.addAttribute("gjjlqk", obj.getGjjlqk());
		if (obj.getSbjjlqk() != null && !obj.getSbjjlqk().trim().equals(""))
			model.addAttribute("sbjjlqk", obj.getSbjjlqk());
		if (obj.getFbxslw() != null && !obj.getFbxslw().trim().equals(""))
			model.addAttribute("fbxslw", obj.getFbxslw());
		if (obj.getCbxszz() != null && !obj.getCbxszz().trim().equals(""))
			model.addAttribute("cbxszz", obj.getCbxszz());
		if (obj.getCdzdxm() != null && !obj.getCdzdxm().trim().equals(""))
			model.addAttribute("cdzdxm", obj.getCdzdxm());
		// end add
		EDljg edljg = dljgService.getDljg((long)pid);
		model.addAttribute("dy", edljg.getDy());//城市
		model.addAttribute("fwmc", edljg.getMc());//服务机构
		model.addAttribute("jgdm", edljg.getJgdm());//机构代码
		model.addAttribute("pid", pid);
		
		//获取累计代理案件数量数据
		Map<String,Object> finalMap = dlrService.getSDlrInfo(id);
		model.addAttribute("finalMap", finalMap);
		
		//获取专利行政诉讼\专利民事诉讼数据
		Map<String,Object> secondMap = dlrService.getSDlrAjslInfo(id);
		model.addAttribute("secondMap", secondMap);	
		
		//获取专利复审\专利无效宣告
		Map<String,Object> thirdMap = dlrService.getSDlrFswxInfo(id);
		model.addAttribute("thirdMap", thirdMap);	
		
		model.addAttribute("action", "update");
		return "dlr/detail";
	}
	
	/*
	 * 代理人详细页
	 * 内部使用
	 */
	
	@RequestMapping(value = "/dlr/form/{id}", method = RequestMethod.GET)
	public String showForm(@PathVariable("id") Long id, Model model) {
		EDlr obj = dlrService.getDlr(id);	
		int pid = obj.getPid();//代理人表主键
		model.addAttribute("gzjy",getGznx(obj.getDtGzjy()));//工作年限
		model.addAttribute("dlrzg", convertZz(obj.getZz()));//代理人资格
		model.addAttribute("jcjl", obj.getBcjjl()==0 ? "无" : "有");//奖惩记录
		// modify by zhangqiuyi
		//model.addAttribute("ljrw", convertLjrw(obj.getLjrw()));//协会领军高层次人才
		if (!convertLjrw(obj.getLjrw()).equals("暂无"))
			model.addAttribute("ljrw", convertLjrw(obj.getLjrw()));//协会领军高层次人才
		//end modify
		model.addAttribute("obj", obj);
		// add by zhangqiuyi
		//model.addAttribute("qtshzw", obj.getQtshzw() == "" ? "无" : obj.getQtshzw());
		if (obj.getQtshzw() != null && !obj.getQtshzw().trim().equals(""))
			model.addAttribute("qtshzw", obj.getQtshzw());
		else
			model.addAttribute("qtshzw", "无");
		//model.addAttribute("dlrxhzc", obj.getDlrxhzc() == "" ? "会员" : obj.getDlrxhzc());
		if (obj.getDlrxhzc() != null && !obj.getDlrxhzc().trim().equals(""))
			model.addAttribute("dlrxhzc", obj.getDlrxhzc());
		else
			model.addAttribute("dlrxhzc", "会员");
		/*model.addAttribute("gzjrc", obj.getGzjrc() == null ? "暂无" : obj.getGzjrc());
		model.addAttribute("gjjlqk", obj.getGjjlqk() == null ? "暂无" : obj.getGjjlqk());
		model.addAttribute("sbjjlqk", obj.getSbjjlqk() == null ? "暂无" : obj.getSbjjlqk());
		model.addAttribute("fbxslw", obj.getFbxslw() == null ? "暂无" : obj.getFbxslw());
		model.addAttribute("cbxszz", obj.getCbxszz() == null ? "暂无" : obj.getCbxszz());
		model.addAttribute("cdzdxm", obj.getCdzdxm() == null ? "暂无" : obj.getCdzdxm());*/
		if (obj.getGzjrc() != null && !obj.getGzjrc().trim().equals(""))
			model.addAttribute("gzjrc", obj.getGzjrc());
		if (obj.getGjjlqk() != null && !obj.getGjjlqk().trim().equals(""))
			model.addAttribute("gjjlqk", obj.getGjjlqk());
		if (obj.getSbjjlqk() != null && !obj.getSbjjlqk().trim().equals(""))
			model.addAttribute("sbjjlqk", obj.getSbjjlqk());
		if (obj.getFbxslw() != null && !obj.getFbxslw().trim().equals(""))
			model.addAttribute("fbxslw", obj.getFbxslw());
		if (obj.getCbxszz() != null && !obj.getCbxszz().trim().equals(""))
			model.addAttribute("cbxszz", obj.getCbxszz());
		if (obj.getCdzdxm() != null && !obj.getCdzdxm().trim().equals(""))
			model.addAttribute("cdzdxm", obj.getCdzdxm());
		// end add
		EDljg edljg = dljgService.getDljg((long)pid);
		model.addAttribute("dy", edljg.getDy());//城市
		model.addAttribute("fwmc", edljg.getMc());//服务机构
		model.addAttribute("pid", pid);
		
		//获取累计代理案件数量数据
		Map<String,Object> finalMap = dlrService.getSDlrInfo(id);
		model.addAttribute("finalMap", finalMap);
		
		//获取专利行政诉讼\专利民事诉讼数据
		Map<String,Object> secondMap = dlrService.getSDlrAjslInfo(id);
		model.addAttribute("secondMap", secondMap);	
		
		//获取专利复审\专利无效宣告
		Map<String,Object> thirdMap = dlrService.getSDlrFswxInfo(id);
		model.addAttribute("thirdMap", thirdMap);	
		
		model.addAttribute("action", "update");
		return "dlr/form";
	}
	
	@RequestMapping(value = "/ryq/dljg/{jgid}/{fl}", method = RequestMethod.GET)
	public String listByDljg(
			@PathVariable("jgid") Long jgid,
			@PathVariable("fl") Long fl,
			@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = "8") int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc,
			Model model, ServletRequest request) {
		Map<String, Object> searchParams = new HashMap<String, Object>();
		searchParams.put("pid", jgid);
		searchParams.put("fl", fl);
		Page<EDlrVO> objs = dljgService.getDljgRyqPage(searchParams, null,pageNumber, pageSize, sortType, sortAsc);
		List<EDlrVO> edd = objs.getContent();
		Calendar calendar = Calendar.getInstance(); 
		int year = calendar.get(Calendar.YEAR);
		for(EDlrVO s : edd){
			Date t = s.getDtGzjy();
			String nx = null;
			if(t != null){
			calendar.setTime(t);
			nx = String.valueOf(year - calendar.get(Calendar.YEAR));
			}
			s.setTt(nx);
		}
		model.addAttribute("objs", objs);
		return "ryq/ryqlist";
	}
	
	/**
	 * 填报代理人保存
	 * @param id
	 * @param rmap
	 * @return
	 * @throws Exception
	 * @author wangshuxin
	 * 2016-04-19
	 */
	@RequestMapping(value = "/rest/EDlrSh/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public <T> T updateEDlrSh(@PathVariable Long id, @RequestBody Map<String, Object> rmap) throws Exception {
		return (T) dlrService.updateEDlrSh(id, rmap);
	}
	/**
	 * 代理人保存
	 * @param id
	 * @param rmap
	 * @return
	 * @throws Exception
	 * @author wangshuxin
	 * 2016-04-20
	 */
	@RequestMapping(value = "/rest/EDlr/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public <T> T updateEDlr(@PathVariable Long id, @RequestBody Map<String, Object> rmap)
			throws Exception {
		return (T) dlrService.updateEDlr(id, rmap);
	}
	@ModelAttribute
	public void getTask(@RequestParam(value = "id", defaultValue = "-1") Long id, Model model) {
		if (id != -1) {
			model.addAttribute("dlr", dlrService.getDlr(id));
		}
	}
	
	void stitchingParameter(Map<String, Object> searchParams,ServletRequest request){
		Map<String,String[]> allParas = request.getParameterMap();
		Set<String> list = allParas.keySet();
		StringBuffer sbf = new StringBuffer(); 
		searchParams.clear();
		if(list.isEmpty()){
			sbf.append("100000");
		}
		if(! allParas.isEmpty()){
		        //s_dlr标识位 (字段对应fl)
		for(String key : allParas.keySet()){
			if(!list.contains("sl_pjqlx") && !list.contains("search_sl_pjqlx") && 
					!list.contains("search_sl_smsys") && !list.contains("sl_smsys")){
				sbf.append("100");
				break;
			}
			if(key.contains("sl_pjqlx")){
				sbf.append("105");
				searchParams.put("sl_pjqlx", "sl_pjqlx");
			}
			if(key.contains("sl_smsys")){
				sbf.append("106");
				searchParams.put("sl_smsys", "sl_smsys");
			}
		}
		
		for(String key : allParas.keySet()){
			if(! list.contains("sl_zllx") && !list.contains("search_sl_zllx")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_zllx")){
				String[] value = allParas.get(key);
			     if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
		for(String key : allParas.keySet()){
			if(! list.contains("sl_ywlx") && !list.contains("search_sl_ywlx")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_ywlx")){
				String[] value = allParas.get(key);
			    if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
		for(String key : allParas.keySet()){
			if(! list.contains("sl_jsly") && !list.contains("search_sl_jsly")){
				sbf.append("0");
				break;
			}
			if(key.contains("sl_jsly")){
				String[] value = allParas.get(key);
			    if(value[0].equals("1")){
					sbf.append("1");
				}else if(value[0].equals("2")){
					sbf.append("2");
				}else if(value[0].equals("3")){
					sbf.append("3");
				}
			}
		}
	
		}
		searchParams.put("EQ_fl", sbf.toString());
	}
	
	
	public void arrangeParams(ServletRequest request,Map<String, String> searchParams){
	    Map<String, String[]> map = request.getParameterMap();  
        Set<Entry<String, String[]>> set = map.entrySet();  
        Iterator<Entry<String, String[]>> it = set.iterator();  
        while (it.hasNext()) {  
            Entry<String, String[]> entry = it.next();  
            String key = entry.getKey(); 
            String value = entry.getValue()[0];
            if(key.contains("sl_zynx")){
        		searchParams.put("sl_zynx", value);
        	}
            if(key.contains("sl_zz")){
        		searchParams.put("sl_zz", value);
        	}
            if(key.contains("sl_gzwy")){
        		searchParams.put("sl_gzwy", value);
        	}
            if(key.contains("sl_fwlx")){
        		searchParams.put("sl_fwlx", value);
        	}
            if(key.contains("sl_dll")){
        		searchParams.put("sl_dll", value);
        	}
            if(key.contains("sl_pjqlx")){
        		searchParams.put("sl_pjqlx", value);
        	}
        	if(key.contains("sl_smsys")){
        		searchParams.put("sl_smsys", value);
        	}
        	if(key.contains("sl_zllx")){
        		searchParams.put("sl_zllx", value);
        	}
        	if(key.contains("sl_ywlx")){
        		searchParams.put("sl_ywlx", value);
        	}
        	if(key.contains("sl_jsly")){
        		searchParams.put("sl_jsly", value);
        	}
        	if(key.contains("dy")){
        		searchParams.put("dy", value);
        	}
        	if(key.contains("name")){
        		searchParams.put("name", value.trim());
        	}
        }  
	}
	void handleEDRParameter(Map<String, Object> searchParams,ServletRequest request){
		Map<String,String[]> allParas = request.getParameterMap();
		Set<String> list = allParas.keySet();
		for(String key : list){
			if (key.contains("dy")){
				searchParams.put("dy", allParas.get(key)[0]);
			}else if(key.contains("sl_zynx")){
				searchParams.put("sl_zynx", allParas.get(key)[0]);
			}else if(key.contains("sl_zz")){
				searchParams.put("sl_zz", allParas.get(key)[0]);
			}else if (key.contains("sl_gzwy")){
				searchParams.put("sl_gzwy", allParas.get(key)[0]);
			}else if (key.contains("sl_fwlx")){
				searchParams.put("sl_fwlx", allParas.get(key)[0]);
			}else if (key.contains("dy")){
				searchParams.put("dy", allParas.get(key)[0]);
			}else if (key.contains("name")){
				if(allParas.get(key)[0] != null && !"".equals(allParas.get(key)[0].trim())){
					searchParams.put("name", allParas.get(key)[0].trim());
        		}
			}else if(key.contains("sl_pjqlx")){
				searchParams.put("sl_pjqlx", allParas.get(key)[0]);
			}else if(key.contains("sl_smsys")){
				searchParams.put("sl_smsys", allParas.get(key)[0]);
			}else if(key.contains("sl_dll")){
				searchParams.put("sl_dll", allParas.get(key)[0]);
			}else if(key.contains("jg")){
				searchParams.put("jg", allParas.get(key)[0]);
			}
		}
	}
	
	
	
	
	
	
	public String arrangeSearchParams(ServletRequest request,Map<String, Object> searchParams){
	    Map<String, String[]> map = request.getParameterMap();
	    StringBuffer str = new StringBuffer();
        Set<Entry<String, String[]>> set = map.entrySet();  
        Iterator<Entry<String, String[]>> it = set.iterator();  
        while (it.hasNext()) {  
            Entry<String, String[]> entry = it.next(); 
            String key = entry.getKey();  
            String value = entry.getValue()[0];
          //  String[] sl_tags= new String[]{"执业年限","资质","工作外语","业务类型","服务类型","代理量","平均权利要求项数","平均说明书页数","专利类型","技术领域"};
		//	String[] sl_names=new String[]{"sl_zynx","sl_zz","sl_gzwy","sl_ywlx","sl_fwlx","sl_dll" ,"sl_pjqlyqxs" ,"sl_pjsmsys","sl_zllx","sl_jsly" };
        	if(key.contains("sl_zynx")){
        		searchParams.put("sl_zynx", value);
        	}
        	if(key.contains("sl_zz")){
        		searchParams.put("sl_zz", value);
        	}
        	if(key.contains("sl_gzwy")){
        		searchParams.put("sl_gzwy", value);
        	}
        	if(key.contains("sl_ywlx")){
        		searchParams.put("sl_ywlx", value);
        	}
        	if(key.contains("sl_fwlx")){
        		searchParams.put("sl_fwlx", value);
        	}
        	if(key.contains("sl_dll")){
				searchParams.put("sl_dll", value);
			}
        	if(key.contains("sl_slnx")){
				searchParams.put("sl_slnx", value);
			}
        	if (key.contains("sl_fwlx")){
				searchParams.put("sl_fwlx", value);
			}
        	if(key.contains("sl_dll")){
				searchParams.put("sl_dll", value);
			} 
        	if (key.contains("dy")){
				searchParams.put("dy", value);
			} 
        	if (key.contains("name")){
        		if(value != null && !"".equals(value.trim())){
        			searchParams.put("name", value.trim());
        		}
			}
        	if(key.contains("sl_pjqlx")){
				searchParams.put("sl_pjqlx", value);
			}
        	if(key.contains("sl_smsys")){
				searchParams.put("sl_smsys", value);
			}
        	if(key.contains("sl_dll")){
				searchParams.put("sl_dll", value);
			}
        	if(key.contains("jg")){
        		searchParams.put("jg", value);
        	}
        }  
        for(String s : searchParams.keySet()){
        	if(s.equals("EQ_fl")){
        		str.append("search_" + s + "=" + searchParams.get(s) +"&");
        	}else{
        		str.append(s + "=" + searchParams.get(s) +"&");
        	}
        	
        }
        return str.toString().substring(0,str.toString().length() -1);
	}

	
	//读取工作年限
	public int getGznx(Date dt){
		if(dt ==  null)
			return 0;
		Calendar cl = Calendar.getInstance(); 
		int jinnian = cl.get(Calendar.YEAR);//今年		
		cl.setTime(dt);
		int gznx = jinnian - cl.get(Calendar.YEAR);
		return gznx;
	}
	
	//读取资质
	public String convertZz (int init){
		String rtn = null;
		if(init == 0){
			rtn = "专利代理人";
		}else if(init == 1){
			rtn = "行政诉讼代理人";
		}else if(init == 2){
			rtn = "民事诉讼代理人";
		}else if(init == 3){
			rtn = "律师双执业证";
		}else{
			rtn = "其他";
		}
		return rtn;
	}
	
	//协会领军高层次人才
	public String convertLjrw (int init){
		String rtn = null;
		if(init == 0){
			rtn = "暂无";
		}else if(init == 1){
			rtn = "高层次人才";
		}else if(init == 2){
			rtn = "领军人才";
		}else{
			rtn = "";
		}
		return rtn;
	}
}
