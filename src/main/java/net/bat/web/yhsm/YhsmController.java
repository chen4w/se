package net.bat.web.yhsm;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class YhsmController {
	
	@RequestMapping(value = "/yhsm", method = RequestMethod.GET)
	public String gotoYhsm(){
		return "yhsm/yhsm";
	}

}
