package net.bat.web.news;

import java.util.List;

import javax.servlet.ServletRequest;

import net.bat.web.dljg.DljgService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller

public class NewsController {
	private static final String PAGE_SIZE = "10";
	
	@Autowired
	private NewsService newsService;
	
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			@RequestParam(value = "sortType", defaultValue = "auto") String sortType,
			@RequestParam(value = "sortAsc", defaultValue = "false") Boolean sortAsc, Model model,
			ServletRequest request) {
		    String lm = request.getParameter("lm")==null ? "专利知识介绍" : request.getParameter("lm");		
		    String name = request.getParameter("name");
		    List<Object[]> objList = newsService.getNewsList(lm,name);//栏目类型数据
		    model.addAttribute("objList",objList);
		    model.addAttribute("lm", lm);
		    
		    //资讯列表页提供全文检索功能
		   
		    if(name != null ){		    	 
		    	long zlzsjs = 0, zlwtfd=0, zlbllc=0, gyfw = 0, hwwq=0, yjbg=0;
		        List<Object[]> posttoSl = newsService.getCountNews(name);//栏目类型记录统计
		        for(Object[] obj : posttoSl){
		        	if(obj[0].equals("专利知识介绍"))
		        		zlzsjs =  (Long)obj[1];
		        	else if	(obj[0].equals("专利委托辅导"))
		        		zlwtfd =  (Long)obj[1];
		        	else if	(obj[0].equals("专利办理流程"))
		        		zlbllc =  (Long)obj[1];
		        	else if	(obj[0].equals("公益服务"))
		        		gyfw =  (Long)obj[1];
		        	else if	(obj[0].equals("海外维权"))
		        		hwwq =  (Long)obj[1];
		        	else if	(obj[0].equals("研究报告"))
		        		yjbg =  (Long)obj[1];
		        }
		        model.addAttribute("name", name);
		        model.addAttribute("zlzsjs", zlzsjs); 
		        model.addAttribute("zlwtfd", zlwtfd); 
		        model.addAttribute("zlbllc", zlbllc); 
		        model.addAttribute("gyfw", gyfw); 
		        model.addAttribute("hwwq",  hwwq); 
		        model.addAttribute("yjbg", yjbg); 
		    }
		return "news/list";
	}
}
