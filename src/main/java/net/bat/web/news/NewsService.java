package net.bat.web.news;


import java.util.List;


import net.bat.dao.UserDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
@Component
@Transactional
public class NewsService {
	
	@Autowired
	private UserDAO dao;
	
	/**
     * 资讯列表查询--根据栏目类型
     * 
     */
	public List<Object[]> getNewsList(String lm,String name ){
		String sql = null;
		if(name == null)
		   sql = " select a.id, a.title, a.href,a.dtCreate from Article a where a.postto like '%"+lm+"%' and a.status in (1,2) ORDER BY a.btop asc,a.dtCreate desc ";
		else
		   sql = " select a.id, a.title, a.href,a.dtCreate from Article a where a.postto like '%"+lm+"%' and (a.title like '%"+name+"%' or a.content like '%"+name+"%') and a.status in (1,2) ORDER BY a.btop asc,a.dtCreate desc ";
		   
		List<Object[]> objList = dao.query(-1, -1, sql, null, null);
		return objList;
	}
	
	/**
     * 资讯检索--根据全文检索
     * 
     */
	public List<Object[]> getCountNews(String name){
		String sql = " select a.postto,COUNT(*) as sl from Article a where (a.title like '%"+name+"%' or a.content like '%"+name+"%') and a.status in (1,2) GROUP BY a.postto ORDER BY a.btop asc,a.dtCreate desc ";	
		List<Object[]> posttoSl = dao.query(-1, -1, sql, null, null);
		return posttoSl;
	}
	
}
