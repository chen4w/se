package net.bat.web.zcb;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * 链接到知产宝
 * @author wangshuxin
 * 2016-04-20
 */
@Controller
public class ZcbController {

	@RequestMapping(value = "/zcb", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,String> testZcb(int type,int casetype,String content,Model model){
		Map<String,String> result = new HashMap<String,String>();
		String url = null;
		try{
			String keyword = type+"|"+casetype+"|"+content;
			
			String token = ZcbService.getZCBUrl(keyword);
			url = "http://www.iphouse.cn/acpaa/search.html?token="+token;

			result.put("status", "succ");
			result.put("url", url);
		}catch(Exception e){
			result.put("status", "failed");
			result.put("msg", "连接知产宝失败,请联系管理员");
		}
		return result;
	}  
}
