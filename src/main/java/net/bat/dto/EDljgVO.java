package net.bat.dto;

import java.util.Date;

import net.bat.entity.IdEntity;

public class EDljgVO  extends IdEntity{

	
	
	
	public EDljgVO(long id,String jgdm, String mc, Integer sl,  Date dtSlsj,Integer dlrrs, String dy) {
		this.id = id;
		this.jgdm = jgdm;
		this.mc = mc;
		this.sl = sl;
		this.dlrrs = dlrrs;
		this.dtSlsj = dtSlsj;
		this.dy = dy;
	}

	private String jgdm;
	private String mc;
	private Integer sl;
	private Integer dlrrs;
	private Date dtSlsj;
	private String dy;
	private String date ;
	public String getJgdm() {
		return jgdm;
	}
	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
	public Integer getSl() {
		return sl;
	}
	public void setSl(Integer sl) {
		this.sl = sl;
	}
	public Integer getDlrrs() {
		return dlrrs;
	}
	public void setDlrrs(Integer dlrrs) {
		this.dlrrs = dlrrs;
	}
	public Date getDtSlsj() {
		return dtSlsj;
	}
	public void setDtSlsj(Date dtSlsj) {
		this.dtSlsj = dtSlsj;
	}
	public String getDy() {
		return dy;
	}
	public void setDy(String dy) {
		this.dy = dy;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	
}
