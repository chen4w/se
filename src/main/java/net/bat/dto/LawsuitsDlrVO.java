package net.bat.dto;

import java.util.Date;

import net.bat.entity.IdEntity;

public class LawsuitsDlrVO  extends IdEntity{
	
	private String xm;
	private Date dtGzjy;
	private String wynl;
	private String mc;
	private String dy;
	private int msajsl;
	private int xzajsl;
	private String nx;
	private String sxzy;
	
	public String getNx() {
		return nx;
	}





	public void setTt(String nx) {
		this.nx = nx;
	}





	public LawsuitsDlrVO(long id,String xm,Date dtGzjy,String wynl, String mc,String dy,int msajsl,int xzajsl,String sxzy) {
		this.id = id;
		this.xm = xm;
		this.dtGzjy = dtGzjy;
		this.wynl = wynl;
		this.mc = mc;
		this.dy = dy;
		this.msajsl = msajsl;
		this.xzajsl = xzajsl;
		this.sxzy = sxzy;
	}

	

	

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}






	public String getXm() {
		return xm;
	}





	public void setXm(String xm) {
		this.xm = xm;
	}





	public Date getDtGzjy() {
		return dtGzjy;
	}





	public void setDtGzjy(Date dtGzjy) {
		this.dtGzjy = dtGzjy;
	}





	public String getWynl() {
		return wynl;
	}




	public void setWynl(String wynl) {
		this.wynl = wynl;
	}






	public String getDy() {
		return dy;
	}





	public void setDy(String dy) {
		this.dy = dy;
	}






	public int getMsajsl() {
		return msajsl;
	}





	public void setMsajsl(int msajsl) {
		this.msajsl = msajsl;
	}





	public int getXzajsl() {
		return xzajsl;
	}





	public void setXzajsl(int xzajsl) {
		this.xzajsl = xzajsl;
	}





	public void setNx(String nx) {
		this.nx = nx;
	}





	public String getSxzy() {
		return sxzy;
	}





	public void setSxzy(String sxzy) {
		this.sxzy = sxzy;
	}



}
