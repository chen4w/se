package net.bat.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.bat.entity.IdEntity;
/**
 * 复审无效
 * @author wangshuxin
 * 2016-04-19
 */
public class FswxDTO  extends IdEntity{
	/**
	 * 决定号
	 */
	private String jdh;
	/**
	 * 决定日
	 */
	private Date dtJdr;
	
	private String dtJdr_str;
	
	private int fl;
	
	public FswxDTO(String jdh,Date dtJdr,int fl){
		this.jdh = jdh;
		this.dtJdr = dtJdr;
		this.fl = fl;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if(dtJdr==null)
			this.dtJdr_str = "";
		this.dtJdr_str = sdf.format(dtJdr);
	}
	public String getJdh() {
		return jdh;
	}
	public void setJdh(String jdh) {
		this.jdh = jdh;
	}
	public Date getDtJdr() {
		return dtJdr;
	}
	public void setDtJdr(Date dtJdr) {
		this.dtJdr = dtJdr;
	}
	public String getDtJdr_str() {
		return dtJdr_str;
	}
	public void setDtJdr_str(String dtJdr_str) {
		this.dtJdr_str = dtJdr_str;
	}
	public int getFl() {
		return fl;
	}
	public void setFl(int fl) {
		this.fl = fl;
	}
	
}
