package net.bat.dto;

import java.util.Date;

import net.bat.entity.IdEntity;

public class EDljgDTO  extends IdEntity{

	public EDljgDTO(long id,String jgdm, String mc, int sl,  Date dtSlsj,int dlrrs, String dy) {
		this.id = id;
		this.jgdm = jgdm;
		this.mc = mc;
		this.sl = sl;
		this.dtSlsj = dtSlsj;
		this.dlrrs = dlrrs;
		this.dy = dy;
	}
	
	
	private String jgdm;
	private String mc;
	private int sl;
	private int dlrrs;
	private Date dtSlsj;
	private String dy;
	private String date ;
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getJgdm() {
		return jgdm;
	}

	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public int getSl() {
		return sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

	public int getDlrrs() {
		return dlrrs;
	}

	public void setDlrrs(int dlrrs) {
		this.dlrrs = dlrrs;
	}

	public Date getDtSlsj() {
		return dtSlsj;
	}

	public void setDtSlsj(Date dtSlsj) {
		this.dtSlsj = dtSlsj;
	}

	public String getDy() {
		return dy;
	}

	public void setDy(String dy) {
		this.dy = dy;
	}

}
