package net.bat.dto;

public class LoginInfo {
	public String username;
	public String password;
	public String pwdnew;
	public String accessToken;
	public boolean rememberMe;
}
