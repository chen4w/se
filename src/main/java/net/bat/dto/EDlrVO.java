package net.bat.dto;

import java.util.Date;

import net.bat.entity.IdEntity;

public class EDlrVO  extends IdEntity{
	
	private String xm;
	private Date dtGzjy;
	private String wynl;
	private String mc;
	private String jgdm;
	private String dy;
	private String sxzy;
	private String nx;
	private int sl;
	//a.id,a.xm,a.dt_gzjy,a.wynl,b.mc,b.dy,a.sxzy,c.sl
	public String getNx() {
		return nx;
	}





	public void setTt(String nx) {
		this.nx = nx;
	}





	public EDlrVO(long id,String xm,Date dtGzjy,String wynl,String jgdm,String mc,String dy,String sxzy,int sl) {
		this.id = id;
		this.xm = xm;
		this.dtGzjy = dtGzjy;
		this.wynl = wynl;
		this.jgdm = jgdm;
		this.mc = mc;
		this.dy = dy;
		this.sxzy = sxzy;
		this.sl = sl;
	}

	

	

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}






	public String getJgdm() {
		return jgdm;
	}





	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}





	public String getXm() {
		return xm;
	}





	public void setXm(String xm) {
		this.xm = xm;
	}





	public Date getDtGzjy() {
		return dtGzjy;
	}





	public void setDtGzjy(Date dtGzjy) {
		this.dtGzjy = dtGzjy;
	}





	public String getWynl() {
		return wynl;
	}




	public void setWynl(String wynl) {
		this.wynl = wynl;
	}






	public String getDy() {
		return dy;
	}





	public void setDy(String dy) {
		this.dy = dy;
	}





	public String getSxzy() {
		return sxzy;
	}





	public void setSxzy(String sxzy) {
		this.sxzy = sxzy;
	}






	public int getSl() {
		return sl;
	}





	public void setSl(int sl) {
		this.sl = sl;
	}





	public void setNx(String nx) {
		this.nx = nx;
	}

	

}
