package net.bat.dto;

import java.util.Date;

import net.bat.entity.IdEntity;

public class MessageDTO extends IdEntity{

	private String title;
	private String content;
	private Date dtCreate;
	private int uid;
	private String urole;
	private String uname;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDtCreate() {
		return dtCreate;
	}
	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getUrole() {
		return urole;
	}
	public void setUrole(String urole) {
		this.urole = urole;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public MessageDTO(String title, String content, Date dtCreate, int uid, String urole, String uname) {
		super();
		this.title = title;
		this.content = content;
		this.dtCreate = dtCreate;
		this.uid = uid;
		this.urole = urole;
		this.uname = uname;
	}
	
}
