package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the d_jslyfl database table.
 * 
 */
@Entity
@Table(name="d_jslyfl")
@NamedQuery(name="DJslyfl.findAll", query="SELECT d FROM DJslyfl d")
public class DJslyfl extends IdEntity  {
	private static final long serialVersionUID = 1L;


	private int flfs;

	private String flh;

	private String mc;

	public DJslyfl() {
	}


	public int getFlfs() {
		return this.flfs;
	}

	public void setFlfs(int flfs) {
		this.flfs = flfs;
	}

	public String getFlh() {
		return this.flh;
	}

	public void setFlh(String flh) {
		this.flh = flh;
	}

	public String getMc() {
		return this.mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

}