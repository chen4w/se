package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the s_fswx database table.
 * 
 */
@Entity
@Table(name="s_fswx")
@NamedQuery(name="SFswx.findAll", query="SELECT s FROM SFswx s")
public class SFswx extends IdEntity  {
	private static final long serialVersionUID = 1L;
 

	private int dlrfl;

	private int fl;

	private String jdh;

	@Temporal(TemporalType.TIMESTAMP)
	private Date jdr;

	private String jgdm;

	@Column(name="pid_dljg")
	private int pidDljg;

	@Column(name="pid_dlr")
	private int pidDlr;

	private String sqh;

	private String xm;

	public SFswx() {
	}

 

	public int getDlrfl() {
		return this.dlrfl;
	}

	public void setDlrfl(int dlrfl) {
		this.dlrfl = dlrfl;
	}

	public int getFl() {
		return this.fl;
	}

	public void setFl(int fl) {
		this.fl = fl;
	}

	public String getJdh() {
		return this.jdh;
	}

	public void setJdh(String jdh) {
		this.jdh = jdh;
	}

	public Date getJdr() {
		return this.jdr;
	}

	public void setJdr(Date jdr) {
		this.jdr = jdr;
	}

	public String getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}

	public int getPidDljg() {
		return this.pidDljg;
	}

	public void setPidDljg(int pidDljg) {
		this.pidDljg = pidDljg;
	}

	public int getPidDlr() {
		return this.pidDlr;
	}

	public void setPidDlr(int pidDlr) {
		this.pidDlr = pidDlr;
	}

	public String getSqh() {
		return this.sqh;
	}

	public void setSqh(String sqh) {
		this.sqh = sqh;
	}

	public String getXm() {
		return this.xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

}