package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_dlr database table.
 * 
 */
@Entity
@Table(name="s_dlr")
@NamedQuery(name="SDlr.findAll", query="SELECT s FROM SDlr s")
public class SDlr extends IdEntity  {
	private static final long serialVersionUID = 1L;

 

	private String fl;

	private String jgdm;

	private Integer nd;

	private Integer pid;

	private Integer sl;

	private String xm;

	public SDlr() {
	}

	

	public String getFl() {
		return this.fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public String getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}

	public Integer getNd() {
		return this.nd;
	}

	public void setNd(Integer nd) {
		this.nd = nd;
	}

	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getSl() {
		return this.sl;
	}

	public void setSl(Integer sl) {
		this.sl = sl;
	}

	public String getXm() {
		return this.xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

}