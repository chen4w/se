package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the e_dlr_lj database table.
 * 
 */
@Entity
@Table(name="e_dlr_lj")
@NamedQuery(name="EDlrLj.findAll", query="SELECT e FROM EDlrLj e")
public class EDlrLj extends IdEntity  {
	private static final long serialVersionUID = 1L;

 

	private int bcjjl;

	private int bhwlx;

	private String cbxszz;

	private int cdzdxm;

	private String dlrxhzc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_gzjy")
	private Date dtGzjy;

	private String fbxslw;

	private String fwlx;

	private String gjjlqk;

	private String gzjl;

	private String gzjrc;

	private String jsly;

	private String jyjl;

	private int ljrw;

	private int msss;

	private String ndkh;

	private int pid;

	private String pxjl;

	private String qtshzw;

	private String sbjjlqk;

	private String sxzy;

	private String szcs;

	private String wynl;

	private String xb;

	private String xm;

	private String xmlyw;

	private String xsyj;

	private int xzss;

	private int xzzfaj;

	private String zgxl;

	private String zgzh;

	private int zldh;

	private int zlfs;

	private int zlfxpj;

	private int zljs;

	private int zlqsjf;

	private int zlsq;

	private int zlxgwx;

	private int zlyj;

	private int zlyy;

	private int zscqgb;

	private String zy;

	private String zyzh;

	private int zz;

	public EDlrLj() {
	}


	public int getBcjjl() {
		return this.bcjjl;
	}

	public void setBcjjl(int bcjjl) {
		this.bcjjl = bcjjl;
	}

	public int getBhwlx() {
		return this.bhwlx;
	}

	public void setBhwlx(int bhwlx) {
		this.bhwlx = bhwlx;
	}

	public String getCbxszz() {
		return this.cbxszz;
	}

	public void setCbxszz(String cbxszz) {
		this.cbxszz = cbxszz;
	}

	public int getCdzdxm() {
		return this.cdzdxm;
	}

	public void setCdzdxm(int cdzdxm) {
		this.cdzdxm = cdzdxm;
	}

	public String getDlrxhzc() {
		return this.dlrxhzc;
	}

	public void setDlrxhzc(String dlrxhzc) {
		this.dlrxhzc = dlrxhzc;
	}

	public Date getDtGzjy() {
		return this.dtGzjy;
	}

	public void setDtGzjy(Date dtGzjy) {
		this.dtGzjy = dtGzjy;
	}

	public String getFbxslw() {
		return this.fbxslw;
	}

	public void setFbxslw(String fbxslw) {
		this.fbxslw = fbxslw;
	}

	public String getFwlx() {
		return this.fwlx;
	}

	public void setFwlx(String fwlx) {
		this.fwlx = fwlx;
	}

	public String getGjjlqk() {
		return this.gjjlqk;
	}

	public void setGjjlqk(String gjjlqk) {
		this.gjjlqk = gjjlqk;
	}

	public String getGzjl() {
		return this.gzjl;
	}

	public void setGzjl(String gzjl) {
		this.gzjl = gzjl;
	}

	public String getGzjrc() {
		return this.gzjrc;
	}

	public void setGzjrc(String gzjrc) {
		this.gzjrc = gzjrc;
	}

	public String getJsly() {
		return this.jsly;
	}

	public void setJsly(String jsly) {
		this.jsly = jsly;
	}

	public String getJyjl() {
		return this.jyjl;
	}

	public void setJyjl(String jyjl) {
		this.jyjl = jyjl;
	}

	public int getLjrw() {
		return this.ljrw;
	}

	public void setLjrw(int ljrw) {
		this.ljrw = ljrw;
	}

	public int getMsss() {
		return this.msss;
	}

	public void setMsss(int msss) {
		this.msss = msss;
	}

	public String getNdkh() {
		return this.ndkh;
	}

	public void setNdkh(String ndkh) {
		this.ndkh = ndkh;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getPxjl() {
		return this.pxjl;
	}

	public void setPxjl(String pxjl) {
		this.pxjl = pxjl;
	}

	public String getQtshzw() {
		return this.qtshzw;
	}

	public void setQtshzw(String qtshzw) {
		this.qtshzw = qtshzw;
	}

	public String getSbjjlqk() {
		return this.sbjjlqk;
	}

	public void setSbjjlqk(String sbjjlqk) {
		this.sbjjlqk = sbjjlqk;
	}

	public String getSxzy() {
		return this.sxzy;
	}

	public void setSxzy(String sxzy) {
		this.sxzy = sxzy;
	}

	public String getSzcs() {
		return this.szcs;
	}

	public void setSzcs(String szcs) {
		this.szcs = szcs;
	}

	public String getWynl() {
		return this.wynl;
	}

	public void setWynl(String wynl) {
		this.wynl = wynl;
	}

	public String getXb() {
		return this.xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}

	public String getXm() {
		return this.xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getXmlyw() {
		return this.xmlyw;
	}

	public void setXmlyw(String xmlyw) {
		this.xmlyw = xmlyw;
	}

	public String getXsyj() {
		return this.xsyj;
	}

	public void setXsyj(String xsyj) {
		this.xsyj = xsyj;
	}

	public int getXzss() {
		return this.xzss;
	}

	public void setXzss(int xzss) {
		this.xzss = xzss;
	}

	public int getXzzfaj() {
		return this.xzzfaj;
	}

	public void setXzzfaj(int xzzfaj) {
		this.xzzfaj = xzzfaj;
	}

	public String getZgxl() {
		return this.zgxl;
	}

	public void setZgxl(String zgxl) {
		this.zgxl = zgxl;
	}

	public String getZgzh() {
		return this.zgzh;
	}

	public void setZgzh(String zgzh) {
		this.zgzh = zgzh;
	}

	public int getZldh() {
		return this.zldh;
	}

	public void setZldh(int zldh) {
		this.zldh = zldh;
	}

	public int getZlfs() {
		return this.zlfs;
	}

	public void setZlfs(int zlfs) {
		this.zlfs = zlfs;
	}

	public int getZlfxpj() {
		return this.zlfxpj;
	}

	public void setZlfxpj(int zlfxpj) {
		this.zlfxpj = zlfxpj;
	}

	public int getZljs() {
		return this.zljs;
	}

	public void setZljs(int zljs) {
		this.zljs = zljs;
	}

	public int getZlqsjf() {
		return this.zlqsjf;
	}

	public void setZlqsjf(int zlqsjf) {
		this.zlqsjf = zlqsjf;
	}

	public int getZlsq() {
		return this.zlsq;
	}

	public void setZlsq(int zlsq) {
		this.zlsq = zlsq;
	}

	public int getZlxgwx() {
		return this.zlxgwx;
	}

	public void setZlxgwx(int zlxgwx) {
		this.zlxgwx = zlxgwx;
	}

	public int getZlyj() {
		return this.zlyj;
	}

	public void setZlyj(int zlyj) {
		this.zlyj = zlyj;
	}

	public int getZlyy() {
		return this.zlyy;
	}

	public void setZlyy(int zlyy) {
		this.zlyy = zlyy;
	}

	public int getZscqgb() {
		return this.zscqgb;
	}

	public void setZscqgb(int zscqgb) {
		this.zscqgb = zscqgb;
	}

	public String getZy() {
		return this.zy;
	}

	public void setZy(String zy) {
		this.zy = zy;
	}

	public String getZyzh() {
		return this.zyzh;
	}

	public void setZyzh(String zyzh) {
		this.zyzh = zyzh;
	}

	public int getZz() {
		return this.zz;
	}

	public void setZz(int zz) {
		this.zz = zz;
	}

}