package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the e_fswx database table.
 * 
 */
@Entity
@Table(name="e_fswx")
@NamedQuery(name="EFswx.findAll", query="SELECT e FROM EFswx e")
public class EFswx extends IdEntity  {
	private static final long serialVersionUID = 1L;
 

	private String csyxm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_ggr")
	private Date dtGgr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_gkr")
	private Date dtGkr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_jdr")
	private Date dtJdr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_sqr")
	private Date dtSqr;

	private String flh;

	private String fmmc;

	private String fssqr;

	private int fswx;

	private String ggh;

	private String gkh;

	private String jdh;

	private int pid;

	private String sqh;

	private String sqr;

	private String wxsqr;

	private String zsyxm;

	private int zt;

	private String zzxm;

	public EFswx() {
	}

 

	public String getCsyxm() {
		return this.csyxm;
	}

	public void setCsyxm(String csyxm) {
		this.csyxm = csyxm;
	}

	public Date getDtGgr() {
		return this.dtGgr;
	}

	public void setDtGgr(Date dtGgr) {
		this.dtGgr = dtGgr;
	}

	public Date getDtGkr() {
		return this.dtGkr;
	}

	public void setDtGkr(Date dtGkr) {
		this.dtGkr = dtGkr;
	}

	public Date getDtJdr() {
		return this.dtJdr;
	}

	public void setDtJdr(Date dtJdr) {
		this.dtJdr = dtJdr;
	}

	public Date getDtSqr() {
		return this.dtSqr;
	}

	public void setDtSqr(Date dtSqr) {
		this.dtSqr = dtSqr;
	}

	public String getFlh() {
		return this.flh;
	}

	public void setFlh(String flh) {
		this.flh = flh;
	}

	public String getFmmc() {
		return this.fmmc;
	}

	public void setFmmc(String fmmc) {
		this.fmmc = fmmc;
	}

	public String getFssqr() {
		return this.fssqr;
	}

	public void setFssqr(String fssqr) {
		this.fssqr = fssqr;
	}

	public int getFswx() {
		return this.fswx;
	}

	public void setFswx(int fswx) {
		this.fswx = fswx;
	}

	public String getGgh() {
		return this.ggh;
	}

	public void setGgh(String ggh) {
		this.ggh = ggh;
	}

	public String getGkh() {
		return this.gkh;
	}

	public void setGkh(String gkh) {
		this.gkh = gkh;
	}

	public String getJdh() {
		return this.jdh;
	}

	public void setJdh(String jdh) {
		this.jdh = jdh;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getSqh() {
		return this.sqh;
	}

	public void setSqh(String sqh) {
		this.sqh = sqh;
	}

	public String getSqr() {
		return this.sqr;
	}

	public void setSqr(String sqr) {
		this.sqr = sqr;
	}

	public String getWxsqr() {
		return this.wxsqr;
	}

	public void setWxsqr(String wxsqr) {
		this.wxsqr = wxsqr;
	}

	public String getZsyxm() {
		return this.zsyxm;
	}

	public void setZsyxm(String zsyxm) {
		this.zsyxm = zsyxm;
	}

	public int getZt() {
		return this.zt;
	}

	public void setZt(int zt) {
		this.zt = zt;
	}

	public String getZzxm() {
		return this.zzxm;
	}

	public void setZzxm(String zzxm) {
		this.zzxm = zzxm;
	}

}