package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_jsly database table.
 * 
 */
@Entity
@Table(name="s_jsly")
@NamedQuery(name="SJsly.findAll", query="SELECT s FROM SJsly s")
public class SJsly extends IdEntity  {
	private static final long serialVersionUID = 1L;

	private String dy;

	private String jgdm;

	private String jsly;

	private int pid;

	private int sl;

	private int zb;

	private String zllx;

	public SJsly() {
	}

	public String getDy() {
		return this.dy;
	}

	public void setDy(String dy) {
		this.dy = dy;
	}

	public String getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}

	public String getJsly() {
		return this.jsly;
	}

	public void setJsly(String jsly) {
		this.jsly = jsly;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getSl() {
		return this.sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

	public int getZb() {
		return this.zb;
	}

	public void setZb(int zb) {
		this.zb = zb;
	}

	public String getZllx() {
		return this.zllx;
	}

	public void setZllx(String zllx) {
		this.zllx = zllx;
	}

}