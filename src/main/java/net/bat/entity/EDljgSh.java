package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the e_dljg_sh database table.
 * 
 */
@Entity
@Table(name="e_dljg_sh")
@NamedQuery(name="EDljgSh.findAll", query="SELECT e FROM EDljgSh e")
public class EDljgSh extends IdEntity  {
	private static final long serialVersionUID = 1L;
 

	private String bz;

	private String dh;

	private int dlrrs;

	private int ds;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_slsj")
	private Date dtSlsj;

	private String dy;

	private String dz;

	private String dzxx;

	private String fzr;

	private String gsgm;

	private String gsjs;

	@Column(name="gsry_fzdw")
	private String gsryFzdw;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="gsry_sj")
	private Date gsrySj;

	@Column(name="gsry_zs")
	private String gsryZs;

	private String gsxz;

	@Column(name="gysq_fm_dztx")
	private int gysqFmDztx;

	@Column(name="gysq_fm_hx")
	private int gysqFmHx;

	@Column(name="gysq_fm_jx")
	private int gysqFmJx;

	@Column(name="gysq_syxx_dztx")
	private int gysqSyxxDztx;

	@Column(name="gysq_syxx_hx")
	private int gysqSyxxHx;

	@Column(name="gysq_syxx_jx")
	private int gysqSyxxJx;

	@Column(name="gysq_wg")
	private int gysqWg;

	private int jczt;

	private String jgdm;

	private String jyln;

	private int lsrs;

	private String lxdh;

	private String lxr;

	private String mc;

	private int msss;

	private int shzt;

	private String wz;

	private int xzss;

	private int xzzfaj;

	private int zldh;

	private int zlfs;

	private int zlfxpj;

	private int zljs;

	private int zlqsjf;

	private int zlsq;

	private int zlwx;

	private int zlyj;

	private int zlyy;

	private int zscqgb;

	public EDljgSh() {
	}
 

	public String getBz() {
		return this.bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	public String getDh() {
		return this.dh;
	}

	public void setDh(String dh) {
		this.dh = dh;
	}

	public int getDlrrs() {
		return this.dlrrs;
	}

	public void setDlrrs(int dlrrs) {
		this.dlrrs = dlrrs;
	}

	public int getDs() {
		return this.ds;
	}

	public void setDs(int ds) {
		this.ds = ds;
	}

	public Date getDtSlsj() {
		return this.dtSlsj;
	}

	public void setDtSlsj(Date dtSlsj) {
		this.dtSlsj = dtSlsj;
	}

	public String getDy() {
		return this.dy;
	}

	public void setDy(String dy) {
		this.dy = dy;
	}

	public String getDz() {
		return this.dz;
	}

	public void setDz(String dz) {
		this.dz = dz;
	}

	public String getDzxx() {
		return this.dzxx;
	}

	public void setDzxx(String dzxx) {
		this.dzxx = dzxx;
	}

	public String getFzr() {
		return this.fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getGsgm() {
		return this.gsgm;
	}

	public void setGsgm(String gsgm) {
		this.gsgm = gsgm;
	}

	public String getGsjs() {
		return this.gsjs;
	}

	public void setGsjs(String gsjs) {
		this.gsjs = gsjs;
	}

	public String getGsryFzdw() {
		return this.gsryFzdw;
	}

	public void setGsryFzdw(String gsryFzdw) {
		this.gsryFzdw = gsryFzdw;
	}

	public Date getGsrySj() {
		return this.gsrySj;
	}

	public void setGsrySj(Date gsrySj) {
		this.gsrySj = gsrySj;
	}

	public String getGsryZs() {
		return this.gsryZs;
	}

	public void setGsryZs(String gsryZs) {
		this.gsryZs = gsryZs;
	}

	public String getGsxz() {
		return this.gsxz;
	}

	public void setGsxz(String gsxz) {
		this.gsxz = gsxz;
	}

	public int getGysqFmDztx() {
		return this.gysqFmDztx;
	}

	public void setGysqFmDztx(int gysqFmDztx) {
		this.gysqFmDztx = gysqFmDztx;
	}

	public int getGysqFmHx() {
		return this.gysqFmHx;
	}

	public void setGysqFmHx(int gysqFmHx) {
		this.gysqFmHx = gysqFmHx;
	}

	public int getGysqFmJx() {
		return this.gysqFmJx;
	}

	public void setGysqFmJx(int gysqFmJx) {
		this.gysqFmJx = gysqFmJx;
	}

	public int getGysqSyxxDztx() {
		return this.gysqSyxxDztx;
	}

	public void setGysqSyxxDztx(int gysqSyxxDztx) {
		this.gysqSyxxDztx = gysqSyxxDztx;
	}

	public int getGysqSyxxHx() {
		return this.gysqSyxxHx;
	}

	public void setGysqSyxxHx(int gysqSyxxHx) {
		this.gysqSyxxHx = gysqSyxxHx;
	}

	public int getGysqSyxxJx() {
		return this.gysqSyxxJx;
	}

	public void setGysqSyxxJx(int gysqSyxxJx) {
		this.gysqSyxxJx = gysqSyxxJx;
	}

	public int getGysqWg() {
		return this.gysqWg;
	}

	public void setGysqWg(int gysqWg) {
		this.gysqWg = gysqWg;
	}

	public int getJczt() {
		return this.jczt;
	}

	public void setJczt(int jczt) {
		this.jczt = jczt;
	}

	public String getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(String jgdm) {
		this.jgdm = jgdm;
	}

	public String getJyln() {
		return this.jyln;
	}

	public void setJyln(String jyln) {
		this.jyln = jyln;
	}

	public int getLsrs() {
		return this.lsrs;
	}

	public void setLsrs(int lsrs) {
		this.lsrs = lsrs;
	}

	public String getLxdh() {
		return this.lxdh;
	}

	public void setLxdh(String lxdh) {
		this.lxdh = lxdh;
	}

	public String getLxr() {
		return this.lxr;
	}

	public void setLxr(String lxr) {
		this.lxr = lxr;
	}

	public String getMc() {
		return this.mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public int getMsss() {
		return this.msss;
	}

	public void setMsss(int msss) {
		this.msss = msss;
	}

	public int getShzt() {
		return this.shzt;
	}

	public void setShzt(int shzt) {
		this.shzt = shzt;
	}

	public String getWz() {
		return this.wz;
	}

	public void setWz(String wz) {
		this.wz = wz;
	}

	public int getXzss() {
		return this.xzss;
	}

	public void setXzss(int xzss) {
		this.xzss = xzss;
	}

	public int getXzzfaj() {
		return this.xzzfaj;
	}

	public void setXzzfaj(int xzzfaj) {
		this.xzzfaj = xzzfaj;
	}

	public int getZldh() {
		return this.zldh;
	}

	public void setZldh(int zldh) {
		this.zldh = zldh;
	}

	public int getZlfs() {
		return this.zlfs;
	}

	public void setZlfs(int zlfs) {
		this.zlfs = zlfs;
	}

	public int getZlfxpj() {
		return this.zlfxpj;
	}

	public void setZlfxpj(int zlfxpj) {
		this.zlfxpj = zlfxpj;
	}

	public int getZljs() {
		return this.zljs;
	}

	public void setZljs(int zljs) {
		this.zljs = zljs;
	}

	public int getZlqsjf() {
		return this.zlqsjf;
	}

	public void setZlqsjf(int zlqsjf) {
		this.zlqsjf = zlqsjf;
	}

	public int getZlsq() {
		return this.zlsq;
	}

	public void setZlsq(int zlsq) {
		this.zlsq = zlsq;
	}

	public int getZlwx() {
		return this.zlwx;
	}

	public void setZlwx(int zlwx) {
		this.zlwx = zlwx;
	}

	public int getZlyj() {
		return this.zlyj;
	}

	public void setZlyj(int zlyj) {
		this.zlyj = zlyj;
	}

	public int getZlyy() {
		return this.zlyy;
	}

	public void setZlyy(int zlyy) {
		this.zlyy = zlyy;
	}

	public int getZscqgb() {
		return this.zscqgb;
	}

	public void setZscqgb(int zscqgb) {
		this.zscqgb = zscqgb;
	}

}