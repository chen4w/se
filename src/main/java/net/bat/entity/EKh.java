package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the e_kh database table.
 * 
 */
@Entity
@Table(name="e_kh")
@NamedQuery(name="EKh.findAll", query="SELECT e FROM EKh e")
public class EKh extends IdEntity  {
	private static final long serialVersionUID = 1L;

	 

	private String dy;

	private int gnw;

	private int pid;

	private int sl;

	public EKh() {
	}

	public String getDy() {
		return this.dy;
	}

	public void setDy(String dy) {
		this.dy = dy;
	}

	public int getGnw() {
		return this.gnw;
	}

	public void setGnw(int gnw) {
		this.gnw = gnw;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getSl() {
		return this.sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

}