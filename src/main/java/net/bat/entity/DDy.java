package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the d_dy database table.
 * 
 */
@Entity
@Table(name="d_dy")
@NamedQuery(name="DDy.findAll", query="SELECT d FROM DDy d")
public class DDy extends IdEntity  {
	private static final long serialVersionUID = 1L;


	private String bm;

	private String zwmc;

	public DDy() {
	}
 
	public String getBm() {
		return this.bm;
	}

	public void setBm(String bm) {
		this.bm = bm;
	}

	public String getZwmc() {
		return this.zwmc;
	}

	public void setZwmc(String zwmc) {
		this.zwmc = zwmc;
	}

}