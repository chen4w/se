package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the bookmark database table.
 * 
 */
@Entity

@NamedQuery(name="Bookmark.findAll", query="SELECT b FROM Bookmark b")
public class Bookmark extends IdEntity  {

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_create")
	private Date dtCreate;

	private String title;

	private int uid;

	private String url;

	public Bookmark() {
	}

	public Date getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}