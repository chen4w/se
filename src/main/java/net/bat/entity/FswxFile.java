package net.bat.entity;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="fswx_file")
@NamedQuery(name="FswxFile.findAll", query="SELECT s FROM FswxFile s")
public class FswxFile extends IdEntity {
	private static final long serialVersionUID = 1L;
	
	private int fl;

	private String jdh;
	
	private String path;

	public int getFl() {
		return fl;
	}

	public void setFl(int fl) {
		this.fl = fl;
	}

	public String getJdh() {
		return jdh;
	}

	public void setJdh(String jdh) {
		this.jdh = jdh;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
 
}
