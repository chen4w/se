package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_dlr_ajsl database table.
 * 
 */
@Entity
@Table(name="s_dlr_ajsl")
@NamedQuery(name="SDlrAjsl.findAll", query="SELECT s FROM SDlrAjsl s")
public class SDlrAjsl extends IdEntity  {
	private static final long serialVersionUID = 1L;
 
	private int ajzl;

	private int jgdm;

	private int msajsl;

	private int pid;

	private String xm;

	private int xzajsl;

	public SDlrAjsl() {
	}

 
	public int getAjzl() {
		return this.ajzl;
	}

	public void setAjzl(int ajzl) {
		this.ajzl = ajzl;
	}

	public int getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(int jgdm) {
		this.jgdm = jgdm;
	}

	public int getMsajsl() {
		return this.msajsl;
	}

	public void setMsajsl(int msajsl) {
		this.msajsl = msajsl;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getXm() {
		return this.xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public int getXzajsl() {
		return this.xzajsl;
	}

	public void setXzajsl(int xzajsl) {
		this.xzajsl = xzajsl;
	}

}