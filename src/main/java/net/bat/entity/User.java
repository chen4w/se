package net.bat.entity;

import javax.persistence.*;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableList;

/**
 * The persistent class for the e_user database table.
 * 
 */
@Entity
@Table(name="e_user")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User extends IdEntity  {

	private String adress;
	private String plainPassword;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_birthday")
	private Date dtBirthday;
	private Date dt_lastlogin;
	private String email;
	@Column(name="login_name")
	private String loginName;

	private String name;

	private String password;

	private String phone;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="register_date")
	private Date registerDate;

	private String roles;

	private String salt;

	private String sex;

	private int ssjg;

	private int status;

	public User() {
	}
	
	public User(Long id) {
		this.id = id;
	}

	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	// 不持久化到数据库，也不显示在Restful接口的属性.
	@Transient
	@JsonIgnore
	public String getPlainPassword() {
		return plainPassword;
	}


	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}
	
	public Date getDtBirthday() {
		return this.dtBirthday;
	}

	public void setDtBirthday(Date dtBirthday) {
		this.dtBirthday = dtBirthday;
	}



	public Date getDt_lastlogin() {
		return dt_lastlogin;
	}

	public void setDt_lastlogin(Date dt_lastlogin) {
		this.dt_lastlogin = dt_lastlogin;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	// 设定JSON序列化时的日期格式
	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}	

	public String getRoles() {
		return this.roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getSsjg() {
		return this.ssjg;
	}

	public void setSsjg(int ssjg) {
		this.ssjg = ssjg;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	@Transient
	@JsonIgnore
	public List<String> getRoleList() {
		// 角色列表在数据库中实际以逗号分隔字符串存储，因此返回不能修改的List.
		return ImmutableList.copyOf(StringUtils.split(roles, ","));
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}