package net.bat.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
//@Table(name="s_dljg")
//@NamedQuery(name="SDljg.findAll", query="SELECT e FROM SDljg e")
public class InvalidReview extends IdEntity implements Serializable {
	/** serialVersionUID. */
	@Temporal(TemporalType.TIMESTAMP)
	private static final long serialVersionUID = 1L;
	private String name;   //名称
	private String petitioner; // 请求人
	private String requestAgent; // 请求代理人
	private String requestBroker; //请求代理机构
	private String patentAgent; //专利代理人
	private String patentAgency; //专利代理人机构
	private String legalBasis; //法律依据
	private String determineResult; //决定结果

	
	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPetitoner() {
		return petitioner;
	}



	public void setPetitoner(String petitioner) {
		this.petitioner = petitioner;
	}



	public String getRequestAgent() {
		return requestAgent;
	}



	public void setRequestAgent(String requestAgent) {
		this.requestAgent = requestAgent;
	}



	public String getRequestBroker() {
		return requestBroker;
	}



	public void setRequestBroker(String requestBroker) {
		this.requestBroker = requestBroker;
	}



	public String getPatentAgent() {
		return patentAgent;
	}



	public void setPatentAgent(String patentAgent) {
		this.patentAgent = patentAgent;
	}



	public String getPatentAgency() {
		return patentAgency;
	}



	public void setPatentAgency(String patentAgency) {
		this.patentAgency = patentAgency;
	}



	public String getLegalBasis() {
		return legalBasis;
	}



	public void setLegalBasis(String legalBasis) {
		this.legalBasis = legalBasis;
	}



	public String getDetermineResult() {
		return determineResult;
	}



	public void setDetermineResult(String determineResult) {
		this.determineResult = determineResult;
	}



	public InvalidReview() {
	}

	
	public InvalidReview(String name,String petitioner,String requestAgent,String requestBroker,String patentAgent,
			String patentAgency,String legalBasis,String determineResult) {
			this.name = name;
			this.petitioner = petitioner;
			this.requestAgent = requestAgent;
			this.requestBroker = requestBroker;
			this.patentAgent = patentAgent;
			this.patentAgency = patentAgency;
			this.legalBasis = legalBasis;
			this.determineResult = determineResult;
	}
}
