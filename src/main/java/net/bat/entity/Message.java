package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@NamedQuery(name="Message.findAll", query="SELECT m FROM Message m")
public class Message extends IdEntity  {

	

	private int bpublish;

	private int close;

	private String content;

	private int del;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_create")
	private Date dtCreate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_reply")
	private Date dtReply;

	private int jid;

	private int pid;

	private String reply;

	private String title;

	private int uid;

	public Message() {
	}



	public int getBpublish() {
		return this.bpublish;
	}

	public void setBpublish(int bpublish) {
		this.bpublish = bpublish;
	}

	public int getClose() {
		return this.close;
	}

	public void setClose(int close) {
		this.close = close;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDel() {
		return this.del;
	}

	public void setDel(int del) {
		this.del = del;
	}

	public Date getDtCreate() {
		return this.dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Date getDtReply() {
		return this.dtReply;
	}

	public void setDtReply(Date dtReply) {
		this.dtReply = dtReply;
	}

	public int getJid() {
		return this.jid;
	}

	public void setJid(int jid) {
		this.jid = jid;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getReply() {
		return this.reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getUid() {
		return this.uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}