package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_dlr_fswx database table.
 * 
 */
@Entity
@Table(name="s_dlr_fswx")
@NamedQuery(name="SDlrFswx.findAll", query="SELECT s FROM SDlrFswx s")
public class SDlrFswx extends IdEntity  {
	private static final long serialVersionUID = 1L;
 

	private int fl;

	private int pid;

	private int sl;

	public SDlrFswx() {
	}

 

	public int getFl() {
		return this.fl;
	}

	public void setFl(int fl) {
		this.fl = fl;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getSl() {
		return this.sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

}