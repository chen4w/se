package net.bat.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the tag database table.
 * 
 */
@Entity
@NamedQuery(name = "Tag.findAll", query = "SELECT t FROM Tag t")
public class Tag extends IdEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private int heat;

	private String name;

	public Tag() {
	}

	public int getHeat() {
		return this.heat;
	}

	public void setHeat(int heat) {
		this.heat = heat;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}