package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_dljg_ajsl database table.
 * 
 */
@Entity
@Table(name="s_dljg_ajsl")
@NamedQuery(name="SDljgAjsl.findAll", query="SELECT s FROM SDljgAjsl s")
public class SDljgAjsl extends IdEntity  {
	private static final long serialVersionUID = 1L;

 

	private int ajzl;

	private int jgdm;

	private int msajsl;

	private int pid;

	private int xzajsl;

	public SDljgAjsl() {
	}
 
	public int getAjzl() {
		return this.ajzl;
	}

	public void setAjzl(int ajzl) {
		this.ajzl = ajzl;
	}

	public int getJgdm() {
		return this.jgdm;
	}

	public void setJgdm(int jgdm) {
		this.jgdm = jgdm;
	}

	public int getMsajsl() {
		return this.msajsl;
	}

	public void setMsajsl(int msajsl) {
		this.msajsl = msajsl;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getXzajsl() {
		return this.xzajsl;
	}

	public void setXzajsl(int xzajsl) {
		this.xzajsl = xzajsl;
	}

}