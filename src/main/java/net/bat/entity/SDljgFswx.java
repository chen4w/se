package net.bat.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the s_dljg_fswx database table.
 * 
 */
@Entity
@Table(name="s_dljg_fswx")
@NamedQuery(name="SDljgFswx.findAll", query="SELECT s FROM SDljgFswx s")
public class SDljgFswx extends IdEntity  {
	private static final long serialVersionUID = 1L;

 

	private int fl;

	private int pid;

	private int sl;

	public SDljgFswx() {
	}

 
	public int getFl() {
		return this.fl;
	}

	public void setFl(int fl) {
		this.fl = fl;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getSl() {
		return this.sl;
	}

	public void setSl(int sl) {
		this.sl = sl;
	}

}