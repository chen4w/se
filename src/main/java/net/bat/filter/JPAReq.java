package net.bat.filter;

import java.util.LinkedHashMap;

public class JPAReq {
	private String whereql;
	private Object[] queryParams;
	private LinkedHashMap<String, String> orderby;

	public String getWhereql() {
		return whereql;
	}

	public void setWhereql(String whereql) {
		this.whereql = whereql;
	}

	public Object[] getQueryParams() {
		return queryParams;
	}

	public void setQueryParams(Object[] queryParams) {
		this.queryParams = queryParams;
	}

	public LinkedHashMap<String, String> getOrderby() {
		return orderby;
	}

	public void setOrderby(LinkedHashMap<String, String> orderby) {
		this.orderby = orderby;
	}
}
