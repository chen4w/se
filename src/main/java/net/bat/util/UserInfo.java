package net.bat.util;

import net.bat.entity.User;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInfo {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 获取当前登录用户
	 *
	 * @return
	 */
	public User getUser() {
		Subject subject = SecurityUtils.getSubject();
		User user = (User) subject.getPrincipal();
		return user;
	}

	/**
	 * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址;
	 *
	 * @param request
	 * @return
	 */
	public String getIpAddress() {
		Subject subject = SecurityUtils.getSubject();
		return subject.getSession().getHost();
	}

}
