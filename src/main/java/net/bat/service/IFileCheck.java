package net.bat.service;


import org.springframework.web.multipart.MultipartFile;

public interface IFileCheck {
	String valid(MultipartFile f);
}
