package net.bat.repository;


import net.bat.entity.Message;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
 
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageDao extends PagingAndSortingRepository<Message, Long>, JpaSpecificationExecutor<Message>{
	
	 
/*	Page<Message> findByUserId(Long id, Pageable pageRequest);*/
	
}
