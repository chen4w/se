/************************* 版权声明 *********************************
 * 
 * 版权所有：百洋软件
 * Copyright (c) 2009 by Pearl Ocean.
 * 
 ************************* 变更记录 *********************************
 *
 * 创建者：yongtree   创建日期： 2009-5-31
 * 创建记录：创建类结构。
 * 
 * 修改者：宋黎晓       修改日期：2010-1-12
 * 修改记录：修改了DAO接口,同时修改了此实现类.
 ************************* 随   笔 *********************************
 *
 * 这里可以写写感想，感慨，疑问什么的。
 * 
 ******************************************************************
 */

package net.bat.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import net.bat.dto.EDljgVO;
import net.bat.dto.EDlrVO;
import net.bat.dto.LawsuitsDlrVO;
import net.bat.entity.IdEntity;

import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 封装常用增删改查操作
 * 
 * @author yongtree
 * @date 2009-5-31 上午11:09:01
 * @version 2.0
 * @since 2.0 泛型T挪到方法声明出,声明对象时不需要指定泛型.
 */
@SuppressWarnings("unchecked")
public abstract class BaseJpaDao implements DAO {

	private QLBuilder sqlBuilder = new QLBuilder();
//c4w for free jpql search
	public  QueryResult<Map> getScrollData(String selectjpql,String selectjpql_cout,
			int firstindex, int maxresult, String wherejpql, Object[] queryParams,
			String orderby) {
		QueryResult<Map> qr = new QueryResult<Map>();
		String jpql = selectjpql+ (StringUtils.isEmpty(wherejpql) ? "" : " where " + wherejpql) 
				+  (StringUtils.isEmpty(orderby) ? "" : " order by "+ orderby);
		Query query = getEntityManager().createQuery(jpql);
		setQueryParams(query, queryParams);
		if ((firstindex != -1) && (maxresult != -1)) {
			query.setFirstResult(firstindex).setMaxResults(maxresult).setHint("org.hibernate.cacheable", true);
		}
		qr.setResultlist(query.getResultList());
		String jpql_cout = selectjpql_cout+ (StringUtils.isEmpty(wherejpql) ? "" : " where " + wherejpql);
		query = getEntityManager().createQuery(jpql_cout);
		setQueryParams(query, queryParams);
		qr.setTotalrecord((Long) query.getSingleResult());	
		return qr;
	}
	
	public  QueryResult<EDljgVO> getScrollDataForOn(String sql,String count,String where,int firstindex, int maxresult,String condition,String orderby) {
		Query queryForCount = getEntityManager().createNativeQuery(count + where + condition);
		Query query = getEntityManager().createNativeQuery(sql + where + condition + (StringUtils.isEmpty(orderby) ? "" : " order by "+ orderby) +  " limit " + firstindex + "," + maxresult);
        //执行查询，返回的是对象数组(Object[])列表,
        //每一个对象数组存的是相应的实体属性
		//a.id, a.jgdm, a.mc, b.sl, a.dt_Slsj, a.dlrrs, a.dy
        List objecArraytList = query.getResultList();
        List <EDljgVO>  array = new ArrayList<EDljgVO>();
        for(int i = 0; i < objecArraytList.size(); i++){
        	Object[] obj = (Object[]) objecArraytList.get(i);
        	Integer id = (Integer) obj[0];
        	String jgdm = (String) obj[1];
        	String mc = (String) obj[2];
        	Integer sl = 0;
        	if(obj[3] != null){
        		sl = (Integer) obj[3];
        	}
        	Date slsj = (Date) obj[4];
        	Integer dlrrs = 0;
        	if(obj[5] != null){
        		dlrrs = (Integer) obj[5];
        	}
        	String dy = (String) obj[6];
        	EDljgVO temp = new EDljgVO(id, jgdm, mc, sl, slsj,dlrrs , dy); 
        	array.add(temp);
        }
		QueryResult<EDljgVO> qr = new QueryResult<EDljgVO>();
		qr.setResultlist(array);
		Object s = queryForCount.getSingleResult();
		Long b = Long.parseLong(s.toString());
		qr.setTotalrecord(b);
		return qr;
	}
	
	public  QueryResult<EDlrVO> getScrollDataForDlr(String sql,String count,String where,int firstindex, int maxresult,String condition,String orderby) {
		Query queryForCount = getEntityManager().createNativeQuery(count + where + condition);
		Query query = getEntityManager().createNativeQuery(sql + where + condition + (StringUtils.isEmpty(orderby) ? "" : " order by "+ orderby) +  " limit " + firstindex + "," + maxresult);
        //执行查询，返回的是对象数组(Object[])列表,
        //每一个对象数组存的是相应的实体属性
		//a.id, a.jgdm, a.mc, b.sl, a.dt_Slsj, a.dlrrs, a.dy
        List objecArraytList = query.getResultList();
        List <EDlrVO>  array = new ArrayList<EDlrVO>();
        for(int i = 0; i < objecArraytList.size(); i++){
        	Object[] obj = (Object[]) objecArraytList.get(i);
        	//a.id,a.xm,a.dt_gzjy,a.wynl,b.mc,b.dy,a.sxzy,c.sl
        	Integer id = (Integer) obj[0];
        	String xm = (String) obj[1];
        	Date dt_gzjy = (Date) obj[2];
        	String wynl = (String) obj[3];
        	String jgdm = (String) obj[4];
        	String mc = (String) obj[5];
        	String dy = (String) obj[6];
        	String sxzy = (String) obj[7];
        	Integer sl = 0;
        	if(obj[8] != null){
        		sl = (Integer) obj[8];
        	}
        	EDlrVO temp = new EDlrVO(id,xm, dt_gzjy, wynl, jgdm, mc, dy,sxzy , sl); 
        	array.add(temp);
        }
		QueryResult<EDlrVO> qr = new QueryResult<EDlrVO>();
		qr.setResultlist(array);
		Object s = queryForCount.getSingleResult();
		Long b = Long.parseLong(s.toString());
		qr.setTotalrecord(b);
		return qr;
	}
	
	public  QueryResult<EDlrVO> getScrollDataForRyqDlr(String sql,String count,int firstindex, int maxresult,String where,String orderby) {
		Query queryForCount = getEntityManager().createNativeQuery(count + where);
		Query query = getEntityManager().createNativeQuery(sql + where + (StringUtils.isEmpty(orderby) ? "" : " order by "+ orderby) +  " limit " + firstindex + "," + maxresult);
        //执行查询，返回的是对象数组(Object[])列表,
        //每一个对象数组存的是相应的实体属性
		//a.id, a.jgdm, a.mc, b.sl, a.dt_Slsj, a.dlrrs, a.dy
        List objecArraytList = query.getResultList();
        List <EDlrVO>  array = new ArrayList<EDlrVO>();
        for(int i = 0; i < objecArraytList.size(); i++){
        	Object[] obj = (Object[]) objecArraytList.get(i);
        	//a.id,a.xm,a.dt_gzjy,a.wynl,b.mc,b.dy,a.sxzy,c.sl
        	Integer id = (Integer) obj[0];
        	String xm = (String) obj[1];
        	Date dt_gzjy = (Date) obj[2];
        	String wynl = (String) obj[3];
        	String jgdm = (String) obj[4];
        	String mc = (String) obj[5];
        	String dy = (String) obj[6];
        	String sxzy = (String) obj[7];
        	Integer sl = 0;
//        	if(obj[7] != null){
//        		sl = (Integer) obj[7];
//        	}
        	EDlrVO temp = new EDlrVO(id,xm, dt_gzjy, wynl, jgdm, mc, dy,sxzy , sl); 
        	array.add(temp);
        }
		QueryResult<EDlrVO> qr = new QueryResult<EDlrVO>();
		qr.setResultlist(array);
		Object s = queryForCount.getSingleResult();
		Long b = Long.parseLong(s.toString());
		qr.setTotalrecord(b);
		return qr;
	}
	
	public  QueryResult<LawsuitsDlrVO> getScrollDataForLawDlr(String sql,String count,String where,int firstindex, int maxresult,String condition,String orderby) {
		Query queryForCount = getEntityManager().createNativeQuery(count + where + condition);
		Query query = getEntityManager().createNativeQuery(sql + where + condition + (StringUtils.isEmpty(orderby) ? "" : " order by "+ orderby) +  " limit " + firstindex + "," + maxresult);
        //执行查询，返回的是对象数组(Object[])列表,
        //每一个对象数组存的是相应的实体属性
		//a.id, a.jgdm, a.mc, b.sl, a.dt_Slsj, a.dlrrs, a.dy
        List objecArraytList = query.getResultList();
        List <LawsuitsDlrVO>  array = new ArrayList<LawsuitsDlrVO>();
        for(int i = 0; i < objecArraytList.size(); i++){
        	Object[] obj = (Object[]) objecArraytList.get(i);
        	//a.id,a.xm,a.dt_gzjy,a.wynl,b.mc,b.dy,a.sxzy,c.sl
        	Integer id = (Integer) obj[0];
        	String xm = (String) obj[1];
        	Date dt_gzjy = (Date) obj[2];
        	String wynl = (String) obj[3];
        	String mc = (String) obj[4];
        	String dy = (String) obj[5];
        	Integer msajsl = 0;
        	if(obj[6] != null){
        		msajsl = (Integer)obj[6];
        	}
        	Integer xzajsl = 0;
        	if(obj[7] != null){
        		xzajsl = (Integer)obj[7];
        	}
        	String sxzy = (String) obj[8];
        	
        	//SELECT a.id,a.xm,a.dt_gzjy,a.wynl,b.mc,b.dy,c.msajsl,c.xzajsl,a.sxzy 
        	LawsuitsDlrVO temp = new LawsuitsDlrVO(id,xm, dt_gzjy, wynl, mc, dy,msajsl,xzajsl,sxzy); 
        	array.add(temp);
        }
		QueryResult<LawsuitsDlrVO> qr = new QueryResult<LawsuitsDlrVO>();
		qr.setResultlist(array);
		Object s = queryForCount.getSingleResult();
		Long b = Long.parseLong(s.toString());
		qr.setTotalrecord(b);
		return qr;
	}
	
	
	
	
	
	
	
	public List<Object[]> query(int firstindex, int maxresult,
			String sql, Object[] queryParams, LinkedHashMap<String, String> orderby){
		Query query  = getEntityManager().createQuery(sql); 
		setQueryParams(query, queryParams);
		if ((firstindex != -1) && (maxresult != -1)) {
			query.setFirstResult(firstindex).setMaxResults(maxresult).setHint("org.hibernate.cacheable", true);
		}
	    return query.getResultList(); 		
	}
	@Override
	public void clear() {
		getEntityManager().clear();
	}

	@Override
	@Transactional
	public <T extends IdEntity> void create(T entity) {
		getEntityManager().persist(entity);
	}

	@Override
	public <T extends IdEntity> void createBatch(List<T> entitys) {
		for (T entity : entitys) {
			create(entity);
		}
	}

	@Transactional
	public <T extends IdEntity> void persist(T entity) {
		getEntityManager().persist(entity);
	}
	
	@Override
	@Transactional
	public <T extends IdEntity> void update(T entity) {
		getEntityManager().merge(entity);
	}

	@Transactional
	public <T extends IdEntity> void saveAll(List<T> entitys) {
		for (int i = 0; i < entitys.size(); i++) {
			T entity = entitys.get(i);
			save(entity);
		}
	}

	@Transactional
	public <T extends IdEntity> void save(T entity) {
		if (entity.getPrimaryKey() == null) {
			this.create(entity);
		} else {
			//审核克隆,主键值不为空
			this.update(entity);
		}
	}

	@Override
	@Transactional
	public <T extends IdEntity> void delete(Class<T> entityClass, Object entityid) {
		delete(entityClass, new Object[] { entityid });
	}

	@Override
	@Transactional
	public <T extends IdEntity> void delete(Class<T> entityClass, Object[] entityids) {
		// StringBuffer sf_QL = new StringBuffer(" DELETE FROM ").append(
		// sqlBuilder.getEntityName(entityClass)).append(" o WHERE ")
		// .append(sqlBuilder.getPkField(entityClass, "o")).append("=? ");
		// Query query = getEntityManager().createQuery(sf_QL.toString());
		for (Object id : entityids) {
			getEntityManager().remove(getEntityManager().find(entityClass, id));
			// query.setParameter(1, id).executeUpdate();
		}
	}

	@Override
	@Transactional
	public <T extends IdEntity> void deleteByWhere(Class<T> entityClass, String where, Object[] delParams) {
		StringBuffer sf_QL = new StringBuffer("DELETE FROM ").append(sqlBuilder.getEntityName(entityClass)).append(
				" o WHERE 1=1 ");
		if ((where != null) && (where.length() != 0)) {
			sf_QL.append(" AND ").append(where);
		}
		Query query = getEntityManager().createQuery(sf_QL.toString());
		this.setQueryParams(query, delParams);

		query.executeUpdate();
	}

	@Override
	public <T extends IdEntity> T find(Class<T> entityClass, Object entityId) {
		return getEntityManager().find(entityClass, entityId);
	}

	@Override
	public <T extends IdEntity> long getCount(Class<T> entityClass) {
		return getCountByWhere(entityClass, null, null);
	}

	public <T extends IdEntity> List<Object> getDistinctByWhere(Class<T> entityClass, String property_name,
			Integer limit, String whereql, Object[] queryParams) {
		StringBuffer sf_QL = new StringBuffer("SELECT DISTINCT(").append(property_name).append(") FROM ")
				.append(sqlBuilder.getEntityName(entityClass)).append(" o WHERE 1=1 ");
		if ((whereql != null) && (whereql.length() != 0)) {
			sf_QL.append(" AND ").append(whereql);
		}
		Query query = getEntityManager().createQuery(sf_QL.toString());
		if (limit != null) {
			query.setMaxResults(limit);
		}
		this.setQueryParams(query, queryParams);
		return query.getResultList();
	}

	@Override
	public <T extends IdEntity> long getCountByWhere(Class<T> entityClass, String whereql, Object[] queryParams) {
		StringBuffer sf_QL = new StringBuffer("SELECT COUNT(").append(sqlBuilder.getPkField(entityClass, "o"))
				.append(") FROM ").append(sqlBuilder.getEntityName(entityClass)).append(" o WHERE 1=1 ");
		if ((whereql != null) && (whereql.length() != 0)) {
			sf_QL.append(" AND ").append(whereql);
		}
		Query query = getEntityManager().createQuery(sf_QL.toString());
		this.setQueryParams(query, queryParams);
		return (Long) query.getSingleResult();
	}

	@Override
	public <T extends IdEntity> boolean isExistedByWhere(Class<T> entityClass, String whereql, Object[] queryParams) {
		long count = getCountByWhere(entityClass, whereql, queryParams);
		return count > 0 ? true : false;
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, int firstindex, int maxresult,
			String wherejpql, Object[] queryParams, LinkedHashMap<String, String> orderby) {
		return scroll(entityClass, firstindex, maxresult, wherejpql, queryParams, orderby);
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, int firstindex, int maxresult,
			String wherejpql, List<Object> queryParams, LinkedHashMap<String, String> orderby) {
		Object[] ps = null;
		if (queryParams != null) {
			ps = queryParams.toArray();
		}
		return getScrollData(entityClass, firstindex, maxresult, wherejpql, ps, orderby);
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, int firstindex, int maxresult,
			String wherejpql, Map<String, Object> queryParams, LinkedHashMap<String, String> orderby) {
		return scroll(entityClass, firstindex, maxresult, wherejpql, queryParams, orderby);
	}

	/**
	 * 根据条件查询某个实体的列表
	 * 
	 * @author slx
	 * @param <T>
	 * @param entityClass
	 *            实体类型
	 * @param firstindex
	 *            开始行
	 * @param maxresult
	 *            结束行
	 * @param wherejpql
	 *            where条件
	 * @param queryParams
	 *            参数
	 * @param orderby
	 *            排序条件
	 * @return
	 */
	private <T extends IdEntity> QueryResult<T> scroll(Class<T> entityClass, int firstindex, int maxresult,
			String wherejpql, Object queryParams, LinkedHashMap<String, String> orderby) {
		QueryResult<T> qr = new QueryResult<T>();
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				"SELECT o FROM " + entityname + " o " + (StringUtils.isEmpty(wherejpql) ? "" : "WHERE " + wherejpql)
						+ sqlBuilder.buildOrderby(orderby));
		setQueryParams(query, queryParams);
		if ((firstindex != -1) && (maxresult != -1)) {
			query.setFirstResult(firstindex).setMaxResults(maxresult).setHint("org.hibernate.cacheable", true);
		}
		qr.setResultlist(query.getResultList());
		query = getEntityManager().createQuery(
				"SELECT COUNT(" + sqlBuilder.getPkField(entityClass, "o") + ") FROM " + entityname + " o "
						+ (StringUtils.isEmpty(wherejpql) ? "" : "WHERE " + wherejpql));
		setQueryParams(query, queryParams);
		qr.setTotalrecord((Long) query.getSingleResult());
		return qr;
	}

	/**
	 * 根据条件查询实体指定字段的值并回填到实体内. <br/>
	 * <b>注意:</b> <br/>
	 * 实体必须有包括要查询的字段为参数的构造函数.
	 * 
	 * @param <T>
	 * @param entityClass
	 * @param queryfields
	 * @param firstindex
	 * @param maxresult
	 * @param wherejpql
	 * @param queryParams
	 * @param orderby
	 * @return
	 */
	private <T extends IdEntity> QueryResult<T> scroll(Class<T> entityClass, String[] queryfields, int firstindex,
			int maxresult, String wherejpql, Object queryParams, LinkedHashMap<String, String> orderby) {
		QueryResult<T> qr = new QueryResult<T>();
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				(sqlBuilder.buildSelect(entityname, queryfields, "o") + "FROM " + entityname + " o "
						+ (StringUtils.isEmpty(wherejpql) ? "" : "WHERE " + wherejpql) + sqlBuilder
						.buildOrderby(orderby)));
		setQueryParams(query, queryParams);
		if ((firstindex != -1) && (maxresult != -1)) {
			query.setFirstResult(firstindex).setMaxResults(maxresult).setHint("org.hibernate.cacheable", true);
		}
		qr.setResultlist(query.getResultList());
		query = getEntityManager().createQuery(
				"SELECT COUNT(" + sqlBuilder.getPkField(entityClass, "o") + ") FROM " + entityname + " o "
						+ (StringUtils.isEmpty(wherejpql) ? "" : "WHERE " + wherejpql));
		setQueryParams(query, queryParams);
		qr.setTotalrecord((Long) query.getSingleResult());
		return qr;
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, String[] queryfields,
			int firstindex, int maxresult, String wherejpql, List<Object> queryParams,
			LinkedHashMap<String, String> orderby) {
		return this.scroll(entityClass, queryfields, firstindex, maxresult, wherejpql, queryParams, orderby);
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, String[] queryfields,
			int firstindex, int maxresult, String wherejpql, Map<String, Object> queryParams,
			LinkedHashMap<String, String> orderby) {
		return this.scroll(entityClass, queryfields, firstindex, maxresult, wherejpql, queryParams, orderby);
	}

	@Override
	public <T extends IdEntity> QueryResult<T> getScrollData(Class<T> entityClass, String[] queryfields,
			int firstindex, int maxresult, String wherejpql, Object[] queryParams, LinkedHashMap<String, String> orderby) {
		return this.scroll(entityClass, queryfields, firstindex, maxresult, wherejpql, queryParams, orderby);
	}

	protected void setQueryParams(Query query, Object queryParams) {
		sqlBuilder.setQueryParams(query, queryParams);
	}

	@Override
	public <T extends IdEntity> List<T> queryByWhere(Class<T> entityClass, String wheresql, Object[] queryParams) {
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				"SELECT o FROM " + entityname + " o "
						+ (((wheresql == null) || (wheresql.length() == 0)) ? "" : "WHERE " + wheresql));
		setQueryParams(query, queryParams);
		query.setHint("org.hibernate.cacheable", true);
		return query.getResultList();
	}

	@Override
	public <T extends IdEntity> List<T> queryByWhere(Class<T> entityClass, String wheresql, Object[] queryParams,
			int startRow, int rows) {
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				"SELECT o FROM " + entityname + " o "
						+ (((wheresql == null) || (wheresql.length() == 0)) ? "" : "WHERE " + wheresql));
		setQueryParams(query, queryParams);
		if (startRow >= 0) {
			query.setFirstResult(startRow);
		}
		if (rows > 0) {
			query.setMaxResults(rows);
		}
		query.setHint("org.hibernate.cacheable", true);
		return query.getResultList();
	}

	@Override
	public <T extends IdEntity> List<T> queryByWhere(Class<T> entityClass, String[] queryfields, String wheresql,
			Object[] queryParams) {
		return queryByWhere(entityClass, queryfields, wheresql, queryParams, -1, -1);
	}

	@Override
	public <T extends IdEntity> List<T> queryByWhere(Class<T> entityClass, String[] queryfields, String wheresql,
			Object[] queryParams, int startRow, int rows) {
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				sqlBuilder.buildSelect(entityname, queryfields, "o") + " FROM " + entityname + " o "
						+ (wheresql == null ? "" : "WHERE " + wheresql));
		setQueryParams(query, queryParams);
		if (startRow >= 0) {
			query.setFirstResult(startRow);
		}
		if (rows > 0) {
			query.setMaxResults(rows);
		}
		return query.getResultList();
	}

	@Override
	public <T extends IdEntity> List<Object[]> queryFieldValues(Class<T> entityClass, String[] queryfields,
			String wheresql, Object[] queryParams) {
		return queryFieldValues(entityClass, queryfields, wheresql, queryParams, -1, -1);
	}

	@Override
	public <T extends IdEntity> List<Object[]> queryFieldValues(Class<T> entityClass, String[] queryfields,
			String wheresql, Object[] queryParams, int startRow, int rows) {
		String entityname = sqlBuilder.getEntityName(entityClass);
		Query query = getEntityManager().createQuery(
				sqlBuilder.buildSelect(queryfields, "o") + " FROM " + entityname + " o "
						+ (wheresql == null ? "" : "WHERE " + wheresql));
		setQueryParams(query, queryParams);
		if (startRow >= 0) {
			query.setFirstResult(startRow);
		}
		if (rows > 0) {
			query.setMaxResults(rows);
		}
		return query.getResultList();
	}

	/**
	 * 设置查询参数
	 * 
	 * @author slx
	 * @date 2009-7-8 上午10:02:55
	 * @modifyNote
	 * @param query
	 *            查询
	 * @param queryParams
	 *            查询参数
	 */
	protected void setQueryParams(Query query, Object[] queryParams) {
		sqlBuilder.setQueryParams(query, queryParams);
	}

	/**
	 * 返回实体管理器
	 * 
	 * @desc 由slx在2010-6-8下午04:06:55重写父类方法
	 */
	@Override
	public abstract EntityManager getEntityManager();

	@Override
	public <T extends IdEntity> T load(Class<T> entityClass, Object entityId) {
		try {
			return getEntityManager().find(entityClass, entityId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public <T extends IdEntity> T findByWhere(Class<T> entityClass, String where, Object[] params) {
		List<T> l = queryByWhere(entityClass, where, params);
		if ((l != null) && (l.size() == 1)) {
			return l.get(0);
		} else if (l.size() > 1) {
			throw new RuntimeException("查寻到的结果不止一个.");
		} else {
			return null;
		}
	}

}
