package net.bat.cms;

import java.util.HashMap;

public class SecNode {
	String text;
	boolean checked;
	boolean leaf;
	SecNode[] children = null;
	String tpl = null;
	String cls;
	boolean expanded;

	// secTpl 全局map；
	public void buildMap(String pPath, String pTpl, HashMap<String, String> secTpl) {
		String mPath, mTpl;
		if (pPath == null) {
			mPath = text;
		} else {
			mPath = pPath + "#" + text;
		}
		if (tpl != null) {
			mTpl = tpl;
		} else {
			mTpl = pTpl;
		}
		secTpl.put(mPath, mTpl);
		if (children != null) {
			for (SecNode sn : children) {
				sn.buildMap(mPath, mTpl, secTpl);
			}
		}
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public SecNode[] getChildren() {
		return children;
	}

	public void setChildren(SecNode[] children) {
		this.children = children;
	}

	public String getTpl() {
		return tpl;
	}

	public void setTpl(String tpl) {
		this.tpl = tpl;
	}

}
