package net.sk.ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HeartThread implements Runnable {
	private final int loop_span = 10 * 1000;
	private boolean bStop = false;
	public static final String ACTION_HEART = "action.heart";
	static final AtomicInteger atomicInt = new AtomicInteger(0);

	@Autowired
	private WSServlet wSServlet;

	HeartThread() {
	}

	void sendHeart() {
		wSServlet.sendAction(ACTION_HEART);
	}

	public static List randEvent1(int size) {
		List<Event1> l = new ArrayList<Event1>();
		for (int i = 0; i < size; i++) {
			long span = new Date().getTime();
			l.add(new Event1(atomicInt.incrementAndGet(), "e1." + span));
		}
		return l;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!bStop) {
			try {
				sendHeart();
				// for test
				wSServlet.sendData("action.event", randEvent1(100));
				Thread.sleep(loop_span);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
