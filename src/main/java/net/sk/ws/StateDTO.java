package net.sk.ws;

import java.util.Date;
import java.util.List;

public class StateDTO<T> {
	public String action;
	public String cn;
	public List<T> data;
	public Date tm;
	StateDTO(){
		tm = new Date();
	}
}
