package net.sk.ws;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service("stateCenter")
@Lazy(false)
public class StateCenter {
	public static final String ACTION_SYS_MSG = "action.sys.msg";
	public static final String PR_SYS_SUBSCRIBE = "PR_SYS_SUBSCRIBE";
	@Autowired
	private HeartThread heartThread;

	public void startHeartThread() {
		Thread th = new Thread(heartThread);
		th.start();
	}

	@PostConstruct
	public void init() {
		startHeartThread();
	}
}
