<%@tag pageEncoding="UTF-8"%>
<%@ tag import="java.util.Map"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="fld_name" type="java.lang.String" required="true"%>
<%@ attribute name="fld_tag" type="java.lang.String" required="true"%>
<%@ attribute name="fld_sp" type="java.lang.String[]" required="true"%>
<%@ attribute name="fld_sv" type="java.lang.String[]" required="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
boolean bShow = true;
Map<String, String[]> pmap = (Map<String, String[]>)request.getAttribute("pmap");
String sp = (String)request.getAttribute("searchParams");


String[] ajlx = pmap.get("sl_ajlx");
if(ajlx != null){
	if(ajlx[0].equals("review") && (fld_name.equals("sl_flsy_wx") || fld_name.equals("sl_jdjl_wx"))){
		bShow=false;
	}
	if(ajlx[0].equals("invalid") && (fld_name.equals("sl_flsy_fs") || fld_name.equals("sl_jdjl_fs"))){
		bShow=false;
	}
}else{
	if(fld_name.equals("sl_flsy_fs") || fld_name.equals("sl_jdjl_fs") || fld_name.equals("sl_flsy_wx") || fld_name.equals("sl_jdjl_wx")){
		bShow=false;
	}
}




String[] hrefs = new String[fld_sp.length];
if(bShow){
	for(int i=0; i<fld_sp.length; i++){
		String href = null;
		//除本条件之外的url
		for(String key: pmap.keySet()){
			//隐藏已选条件
			if(key.equals(fld_name)){
				bShow=false;
				continue;
			}
			if(key.equals("page")){
				continue;
			}
			String[]pv = pmap.get(key);
			if(href==null)
				href = "?"+key+"="+pv[0];
			else
				href += "&"+key+"="+pv[0];
		}
		//附加本条件
		if(href==null)
			hrefs[i]="?"+fld_name+"="+fld_sv[i];
		else
			hrefs[i]=href+"&"+fld_name+"="+fld_sv[i];
	}
}
request.setAttribute("hrefs", hrefs);
request.setAttribute("bShow", bShow);
%>

<c:if test="${bShow}">
<div class="J_selectorLine s-line">
	<div class="sl-wrap">
	<div class="sl-key"><span><strong><%=fld_tag%>：</span></strong></div>
	<div class="sl-value">
		<div class="sl-v-list">
			<ul class="J_valueList<%=fld_name%>">
			<c:forEach var="i" begin="0" end="<%=fld_sp.length%>">
			   <li><a  href="${hrefs[i]}">${fld_sp[i]} </a></li>
			</c:forEach>
			<c:if test="${fld_name == 'sl_jdnf' }">
			<input type = "text" style="width: 50px ; height: 40px name = "" value = "" id = "nf1"/>
			-
			<input type = "text" style="width: 50px ; height: 40px name = "" value = "" id = "nf2"/>
			<input type = "button" name = "" value = "确定" onclick = "jdnf()"/>
			</c:if>
			</ul>
		</div>
	</div>
	</div>
</div>
</c:if>