<%@tag pageEncoding="UTF-8"%>
<%@ tag import="java.util.Map"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ attribute name="fld_name" type="java.lang.String" required="true"%>
<%@ attribute name="fld_tag" type="java.lang.String" required="true"%>
<%@ attribute name="fld_sp" type="java.lang.String[]" required="true"%>
<%@ attribute name="fld_sv" type="java.lang.String[]" required="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
boolean bShow = true;
Map<String, String[]> pmap = (Map<String, String[]>)request.getAttribute("pmap");
String sp = (String)request.getAttribute("searchParams");

//服务类型以下筛选项默认隐藏，服务类型选‘专利申请’，弹出
boolean b_zlsq = false;
boolean b_f4 = false;

String[]pv_fwlx = pmap.get("sl_fwlx");
if(pv_fwlx!=null){
	if(pv_fwlx[0].equals("zlsq")){
		b_zlsq=true;
	}else if(pv_fwlx[0].equals("zlfs") || pv_fwlx[0].equals("zlwx") 
			|| pv_fwlx[0].equals("zlxzss") || pv_fwlx[0].equals("zlmsss")){
		b_f4=true;
	}
}

if(!b_zlsq && (fld_name.equals("sl_dll")|| fld_name.equals("sl_ywlx")
		|| fld_name.equals("sl_zllx")|| fld_name.equals("sl_jsly")
		|| fld_name.equals("sl_pjqlx") || fld_name.equals("sl_smsys"))){
	bShow=false;
}

//选 专利复审 专利无效 专利行政诉讼 专利民事诉讼,弹出 ‘近五年代理量’
if(b_f4 && fld_name.equals("sl_dll")){
	bShow=true;
}
//业务类型 专利类型 同时选 ，技术领域，“平均权利要求项数”，“平均说明书页数 不出现
//技术领域，“平均权利要求项数”，“平均说明书页数：” 只在点 ‘发明’ ‘实用新型’ 才弹出
if(fld_name.equals("sl_jsly") || fld_name.equals("sl_pjqlx") || fld_name.equals("sl_smsys")){
	bShow=false;
	if(b_zlsq){
		String[]pv_zllx = pmap.get("sl_zllx");
		if( pmap.get("sl_ywlx")==null && pv_zllx!=null && (pv_zllx[0].equals("1") || pv_zllx[0].equals("2"))){
			bShow=true;
		}			
	}
}


//“平均权利要求项数”，“平均说明书页数：”，“技术领域”， 三者互斥。 三者任选一,隐藏服务类型
if(pmap.get("sl_pjqlx") != null){
	if(fld_name.equals("sl_jsly") || fld_name.equals("sl_smsys") || fld_name.equals("sl_ywlx"))
		bShow=false;
}
if(pmap.get("sl_smsys") != null){
	if(fld_name.equals("sl_jsly") || fld_name.equals("sl_pjqlx") || fld_name.equals("sl_ywlx"))
		bShow=false;
}
if(pmap.get("sl_jsly") != null){
	if(fld_name.equals("sl_pjqlx") || fld_name.equals("sl_smsys") || fld_name.equals("sl_ywlx"))
		bShow=false;
}

String[] hrefs = new String[fld_sp.length];
if(bShow){
	for(int i=0; i<fld_sp.length; i++){
		String href = null;
		//除本条件之外的url
		for(String key: pmap.keySet()){
			//隐藏已选条件
			if(key.equals(fld_name)){
				bShow=false;
				continue;
			}
			if(key.equals("page")){
				continue;
			}
			String[]pv = pmap.get(key);
			if(href==null)
				href = "?"+key+"="+pv[0];
			else
				href += "&"+key+"="+pv[0];
		}
		//附加本条件
		if(href==null)
			hrefs[i]="?"+fld_name+"="+fld_sv[i];
		else
			hrefs[i]=href+"&"+fld_name+"="+fld_sv[i];
	}
}
request.setAttribute("hrefs", hrefs);
request.setAttribute("bShow", bShow);
%>
<style>
.queding{ 
 	line-height:28px;
	height:28px;
	width:50px;
	color:#777777;
	background-color:#ededed;
	font-size:16px;
	font-weight:normal;
	font-family:Microsoft YaHei;
	background:-webkit-gradient(linear, left top, left bottom, color-start(0.05, #ededed), color-stop(1, #f5f5f5));
	background:-moz-linear-gradient(top, #ededed 5%, #f5f5f5 100%);
	background:-o-linear-gradient(top, #ededed 5%, #f5f5f5 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #f5f5f5 100%);
	background:linear-gradient(to bottom, #ededed 5%, #f5f5f5 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #f5f5f5 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#f5f5f5',GradientType=0);
	border:1px solid #dcdcdc;
	-webkit-border-top-left-radius:5px;
	-moz-border-radius-topleft:5px;
	border-top-left-radius:5px;
	-webkit-border-top-right-radius:5px;
	-moz-border-radius-topright:5px;
	border-top-right-radius:5px;
	-webkit-border-bottom-left-radius:5px;
	-moz-border-radius-bottomleft:5px;
	border-bottom-left-radius:5px;
	-webkit-border-bottom-right-radius:5px;
	-moz-border-radius-bottomright:5px;
	border-bottom-right-radius:5px;
	-moz-box-shadow: inset 0px 0px 0px 0px #ffffff;
	-webkit-box-shadow: inset 0px 0px 0px 0px #ffffff;
	box-shadow: inset 0px 0px 0px 0px #ffffff;
	text-align:center;
	display:inline-block;
	text-decoration:none;
}
.queding:hover {
	background-color:#f5f5f5;
	background:-webkit-gradient(linear, left top, left bottom, color-start(0.05, #f5f5f5), color-stop(1, #ededed));
	background:-moz-linear-gradient(top, #f5f5f5 5%, #ededed 100%);
	background:-o-linear-gradient(top, #f5f5f5 5%, #ededed 100%);
	background:-ms-linear-gradient(top, #f5f5f5 5%, #ededed 100%);
	background:linear-gradient(to bottom, #f5f5f5 5%, #ededed 100%);
	background:-webkit-linear-gradient(top, #f5f5f5 5%, #ededed 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f5f5f5', endColorstr='#ededed',GradientType=0);
}
</style>
<c:if test="${bShow}">
<div class="J_selectorLine s-line">
	<div class="sl-wrap">
	<div class="sl-key"><span><strong><%=fld_tag%>：</span></strong></div>
	<div class="sl-value">
		<div class="sl-v-list">
			<ul class="J_valueList<%=fld_name%>">
			<c:forEach var="i" begin="0" end="<%=fld_sp.length%>">
			   <li><a  href="${hrefs[i]}">${fld_sp[i]} </a></li>
			</c:forEach>
			<c:if test="${fld_name == 'sl_rs' }">
			<input type = "text"  style="width: 50px ; height: 28px" name = "" value = "" id = "rs1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "rs2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "slrs()"/>
			</c:if>
			<c:if test="${fld_name == 'sl_slnx' }">
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "nx1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "nx2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "slnx()"/>
			</c:if>
			<c:if test="${fld_name == 'sl_pjqlx' }">
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "qlx1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "qlx2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "qlx()"/>
			</c:if>
			<c:if test="${fld_name == 'sl_smsys' }">
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "sms1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "sms2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "sms()"/>
			</c:if>
			<c:if test="${fld_name == 'sl_dll' }">
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "dll1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "dll2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "dll()"/>
			</c:if>
			<c:if test="${fld_name == 'sl_zynx' }">
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "zynx1"/>
			-
			<input type = "text" style="width: 50px ; height: 28px" name = "" value = "" id = "zynx2"/>
			<input type = "button" class='queding' name = "" value = "确定" onclick = "zynx()"/>
			</c:if>
			</ul>
		</div>
	</div>
	</div>
</div>
</c:if>