<%@tag pageEncoding="UTF-8"%>
<%@ tag import="java.util.Map"%> 
<%@ tag import="java.util.HashMap"%> 
<%@ attribute name="fld_names" type="java.lang.String[]" required="true"%>
<%@ attribute name="fld_tags" type="java.lang.String[]" required="true"%>
<%@ attribute name="fvp" type="java.util.HashMap<String,String>" required="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
Map<String, String> pmap = (Map<String, String>)request.getAttribute("map");
String[] hrefs= new String[fld_names.length];
String[] sp= new String[fld_names.length];
String flag = null;
for(int i=0; i<fld_names.length; i++){
	String fld_name = fld_names[i];
	String pv = pmap.get(fld_name);
	if(pv!=null){
		//构造出除本条件之外的url
		String href=null;
		for(String key: pmap.keySet()){
			if(key.contains(fld_name))
				continue;
			String fv = pmap.get(key);
			if(href==null)
				href = "?"+key+"="+fv;
			else
				href += "&"+key+"="+fv;
		}
		if(href==null){
			href="?";
		}
		hrefs[i]=href;
		if(fld_name.contains("dy")){
			sp[i]=pmap.get("dy");
		}else if(fld_name.contains("name")){
			sp[i]=pmap.get("name");
			if(sp[i] == null || "".equals(sp[i])){
				flag = String.valueOf(i);
			}
		}else{
			sp[i]=fvp.get(fld_name+"."+pv);
			if(sp[i] == null){
				sp[i] = pmap.get(fld_name);
			}
		}
		
	}else hrefs[i]=null;
}
request.setAttribute("hrefs", hrefs);
request.setAttribute("sp", sp);
request.setAttribute("flag", flag);
//System.out.println(pmap);
%>
<c:forEach var="i" begin="0" end="<%=hrefs.length-1%>">
	<c:if test="${hrefs[i] != null}">
		
		<c:if test="${flag != i}">
			<a class="crumb-select-item" href="${hrefs[i]}" ><b>${fld_tags[i]}：</b>
			<em>
			${sp[i]}
			</em>
			<i></i></a>   
		</c:if>
    </c:if>
</c:forEach>
