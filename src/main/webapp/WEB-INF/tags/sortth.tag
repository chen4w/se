<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="fld_name" type="java.lang.String" required="true"%>
<%@ attribute name="fld_tag" type="java.lang.String" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
String cls;
String st =(String) request.getAttribute("sortType");
Boolean sa =(Boolean) request.getAttribute("sortAsc");
String sp = (String)request.getAttribute("searchParams");
String href;

if(st!=null && st.equals(fld_name)){
	if(sa){
		cls="asc";
		href="?"+sp;
	}else{
		cls="desc";
		href="?sortType="+st+"&sortAsc=true&"+sp;
	}
}else{
	cls="both";
	href="?sortType="+fld_name+"&"+sp;
}
%>
<a href="<%=href%>">
<div class="th-inner sortable <%=cls%>">
<%=fld_tag%>
</div>
</a>