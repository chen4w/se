<%@ page contentType="text/html;charset=UTF-8"%>

<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?8ae7ee3c699698739e9d88733ff5102d";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

<div style="background:#1E2128;">
	<div class="dljgcontainer" id="footer">
		<!-- 底部导航 -->
	    <div class="bottom-nav" style="padding: 20px 0;">
	        <div class="logon_footor">
	            <img src="${ctx}/static/images/logo_004.jpg" >
	        </div>        
	        <div class="link-nav" style="width: 600px; text-align: center;">          
	            <!-- links -->
			    <div class="links">
			    	<a rel="nofollow" href="${ctx}/aboutus/gywm">关于全国专利代理公共服务平台</a>|
				    <a rel="nofollow" href="${ctx}/aboutus/wzsm">网站声明</a>|
				    <a rel="nofollow" href="${ctx}/aboutus/lxwm">联系我们</a>|
					<a rel="nofollow" href="${ctx}/zxzx?jid=0">网站留言</a>|
				    <a rel="nofollow" href="http://tongji.baidu.com/web/welcome/ico?s=8ae7ee3c699698739e9d88733ff5102d" target="_blank">网站统计</a>
			    </div>
			    <!-- copyright -->
			    <div class="copyright">
			    	<p>主办单位：<a href="http://www.acpaa.cn/" target="_blank">中华全国专利代理人协会</a> | 技术支持：<a href="http://www.cnpat.com.cn/" target="_blank">中国专利信息中心</a></p>
			        <p>ICP备案编号：京ICP备12047258号-7</p>
			    </div>
	        </div>  
	        <div class="weixin-pub">          
	             <img width="100px" height="100px" src="${ctx}/static/images/100px.png" style="margin-top: 10px;">    
	       </div>   
	    </div>
	 </div>
 </div>
