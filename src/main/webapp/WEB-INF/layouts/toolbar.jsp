<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!-- 用户栏 --> 
<div style="background: #3B454D;"> 
	<div class="dljgcontainer" id="first-header">
		<ul class="fl"><li><img class="index-search-img"  style="width:24px;height:24px"  src="${ctx}/static/images/logo_000.png" />欢迎使用全国专利代理公共服务平台</li></ul>
		<ul class="fr">
		  <li><div class="dt" id="a_login" style="display:none"><a  class="dt_a"  href="${ctx}/login">登录</a></div></li>
		  <li><div class="dt" id="a_uname" style="display:none"><a  class="dt_a" href="#"></a></div></li>
		  <li class="spacer"></li>
		  <li><div class="dt"  id="a_register" style="display:none"><a class="dt_a" href="${ctx}/register">注册</a></div></li>
		  <li><div class="dt" id="a_logout" style="display:none"><a  class="dt_a" href="${ctx}/logout">退出</a></div></li>
		  <li class="spacer"></li>
		  <li><div class="dt"><a class="dt_a" href="${ctx}/grzx">用户中心</a></div></li>
		  <!-- 
		   <li class="spacer"></li>
	       <li><div class="dt"><a class="dt_a" href="#">常用工具</a></div></li>
		   <li class="spacer"></li>
		   <li><div class="dt"><a  class="dt_a" href="#">下载工具</a></div></li>
		  -->
		</ul>
	</div>
</div>

<script type="text/javascript">
   $(document).ready(function() {
	   $('#a_logout a').click(function(e){
		   $.removeCookie('uname', { path: '/' });
	   });
	   var uname = $.cookie("uname");
	   if(uname){
		   $('#a_uname').show();
		   $('#a_logout').show();
		   $('#a_uname').html(uname);
		   $('#a_register').hide();
	   }else{
		   $('#a_uname').hide();
		   $('#a_logout').hide();
		   $('#a_register').show();
		   $('#a_login').show();
	   }
	});
</script>
