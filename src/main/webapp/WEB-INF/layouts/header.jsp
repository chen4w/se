<%@ page contentType="text/html;charset=UTF-8"%>
<!-- 搜索 -->
<div style="background: #000000;" >
	<div class="dljgcontainer" id="second-header">
	    <div style="background: url(${ctx}/static/images/header_001.jpg) no-repeat center;">	    
		<div style="height:175px;overflow:hidden;">
			   <!-- logo -->
			   <div class="logo-header"><a href="${ctx}/index"><img src="${ctx}/static/images/logo_002.png" /></a></div>
			   <!-- search -->
			   <div class="search-2016">
			    	<ul class="nav nav-tabs" id="searchTab" style="border:0;">
					  <li class="active"><a href="#header-dljg">代理机构</a></li>						  
					  <li><a href="#header-dlr">代理人</a></li>
					  <li><a href="#header-ssdlr">诉讼代理人</a></li>
					  <li><a href="#header-news">资讯</a></li>			  
					</ul>
					<div class="tab-content">
					    <!-- 代理机构模块 -->
					    <div class="tab-pane active" id="header-dljg">
							<div class="form">
								<input placeholder="输入代理机构名称；比如：北京市海淀区专利代理中心"  type="text" onkeydown="javascript:if(event.keyCode==13) search('dljg');" id="dljg" class="text">
								<button onclick="search('dljg');return false;" class="button">找代理机构</button>
							</div>
							<!-- div class="hotworld-2014">推荐代理机构：海淀代理机构，朝阳代理机构，其他代理机构</div -->
						</div>
						<!-- 代理人模块 -->
					    <div class="tab-pane" id="header-dlr">
							<div class="form">
								<input placeholder="输入代理人姓名关键词" type="text" onkeydown="javascript:if(event.keyCode==13) search('dlr');" id="dlr" class="text">
								<button onclick="search('dlr');return false;" class="button"><i></i>找代理人</button>
							</div>
						</div>
						<!-- 诉讼代理人模块 -->
					    <div class="tab-pane" id="header-ssdlr">
							<div class="form">
								<input placeholder="输入诉讼代理人姓名关键词" type="text" onkeydown="javascript:if(event.keyCode==13) search('ssdlr');" id="ssdlr" class="text">
								<button onclick="search('ssdlr');return false;" class="button"><i></i>找诉讼代理人</button>
							</div>
						</div>
						<!-- 资讯模块 -->
					    <div class="tab-pane" id="header-news">
							<div class="form">
								<input placeholder="输入资讯内容关键词" type="text" onkeydown="javascript:if(event.keyCode==13) search('news');" id="news" class="text">
								<button onclick="search('news');return false;" class="button"><i></i>找资讯</button>
							</div>
						</div>
					</div>
			   </div>
		</div>
		<!-- menu -->
		<div id="menuTab" style="height:50px;">
		     <ul class="nav nav-tabs" style="border:0;">
			  <li><a href="${ctx}/index">首页</a></li>
			  <li><a href="${ctx}/dljg">专利代理机构</a></li>
			  <li><a href="${ctx}/dlr">专利代理人</a></li>
			  <li><a href="${ctx}/lawsuitsdlr">诉讼代理人</a></li>
			  <li><a href="${ctx}/invalidreview">复审/无效决定</a></li>
			  <!-- 0426隐藏内容
			  <li><a href="#">复审/无效决定</a></li>
			  <li><a href="#">诉讼裁决</a></li -->
			</ul>
	   </div>
	   </div>
	</div>
</div>	
<script>
$(document).ready(function() {
	//显示被选中的导航菜单
	var dlrzg ='${dlrzg}';
	var url = location.href;
	if(url.indexOf("/dljg") !=-1){
		$("#menuTab>ul>li").eq(1).attr("class","active");		
	}else if(url.indexOf("/dlr") !=-1 && url.indexOf("/dlr/detail") ==-1){
		$("#menuTab>ul>li").eq(2).attr("class","active");
	}else if(url.indexOf("/lawsuitsdlr") !=-1){
		$("#menuTab>ul>li").eq(3).attr("class","active");
	}else if(url.indexOf("/dlr/detail") !=-1 && dlrzg.indexOf("专利代理人") !=-1){ 
		$("#menuTab>ul>li").eq(2).attr("class","active");
	}else if(url.indexOf("/dlr/detail") !=-1 && dlrzg.indexOf("专利代理人") ==-1){ 
		$("#menuTab>ul>li").eq(3).attr("class","active");
	}
	
	//实现tab分页
	$('#searchTab a').click(function(e) {
        e.preventDefault();//阻止a链接的跳转行为
        $(this).tab('show');//显示当前选中的链接及关联的content
    });
});

//检索按钮事件
function search(id){
	var name = $("#"+id).val();//#gotopagenum是文本框的id属性
    if (name != null) {
    	name = $.trim(name);
    	if(id == "dljg") {
    		if (name != "")
    			location.href = "${ctx}/dljg?name="+name;//代理机构检索页
    		else
    			location.href = "${ctx}/dljg";
    	}
    	else if(id == "dlr") {
    		if (name != "")
    			location.href = "${ctx}/dlr?name="+name;//代理人检索页
    		else
    			location.href = "${ctx}/dlr";
    	}else if(id == "ssdlr") {
    		if (name != "")
    			location.href = "${ctx}/lawsuitsdlr?name="+name;//诉讼代理人检索页
    		else
    			location.href = "${ctx}/lawsuitsdlr";
    	}else if(id == "news") {
    		if (name != "")
    			location.href = "${ctx}/news?name="+name;//资讯检索页
    		else
    			location.href = "${ctx}/news";
    	}
    }
}
</script>