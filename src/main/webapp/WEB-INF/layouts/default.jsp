<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
<head>
<title>全国专利代理公共服务平台:<sitemesh:title/></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<link type="image/x-icon" href="${ctx}/static/images/favicon.ico" rel="shortcut icon">
<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />

<link href="${ctx}/static/jquery-validation/1.11.1/validate.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/static/styles/default.css" type="text/css" rel="stylesheet" />
<script src="${ctx}/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery/jquery.placeholder.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>


<script src="${ctx}/static/jquery-validation/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery-validation/1.11.1/messages_bs_zh.js" type="text/javascript"></script>


<sitemesh:head/>
</head>

<body>
<%@ include file="/WEB-INF/layouts/toolbar.jsp"%>
<%@ include file="/WEB-INF/layouts/header.jsp"%>
<div class="dljgcontainer" id="dljgcontainer">
	<div id="content">
		<sitemesh:body/>
	</div>
</div>
<%@ include file="/WEB-INF/layouts/footer.jsp"%>
</body>
<script>
$(document).ready(function() {
	$('input, textarea').placeholder();
	
	//阻止页面内容高度不够，footer不能置底的问题
	var toolbar_height = $("#first-header").height();
	var header_height = $("#second-header").height();
	var footer_height = $("#footer").height();
	var contentHeight = document.documentElement.clientHeight - toolbar_height - header_height - footer_height+"px";
	$("#dljgcontainer").css("min-height",contentHeight);
	
	// 将资讯内容--分享到微信
	$(".share-weixin").qrcode({
		size: 140,
		quiet: 1,
		radius: 0.2,
		mode: 4,
	    text: location.href,
	    image: $('#wximg-buffer')[0]
	});
	
	// 分享微信好友
	$('#shareWx').hover(
	  function () {
	    $(".share-weixin").show();
	  },
	  function () {
	    $(".share-weixin").hide();
	  }			
    );
});
</script>
</html>
