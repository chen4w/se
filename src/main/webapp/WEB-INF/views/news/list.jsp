<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<title>资讯</title>
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-table.css" type="text/css" rel="stylesheet" />
</head>
<body>
<!-- div class="bigtitle">资讯</div -->

<!-- 资讯栏目 -->
<div id="lm-nav">
     <ul class="nav nav-tabs" style="border:0;">
     <c:choose>
		<c:when test="${not empty name}">				
	      <li><a href="${ctx}/news?lm=专利知识介绍&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_01.png" />专利知识介绍<span style='color:#e4393c'>(${zlzsjs})</span></a></li>
		  <li><a href="${ctx}/news?lm=专利委托辅导&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_02.png" />专利委托辅导<span style='color:#e4393c'>(${zlwtfd})</span></a></li>
		  <li><a href="${ctx}/news?lm=专利办理流程&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_03.png" />专利办理流程<span style='color:#e4393c'>(${zlbllc})</span></a></li>
		  <li><a href="${ctx}/news?lm=公益服务&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_04.png" />公益服务<span style='color:#e4393c'>(${gyfw})</span></a></li>
		  <li><a href="${ctx}/news?lm=海外维权&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_05.png" />海外维权<span style='color:#e4393c'>(${hwwq})</span></a></li>
		  <li><a href="${ctx}/news?lm=研究报告&name=${name}"><img class="lm-img" src="${ctx}/static/images/news_06.png" />研究报告<span style='color:#e4393c'>(${yjbg})</span></a></li>	
	    </c:when>
	    <c:otherwise>
	      <li><a href="${ctx}/news?lm=专利知识介绍"><img class="lm-img" src="${ctx}/static/images/news_01.png" />专利知识介绍</a></li>
		  <li><a href="${ctx}/news?lm=专利委托辅导"><img class="lm-img" src="${ctx}/static/images/news_02.png" />专利委托辅导</a></li>
		  <li><a href="${ctx}/news?lm=专利办理流程"><img class="lm-img" src="${ctx}/static/images/news_03.png" />专利办理流程</a></li>
		  <li><a href="${ctx}/news?lm=公益服务"><img class="lm-img" src="${ctx}/static/images/news_04.png" />公益服务</a></li>
		  <li><a href="${ctx}/news?lm=海外维权"><img class="lm-img" src="${ctx}/static/images/news_05.png" />海外维权</a></li>
		  <li><a href="${ctx}/news?lm=研究报告"><img class="lm-img" src="${ctx}/static/images/news_06.png" />研究报告</a></li>	
	    </c:otherwise>
	  </c:choose>
	 </ul>
</div>
<nav class="navbar navbar-default" id = nav> <!-- 加 ID = nav 控制显隐性 -->
	<div class="navbar-header" style="margin-top:10px;">
	    <div class="crumbs-nav-main clearfix">
			<div class="crumbs-nav-item" >
				<div class="crumbs-first"><a href="${ctx}/news?lm=${lm}" style="font-family: '微软雅黑 Bold','微软雅黑';">全部结果</a></div>
			</div>							
			<i class="crumbs-arrow">&gt;</i>
			<c:if test="${not empty name}">
			<a class="crumb-select-item" href="${ctx}/news?lm=${lm}"><b>资讯内容：</b><em>${name}</em><i></i></a>
			</c:if>	
        </div>
		</div>			
		<div class="navbar-header pull-right">
			<form class="navbar-form navbar-right" role="search" action="#">
			<div class="input-group">
				 <input placeholder="资讯内容" type="text" name="name" class="form-control" id="ipt_name" value="${name}" > 
				  <span class="input-group-btn">
					<button type="submit" class="btn btn-default" id="search_btn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
			</div><!-- /input-group -->
		    </form>
		 </div>   
 </nav>
<!-- 栏目内容列表 -->
<div id="lm-list">
    <!-- 左侧图片区块 --> 
    <!-- 
         <div class="lm-list-pic" >
		 <div id="slidershow" class="carousel slide" data-ride="carousel" style="width:295px;">
			 <div class="carousel-inner">
			     <div class="item active">
				     <img style="height: 365px;width: 295px;" src="${ctx}/static/images/news_001.jpg" />								  
				 </div>
				 <div class="item">
				     <img style="height: 365px;width: 295px;" src="${ctx}/static/images/news_002.jpg" />								  
				 </div>
				 <div class="item">
				     <img style="height: 365px;width: 295px;" src="${ctx}/static/images/news_003.jpg" />								  
				 </div>		
				 <div class="item">
				     <img style="height: 365px;width: 295px;" src="${ctx}/static/images/news_004.jpg" />								  
				 </div>						
		         <a class="left carousel-control" href="#slidershow" role="button" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left"></span>
				 </a>
				 <a class="right carousel-control" href="#slidershow" role="button" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right"></span>
				 </a>	
			 </div>						  					    				 						
		</div>
	</div>
	 -->
	<!-- 右侧栏目列表区块 -->				
	<div class="lm-list-cnt">
		<div class="lm-list-cnt-stitle" style="font-size: 28px;"><p><span>${lm}</span></p></div>    
		<div class="lm-list-cnt-line"><img src="${ctx}/static/images/div_line.png" width=100%/></div>
		
		<!-- div class="lm-list-cnt-stitle"><p><span>改变世界，不仅仅需要挑战的勇气，还需要你的鼓励！</span></p></div>
		<div class="lm-list-cnt-xtitle"><p><span>2015开始，穿戴设备、智能驾驶、清洁能源、新材料……迎来飞跃式的发展。</span></p></div -->
		
		<div class="lm-list-cnt-ul">
			<table class="table table-bordered  table-striped table-hover" > 
			 <c:forEach items="${objList}" var="obj" varStatus="status">	
			     <c:choose>
					<c:when test="${empty obj[2]}">				
				       <tr><td>${status.index+1}</td><td><a href="${ctx}/article/${obj[0]}">${obj[1]}</a></td><td>${obj[3]}</td></tr>
				    </c:when>
				    <c:otherwise>
				      <tr><td>${status.index+1}</td><td><a href="${obj[2]}" target=_blank >${obj[1]}</a></td><td>${obj[3]}</td></tr>
				    </c:otherwise>
				  </c:choose>
		     </c:forEach>
		    </table>
		</div>
    </div>
<div>
</div>
</div>
<script>
	$(document).ready(function() {       
		//检索名称带其他条件		
		$("#search_btn").click(function(e){
			e.preventDefault();
			window.location.href=getActionURL($("#ipt_name").val());
		});
	});
	function getActionURL(val) {
		//c4w
		var lhref = location.href;
		var pos1 = lhref.indexOf('name='),pos2;
		var hf1,hf2,pid;
		if(pos1!=-1){
			var dy;
			pos2 = lhref.indexOf('&',pos1);
			if(pos2==-1){
				pos2 = lhref.lastIndexOf('#');
			}
			if(pos2==-1){
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = '';
				dy = lhref.substring(pos1+3);
			}else{
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = lhref.substring(pos2);
				dy =  lhref.substring(pos1+3,pos2);
			}
			dy = decodeURI(dy);
		}else{
			if(location.search && location.search!='')
				hf1=lhref+'&name=';
			else
				hf1=lhref+'?name=';
			pos2 = lhref.lastIndexOf('#');
			if(pos2==-1){
				hf2 = '';
			}else{
				hf2 = lhref.substring(pos2);
			}
		}
		//c4w end
		return hf1+val+hf2;
	};
	function openUrl(url){
		location.href = url;
	}
</script>
</body>
</html>
