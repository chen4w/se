<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>个人资料修改</title>
		<script type="text/javascript">

		$(document).ready(function() {
			
		   
			
			
			//为inputForm注册validate函数
			$("#inputForm").validate();
			jQuery.validator.addMethod("isMobile", function(value, element) {       
			var length = value.length;   
			 var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;   
			return this.optional(element) || (length == 11 && mobile.test(value));     
			}, "手机号码格式不正确");   
		});
		   function oncheck(){
		    		
	                var orgPassword=$("#orgPassword").val();
	               
	                $.ajax({
	                	type:"post",
	                    url:"${ctx}/updateinfo/checkPassword",
	                    data:{orgPassword:orgPassword},                 
	                    success:function(e){
	                    	var reg=e.toString();
	                        if(reg=="true"){                            
	                        	$("#err").html("");
	                        } 
	                        else{                                 
	                        	$("#err").html("<font color=\"red\">！原始密码匹配失败</font>");
	                        }
	                    }                 
	                });
	                } 

		   
		function updatetext(id){
		
		var cond;
		var id=document.getElementById(id).id;				
		var text=document.getElementById(id);
		var val=text.innerHTML;
		
		if(id=="email"){
			cond="email";
		}
		if(id=="adress"){
			cond="";
		}
		if(id=="phone"){
			cond="isMobile";
		}
		text.innerHTML="<input type='text' id='"+id+"' class='input-large required "+cond+"' name='"+id+"' value="+val+" />"; 
		}
		
		function updatepass(){
			document.getElementById("passw0").style.display="none";
			document.getElementById("passw1").style.display="";
			document.getElementById("passw2").style.display="";
			document.getElementById("passw3").style.display="";
			
			}
		
	</script>
	
</head>

<body>


	<form  id="inputForm" action="${ctx}/updateinfo" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="${user.id}"/>
		<fieldset >
			<legend><small>个人资料修改</small></legend>
			<div class="control-group">
				<label for="loginName" class="control-label">用户名:</label>
				
				<span id="loginName" name="loginName" >${user.loginName}</span>
					
				
			</div>
			<div class="control-group" >
				<label for="name" class="control-label">真实姓名:</label>
				
					<span  id="name" name="name" />${user.name}</span>
				
			</div>
			<div class="control-group" >
				<label for="sex" class="control-label">性别:</label>
			<span id="sex" name="sex"><c:if test="${user.sex=='1'}">男</c:if> 
             <c:if test="${user.sex=='2'}">女</c:if></span>

			</div>
			<div class="control-group" >
				<label for="birthday" class="control-label">出生日期:</label>		
					<span class="help-inline" id="birthday" name="birthday" ><fmt:formatDate value="${user.birthday}" pattern="yyyy-MM-dd" /></span>

			</div>
			<div class="control-group" id="passw0">
				<label for="password" class="control-label">密码:</label>
				<div class="controls">
					<input type="password" id="password" name="password"  value="${user.password}" class="input-large" /><a href="#" onclick="updatepass()" >&nbsp;&nbsp;<font color='red'>点击修改</font></a>
				</div>
			</div>
			<div class="control-group"  style="display: none" id="passw1">
				<label for="orgPassword" class="control-label" >原始密码:</label>
				<div class="controls">
					<input  type="password" id="orgPassword" name="orgPassword"  class="input-large required" onblur="oncheck()" /><span id="err"></span>
				</div>
			</div>
			
			<div class="control-group"  style="display: none" id="passw2">
				<label for="plainPassword" class="control-label">新密码:</label>
				<div class="controls">
					<input type="password" id="plainPassword" name="plainPassword" class="input-large required" placeholder="...Leave it blank if no change"/>
				</div>
			</div>
			<div class="control-group"  style="display: none" id="passw3">
				<label for="confirmPassword" class="control-label">确认密码:</label>
				<div class="controls">
					<input type="password" id="confirmPassword" name="confirmPassword" class="input-large required" equalTo="#plainPassword" />
				</div>
			</div>
			<div class="control-group" >
				<label for="email" class="control-label ">邮箱/Email:</label>
				
					<span id="email">${user.email}</span><a  href="#" onclick="updatetext('email')">&nbsp;&nbsp;<font color='red'>点击修改</font></a>
				
			</div>
			<div class="control-group" >
				<label for="adress" class="control-label">联系地址:</label>
				
					<span id="adress">${user.adress}</span><a href="#" onclick="updatetext('adress')">&nbsp;&nbsp;<font color='red'>点击修改</font></a>
				
			</div>
			<div class="control-group" >
				<label for="phone" class="control-label">联系电话:</label>
				
					<span id="phone">${user.phone}</span><a href="#" onclick="updatetext('phone')" >&nbsp;&nbsp;<font color='red'>点击修改</font></a>
				
			</div>
			<div class="form-actions" >
				<input id="submit_btn" class="btn btn-primary" type="submit" value="修改"/>&nbsp;	
				<input id="cancel_btn" class="btn" type="button" value="取消" onclick="history.back()"/>
			</div>
		</fieldset>
	</form>
	

</body>
</html>
