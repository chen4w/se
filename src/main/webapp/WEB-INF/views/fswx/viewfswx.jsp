<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <title>查看复审/无效详情</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="${ctx}/static/flexpaper/jquery.js"></script>
		<script type="text/javascript" src="${ctx}/static/flexpaper/swfobject.js"></script>  
		<script type="text/javascript" src="${ctx}/static/flexpaper/flexpaper_flash.js"></script>
		<script type="text/javascript" src="${ctx}/static/flexpaper/flexpaper_flash_debug.js"></script>
		<style type="text/css" media="screen">
html,body {
	height: 100%;
}

body {
	margin: 0;
	padding: 0;
	overflow: auto;
}

#flashContent {
	display: none;
}
</style>
		<title>文档在线预览系统</title>
	</head> 
	<body>
	<div style="margin:0 auto;width:1024px">
	<script type="text/javascript">  
    var swfVersionStr = "9.0.124";  
    var xiSwfUrlStr = "";  
 
    var flashvars = {  
        SwfFile :encodeURIComponent('/fswx/getswf/${decisionNo}/${fl}'),
        Scale: 1,  
        ZoomTransition: "linear",  
        ZoomTime: "greater",  
        ZoomInterval: 0.1,  
        FitPageOnLoad: false,  
        FitWidthOnLoad: false,  
        PrintEnabled: true,  
        FullScreenAsMaxWindow: false,  
        ProgressiveLoading: false,  
       
        PrintToolsVisible: true,  
        ViewModeToolsVisible: true,  
        ZoomToolsVisible: true,  
        FullScreenVisible: true,  
        NavToolsVisible: true,  
        CursorToolsVisible: true,  
        SearchToolsVisible: false,  
  
        localeChain: "zh_CN"  
    };  
    var params = {  
      
    }  
    params.quality = "high";  
    params.bgcolor = "#ffffff";  
    params.allowscriptaccess = "sameDomain";  
    params.allowfullscreen = "true";  
    var attributes = {};  
    attributes.id = "FlexPaperViewer";  
    attributes.name = "FlexPaperViewer";  
    swfobject.embedSWF(  
        "${ctx}/static/flexpaper/FlexPaperViewer.swf", "flashContent",  
        "1024", "705", 
        swfVersionStr, xiSwfUrlStr,  
        flashvars, params, attributes);  
        swfobject.createCSS("#flashContent", "display:none;text-align:left;");  
</script>  
      
<div id="flashContent">  
  </div>
    <script type="text/javascript">  
        var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://");  
        <%--document.write("<a href='http://www.adobe.com/go/getflashplayer'>"); --%>  
    </script> 
</div> 
	</body>
</html>
