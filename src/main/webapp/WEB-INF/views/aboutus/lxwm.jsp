<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>联系我们</title>
<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function () {
			if ($(window).scrollTop() - $(".col-md-3").offset().top > 20) {
				if ($(".col-md-3").find("nav").hasClass("affix-top")) {
					$(".col-md-3").find("nav").removeClass("affix-top");	
					$(".col-md-3").find("nav").addClass("affix");	
				}
			} else {
				if ($(".col-md-3").find("nav").hasClass("affix")) {
					$(".col-md-3").find("nav").removeClass("affix");
					$(".col-md-3").find("nav").addClass("affix-top");
				}
			}
		});
	});
</script>
<link href="${ctx}/static/styles/docs.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div class="container bs-docs-container">
		<div class="row" style="padding-top:10px">
			<div class="col-md-9">
				<p style="text-align:center">
				    <strong><span style="font-size:24px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">联系我们</span></strong>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>单位名称：</strong>中华全国专利代理人协会</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>地　址：</strong>北京市西城区北三环中路乙6号伦洋大厦五层507号&nbsp;<br/><strong>邮　编：</strong>100120&nbsp;</span>
				</p>
				<p>
				    <span style="font-size:16px">&nbsp;</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>服务热线：</strong></span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">5*8</span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">小时（9：00-17：00），为您提供各种平台服务咨询内容，涉及到具体业务方面的咨询您可以联系专利代理机构为您提供服务</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">&nbsp;</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>联系人：</strong>许立瑶</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Tel</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">： 86-10-58572665</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Fax</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">：86-10-58572728</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Email</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">：<a href="mailto:mail@acpaa.cn"><span style="color:black;text-underline:none">mail@acpaa.cn</span></a></span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">&nbsp;</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>联系人：</strong>白朝品</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Tel</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">： 86-10-58572724</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Fax</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">：86-10-58572728</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>Email</strong></span><span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">：<a href="mailto:mail@acpaa.net"><span style="color:black;text-underline:none">mail@acpaa.net</span></a></span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white">&nbsp;</span>
				</p>
				<p>
				    <span style="font-size: 16px;font-family: 微软雅黑, sans-serif;background: white"><strong>技术支持：</strong>Tel：86-10-61073144</span>
				</p>
			</div>
			<div class="col-md-3">
				<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top">
            	<ul class="nav bs-docs-sidenav">
            		<li>
					  <a href="${ctx}/aboutus/gywm">关于全国专利代理公共服务平台</a>
					  <ul class="nav">
					    <li class=""><a href="#gywm-E-commerce">申请入驻电商条件和资质要求</a></li>
					    <li><a href="#gywm-Foreign-rights">申请入驻海外维权机构的条件和资质</a></li>
					    <li><a href="#gywm-Public-service">提供公益服务的代理机构资质和条件</a></li>
					  </ul>
					</li>
					<li>
					  <a href="${ctx}/aboutus/wzsm">网站声明</a>
					</li>
					<li class="active">
					  <a href="${ctx}/aboutus/lxwm">联系我们</a>
					</li>
					<li>
					  <a href="${ctx}/zxzx?jid=0">网站留言</a>
					</li>
				</ul>
            	<a class="back-to-top" href="#top">返回顶部</a>
            	</nav>
			</div>
		</div>
	</div>
</body>
</html>