<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>关于我们</title>
<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function () {
			//console.log($(".col-md-3").offset().top);
			//console.log($(window).scrollTop());
			if ($(window).scrollTop() - $(".col-md-3").offset().top > 20) {
				if ($(".col-md-3").find("nav").hasClass("affix-top")) {
					$(".col-md-3").find("nav").removeClass("affix-top");	
					$(".col-md-3").find("nav").addClass("affix");	
				}
			} else {
				if ($(".col-md-3").find("nav").hasClass("affix")) {
					$(".col-md-3").find("nav").removeClass("affix");
					$(".col-md-3").find("nav").addClass("affix-top");
				}
			}
		});
	});
</script>
<style type="text/css" media="screen">
	/*a:active{text-decoration:none;}    
	a:focus{outline:none;}*/
</style>
<link href="${ctx}/static/styles/docs.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div class="container bs-docs-container">
		<div class="row" style="padding-top:10px">
			<div class="col-md-9">
				<p style="text-align:center">
				    <strong><span style="font-size:24px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">关于全国专利代理公共服务平台</span></strong>
				</p>
				<p>
				    <strong>一、</strong><strong>网站介绍</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp;&nbsp;1、名称：全国专利代理公共服务平台
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 英文名称：Public Platform for Patent Attorney Services -- PPPAS
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; 网址：<a href="http://www.pppas.net">www.pppas.net</a>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp;&nbsp;2、平台定位
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●面向创新主体：
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 依托国家知识产权局强大的专利申请、复审无效数据，法院诉讼判决数据的支撑，集成专利代理服务的供给侧信息，实现专利代理服务信息的一站式查询，搭建双创企业和代理机构之间互通的权威性平台，让双创企业能够获取充足的专利代理服务信息，行业知识介绍和行业资讯，通过服务、管理、交流等功能集成，为双创主体提供专利申请和代理服务的指引帮助和在线商务。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●面向服务机构：
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 依托国家知识产权局强大的专利申请、复审无效数据，法院诉讼判决数据的支撑，方便服务机构及时了解其专利代理的信息，促进专利代理质量提升，促使知识产权服务向专业化、职业化方向发展。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●面向主管部门：
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 辅助理顺行业管理机制，完善行业监管机制，方便科学评价与管理。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 3、平台功能
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; (1)行业资讯:国内外专利知识介绍、行业最新资讯和研究报告通过形象化、碎片化的形式展示出来，主要包括国内专利法律法规规章知识普及；办理流程；专利代理委托辅导；海外专利知识普及、海外申请、海外维权；行业研究报告、行业公益申请等。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; (2)信息展示：信息展示模块将通过多维度、多入口的检索方式，为用户提供权威的、全面的代理机构和代理人信息，为创新主体提供专利申请、专利代理服务相关内容的指引和帮助<a></a>。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; (3)电子商务:由中华全国专利代理人协会主管、北京源创云网络科技有限公司开发的电子商务，为您提供安全的服务交易。创意存证、IP管理等功能的开发为为您提供更便捷、专业、优质的服务，为您的创新保驾护航。
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 4、专利代理机构统计指标的运算规则和统计方法
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; <span>待定。</span><br/>
				</p>
				<p>
				    <strong>二、全国专利代理公共服务平台顾问委员会</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp;&nbsp; 委员会主任：
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 委员：
				</p>
				<p>
				    <strong>三、全国专利代理公共服务平台公益法律顾问团队</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 北京市永新智财律师事务所&nbsp;&nbsp;&nbsp;&nbsp; 邵伟律师&nbsp; 穆豪亮律师
				</p>
				<p>
				    <strong>四、加入与合作</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●国内服务机构
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; <a id="gywm-E-commerce" href="#">1、申请入驻电商条件和资质要求</a>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 2、申请入驻流程
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 3、入驻须知（承诺书）
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 4、入驻申请材料下载
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●海外维权海外机构咨询库
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; <a id="gywm-Foreign-rights" href="#">1、申请入驻条件和资质</a>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 2、申请入驻流程
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 3、入驻申请材料下载
				</p>
				<p>
				    <strong>五、公益申请条件</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●国内服务机构
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; <a id="gywm-Public-service" href="#">1、提供公益服务的代理机构资质和条件</a>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 2、申请流程
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 3、申请材料下载
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; ●国内创新主体
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 1、申请流程
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 2、申请材料下载
				</p>
				<p>
				    <strong>六、鸣谢</strong>
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 北京知产宝网络科技发展有限公司
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 北京源创云网络科技有限公司
				</p>
				<p>
				    &nbsp;&nbsp;&nbsp; 北京市永新智财律师事务所
				</p>
				<p>
				    <br/>
				</p>
			</div>
			<div class="col-md-3">
				<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top">
            	<ul class="nav bs-docs-sidenav">
            		<li class="active">
					  <a href="${ctx}/aboutus/gywm">关于全国专利代理公共服务平台</a>
					  <ul class="nav">
					    <li class=""><a href="#gywm-E-commerce">申请入驻电商条件和资质要求</a></li>
					    <li><a href="#gywm-Foreign-rights">申请入驻海外维权机构的条件和资质</a></li>
					    <li><a href="#gywm-Public-service">提供公益服务的代理机构资质和条件</a></li>
					  </ul>
					</li>
					<li>
					  <a href="${ctx}/aboutus/wzsm">网站声明</a>
					</li>
					<li>
					  <a href="${ctx}/aboutus/lxwm">联系我们</a>
					</li>
					<li>
					  <a href="${ctx}/zxzx?jid=0">网站留言</a>
					</li>
				</ul>
            	<a class="back-to-top" href="#top">返回顶部</a>
            	</nav>
			</div>
		</div>
	</div>
</body>
</html>