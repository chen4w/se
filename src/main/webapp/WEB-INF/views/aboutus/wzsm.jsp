<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>网站声明</title>
<script type="text/javascript">
	$(document).ready(function() {
		$(window).scroll(function () {
			if ($(window).scrollTop() - $(".col-md-3").offset().top > 20) {
				if ($(".col-md-3").find("nav").hasClass("affix-top")) {
					$(".col-md-3").find("nav").removeClass("affix-top");	
					$(".col-md-3").find("nav").addClass("affix");	
				}
			} else {
				if ($(".col-md-3").find("nav").hasClass("affix")) {
					$(".col-md-3").find("nav").removeClass("affix");
					$(".col-md-3").find("nav").addClass("affix-top");
				}
			}
		});
	});
</script>
<style type="text/css" media="screen">
/*body {
	margin: 0;
	padding: 0;
	overflow: auto;
}*/
</style>
<link href="${ctx}/static/styles/docs.min.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<div class="container bs-docs-container">
		<div class="row" style="padding-top:10px;">
			<div class="col-md-9">
				<p style="text-align:center">
				    <strong><span style="font-size:24px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">网站声明</span></strong>
				</p>
				<p style="text-indent: 128px;line-height: 30px;background: white">
				    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">&nbsp;</span>
				</p>
				<p style="text-indent: 32px;line-height: 28px;background: white">
				    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本平台所载的所有信息和数据仅供参考。本平台所载的观点和判断仅代表本平台对数据的分析，并不构成任何商业或者法律上的建议或实际结果，不能保证其中的观点和判断不会发生任何调整或变更。</span>
				</p>
				<p style="text-indent: 32px;line-height: 28px;background: white">
				    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本平台不对本平台提供的信息、数据、观点和判断的准确性、充足性或完整性做出任何保证，也不对相关资料的任何错误或遗漏负任何法律责任；本平台没有授权任何人使用本平台的信息、数据、观点和判断，使用者据此发生的任何商业、法律行为与本平台无关。</span>
				</p>
				<p style="text-indent: 32px;line-height: 28px;background: white">
				    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本平台上所有指向具体代理机构网址的链接，都是平台上显示的代理机构以该代理机构的名义向本平台提供的，本平台不保证该网址内容的合法性、完整性和准确性。由于该网址中的内容给任何人造成损害的，由该内容的提供者承担责任。如果访问者发现上述网址内的内容违反《专利代理条例》等法律法规相关文件的<a name="_GoBack"></a>，可以向中华全国代理人协会协会举报。</span>
				</p>
				<p style="text-indent: 32px; line-height: 28px; background: white;">
				    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本平台电商入口链接所指向的服务平台不是本平台开发的也不是本平台运行的。请认真阅读进入该链接后的用户服务协议的内容。因该服务平台的服务给任何人造成损失的，根据该服务平台的用户服务协议确定责任的承担，本平台不承担任何责任。</span>
				</p>
			</div>
			<div class="col-md-3">
				<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top">
            	<ul class="nav bs-docs-sidenav">
            		<li>
					  <a href="${ctx}/aboutus/gywm">关于全国专利代理公共服务平台</a>
					  <ul class="nav">
					    <li class=""><a href="#gywm-E-commerce">申请入驻电商条件和资质要求</a></li>
					    <li><a href="#gywm-Foreign-rights">申请入驻海外维权机构的条件和资质</a></li>
					    <li><a href="#gywm-Public-service">提供公益服务的代理机构资质和条件</a></li>
					  </ul>
					</li>
					<li class="active">
					  <a href="${ctx}/aboutus/wzsm">网站声明</a>
					</li>
					<li>
					  <a href="${ctx}/aboutus/lxwm">联系我们</a>
					</li>
					<li>
					  <a href="${ctx}/zxzx?jid=0">网站留言</a>
					</li>
				</ul>
            	<a class="back-to-top" href="#top">返回顶部</a>
            	</nav>
			</div>
		</div>
	</div>
</body>
</html>