<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
<link href="${ctx}/static/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" rel="stylesheet" />
<script src="${ctx}/static/bootstrap-datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${ctx}/static/bootstrap-datepicker/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
<title>个人中心</title>
<style type="text/css">

.radiohori span.error {
    position: absolute;
    top: 10px;
    line-height: normal;
}

#div2 {display:none;}
#userpasswd {display:none;}
#table2 {display:none;}
#table3 {display:none;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	//个人资料--提交表单
	$("#userInfoForm").validate({
		rules:{
        sex:{
            required:true
        },
		email: {
			remote: "${ctx}/grzx/checkEmail/${user.id}"
		}
		},
		messages: {
			email: {
				remote: "邮箱已存在,请重试"
			}
		},
		errorPlacement : function(error, element) {
		    if (element.is(":radio"))
		        error.appendTo(element.parent().parent());
		    else if (element.is(":checkbox"))
		        error.appendTo(element.parent().parent());
		    else
		        error.appendTo(element.parent());
		}
	
	});
	
	//密码修改--提交表单
	$("#userpasswdForm").validate({
			debug: true,
			submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form     
                passwdSubmit();
                //form.submit();   //提交表单   
            },
			rules:{
				password:{
					remote: "${ctx}/grzx/checkPwd/${user.id}"
				},
	            confirmPassword:{
	                equalTo:"#plainPassword"
	            }                    
	        },
	        messages:{
	        	password: {
					remote: "原密码不正确,请重试"
				},
	            confirmPassword:{
	                equalTo:"两次密码输入不一致"
	            }                                    
	        }
		});	
	
	// 个人资料--生日日期
	$('#dtBirthday').datepicker({
	    format: "yyyy-mm-dd",
	    clearBtn: true,
	    language: "zh-CN",
	    autoclose: true
	});	
	
	$("#message").children().each(function(){
		var x=$(this).children(".td");
		var y=x.children(".first");
		y.click(function(){			
		  var z=x.children(".second");
		  var m=x.children(".third");
		  m.slideToggle("slow");
		  z.slideToggle("slow"); 		  		 			
		});
	});
	
	//左侧导航菜单点击事件
	$("#button1").click(function(){
		$("#button1").css("color","#333333");
		$("#button11").css("color","#15737F");
		$("#button2").css("color","#15737F");
		$("#button4").css("color","#15737F");
		
		$("#userInfo").show();
		$("#userpasswd").hide();
		$("#table2").hide();
		$("#table3").hide();
	});
	
	$("#button2").click(function(){
		$("#button2").css("color","#333333");
		$("#button1").css("color","#15737F");
		$("#button11").css("color","#15737F");
		$("#button4").css("color","#15737F");
		$("#table2").show();
		$("#userInfo").hide();
		$("#userpasswd").hide();
		$("#table3").hide();
	});
	
	$("#button4").click(function(){
		$("#button4").css("color","#333333");
		$("#button1").css("color","#15737F");
		$("#button11").css("color","#15737F");
		$("#button2").css("color","#15737F");
		$("#table3").show();
		$("#userInfo").hide();
		$("#userpasswd").hide();
		$("#table2").hide();
	});
	$("#button11").click(function(){
		$("#button11").css("color","#333333");
		$("#button1").css("color","#15737F");		
		$("#button2").css("color","#15737F");
		$("#button4").css("color","#15737F");		
		$("#userpasswd").show();
		$("#userInfo").hide();		
		$("#table2").hide();
		$("#table3").hide();
	});
	
	//add by wsx 20160420
	$("#button3").click(function(){
		var uname = $.cookie("uname");
		if(uname){
			$('#yhsm_modal').modal('show');	
		}
	});
	$("#yhsm_modal").on("hidden.bs.modal", function() {
	    //$(this).removeData("bs.modal");  
	    $("#chk-yhsm").attr('checked',false);
	    $("#btn-gotocyb").hide();
	});
	//end
	
});

//修改密码事件
function passwdSubmit(){
	var userid = $("#id").val();	
	var pwd =  $("#plainPassword").val();
	if(pwd == "")
		return;
	$.ajax({
		type : "POST",
		url : "${ctx}/grzx/passwd",
		data : { //发送给数据库的数据
			id : userid,
			pwd : pwd
		},
		dataType : 'text',
		success : function(data) {
			alert(data);
			$("#password").val("");
			$("#plainPassword").val("");
			$("#confirmPassword").val("");
		}
	});		
 }
 function findbyID(pid){
	 var pid=pid;
	 
	  $.ajax({		  
		  type:"post",
         url:"${ctx}/getQues",
         dataType:"json",
         async:false,
         data:{pid:pid},
         success:function(data){
          $("#"+pid+"").html("");         
             $.each(data,function(i,item){           	 
                 $("#"+pid+"").append("<tr><td id="+item.id+"><p>回复：</p>"+item.content+"</td></tr>");
             });
         }
	  });
 }
 //add by wsx 20160420 begin
 function loginCYB(){
	 $('#yhsm_modal').modal('hide');
	 var w = window.open('about:blank');
	 $.ajax({
     	type:"get",
         url:"${ctx}/cyb/user/",
         //contentType : 'application/json',
         dataType : 'json',
         data:{},
         success:function(e){
            if(e.status=='succ'){
            	//window.open(e.url,'_blank');
            	w.location = e.url;
            }else{
            	w.close();
            	alert(e.msg);
            	//window.open(e.url,'_blank');
            }
         },error:function(e){
        	 //alert(1);
        	 w.close();
         }                 
     });
}
//add by wsx 20160420 begin

//add by wsx 20160421 begin
function checkyhsm(obj){
	if(obj.checked){
		$("#btn-gotocyb").show();
	}else{
		$("#btn-gotocyb").hide();
	}
}
//add by wsx 20160421 end
</script>
</head>
<body>
 
<div class="bigtitle">	<img src="${ctx}/static/images/grzx_001.png" />&nbsp;个人中心</div>

<div>
<div  class="grzxinfo-left">
      <ul>
      	<li>
	      	<a id="button1">个人资料</a>
	      	<img style="width:20px;height:20px;" src="${ctx}/static/images/grzx_002.png" />
	      	<div>
	      	<img  src="${ctx}/static/images/grzx_line.png" />
	      	</div>	
	      	<!-- <hr style="height:1px;border:none;border-top:1px dashed #0066CC;margin-top: 2px;" /> -->
      	</li>
      	<li>
	      	<a id="button11">密码修改</a>
	      	<img style="width:20px;height:20px;" src="${ctx}/static/images/grzx_002.png" />
	      	<div>
	      	<img  src="${ctx}/static/images/grzx_line.png" />
	      	</div>	
      	</li>
      	<li>
	      	<a id="button2">我的收藏</a>
	      	<img style="width:20px;height:20px;" src="${ctx}/static/images/grzx_003.png" />	
	      	<div>
	      	<img  src="${ctx}/static/images/grzx_line.png" />
	      	</div>
      	</li>
      	<li>
	      	<a id="button3">我的订单</a>
	      	<img style="width:20px;height:20px;" src="${ctx}/static/images/grzx_004.png" />	
	      	<div>
	      	<img  src="${ctx}/static/images/grzx_line.png" />
	      	</div>
      	</li>
      	<li>
      	 <a id="button4">留言</a>
      	 <img style="width:20px;height:20px;" src="${ctx}/static/images/grzx_005.png" />	
      	 <div>
      	<img  src="${ctx}/static/images/grzx_line.png" />
      	</div>
      	 </li>
      </ul>               
</div>

<div class="grzxinfo-right">
 
<form  id="userInfoForm" action="${ctx}/grzx/profile" method="post" class="form-horizontal" role="form">
<!-- 个人资料 -->
<input type="hidden" name="id" id="id" value="${user.id}"/>
<div id="userInfo">
   <div class="form-group">
      <label for="loginName" class="col-sm-2 control-label">用户名</label>
      <div class="col-sm-10">
         <input type="text" class="form-control" id="loginName" value="${user.loginName}" disabled>
      </div>
   </div>
   <div class="form-group">
      <label for="name" class="col-sm-2 control-label">真实姓名</label>
      <div class="col-sm-10">
         <input type="text" id="name" name="name" placeholder="请输入真实姓名" class="form-control  required" value="${user.name}" autofocus>
      </div>
   </div>
   <div class="form-group">
      <label class="col-sm-2 control-label">性别</label>
      <div class="col-sm-10 radiohori" >
      <c:choose>
			<c:when test="${empty user.sex}">
			     <label class="checkbox-inline">
			      <input type="radio" name="sex" id="man" 
			         value="男" > 男
				   </label>
				   <label class="checkbox-inline">
				      <input type="radio" name="sex" id="woman" 
				         value="女"> 女
				   </label>				   
			</c:when>
			<c:when test="${user.sex == '男'}">
			  <label class="checkbox-inline">
			      <input type="radio" name="sex" id="man" 
			         value="男"  checked> 男
			   </label>
			   <label class="checkbox-inline">
			      <input type="radio" name="sex" id="woman" 
			         value="女" > 女
			   </label>
			</c:when>
			<c:otherwise>
		        <label class="checkbox-inline">
			      <input type="radio" name="sex" id="man" 
			         value="男" > 男
			   </label>
			   <label class="checkbox-inline">
			      <input type="radio" name="sex" id="woman" 
			         value="女" checked > 女
			   </label>
		    </c:otherwise>
		</c:choose>
      </div>
   </div>
   <div class="form-group">
      <label for="dtBirthday" class="col-sm-2 control-label">出生日期</label>
      <div class="col-sm-10">
         <fmt:formatDate value='${user.dtBirthday}' pattern='yyyy-MM-dd' var="formatteddtBirthday"/>
         <input type="text" id="dtBirthday" name="dtBirthday" placeholder="请输入出生日期" class="form-control dateISO" value="${formatteddtBirthday}">
      </div>
   </div>
   <div class="form-group">
      <label for="email" class="col-sm-2 control-label">邮箱/Eamil</label>
      <div class="col-sm-10">
         <input type="text" id="email" name="email" placeholder="请输入电子邮箱" class="form-control  required email" value="${user.email}">
      </div>
   </div>
   <div class="form-group">
      <label for="adress" class="col-sm-2 control-label">联系地址</label>
      <div class="col-sm-10">
         <input type="text" id="adress" name="adress" placeholder="请输入联系地址" class="form-control  " value="${user.adress}">
      </div>
   </div>
   <div class="form-group">
      <label for="phone" class="col-sm-2 control-label">联系电话</label>
      <div class="col-sm-10">
         <input type="text" id="phone" name="phone" placeholder="请输入联系电话" class="form-control  " value="${user.phone}">
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
         <button type="submit" class="btn btn-default">提交</button>
         &nbsp;
         <button type="reset" class="btn btn-default">重置</button>
      </div>
   </div>
 </div>
</form>

<!-- 密码修改 -->
 <div id="userpasswd">
<form  id="userpasswdForm" action="${ctx}/grzx/passwd" method="post" class="form-horizontal" role="form">
<input type="hidden" name="id" id="id" value="${user.id}"/>
    <div class="form-group">
      <label for="password" class="col-sm-2 control-label">原密码</label>
      <div class="col-sm-10">
         <input type="password" id="password" name="password" placeholder="请输入原密码" class="form-control required" >
      </div>
   </div>
   <div class="form-group">
      <label for="plainPassword" class="col-sm-2 control-label">新密码</label>
      <div class="col-sm-10">
         <input type="password" id="plainPassword" name="plainPassword" placeholder="请输入新密码" class="form-control " >
      </div>
   </div>
   <div class="form-group">
      <label for="confirmPassword" class="col-sm-2 control-label">重输一次</label>
      <div class="col-sm-10">
         <input type="password" id="confirmPassword" name="confirmPassword" placeholder="请重输一次" class="form-control"  >
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
         <input type="submit" class="btn btn-default" value="提交">
          &nbsp;
         <button type="reset" class="btn btn-default">重置</button>
      </div>
      </div>
</form>
 </div>


<table id="table2" class="grzxinfo_table">
      <thead>
			<tr>
				<th>我的收藏：</th>
				<th>收藏个数${booksize}</th>
			</tr>
		</thead>
		<tbody id="book">
			<c:forEach items="${book}" var="b">
				<tr>
					<td>${b.title}</td>
				    <td>${b.url}</td>
				</tr>
			</c:forEach>
		</tbody>
		

</table>

<table  id="table3" class="grzxinfo_table">
<thead>
			<tr>
			    
				<th>留言：</th>
				<th>留言个数 ${messize}</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
			<tbody id="message">
			<c:forEach items="${question}" var="c" varStatus="status">
				<tr>
					<td>${status.index+1}</td>
				 <td class="td">
					 <div id="div1" class="first" ><a href="javaScript:void(0)" onclick="findbyID(${c.id})">${c.title}（详细内容请点击）</a></div>
					 <div id="div2" class="third"><p style="color: blue">问题详情：</p>${c.content}</div>
                     <div id="${c.id}"  class="second"><br/></div>                                      
				</td>
				<td>${c.dtCreate}</td>
				</tr>
			</c:forEach>
		</tbody>
</table>    
    </div>
</div>

<!-- 登录创意宝用户声明 add by wsx 20160421 begin -->
<div class="modal fade" id="yhsm_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">用户声明</h4>
      </div>
      <div class="modal-body">
       <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
          <p style="text-indent:24px;line-height:26px">
             <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">欢迎您（与以下使用的“用户”一词具有同一含义）进入由北京源创云网络科技有限公司开发的、由北京源创云网络科技有限公司与您点击的代理机构共同提供服务的服务平台（以下简称“本平台”），请您仔细阅读以下协议，如果同意本并接受全部条款，请您点击“同意”，您点击同意按钮后，本协议即构成对您有约束力的法律文件。</span>
          </p>
          </div>
          <div class="col-xs-12">
            <div class="checkbox"><label><input id="chk-yhsm" type="checkbox" onclick="checkyhsm(this)">我已阅读并同意<a href="/yhsm" target="_blank">查看用户声明</a></label></div>
          </div>
          <!-- div class="col-xs-12"><button id="btn-gotocyb" type="button" class="btn btn-default" style="display:none" onclick="loginCYB()">跳转</button></div-->
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-default" data-dismiss="modal">关闭</button-->
        <button id="btn-gotocyb" type="button" class="btn btn-primary" style="display:none" onclick="loginCYB()">进入</button>
        
      </div>
    </div>
  </div>
</div>
<!-- 登录创意宝用户声明 add by wsx 20160421 end -->
</body>
</html>
