<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>用户注册</title>
	
	<script type="text/javascript">
		$(document).ready(function() {
			//聚焦第一个输入框
			$("#loginName").focus();
			//为inputForm注册validate函数
			$("#inputForm").validate({
				rules: {
					loginName: {
						remote: "${ctx}/register/checkLoginName"
					},
					email: {
						remote: "${ctx}/register/checkEmail"
					}
				},
				messages: {
					loginName: {
						remote: "用户登录名已存在"
					},
					email: {
						remote: "邮箱已存在,请重试"
					}
				}
			});
		});
	</script>
	
<style type="text/css">
#content{
	background-color:#1F3448;
}
body{
	background-color:#1F3448;
}
#header,#first-header,#footer{
  /* IE 8 */
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";

  /* IE 5-7 */
  filter: alpha(opacity=50);

  /* Netscape */
  -moz-opacity: 0.5;

  /* Safari 1.x */
  -khtml-opacity: 0.5;

  /* Good browsers */
  opacity: 0.5;
}

.control-label,label{
color: #ffffff;
}

#submit_btn{
	background-color: #0c67f0;
    color: #ffffff;
}
.col-md-6 .p1{font-family: '微软雅黑 Bold', '微软雅黑';font-weight: 700;font-style: normal;font-size: 18px;}
.col-md-6 .p2{font-family: '微软雅黑 Regular', '微软雅黑';font-weight: 400;font-style: normal;font-size: 13px;}
.form-group{margin-bottom:12px;}
</style>	
	
</head>

<body>

<!-- 内容 -->
<div class="row" style="margin-top:40px;">
	<div class="col-md-6">
	<img style="margin-top: 10px;" src="${ctx}/static/images/logo_003.png" />
	
	</div>
	<div   class="col-md-4">
	<form id="inputForm" action="${ctx}/register" method="post" class="header_form-horizontal2">
		<fieldset>
			<div class="header_form-group">
				<div class="header_col-sm-3">
				<div style="color: #ffffff;float: left;">
				<p class="p1">成为注册用户享受专业服务</p>
				<p class="p2">填写下面表格中的信息成为平台的注册用户</p>
			</div>
			<div style="float: right;"><img width="40px" height="50px" src="${ctx}/static/images/header_login_02.png" /></div>
				</div>
			</div>
			<img width="400px;" src="${ctx}/static/images/header_login_line.png" />
			
			<div class="header_form-group" style="margin-top:12px;">
				<div class="header_col-sm-3">
					<input type="text" id="loginName" name="loginName" placeholder="请输入登录名" class="form-control  required" minlength="6"/>
				</div>
			</div>
			<div class="header_form-group">
				<div class="header_col-sm-3">
					<input type="text" id="email" name="email" placeholder="请输入电子邮箱" class="form-control  required email" />
				</div>
			</div>
			<div class="header_form-group">
				<div class="header_col-sm-3">
					<input type="text" id="name" name="name" placeholder="请输入真实姓名" class="form-control  required"/>
				</div>
			</div>
			<div class="header_form-group">
				<div class="header_col-sm-3">
					<input type="password" id="plainPassword" name="plainPassword" placeholder="请输入您的密码" class="form-control  required"/>
				</div>
			</div>
			<div class="header_form-group">
				<div class="header_col-sm-3">
					<input type="password" id="confirmPassword" name="confirmPassword" placeholder="请再次输入您的密码" class="form-control  required" equalTo="#plainPassword"/>
				</div>
			</div>
			
			<div class="header_form-group">
    <div class="header_col-sm-3">
		<input id="header_submit_btn" class="btn btn-default" type="submit" value="注册"/> 
    </div>
  </div> 
		</fieldset>
	</form>
	</div>
</div>
</body>
</html>
