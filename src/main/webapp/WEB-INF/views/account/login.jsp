<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>登录页</title>
<style type="text/css">
#content{
	background-color:#1F3448;
}
body{
	background-color:#1F3448;
}
#header,#first-header,#footer{
  /* IE 8 */
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";

  /* IE 5-7 */
  filter: alpha(opacity=50);

  /* Netscape */
  -moz-opacity: 0.5;

  /* Safari 1.x */
  -khtml-opacity: 0.5;

  /* Good browsers */
  opacity: 0.5;
}

.control-label,label{
color: #ffffff;
}

#submit_btn{
	background-color: #0c67f0;
    color: #ffffff;
}
.col-md-4 .p1{font-family: '微软雅黑 Bold', '微软雅黑';font-weight: 700;font-style: normal;font-size: 18px;}
.col-md-4 .p2{font-family: '微软雅黑 Regular', '微软雅黑';font-weight: 400;font-style: normal;font-size: 13px;}

</style>	
</head>

<body style="background-color:#1F3448">


<!--弹出层开始-->



<!-- 内容 -->
<div class="row" style="margin-top:40px;" >
	<div  class="col-md-1" ></div>
	<div  class="col-md-6" >
		<img style="margin-top: 0px;" src="${ctx}/static/images/logo_003.png" />
	</div>
	<div  class="col-md-4" style="margin-top: 20px;" >
	<form id="loginForm" action="${ctx}/login" method="post" class="form-horizontal">
	
		<div class="header_form-group">
			<div class="header_col-sm-3">
			<div style="color: #ffffff;float: left;">
			<p class="p1">登录网站享受个性化服务</p>
			<p class="p2">请输入您的用户名&密码登录网站</p>
		</div>
		<div style="float: right;"><img width="40px" height="50px" src="${ctx}/static/images/header_login_01.png" /></div>
			</div>
		</div>
		<img width="400px;" src="${ctx}/static/images/header_login_line.png" />
		
		<div id="login_tip" class="header_alert alert-error input-medium controls">
			
		</div>
	
		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">名称:</label>
			<div class="col-sm-10">
			<input type="text" id="username" name="username" placeholder="请输入登录名"  value="${username}" class="form-control required"/>
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">密码:</label>
			<div class="col-sm-10">
				<input type="password" id="password" name="password" placeholder="请输入您的密码" class="form-control  required"/>
			</div>
		</div>
	<div class="form-group">
	<label for="submit_btn" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
		<input id="submit_btn" class="form-control  btn btn-default" type="submit" value="登录"/> 
    </div>
  </div>
		
	<%
	String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
	if(error != null){
	%>
	<div class="alert alert-danger fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  <strong>登录失败!</strong> 请检查用户名和密码重试.
	</div>
	<%
	}
	%>
		  				
	</form>

	</div>
	<div  class="col-md-1" ></div>
</div>


	<script>
		$(document).ready(function() {
			$("#loginForm").validate();
		});
	</script>
</body>
</html>
