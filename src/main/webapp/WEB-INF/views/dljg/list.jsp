
<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ page contentType="text/html;charset=UTF-8" import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>代理机构检索</title>
	<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap-chinese-r.js" type="text/javascript"></script>
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-chinese-r.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-table.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript">
	function getActionURL(val) {
		//c4w
		var lhref = location.href;
		var pos1 = lhref.indexOf('name='),pos2;
		var hf1,hf2,pid;
		if(pos1!=-1){
			var dy;
			pos2 = lhref.indexOf('&',pos1);
			if(pos2==-1){
				pos2 = lhref.lastIndexOf('#');
			}
			if(pos2==-1){
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = '';
				dy = lhref.substring(pos1+3);
			}else{
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = lhref.substring(pos2);
				dy =  lhref.substring(pos1+3,pos2);
			}
			dy = decodeURI(dy);
		}else{
			if(location.search && location.search!='')
				hf1=lhref+'&name=';
			else
				hf1=lhref+'?name=';
			pos2 = lhref.lastIndexOf('#');
			if(pos2==-1){
				hf2 = '';
			}else{
				hf2 = lhref.substring(pos2);
			}
		}
		//c4w end
		return hf1+val+hf2;
	};
	
	function slrs(){
		var rs1 = $("#rs1").val();
		var rs2 = $("#rs2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_rs=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_rs=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_rs=" + rs1 + "-" + rs2;
		}
		
	}
	
	function slnx(){
		var rs1 = $("#nx1").val();
		var rs2 = $("#nx2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_slnx=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_slnx=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_slnx=" + rs1 + "-" + rs2;
		}
		
	}
	
	function qlx(){
		var rs1 = $("#qlx1").val();
		var rs2 = $("#qlx2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_pjqlx=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_pjqlx=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_pjqlx=" + rs1 + "-" + rs2;
		}
		
	}
	
	function sms(){
		var rs1 = $("#sms1").val();
		var rs2 = $("#sms2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_smsys=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_smsys=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_smsys=" + rs1 + "-" + rs2;
		}
		
	}
	
	function dll(){
		var rs1 = $("#dll1").val();
		var rs2 = $("#dll2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_dll=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_dll=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_dll=" + rs1 + "-" + rs2;
		}
	}
	
	$(document).ready(function(){
		//检索名称带其他条件		
		$("#search_btn").click(function(e){
			e.preventDefault();
			window.location.href=getActionURL($("#ipt_name").val());
		});
		
		var ipt = $("#address");
		ipt.height(ipt.parent().height());
		if(${divFlag} == true){
			$("#nav").show();
		}else{
			$("#nav").hide();
		}
		if(!$("#nav").is(":hidden")){
			$("body,html").animate({
				   scrollTop:$("#scrollpos").offset().top  //让body的scrollTop等于pos的top，就实现了滚动
			},0);
		}
	});
</script>
	
</head>

<body>
	<c:if test="${not empty message}">
		<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close">×</button>${message}</div>
	</c:if>

<%
String[] sp1 = new String[]{"5人以下","5-10人","10-15人","15-20人","20-40人","40人以上"};
String[] sv1 = new String[]{"5lt","5-10","10-15","15-20","20-40","40gt"};

String[] sp2 = new String[]{"5年以下","5-10年","10-15年","15-20年","20年以上"};
String[] sv2 = new String[]{"5lt","5-10","10-15","15-20","20gt"};

String[] sp3 = new String[]{"专利申请","专利复审","专利无效","专利行政诉讼","专利民事诉讼","专利权属纠纷","行政执法案件","专利检索","专利分析评议","专利预警","专利导航","专利运营","知识产权贯标"};
String[] sv3 = new String[]{"zlsq","zlfs","zlwx","zlxzss","zlmsss","zlqsjf","xzzfaj","zljs","zlfxpy","zlyj","zldh","zlyy","qyzscqgb"};

String[] sp4 = new String[]{"国内申请","海外申请","港澳台申请"};
String[] sv4 = new String[]{"1","2","3"};

String[] sp5 = new String[]{"5项以下","5-10项","10-20项","20项以上"};
String[] sv5 = new String[]{"5lt","5-10","10-20","20gt"};

String[] sp6 = new String[]{"5页以下","5-10页","10-20页","20页以上"};
String[] sv6 = new String[]{"5lt","5-10","10-20","20gt"};

String[] sp7 = new String[]{"50件以下","50-500件","500-5000件","5000件以上"};
String[] sv7 = new String[]{"50lt","50-500","500-5000","5000gt"};

String[] sp8 = new String[]{"发明","实用新型","外观设计"};
String[] sv8 = new String[]{"1","2","3"};

String[] sp9 = new String[]{"机械","电子通讯","化学·生物·医药"};
String[] sv9 = new String[]{"1","2","3"};

String[] sp10 = new String[]{};
String[] sv10 = new String[]{};

String[] sp11 = new String[]{};
String[] sv11 = new String[]{};

String[] sl_tags= new String[]{"代理人人数","成立年限","服务类型","业务类型","平均权利要求项数","平均说明书页数","近五年代理量","专利类型","技术领域" ,"地区","机构名称" };
String[] sl_names=new String[]{"sl_rs","sl_slnx","sl_fwlx","sl_ywlx","sl_pjqlx","sl_smsys","sl_dll" ,"sl_zllx" ,"sl_jsly","dy","name" };

String[][] sp = new String[][]{sp1,sp2,sp3,sp4,sp5,sp6,sp7,sp8,sp9,sp10,sp11};
String[][] sv = new String[][]{sv1,sv2,sv3,sv4,sv5,sv6,sv7,sv8,sv9,sv10,sv11};

HashMap<String,String> fvp = new HashMap<String,String>();
for(int i=0; i<sl_names.length; i++){
	String[] v = sv[i];
	String[] p = sp[i];
	for(int j=0; j<v.length; j++){
		fvp.put(sl_names[i]+"."+v[j],p[j]);
	}
}
%>
<div id="scrollpos"></div>            
<nav class="navbar navbar-default" id = nav> <!-- 加 ID = nav 控制显隐性 -->
	<div class="navbar-header" style="margin-top:10px;">
	<div class="crumbs-nav-main clearfix">
			<div class="crumbs-nav-item">
				<div class="crumbs-first"><a href="${ctx}/dljg" style="font-family: '微软雅黑 Bold','微软雅黑';">全部结果</a></div>
			</div>
			<i class="crumbs-arrow">&gt;</i>
			<tags:slcrumb fld_tags="<%=sl_tags%>" fld_names="<%=sl_names%>" fvp="<%=fvp%>"/>
			</div>
		</div>
	
		<div class="navbar-header pull-right">
			<form class="navbar-form navbar-right" role="search" action="#">
			<div class="input-group">
				 <input placeholder="机构名称" type="text" name="name" class="form-control" id="ipt_name" value="<c:out value="${sname}"/>"> 
				  <span class="input-group-btn">
					<button type="submit" class="btn btn-default" id="search_btn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
			</div><!-- /input-group -->
		    </form>
		 </div>   
	</div>
 </nav>
            
<div id="J_selector" class="selector">


<div class="J_selectorLine s-category">
	<div class="sl-wrap">
		<div class="sl-key"><strong>地区：</strong></div>
		<div class="sl-value" style="overflow:visible;margin-left:140px;">
	<div class="bs-chinese-region flat dropdown" data-min-level="1" data-max-level="2" data-def-val="[name=address]">
		<input type="text"  id="address" placeholder="选择你的地区" data-toggle="dropdown" readonly="">
		<div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<div>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#province" data-next="city" role="tab" data-toggle="tab">
							省份
						</a>
					</li>
					<li role="presentation">
						<a href="#city" data-next="district" role="tab" data-toggle="tab">
							城市
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="province">
						--
					</div>
					<div role="tabpanel" class="tab-pane" id="city">
						--
					</div>

				</div>
			</div>
		</div>
	</div>

		</div>
	</div>
</div>

<tags:sl fld_tag="<%=sl_tags[0]%>" fld_name="<%=sl_names[0]%>" fld_sp="<%=sp1%>"  fld_sv="<%=sv1%>"/>
<tags:sl fld_tag="<%=sl_tags[1]%>" fld_name="<%=sl_names[1]%>" fld_sp="<%=sp2%>"  fld_sv="<%=sv2%>"/>
<tags:sl fld_tag="<%=sl_tags[2]%>" fld_name="<%=sl_names[2]%>" fld_sp="<%=sp3%>"  fld_sv="<%=sv3%>"/>
<tags:sl fld_tag="<%=sl_tags[6]%>" fld_name="<%=sl_names[6]%>" fld_sp="<%=sp7%>"  fld_sv="<%=sv7%>"/>
<tags:sl fld_tag="<%=sl_tags[3]%>" fld_name="<%=sl_names[3]%>" fld_sp="<%=sp4%>"  fld_sv="<%=sv4%>"/>
<tags:sl fld_tag="<%=sl_tags[7]%>" fld_name="<%=sl_names[7]%>" fld_sp="<%=sp8%>"  fld_sv="<%=sv8%>"/>
<tags:sl fld_tag="<%=sl_tags[8]%>" fld_name="<%=sl_names[8]%>" fld_sp="<%=sp9%>"  fld_sv="<%=sv9%>"/>
<tags:sl fld_tag="<%=sl_tags[4]%>" fld_name="<%=sl_names[4]%>" fld_sp="<%=sp5%>"  fld_sv="<%=sv5%>"/>
<tags:sl fld_tag="<%=sl_tags[5]%>" fld_name="<%=sl_names[5]%>" fld_sp="<%=sp6%>"  fld_sv="<%=sv6%>"/>

</div>



  	
	<table id="contentTable" class="table table-striped table-bordered table-condensed" style="margin-top:10px;">
		<thead><tr><th><tags:sortth fld_tag="机构代码" fld_name="a.jgdm"/></th>
		<th><tags:sortth fld_tag="机构名称" fld_name="a.mc"/></th>
		<c:choose>
			<c:when test="${empty sl_fwlx or sl_fwlx == 'zlsq'}">
				<th><tags:sortth fld_tag="申请代理量" fld_name="b.sl"/></th>
			</c:when>
			<c:when test="${sl_fwlx == 'zlfs'}">
				<th><tags:sortth fld_tag="复审代理量" fld_name="b.sl"/></th>
			</c:when>
			<c:when test="${sl_fwlx == 'zlwx'}">
				<th><tags:sortth fld_tag="无效代理量" fld_name="b.sl"/></th>
			</c:when>
			<c:when test="${sl_fwlx == 'zlxzss'}">
				<th><tags:sortth fld_tag="行政诉讼代理量" fld_name="b.sl"/></th>
			</c:when>
			<c:when test="${sl_fwlx == 'zlmsss'}">
				<th><tags:sortth fld_tag="民事诉讼代理量" fld_name="b.sl"/></th>
			</c:when>
			<c:otherwise>
				
			</c:otherwise>
		</c:choose>
		<th><tags:sortth fld_tag="成立年限" fld_name="a.dt_slsj"/></th>
		<th><tags:sortth fld_tag="代理人人数" fld_name="a.dlrrs"/></th>
		<th><tags:sortth fld_tag="地域" fld_name="a.dy"/></th>
		</tr></thead>
		<tbody>
		<c:forEach items="${objs.content}" var="obj">
			<tr>
				<td>${obj.jgdm}</td>
				<td><a href="${ctx}/dljg/detail/${obj.id}">${obj.mc}</a></td>
				
				<c:choose>
					<c:when test="${empty sl_fwlx or sl_fwlx == 'zlsq'}">
						<td action="patentstar">
							<c:if test="${empty obj.sl }">
							0
							</c:if>
							<c:if test="${not empty obj.sl }">
								<a href="#">${obj.sl }</a>
							</c:if>
						</td>
					</c:when>
					<c:when test="${sl_fwlx == 'zlfs' or sl_fwlx == 'zlwx' or sl_fwlx == 'zlxzss' or sl_fwlx == 'zlmsss'}">
						<td>
							<c:if test="${empty obj.sl }">
							0
							</c:if>
							<c:if test="${not empty obj.sl }">
							${obj.sl }
							</c:if>
						</td>
					</c:when>
				</c:choose>
				
				<td>${obj.date}</td>
				<td>${obj.dlrrs}</td>
				<td>${obj.dy}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<tags:pagination page="${objs}" paginationSize="5"/>
<script type="text/javascript">

$(function() {
	$.getJSON('/static/json/sql_areas.json',function(data) {

		for (var i = 0; i < data.length; i++) {
			var area = {
				id: data[i].id,
				name: data[i].cname,
				level: data[i].level,
				parentId: data[i].upid
			};
			data[i] = area;
		}

		$('.bs-chinese-region').chineseRegion('source', data).on('completed.bs.chinese-region',
		function(e, areas) {
			//$(this).find('[name=address]').val(areas[areas.length - 1].id);
		});
	});
	
	// add by zhangqiuyi 20160525 begin
	$("#contentTable tr td[action='patentstar'] a").click(function() {
		//console.log($(this).parent("td").parent("tr").find("td:first").text());
		var ad = "20110101>20151231";
		var ag = $.trim($(this).parent("td").parent("tr").find("td:first").text());
		if (ag)
			window.open('http://www.patentstar.com.cn/My/frmCnTbSearch4dlr.aspx?ad=' + ad + '&ag=' + ag);
	});
	// end

});
</script>

</body>
</html>
