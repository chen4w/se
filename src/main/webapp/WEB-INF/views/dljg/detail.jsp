<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<title>代理机构详细</title>
    <script src="${ctx}/static/echarts3/echarts.min.js" type="text/javascript"></script>
    <script src="${ctx}/static/echarts3/china.js" type="text/javascript"></script>
    <script src="${ctx}/static/echarts3/world.js" type="text/javascript"></script>
    <style type="text/css">
    	.carousel-fade .carousel-inner .item{  
	        opacity:0;  
	        -webkit-transition-property:opacity ;  
	        -moz-transition-property:opacity ;  
	        -ms-transition-property:opacity ;  
	        -o-transition-property:opacity ;  
	        transition-property:opacity ;
	    }
	    .carousel-fade .carousel-inner .active{  
	        opacity: 1;
	    }
	    .carousel-fade .carousel-inner .active.left,.carousel-fade .carousel-inner .active.right{  
	        left: 0;  
	        opacity: 0;
	    }
	    .carousel-fade .carousel-inner .next.left,.carousel-fade .carousel-inner .prev.right {
	        opacity: 1;
	    }
    </style>
</head>
<body>
<form id="inputForm" action="${ctx}/detail/${action}" method="post" class="form-horizontal">
<input type="hidden" name="id" value="${obj.id}"/>
<!-- div class="bigtitle">代理机构详细</div -->
<!-- 公司简介 -->
<div style="overflow:hidden;padding-top:5px;">
	<div class="dljginfo-left">
	   <div class="smalltitle"><img class="title-img" src="${ctx}/static/images/dljg_001.png" />公司简介</div>
	   <div style="margin:15px 0;height:420px;">                         
              <div style="float:left;width:43%;">
					 <div id="slidershow" class="carousel slide" data-ride="carousel" style="width:270px;">
						 <div class="carousel-inner">
						  <c:forEach items="${robjs}" var="ro" varStatus="status">
							<c:choose>
								<c:when test="${status.index==0}">
								  <div class="item active">
								     <img style="height: 390px;width: 270px;" src="${url_fopen}${ro.path}" />								  
								 </div>
								</c:when>
								<c:otherwise>
							        <div class="item">
							        <img style="height: 390px;width: 270px;" src="${url_fopen}${ro.path}" />
							        </div>
							    </c:otherwise>
							</c:choose>
						   </c:forEach>
						   <c:choose>
						   <c:when test="${empty robjs}">
							     <div class="item active">
								     <img style="height: 390px;width:270px;" src="${ctx}/static/images/scroll_001.jpg" />								  
								 </div>
								 <div class="item">
								     <img style="height: 390px;width:270px;" src="${ctx}/static/images/scroll_002.jpg" />								  
								 </div>
								 <div class="item">
								     <img style="height: 390px;width:270px;" src="${ctx}/static/images/scroll_003.jpg" />								  
								 </div>
							</c:when>
							</c:choose>
					         <a class="left carousel-control" href="#slidershow" role="button" data-slide="prev">
							      <span class="glyphicon glyphicon-chevron-left"></span>
							 </a>
							 <a class="right carousel-control" href="#slidershow" role="button" data-slide="next">
							      <span class="glyphicon glyphicon-chevron-right"></span>
							 </a>	
						  </div>						  					    				 						
					</div>
				</div>				
				<div style="float:right;width:56%;padding-left:0;">          
						<div class="contentTitle"><strong>${obj.mc}</strong></div>
						<div class="content">${gsjs}</div>
               </div>
         </div>
	</div>
<!-- 基本信息 -->
    <div class="dljginfo-right">
        <div class="smalltitle"><img class="title-img" src="${ctx}/static/images/dljg_002.png" />基本信息</div>
        <div class="div-padding">
	            <table class="table" id="table-nobordered">
	            <tr><td style="width:100px;">机构代码：</td> <td>${obj.jgdm}</td></tr>
		        <tr><td>成立时间：</td> <td>${slsj}</td></tr>
		        <tr><td>公司性质：</td> <td>${obj.gsxz}</td></tr>
		        <!-- tr><td>总代理量：</td> <td>${dll}</td></tr -->
		        <tr><td>代理人人数：</td> <td><a href="${ctx}/dlr?jg=${obj.id}">${obj.dlrrs}</a></td></tr>
		        <tr><td>负责人：</td> <td>${obj.fzr}</td></tr>
		        <tr><td>电话：</td> <td>${obj.dh}</td></tr>
		        <tr><td>地址：</td> <td>${obj.dz}</td></tr>
		        <tr><td>官网：</td> <td><a href="//${obj.wz}" target="_blank">${obj.wz}</a></td></tr>
		        <c:choose>
					<c:when test="${jczt=='有'}">
					  <tr><td>奖惩状态：</td> <td><a href="${ctx}${url_dljg_attach}${obj.jgdm}.rar">${jczt}</a></td></tr>
					</c:when>
					<c:otherwise>
				      <tr><td>奖惩状态：</td> <td>${jczt}</td></tr>
				    </c:otherwise>
				</c:choose>
		        <tr><td>经营理念：</td> <td>${obj.jyln}</td></tr>
		        <!--<tr><td>电子信箱：</td> <td><div>${obj.dzxx}</div></td></tr>  -->
		       </table>
        </div>	             
    </div>
</div>
<!-- 统计分页 -->
    <div class="dljginfo-tab">
        <ul class="nav nav-tabs" id="otherInfoTab" style="border:0;">		  
		  <li role="presentation" class="active">
		  	<a href="#khfbt" class="tabTitle">客户分布</a>
		  </li>
		  <li role="presentation">
		  	<a href="#dlq" class="tabTitle">代理案量</a>
		  </li>
		  <li role="presentation">
		  	<a href="#ryq" class="tabTitle">人员结构</a>
		  </li>
		  <li role="presentation">
		  	<a href="#jsq" class="tabTitle">代理案件主要技术领域分布</a>
		  </li>
		  <!-- 0513隐藏内容 -->
		  <!-- 
		  <li role="presentation" class="active" style="width: 33%;">
		  	<a href="#dlq" class="tabTitle">代理案量</a>
		  </li>
		  <li role="presentation" style="width: 33%;">
		  	<a href="#ryq" class="tabTitle">人员结构</a>
		  </li>
		  <li role="presentation" style="width: 34%;">
		  	<a href="#jsq" class="tabTitle">代理案件主要技术领域分布</a>
		  </li>
		   -->
		</ul>
		<div class="tab-content" style="background:#ffffff;overflow:hidden;">
		     <!-- 客户分布图模块 -->
		    <div class="tab-pane active" id="khfbt">		    		    
			    <div id="mapCarousel" class="carousel slide" data-ride="" data-interval="0">
				    <div class="carousel-inner" role="listbox">			  						  
					     <div class="item active">
						     <div id="chinamap" style="width:1000px;height:800px;margin:0 auto;"></div>									  
						 </div>
						 <div class="item">
						    <div id="worldmap" style="width:1000px;height:800px;margin:0 auto;"></div>								  
						 </div>
					</div>
		            <!-- Left and right controls -->
				    <a class="left carousel-control" href="#mapCarousel" role="button" data-slide="prev" >
				      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				      <span class="sr-only">Previous</span>
				   </a>
				   <a class="right carousel-control" href="#mapCarousel" role="button" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				      <span class="sr-only">Next</span>
				   </a>
			   </div>
		    </div>
		    <!-- 代理案量展示区模块 -->
		    <div class="tab-pane" id="dlq">
			     <div style="float:left;width:100%;padding:15px 0px;">
				       <div style="width:1%;float:left;">
				       		<div style="padding-top:2px; cursor:help;"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="bottom" title="点击图表可查看专利申请详情"></span></div>
				       		<div style="padding-top:332px; cursor:help;"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" title="点击图表可查看专利申请详情"></span></div>
				       </div>
				       <div style="width:49%;float:left;">
					       <div id="dllxdlqchart" style="width:500px;height:300px;"></div>
					       <!--<div class="input-large" style="text-align:center;">当年总量</div>  -->
					       <hr />
					       <div id="dllxdlqchart2" style="width:500px;height:300px;margin-top:30px;"></div>
					       <!-- <div class="input-large" style="text-align:center;">授权量</div> -->
					       <hr />
					       <div id="ywlxdlqchart" style="width:500px;height:350px;margin-top:30px;"></div>
				        </div>
				       <div style="width:1%;float:left;">
				       		<div style="padding-top:2px; cursor:help;"><span class="glyphicon glyphicon-bookmark" data-toggle="tooltip" data-placement="bottom" title="点击图表可查看决定正文"></span></div>
				       		<div style="padding-top:332px; cursor:help;"><span class="glyphicon glyphicon-bookmark" data-toggle="tooltip" title="点击图表可查看裁决文书"></span></div>
				       </div>
				       <div style="width:49%;float:right;">
					       <div id="firstfwqchart" style="width:500px;height:300px;"></div>
					       <hr />
		                   <div id="secondfwqchart" style="width:500px;height:300px;margin-top:30px;"></div>
		                   <hr />
		                   <!-- 0426隐藏内容
		                   <div id="thirdfwqchart" style="width:500px;height:300px;margin-top:30px;"></div -->
					       <!-- div id="dllxdlqtable" style="width:500px;height:200px;margin-top:60px;">
						       <table class="table table-bordered table-striped"> 
						       <thead> 
						       <tr> <th>分类</th> <th>${year5[0]}</th> <th>${year5[1]}</th><th>${year5[2]}</th><th>${year5[3]}</th><th>${year5[4]}</th> </tr> 
						       </thead> 
						       <tbody>								       
						        <tr> <th scope="row">授权率</th> <td>${firstMap.sqlnd1}</td><td>${firstMap.sqlnd2}</td><td>${firstMap.sqlnd3}</td><td>${firstMap.sqlnd4}</td>
						        <td>${firstMap.sqlnd5}</td> </tr>
						        <tr> <th scope="row">驳回率</th><td>${firstMap.bhlnd1}</td> <td>${firstMap.bhlnd2}</td><td>${firstMap.bhlnd3}</td><td>${firstMap.bhlnd4}</td><td>${firstMap.bhlnd5}</td> </tr> 
						        </tbody> 
						       </table>								       
					       </div -->
				       </div>
			     </div>					     
		    </div>
		    <!-- 人员结构展示区模块 -->
		    <div class="tab-pane" id="ryq">
			    <div style="float:left;width:100%;padding:15px 0px;">
			       <div id="firstryqchart" style="width:600px;height:500px;float:left;margin:15px auto;"></div>
			       <div id="secondryqchart" style="width:500px;height:500px;float:right;margin:15px auto;"></div>
			     </div>
		    </div>
		    <!-- 技术领域展示代理量区域模块 -->
		    <div class="tab-pane" id="jsq">
		       	           
		        <div id="jsqchart" style="width:1140px;height:500px;margin:15px auto;"></div>
		        <!-- <div style="width:1100px;margin:0 auto;text-align:center;">发明实用新型</div> -->
		        
		        <hr />
		        
		        <div id="jsqchart2" style="width:1140px;height:500px;margin:15px auto;"></div>
		       <!-- <div style="width:1100px;margin:0 auto;text-align:center;">外观设计</div> -->
		       
		       <hr />
		       
		       <div id="jsqchart3" style="width:1140px;height:500px;margin:15px auto;"></div>
		    </div>
		</div>
 </div>
<!-- 外网链接 --> 
<div class="linkbox">
 <div class="dljginfo-link">
  <a href="#" onclick="keep()"><img style="position: absolute;left: 19px;margin-top:9px;height: 32px;width: 32px;" src="${ctx}/static/images/dljg_003.png" /><div class="hot" >收藏机构</div></a>
  <a href="#" onclick="showHonorImgs()"><img style="position: absolute;left: 19px;margin-top:9px;height: 32px;width: 32px;" src="${ctx}/static/images/dljg_003.png" /><div class="hot" >荣誉资格</div></a>
  <a href="#" onclick="showYhsm()"><img style="position: absolute;left: 19px;margin-top:9px;height: 32px;width: 32px;" src="${ctx}/static/images/dljg_004.png" /><div class="hot" >进入店铺</div></a>
  <a href="${ctx}/zxzx?jid=${obj.id}" target="_blank" ><img style="position: absolute;left: 19px;margin-top:9px;height: 32px;width: 32px;" src="${ctx}/static/images/dljg_005.png" /><div class="hot" >在线交流</div></a>
 </div>
</div>
</form>
<!-- 登录创意宝用户声明 add by wsx 20160421 begin -->
<div class="modal fade" id="yhsm_modal" tabindex="-1" role="dialog" aria-labelledby="yhsmModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="yhsmModalLabel">用户声明</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <p style="text-indent:24px;line-height:26px">
             <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">欢迎您（与以下使用的“用户”一词具有同一含义）进入由北京源创云网络科技有限公司开发的、由北京源创云网络科技有限公司与您点击的代理机构共同提供服务的服务平台（以下简称“本平台”），请您仔细阅读以下协议，如果同意本并接受全部条款，请您点击“同意”，您点击同意按钮后，本协议即构成对您有约束力的法律文件。</span>
            </p>
          </div>
          <div class="col-xs-12">
            <div class="checkbox"><label><input id="chk-yhsm" type="checkbox" onclick="checkyhsm(this)">我已阅读并同意<a href="/yhsm" target="_blank">查看用户声明</a></label></div>
          </div>
          <!-- div class="col-xs-12"><button id="btn-gotocyb" type="button" class="btn btn-default" style="display:none" onclick="loginCYB()">跳转</button></div-->
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-default" data-dismiss="modal">关闭</button-->
        <button id="btn-gotocyb" type="button" class="btn btn-primary" style="display:none" onclick="loginCYB()">进入</button>
        
      </div>
    </div>
  </div>
</div>
<!-- 登录创意宝用户声明 add by wsx 20160421 end -->
<script>

    //定义全局变量
    //近五年
    var year5 = new Array('${year5[0]}','${year5[1]}','${year5[2]}','${year5[3]}','${year5[4]}');
    
	//客户分布图区-响应数据	
	var china_json = jQuery.parseJSON('${chinaJson}');
	var world_json = jQuery.parseJSON('${worldJson}');
	
	//代理案量展示区-响应数据	
	//人员结构展示区分页-响应数据	
	//技术领域展示区分页-响应数据
	var xxmc_json =jQuery.parseJSON('${xxmc_json}');
	var wgmc_json =jQuery.parseJSON('${wgmc_json}');
	var xx_json =jQuery.parseJSON('${xx_json}');
	var wg_json =jQuery.parseJSON('${wg_json}');
	var economyLegend_json =jQuery.parseJSON('${economyLegend_json}');
	var economyData_json =jQuery.parseJSON('${economyData_json}');

	$(document).ready(function() {
		// add by zhangqiuyi 20160525 begin
		$("#mapCarousel").hover(function(){
			$("#mapCarousel").css("border-left","2px solid crimson");
			$("#mapCarousel").css("border-right","2px solid crimson");
		},function(){
			$("#mapCarousel").css("border","none");
		});
		
	    // 模态框垂直居中显示
		function centerModals(){
		    $('.modal').each(function(i){
		        var $clone = $(this).clone().css('display', 'block').appendTo('body');    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
		        top = top > 0 ? top : 0;
		        $clone.remove();
		        $(this).find('.modal-content').css("margin-top", top);
		    });
		}
		$('.modal').on('show.bs.modal', centerModals);
		$(window).on('resize', centerModals);
		
		$("[data-toggle='tooltip']").tooltip();
		// end
		
		//实现tab分页
		$('#otherInfoTab a').hover(function(e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
        });
		
		//-------------------------------1.客户分布图模块 开始---------
		/* 0426隐藏内容 */
		//1.中国地图
		//基于准备好的dom，初始化echarts实例
		var chinamap = echarts.init(document.getElementById('chinamap'));

        // 指定图表的配置项和数据
        opt_chinamap = {
        		title : {
    		        text: '国内及港澳台地区客户分布情况 ',
    		        x:'center',
    		        top:'50'
    		    },
        	    tooltip: {
        	        trigger: 'item',
        	        formatter: function (params) {
        	        	var value = params.value;
        	        	if(isNaN(value)){
        	        		params.value=0;
        	        		value = 0;
        	        	}       	        		
        	            return params.name + ' : ' + value;
        	        }
        	    },
        	    visualMap: {
        	        min: 0,
        	        max: '${chinaMax}',
        	        left: 'left',
        	        top: 'bottom',
        	        text: ['高','低'],           // 文本，默认为数值文本
        	        calculable: true
        	    },
        	    toolbox: {
        	        show: true,
        	        orient: 'vertical',
        	        left: 'right',
        	        top: 'center',
        	        feature: {
        	            dataView: {readOnly: true,optionToContent: function(opt) {
        	                var seriesData = opt.series[0].data;
        	                var series = opt.series;
        	                var table = '<div style="height:700px;overflow-y:auto;border:1px solid;"><table class="table table-bordered  table-striped table-hover"><tbody style="text-indent:1em;"><tr>'
	                             + '<th>国内及港澳台地区</th>'
	                             + '<th>客户分布情况</th>'
	                             + '</tr>';  
			                for (var i = 0, l = seriesData.length; i < l; i++) {
			                	var isnan = seriesData[i].value;
			                	if(isNaN(isnan)){
			                		isnan = 0;
			                	}			                		
			                    table += '<tr>'
			                             + '<td>' + seriesData[i].name + '</td>'
			                             + '<td>' + isnan + '</td>'
			                             + '</tr>';
			                }
        	                table += '</tbody></table>';
        	                return table;
        	            }},
        	            restore: {},
        	            saveAsImage: {}
        	        }
        	    },
        	    series: [
        	        {
        	            name: '国内及港澳台地区客户分布情况',
        	            type: 'map',
        	            mapType: 'china',
        	            roam: true,
        	            label: {
        	                normal: {
        	                    show: true
        	                },
        	                emphasis: {
        	                    show: true
        	                }
        	            },
        	            mapLocation: {
                            x: 'left',
                            y: 'top',
                            width: '100%'
                        },
                        data: china_json
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        chinamap.setOption(opt_chinamap);
        
        
        //2.世界地图
		//基于准备好的dom，初始化echarts实例
		var worldmap = echarts.init(document.getElementById('worldmap'));

        // 指定图表的配置项和数据
        opt_worldmap = {
        		title : {
    		        text: '国外客户分布情况 ',
    		        x:'center',
    		        top:'50'
    		    },
        	    tooltip: {
        	        trigger: 'item',
        	        formatter: function (params) {
        	        	var value = params.value;
        	        	if(isNaN(value)){
        	        		params.value=0;
        	        		value = 0;
        	        	}       	        		
        	            return params.name + ' : ' + value;
        	        }
        	    },
        	    toolbox: {
        	        show: true,
        	        orient: 'vertical',
        	        left: 'right',
        	        top: 'center',
        	        feature: {
        	        	dataView: {readOnly: true,optionToContent: function(opt) {
        	                var seriesData = opt.series[0].data;
        	                var series = opt.series;
        	                var table = '<div style="height:700px;border:1px solid;overflow-y:auto;"><table class="table table-bordered  table-striped table-hover"><tbody style="text-indent:1em;"><tr>'
        	                             + '<th>国外</th>'
        	                             + '<th>客户分布情况</th>'
        	                             + '</tr>';           
        	                for (var i = 0, l = seriesData.length; i < l; i++) {
        	                	var isnan = seriesData[i].value;
			                	if(isNaN(isnan)){
			                		isnan = 0;
			                	}	
        	                    table += '<tr>'
        	                             + '<td>' + seriesData[i].name + '</td>'
        	                             + '<td>' + isnan + '</td>'
        	                             + '</tr>';
        	                }
        	                table += '</tbody></table></div>';
        	                return table;
        	            }},
        	            restore: {},
        	            saveAsImage: {}
        	        }
        	    },
        	    visualMap: {
        	        min: 0,
        	        max: '${worldMax}',
        	        text:['高','低'],
        	        realtime: false,
        	        calculable: true,
        	        color: ['orangered','yellow','lightskyblue']
        	    },
        	    series: [
        	        {
        	            name: '国外客户分布情况',
        	            type: 'map',
        	            mapType: 'world',
        	            roam: true,
        	            itemStyle:{
        	                emphasis:{label:{show:true}}
        	            },
        	            mapLocation: {
                            x: 'left',
                            y: 'top',
                            width: '100%'
                        },
        	            data : world_json
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        worldmap.setOption(opt_worldmap);
        
		//-------------------------------1.客户分布图模块 结束---------

		//-------------------------------2.代理案量展示区模块 开始---------
		//1.柱状图
		//基于准备好的dom，初始化echarts实例
		var dllxdlqchart = echarts.init(document.getElementById('dllxdlqchart'));

        // 指定图表的配置项和数据
        opt_dllxdlqchart = {
        		title : {
    		        text: '近五年专利申请代理量 ',
    		        subtext: '依据申请日统计    【点击图表可查看专利申请详情】'
    		    },
        	    tooltip : {
        	        trigger: 'axis',
        	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
        	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        	        }
        	    },
        	    legend: {
        	    	data: ['发明', '实用新型','外观设计'],
        	        x: 'right',
        	        top: '10'
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis:  {
        	        type: 'value'
        	    },
        	    yAxis: {
        	        type: 'category',
        	        data: year5
        	    },
        	    series: [
        	        {
        	            name: '发明',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.fmnd1}', '${firstMap.fmnd2}', '${firstMap.fmnd3}', '${firstMap.fmnd4}', '${firstMap.fmnd5}']
        	        },
        	        {
        	            name: '实用新型',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.xxnd1}', '${firstMap.xxnd2}', '${firstMap.xxnd3}', '${firstMap.xxnd4}', '${firstMap.xxnd5}']
        	        },
        	        {
        	            name: '外观设计',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.wgnd1}', '${firstMap.wgnd2}', '${firstMap.wgnd3}', '${firstMap.wgnd4}', '${firstMap.wgnd5}']
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        dllxdlqchart.setOption(opt_dllxdlqchart);
        
       //2.柱状图
		//基于准备好的dom，初始化echarts实例
		var dllxdlqchart2 = echarts.init(document.getElementById('dllxdlqchart2'));

        // 指定图表的配置项和数据
        opt_dllxdlqchart2 = {
        		title : {
    		        text: '近五年专利申请授权量 ',
    		        subtext: '依据授权公告日统计    【点击图表可查看专利申请详情】'
    		    },
        	    tooltip : {
        	        trigger: 'axis',
        	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
        	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        	        }
        	    },
        	    legend: {
        	    	data: ['发明', '实用新型','外观设计'],
        	        x: 'right',
        	        top: '10'
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis:  {
        	        type: 'value'
        	    },
        	    yAxis: {
        	        type: 'category',
        	        data: year5
        	    },
        	    series: [
        	        {
        	            name: '发明',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.sqfmnd1}', '${firstMap.sqfmnd2}', '${firstMap.sqfmnd3}', '${firstMap.sqfmnd4}', '${firstMap.sqfmnd5}']
        	        },
        	        {
        	            name: '实用新型',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.sqxxnd1}', '${firstMap.sqxxnd2}', '${firstMap.sqxxnd3}', '${firstMap.sqxxnd4}', '${firstMap.sqxxnd5}']
        	        },
        	        {
        	            name: '外观设计',
        	            type: 'bar',
        	            stack: '总量',
        	            label: {
        	                normal: {
        	                    show: true,
        	                    position: 'insideRight'
        	                }
        	            },
        	            data: ['${firstMap.sqwgnd1}', '${firstMap.sqwgnd2}', '${firstMap.sqwgnd3}', '${firstMap.sqwgnd4}', '${firstMap.sqwgnd5}']
        	        }
        	        
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        dllxdlqchart2.setOption(opt_dllxdlqchart2);
		
        // 3.饼状图 
        //基于准备好的dom，初始化echarts实例
        var ywlxdlqchart = echarts.init(document.getElementById('ywlxdlqchart'));

        // 指定图表的配置项和数据
        opt_ywlxdlqchart = {
		    title : {
		        text: '近五年国内案件和涉外案件比例',
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        orient: 'vertical',
		        left: 'left',
		        data: ['国内','港澳台','海外']
		    },
		    series : [
		        {
		            name: '案件来源',
		            type: 'pie',
		            radius : '50%',
		            center: ['50%', '50%'],
		            data:[
		                {value:'${secondMap.gnyear5}', name:'国内'},
		                {value:'${secondMap.gatyear5}', name:'港澳台'},
		                {value:'${secondMap.gwyear5}', name:'海外'}
		            ],
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};

        // 使用刚指定的配置项和数据显示图表。
        ywlxdlqchart.setOption(opt_ywlxdlqchart);
        
        //4.柱状图
		//基于准备好的dom，初始化echarts实例
		var firstfwqchart = echarts.init(document.getElementById('firstfwqchart'));

        // 指定图表的配置项和数据
        opt_firstfwqchart = {
        		title : {
    		        text: '专利复审/无效代理量',
    		        subtext: '【点击图表可查看决定正文】'
    		    },
        	    tooltip : {
        	        trigger: 'item'
        	    },
        	    legend: {
        	    	data:['专利复审', '专利无效'],
        	    	orient: 'vertical',
        	        x: 'right'
        	    },
        	    grid: {
        	        left: '20%',
        	        right: '30%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'专利复审',
        	            type:'bar',
        	            data:['${fourMap.fs}']
        	        },
        	        {
        	            name:'专利无效',
        	            type:'bar',
        	            data:['${fourMap.wx}']
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        firstfwqchart.setOption(opt_firstfwqchart);
     
        //5.柱状图
		//基于准备好的dom，初始化echarts实例
		var secondfwqchart = echarts.init(document.getElementById('secondfwqchart'));

        // 指定图表的配置项和数据
        opt_secondfwqchart = {
        		title : {
    		        text: '专利诉讼代理量',
    		        subtext: '数据来源于知产宝(IPHOUSE)    【点击图表可查看裁决文书】'
    		    },
        	    tooltip : {
        	        trigger: 'item'
        	    },
        	    legend: {
        	    	data:['专利行政诉讼', '专利民事诉讼'],
        	    	orient: 'vertical',
        	        x: 'right'
        	    },
        	    grid: {
        	        left: '20%',
        	        right: '30%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'专利行政诉讼',
        	            type:'bar',
        	            data:['${thirdMap.xzajsl}']
        	        },
        	        {
        	            name:'专利民事诉讼',
        	            type:'bar',
        	            data:['${thirdMap.msajsl}']
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        secondfwqchart.setOption(opt_secondfwqchart);
        
        
      //6.堆叠图
      /* 0426隐藏内容
		//基于准备好的dom，初始化echarts实例
		var thirdfwqchart = echarts.init(document.getElementById('thirdfwqchart'));

        // 指定图表的配置项和数据
        opt_thirdfwqchart = {
        		title : {
    		        text: '近五年发明专利结案授权/驳回率'
    		    },
        	    tooltip : {
        	        trigger: 'axis',
        	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
        	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        	        },
        	        formatter: "{b} <br/> {a1}: {c1}% <br/> {a0}: {c0}%"
        	    },
        	    legend: {
        	    	data:['发明专利结案授权率', '发明专利结案驳回率'],
        	    	orient: 'vertical',
        	        x: 'right'
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            boundaryGap : false,
        	            data : year5
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
			            name:'发明专利结案驳回率',
			            type:'line',
			            stack: '总量',
			            areaStyle: {normal: {}},
			            data: ['${firstMap.bhlnd1}', '${firstMap.bhlnd2}', '${firstMap.bhlnd3}', '${firstMap.bhlnd4}', '${firstMap.bhlnd5 }'],
			            itemStyle:{
			            	normal:{color:'#d48265'}
			            }
			        },
			        {
			            name:'发明专利结案授权率',
			            type:'line',
			            stack: '总量',
			            label: {
			                normal: {
			                    show: true,
			                    position: 'top'
			                }
			            },
			            areaStyle: {normal: {}},
			            data: ['${firstMap.sqlnd1}', '${firstMap.sqlnd2}', '${firstMap.sqlnd3}', '${firstMap.sqlnd4}', '${firstMap.sqlnd5}'],
			            itemStyle:{
			            	normal:{color:'#91c7ae'}
			            }
			        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        thirdfwqchart.setOption(opt_thirdfwqchart);
        */
      
        //-------------------------------2.代理案量展示区模块  结束

        //-------------------------------3.人员结构展示区模块 开始	
        // 1.饼状图 
        //基于准备好的dom，初始化echarts实例
        var firstryqchart = echarts.init(document.getElementById('firstryqchart'));

        // 指定图表的配置项和数据
        opt_firstryqchart = {
        		title : {
    		        text: '执业资质分布情况',
    		        subtext: '【点击图表可查看相应人员信息】',
    		        subtextStyle: {
    		        	color: '#337ab7'
    		        }
    		    },
        	    tooltip : {
        	        trigger: 'item'
        	    },
        	    legend: {
        	    	data:['专利代理人','行政诉讼代理人','民事诉讼代理人','律师双执业证'],
        	    	x: 'right',
        	    	orient: 'vertical'
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '22%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data:[]
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'专利代理人',
        	            type:'bar',
        	            data:['${fiveMap.all}']
        	        },
        	        {
        	            name:'行政诉讼代理人',
        	            type:'bar',
        	            data:['${fiveMap.ss}']
        	        },
        	        {
        	            name:'民事诉讼代理人',
        	            type:'bar',
        	            data:['${fiveMap.ms}']
        	        },
        	        {
        	            name:'律师双执业证',
        	            type:'bar',
        	            data:['${fiveMap.ls}']
        	        }
        	    ]
        	};
        /*opt_firstryqchart = {
        	    tooltip: {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b}: {c} ({d}%)"
        	    },
        	    legend: {
        	    	orient: 'vertical',
    		        left: 'left',
        	        data:['律师','民事诉讼代理人','行政诉讼代理人','其他']
        	    },
        	    series : [
        			        {
        			            name: '代理人类型',
        			            type: 'pie',
        			            radius : '50%',
        			            center: ['50%', '50%'],
        			            data:[        	        	               
        	        	                {value:'${fiveMap.ls}', name:'律师', selected:true},
        	        	                {value:'${fiveMap.ms}', name:'民事诉讼代理人'},
        	        	                {value:'${fiveMap.xz}', name:'行政诉讼代理人'},
        	        	                {value:'${fiveMap.qt}', name:'其他'}
        	        	            ],
        			            itemStyle: {
        			                emphasis: {
        			                    shadowBlur: 10,
        			                    shadowOffsetX: 0,
        			                    shadowColor: 'rgba(0, 0, 0, 0.5)'
        			                }
        			            }
        			        }
        			    ]
        	};*/
        // 使用刚指定的配置项和数据显示图表。
        firstryqchart.setOption(opt_firstryqchart);
      
        // 2.饼状图 
        //基于准备好的dom，初始化echarts实例
        var secondryqchart = echarts.init(document.getElementById('secondryqchart'));

        // 指定图表的配置项和数据
        opt_secondryqchart = {
        		title : {
    		        text: '执业年限分布情况',
    		        subtext: '【点击图表可查看相应人员信息】',
    		        subtextStyle: {
    		        	color: '#337ab7'
    		        },
    		        x: 'right'
    		    },
        		tooltip: {
        	        trigger: 'item',
        	        formatter: "{a} <br/>{b}: {c} ({d}%)"
        	    },
        	    legend: {
        	    	orient: 'vertical',
    		        left: 'left',
        	        data:['20年以上','10-20年','5-10年','3-5年','3年以下']
        	    },
        	    series : [
        			        {
        			            name: '执业年限',
        			            type: 'pie',
        			            radius : '50%',
        			            center: ['50%', '50%'],
        			            data:[        	        	               
										{value:'${fiveMap.year21}', name:'20年以上', selected:true},
										{value:'${fiveMap.year20}', name:'10-20年'},
										{value:'${fiveMap.year10}', name:'5-10年'},
										{value:'${fiveMap.year5}', name:'3-5年'},
										{value:'${fiveMap.year3}', name:'3年以下'}
        	        	            ],
        			            itemStyle: {
        			                emphasis: {
        			                    shadowBlur: 10,
        			                    shadowOffsetX: 0,
        			                    shadowColor: 'rgba(0, 0, 0, 0.5)'
        			                }
        			            }
        			        }
        			    ]
        	};
        // 使用刚指定的配置项和数据显示图表。
        secondryqchart.setOption(opt_secondryqchart);
        //-------------------------------3.人员结构展示区模块  结束
        
        //-------------------------------4.技术领域展示代理量区域模块 开始---------
        //1.柱状图(从左到右按大到小排序，初始有技术领域10个)
		//基于准备好的dom，初始化echarts实例
		var jsqchart = echarts.init(document.getElementById('jsqchart'));

        // 指定图表的配置项和数据
        opt_jsqchart = {
        		title : {
        	        text: '国际专利分类(IPC)分布',
        	        x:'center'
        	    },
        		tooltip : {
        	        trigger: 'item',
        	        formatter: '{a}'
        	    },
        	    legend: {
        	    	orient: 'vertical',
       	            left: '3%',
       	            bottom: 50,
        	        data : xxmc_json
        	    },
        	    grid: {
        	        left: '42%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'category',
        	            data: []
        	        }
        	    ],
        	    series : xx_json
        	};

        // 使用刚指定的配置项和数据显示图表。
        jsqchart.setOption(opt_jsqchart);
        
        //2.柱状图(从左到右按大到小排序，初始有技术领域10个)
		//基于准备好的dom，初始化echarts实例
		var jsqchart2 = echarts.init(document.getElementById('jsqchart2'));

        // 指定图表的配置项和数据
        opt_jsqchart2 = {
        		title : {
        	        text: '外观设计国际分类(Locarno)分布',
        	        x:'center'
        	    },
        		tooltip : {
        	        trigger: 'item',
        	        formatter: '{a}'
        	    },
        	    legend: {
        	    	orient: 'vertical',
        	    	//x: 'right',
       	            bottom: 50,
       	            right: '4%',
        	        data : wgmc_json
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '40%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	        	type : 'category',
        	            data: []
        	        }
        	    ],
        	    series : wg_json
        	};

        // 使用刚指定的配置项和数据显示图表。
        jsqchart2.setOption(opt_jsqchart2);
        
        
      //3.柱状图(从左到右按大到小排序，初始有技术领域10个)
		//基于准备好的dom，初始化echarts实例
		var jsqchart3 = echarts.init(document.getElementById('jsqchart3'));

        // 指定图表的配置项和数据
        opt_jsqchart3 = {
        		title : {
        	        text: '国民经济产业分类分布',
        	        x:'center'
        	    },
        		tooltip : {
        	        trigger: 'item',
        	        formatter: '{a}'
        	    },
        	    legend: {
        	    	orient: 'vertical',
       	            left: '3%',
       	            bottom: 50,
        	        data : economyLegend_json
        	    },
        	    grid: {
        	        left: '38%',
        	        right: '4%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	        	type : 'category',
        	            data: []
        	        }
        	    ],
        	    series : economyData_json
        	};

        // 使用刚指定的配置项和数据显示图表。
        jsqchart3.setOption(opt_jsqchart3);
        //-------------------------------4.技术领域展示代理量区域模块 结束------
      
	// add by zhangqiuyi 20160525 begin
	dllxdlqchart.on('click', function(params) {
    	var ad = params.name;
		if (ad)
			window.open('http://www.patentstar.com.cn/My/frmCnTbSearch4dlr.aspx?ad=' + ad + '&ag=' + ${obj.jgdm});	
    });
    
	dllxdlqchart2.on('click', function(params) {
    	var gd = params.name;
		if (gd)
			window.open('http://www.patentstar.com.cn/My/frmCnTbSearch4dlr.aspx?gd=' + gd + '&ag=' + ${obj.jgdm});	
    });
	// end
	
    //add by wsx 20160420 begin
    firstfwqchart.on('click', function(params) {
    	var series = opt_firstfwqchart.series[params.seriesIndex];  
    	if(series.name=='专利复审'){
    		$('#fswx_iframe').attr('src','${ctx}/fswx/dljg/${obj.id}/1')
    	}else if(series.name=='专利无效'){
    		$('#fswx_iframe').attr('src','${ctx}/fswx/dljg/${obj.id}/2')
    	}
    	$('#fswx_modal').modal('show');	
    });
	
    secondfwqchart.on('click', function(params) {
    	var series = opt_secondfwqchart.series[params.seriesIndex]; 
    	var casetype = 0;
    	if(series.name=='专利行政诉讼'){
    		casetype = 1;
    	}else if(series.name=='专利民事诉讼'){
    		casetype = 2;
    	}
    	loginZCB(0,casetype,'${obj.mc}');
    });
    //end
	//add by chenlh 20160517 

    firstryqchart.on('click', function(params) {
    	var series = opt_firstryqchart.series[params.seriesIndex];  
    	if(series.name=='专利代理人'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/0')
    	}else if(series.name=='行政诉讼代理人'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/12')
    	}else if(series.name=='民事诉讼代理人'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/2')
    	}else if(series.name=='律师双执业证'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/23')
    	}
    	$('#ryq_modal').modal('show');	
    });
    secondryqchart.on('click', function(params) {
    	var series = opt_secondryqchart.series[0].data[params.dataIndex];  
    	if(series.name=='3年以下'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/3')
    	}else if(series.name=='3-5年'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/5')
    	}else if(series.name=='5-10年'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/10')
    	}else if(series.name=='10-20年'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/20')
    	}else if(series.name=='20年以上'){
    		$('#ryq_iframe').attr('src','${ctx}/ryq/dljg/${obj.id}/21')
    	}
    	$('#ryq_modal').modal('show');	
    });

});

//add by wsx 20160420 begin
function loginZCB(type,casetype,content){
	 $.ajax({
        type:"post",
        url:"${ctx}/zcb",
        dataType : 'json',  
        data:{type:type,casetype:casetype,content:content},   
        async:false,
        success:function(e){
           if(e.status=='succ'){
           	window.open(e.url,'_blank');
           }else{
           	alert(e.msg);
           	window.open(e.url,'_blank');
           }
        },error:function(e){
       	    //alert(1);
        }                 
    });
}

function loginCYB(){
	$('#yhsm_modal').modal('hide');
	var w = window.open('about:blank');
	 $.ajax({
     	type:"get",
         url:"${ctx}/cyb/dljg/${obj.id}",
         dataType : 'json',                  
         success:function(e){
            if(e.status=='succ'){
            	//window.open(e.url,'_blank');
            	w.location = e.url;
            }else{
            	w.close();
            	alert(e.msg);
            	//window.open(e.url,'_blank');
            }
         },error:function(data,e){
        	 w.close();
        	 //alert(data.status);
         }                 
     });
}
//add by wsx 20160420 end
//add by wsx 20160421 begin
function showYhsm(){
	//判断用户是否登录
	var uname = $.cookie("uname");
	if(!uname){
		alert("请登录后进入店铺");
		window.location.href="/login";
		return;
	}
	//判断代理机构是否开通店铺
	$.ajax({
     	type:"get",
         url:"${ctx}/cyb/hasdljgds/${obj.id}",
         dataType : 'json',                  
         success:function(e){
            if(e.status=='yes'){
            	$('#yhsm_modal').modal('show');
            }else{
            	alert(e.msg);
            }
         },error:function(data,e){
         }                 
     });
}
function checkyhsm(obj){
	if(obj.checked){
		$("#btn-gotocyb").show();
	}else{
		$("#btn-gotocyb").hide();
	}
}
$("#yhsm_modal").on("hidden.bs.modal", function() {
    //$(this).removeData("bs.modal");  
    $("#chk-yhsm").attr('checked',false);
    $("#btn-gotocyb").hide();
});
//add by wsx 20160421 end

//公司简介--详情点击事件
function dljgInfo(){
	$('#dlig_info').modal('show');
	return;
}

//荣誉附图展现
function showHonorImgs(){
	$('#dlig_honorimgs').modal('show');
	return;
}
function keep()
{
	alert(window.location.href);
	}
</script>

<!-- 查看复审无效 add by wsx 20160419 begin -->
<div class="modal fade" id="fswx_modal" tabindex="-1" role="dialog" aria-labelledby="fswxModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="fswxModalLabel">复审/无效</h4>
      </div>
      <div class="modal-body">
        <iframe id="fswx_iframe" src="" width="570" height="430" frameborder="no" scrolling="no"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- button type="button" class="btn btn-primary">Save changes</button-->
      </div>
    </div>
  </div>
</div>
<!-- 查看复审无效 add by wsx 20160419 end -->

<!-- 查看公司简介内容 -->
<div class="modal fade" id="dlig_info" tabindex="-1" role="dialog" aria-labelledby="gsjsModalLabel">
  <div class="modal-dialog" role="document" style="width:600px;margin:20px auto;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gsjsModalLabel">公司简介</h4>
      </div>
      <div class="modal-body" style="text-indent:2em;">
	  ${obj.gsjs}
      </div>
    </div>
  </div>
</div>

<!-- 查看荣誉附图 -->
<div class="modal fade" id="dlig_honorimgs" tabindex="-1" role="dialog" aria-labelledby="honorModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="honorModalLabel">荣誉附图</h4>
      </div>
      <div class="modal-body">
			<div id="honorCarousel" class="carousel-fade carousel slide" data-ride="carousel">
			   <!-- Indicators -->
			   <ol class="carousel-indicators">
			      <c:forEach items="${honorls}" var="ro" varStatus="status">
					<c:choose>
						<c:when test="${status.index==0}">
						  <li data-target="#honorCarousel" data-slide-to="${status.index}" class="active"></li>
						</c:when>
						<c:otherwise>
					        <li data-target="#honorCarousel" data-slide-to="${status.index}"></li>
					    </c:otherwise>
					</c:choose>
				   </c:forEach>
				   <c:choose>
					   <c:when test="${empty honorls}">
						     <li data-target="#honorCarousel" data-slide-to="0" class="active"></li>
						     <li data-target="#honorCarousel" data-slide-to="1"></li>
						     <li data-target="#honorCarousel" data-slide-to="2"></li>
						</c:when>
					</c:choose>
			   </ol>
			   <div class="carousel-inner" role="listbox">			  
					  <c:forEach items="${honorls}" var="ro" varStatus="status">
						<c:choose>
							<c:when test="${status.index==0}">
							  <div class="item active">
							     <img style="width: 100%;height: auto;margin:0 auto;" src="${url_fopen}${ro.path}" />							  
							 </div>
							</c:when>
							<c:otherwise>
						        <div class="item">
						        <img style="width: 100%;height: auto;margin:0 auto;" src="${url_fopen}${ro.path}" />
						        </div>
						    </c:otherwise>
						</c:choose>
					   </c:forEach>
					   <c:choose>
					   <c:when test="${empty honorls}">
						     <div class="item active">
							     <img style="width: 100%;height: auto;margin:0 auto;" src="${ctx}/static/images/scroll_001.jpg" />								  
							 </div>
							 <div class="item">
							     <img style="width: 100%;height: auto;margin:0 auto;" src="${ctx}/static/images/scroll_002.jpg" />								  
							 </div>
							 <div class="item">
							     <img style="width: 100%;height: auto;margin:0 auto;" src="${ctx}/static/images/scroll_003.jpg" />								  
							 </div>
						</c:when>
						</c:choose>
				</div>
	            <!-- Left and right controls -->
			    <a class="left carousel-control" href="#honorCarousel" role="button" data-slide="prev">
			      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			      <span class="sr-only">Previous</span>
			   </a>
			   <a class="right carousel-control" href="#honorCarousel" role="button" data-slide="next">
			      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			      <span class="sr-only">Next</span>
			   </a>					  					    				 						
		   </div>
      </div>
    </div>
  </div>
</div>

<!-- 点击人员结构图表，查看人员名片 add by chenlh 20160517 begin -->
<div class="modal fade" id="ryq_modal" tabindex="-1" role="dialog" aria-labelledby="ryqModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ryqModalLabel">查看人员名片</h4>
      </div>
      <div class="modal-body">
        <iframe id="ryq_iframe" src="" width="570" height="430" frameborder="no" scrolling="no"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- button type="button" class="btn btn-primary">Save changes</button-->
      </div>
    </div>
  </div>
</div>
<!-- add by chenlh 20160517 end -->
</body>
</html>
