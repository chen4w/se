
<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ page contentType="text/html;charset=UTF-8" import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>代理机构检索</title>
	<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap-chinese-r.js" type="text/javascript"></script>
	<script src="${ctx}/static/jquery/jquery.base64v1.1.js" type="text/javascript"></script>
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-chinese-r.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-table.css" type="text/css" rel="stylesheet" />
	<!-- <script type = "text/javascript" src="${ctx}/static/jquery/jquery-1.10.2.js"></script> 
	<script type = "text/javascript" src="${ctx}/static/jquery/jquery-ui-1.10.4.js"></script>
	<link type="text/css" href="${ctx}/static/jquery/jquery-ui-1.10.4.css" rel="stylesheet" /> -->
	<script type="text/javascript">
	function getActionURL(val) {
		//c4w
		var lhref = location.href;
		var pos1 = lhref.indexOf('name='),pos2;
		var hf1,hf2,pid;
		if(pos1!=-1){
			var dy;
			pos2 = lhref.indexOf('&',pos1);
			if(pos2==-1){
				pos2 = lhref.lastIndexOf('#');
			}
			if(pos2==-1){
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = '';
				dy = lhref.substring(pos1+3);
			}else{
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = lhref.substring(pos2);
				dy =  lhref.substring(pos1+3,pos2);
			}
			dy = decodeURI(dy);
		}else{
			if(location.search && location.search!='')
				hf1=lhref+'&name=';
			else
				hf1=lhref+'?name=';
			pos2 = lhref.lastIndexOf('#');
			if(pos2==-1){
				hf2 = '';
			}else{
				hf2 = lhref.substring(pos2);
			}
		}
		//c4w end
		return hf1+val+hf2;
	};
	
	function jdnf(){
		var rs1 = $("#nf1").val();
		var rs2 = $("#nf2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_jdnf=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_jdnf=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_jdnf=" + rs1 + "-" + rs2;
		}
		
	}
	
	function getName(){
		var rs1 = $.trim($("#leader").val());
		var rs2 = $.trim($("#examiner").val());
		var href = window.location.href;
		var search = window.location.search;
		if(href.indexOf("?") == -1){
			window.location.href =  href + "?sl_leaderName=" + rs1 + "&" + "sl_examinerName=" + rs2;
		}else{
			if(href.indexOf("sl_leaderName") != -1 && href.indexOf("sl_examinerName") != -1){
				var s = search.substring(1);
				var str = s.split("&");
				var addr = "?";
				for(i=0;i<str.length ;i++ ){
					var temp = str[i].split("=");
					if(temp[0] == "sl_leaderName"){
						temp[1] = rs1;
					}
					if(temp[0] == "sl_examinerName"){
						temp[1] = rs2;
					}
					addr = addr + temp[0] + "=" + temp[1] + "&"
				}
				window.location.href = href.substring(0,href.substring("?")) + addr.substring(0,addr.lastIndexOf("&")); 
			}else{
				window.location.href = href + "&sl_leaderName=" + rs1 + "&sl_examinerName=" + rs2; 
			}
			
		}
	}
	
	
	$(document).ready(function(){
		//检索名称带其他条件		
		$("#search_btn").click(function(e){
			e.preventDefault();
			window.location.href=getActionURL($("#ipt_name").val());
		});
		
	
		if(!$("#nav").is(":hidden")){
			$("body,html").animate({
				   scrollTop:$("#scrollpos").offset().top  //让body的scrollTop等于pos的top，就实现了滚动
			},0);
		}
		
		var url = location.href;
		if(url.indexOf("/invalidreview") !=-1){
			$("#menuTab>ul>li").eq(4).attr("class","active");
		}
		
		//实现tab分页
		$('#searchTab a').click(function(e) {
	        e.preventDefault();//阻止a链接的跳转行为
	        $(this).tab('show');//显示当前选中的链接及关联的content
	    });
		
		
		
	/*	$("#leader").autocomplete({
			maxRows:10,
			source:function(request,response){
				var name = $("#leader").val();
				//alert(name);
				$.ajax({
					type:"get",
					url:"leader",
					dataType:"json",
					data:{
						"name":name
					},
					success:function(data){
						alert(data.file);
						alert(data.length);
							
							response($.map(data,function(item){
								return{
									label:item.label,
									value:item.value
								};
							}));
					}
					
				});
			}
		
		});*/

		
	
		var firstpage = '<a href = "#">首页</a>';
		var lastpage = '<a href = "#">尾页</a>';
		var  count = 89;
		var pagesize = 10;
		var pagecount = Math.ceil(count/pagesize);
		var currentpage ;
		$("#divpage").append(firstpage);
		for(var i = 1; i <= pagecount; i++){
			var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>'
			$("#divpage").append(page);
		}
		$("#divpage").append(lastpage);
		
	});
	function check(num,count,pagesize){
		$("#divpage").empty();
		var firstpage = '<a href = "#">首页</a>';
		var lastpage = '<a href = "#">尾页</a>';
		$("#divpage").append(firstpage);
		var pagecount = Math.ceil(count/pagesize);
		if(pagecount - num >=9){
			for(var i = num; i <= num + 9; i++){
				var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
				$("#divpage").append(page);
			}
		}else{
			if(pagecount <= 10){
				for(var i = 1;i <= pagecount;i++){
					var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
					$("#divpage").append(page);
				}
			}else{
				for(var i = pagecount - 9;i <= pagecount;i++){
					var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
					$("#divpage").append(page);
				}
			}
			
		}
		$("#divpage").append(lastpage);
	} 
	function tst(){
	 
	var s = $.base64.encode('{"type":["复审"]}');	
	alert(s);
	$.ajax({
            type: "get",
            async: false,
            url: "http://103.41.55.29/FSWXSearchHandler/SearchListHandler.ashx?callback=fswx",
            data:{
  		        	 "FswxQuery":s.replace(/"+"/, "%2B")
  		         },
            dataType: "jsonp",
            jsonp: "callback",//传递给请求处理程序或页面的，用以获得jsonp回调函数名的参数名(一般默认为:callback)
            jsonpCallback:"fswx",//自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名，也可以写"?"，jQuery会自动为你处理数据
            success: function(json){
                alert(json.Fswxitem[0].title);
            },
            error: function(){
                alert('fail');
            }
        });
	
		}
	
</script>
	
</head>

<body>



hello
<input type = "button" value = "submit" onclick = "tst()"/>
	
	
</body>
</html>
