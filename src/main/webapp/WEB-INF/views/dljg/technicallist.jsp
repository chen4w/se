<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>代理机构检索</title>
	<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap-chinese-r.js" type="text/javascript"></script>
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-chinese-r.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-table.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript">
	function getActionURL(val) {
		//c4w
		var lhref = location.href;
		var pos1 = lhref.indexOf('name='),pos2;
		var hf1,hf2,pid;
		if(pos1!=-1){
			var dy;
			pos2 = lhref.indexOf('&',pos1);
			if(pos2==-1){
				pos2 = lhref.lastIndexOf('#');
			}
			if(pos2==-1){
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = '';
				dy = lhref.substring(pos1+3);
			}else{
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = lhref.substring(pos2);
				dy =  lhref.substring(pos1+3,pos2);
			}
			dy = decodeURI(dy);
		}else{
			if(location.search && location.search!='')
				hf1=lhref+'&name=';
			else
				hf1=lhref+'?name=';
			pos2 = lhref.lastIndexOf('#');
			if(pos2==-1){
				hf2 = '';
			}else{
				hf2 = lhref.substring(pos2);
			}
		}
		//c4w end
		return hf1+val+hf2;
	};
	function slrs(){
		var rs1 = $("#rs1").val();
		var rs2 = $("#rs2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_rs=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_rs=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_rs=" + rs1 + "-" + rs2;
		}
		
	}
	
	function slnx(){
		var rs1 = $("#nx1").val();
		var rs2 = $("#nx2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_slnx=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_slnx=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_slnx=" + rs1 + "-" + rs2;
		}
		
	}
	$(document).ready(function(){
		$("#search_btn").click(function(e){
			e.preventDefault();
			window.location.href=getActionURL($("#ipt_name").val());
		});
		var ipt = $("#address");
		ipt.height(ipt.parent().height());
		if(${divFlag} == true){
			$("#nav").show();
		}else{
			$("#nav").hide();
		}
		if(!$("#nav").is(":hidden")){
			$("body,html").animate({
				   scrollTop:$("#scrollpos").offset().top  //让body的scrollTop等于pos的top，就实现了滚动
			},0);
		}
	  
	});
</script>
</head>

<body>
	<c:if test="${not empty message}">
		<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close">×</button>${message}</div>
	</c:if>

<%
String[] sp1 = new String[]{"5人以下","5-10人","10-15人","15-20人","20-40人","40人以上"};
String[] sv1 = new String[]{"5lt","5-10","10-15","15-20","20-40","40gt"};

String[] sp2 = new String[]{"5年以下","5-10年","10-15年","15-20年","20年以上"};
String[] sv2 = new String[]{"5lt","5-10","10-15","15-20","20gt"};

String[] sp3 = new String[]{"IPC分类","洛迦诺分类","国民经济产业分类"};
String[] sv3 = new String[]{"1","2","3"};

String[] sp4 = new String[]{};
String[] sv4 = new String[]{};

String[] sp5 = new String[]{};
String[] sv5 = new String[]{};

String[] sl_tags= new String[]{"代理人人数","成立年限","技术领域","地区","机构名称"};
String[] sl_names=new String[]{"sl_rs","sl_slnx","sl_jsly","dy","name"};

String[][] sp = new String[][]{sp1,sp2,sp3,sp4,sp5};
String[][] sv = new String[][]{sv1,sv2,sv3,sv4,sv5};

HashMap<String,String> fvp = new HashMap<String,String>();
for(int i=0; i<sl_names.length; i++){
	String[] v = sv[i];
	String[] p = sp[i];
	for(int j=0; j<v.length; j++){
		fvp.put(sl_names[i]+"."+v[j],p[j]);
	}
}

%>
           
           
           
<div id="scrollpos"></div>   
   <nav class="navbar navbar-default"> <!-- 加 ID = nav 控制显隐性 -->
<!-- div class="container-fluid"-->
	<div class="navbar-header" style="margin-top:10px;">
	<div class="crumbs-nav-main clearfix">
			<div class="crumbs-nav-item">
				<div class="crumbs-first"><a href="?" style="font-family: '微软雅黑 Bold','微软雅黑';">全部结果</a></div>
			</div>
			<i class="crumbs-arrow">&gt;</i>
			<tags:forjsly fld_tags="<%=sl_tags%>" fld_names="<%=sl_names%>" fvp="<%=fvp%>"/>
			</div>
		</div>
	<!-- /div-->
			<form class="navbar-form navbar-right" role="search" action="">
			<div class="input-group">
				 <input placeholder="机构名称" type="text" name="name" class="form-control" id="ipt_name" value="<c:out value="${sname}"/>"> 
				  <span class="input-group-btn">
				  <input type = "hidden" name = "flfs"  value = "${flfs}"/>
				  <input type = "hidden" name = "flh"  value = "${flh }"/>
				  <input type = "hidden" name = "sl_jsly"  value = "${sl_jsly}"/>
					<button type="submit" class="btn btn-default" id="search_btn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
			</div><!-- /input-group -->
		    </form>
		    
	<!-- /div-->
 </nav> 
<div id="J_selector" class="selector">


<div class="J_selectorLine s-category">
	<div class="sl-wrap">
		<div class="sl-key"><strong>地区：</strong></div>
		<div class="sl-value" style="overflow:visible;margin-left:140px;">
	<div class="bs-chinese-region flat dropdown" data-min-level="1" data-max-level="2" data-def-val="[name=address]">
		<input type="text"  id="address" placeholder="选择你的地区" data-toggle="dropdown" readonly="">
		<div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<div>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#province" data-next="city" role="tab" data-toggle="tab">
							省份
						</a>
					</li>
					<li role="presentation">
						<a href="#city" data-next="district" role="tab" data-toggle="tab">
							城市
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="province">
						--
					</div>
					<div role="tabpanel" class="tab-pane" id="city">
						--
					</div>

				</div>
			</div>
		</div>
	</div>

		</div>
	</div>
</div>

<tags:sl fld_tag="<%=sl_tags[0]%>" fld_name="<%=sl_names[0]%>" fld_sp="<%=sp1%>"  fld_sv="<%=sv1%>"/>
<tags:sl fld_tag="<%=sl_tags[1]%>" fld_name="<%=sl_names[1]%>" fld_sp="<%=sp2%>"  fld_sv="<%=sv2%>"/>


</div>



  	
	<table id="contentTable" class="table table-striped table-bordered table-condensed" style="margin-top:10px;">
		<thead><tr><th><tags:sortth fld_tag="机构代码" fld_name="a.jgdm"/></th>
		<th><tags:sortth fld_tag="机构名称" fld_name="a.mc"/></th>
		<c:choose>
		
		<c:when test="${fsflag == true}">
		<th><tags:sortth fld_tag="复审代理量" fld_name="b.sl"/></th>
		</c:when>
		<c:when test="${wxflag == true}">
		<th><tags:sortth fld_tag="无效代理量" fld_name="b.sl"/></th>
		</c:when>
		<c:otherwise>
		<th><tags:sortth fld_tag="申请代理量" fld_name="b.sl"/></th>
		</c:otherwise>
		</c:choose>
		<th><tags:sortth fld_tag="成立年限" fld_name="a.dt_slsj"/></th>
		<th><tags:sortth fld_tag="代理人人数" fld_name="a.dlrrs"/></th>
		<th><tags:sortth fld_tag="地域" fld_name="a.dy"/></th>
		</tr></thead>
		<tbody>
		<c:forEach items="${objs.content}" var="obj">
			<tr>
				<td>${obj.jgdm}</td>
				<td><a href="${ctx}/dljg/detail/${obj.id}">${obj.mc}</a></td>
				<td>
				<c:if test="${ not empty obj.sl}">
				${obj.sl}
				</c:if>
				<c:if test="${empty obj.sl}">
				0
				</c:if>
				</td>
				<td>${obj.date}</td>
				<td>${obj.dlrrs}</td>
				<td>${obj.dy}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<tags:pagination page="${objs}" paginationSize="5"/>
<script type="text/javascript">
$(document).ready(function(){
	var ipt = $("#address");
	ipt.height(ipt.parent().height());
});
$(function() {
	$.getJSON('/static/json/sql_areas.json',function(data) {

		for (var i = 0; i < data.length; i++) {
			var area = {
				id: data[i].id,
				name: data[i].cname,
				level: data[i].level,
				parentId: data[i].upid
			};
			data[i] = area;
		}

		$('.bs-chinese-region').chineseRegion('source', data).on('completed.bs.chinese-region',
		function(e, areas) {
			$(this).find('[name=address]').val(areas[areas.length - 1].id);
		});
	});

});
</script>

</body>
</html>
