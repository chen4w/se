
<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ page contentType="text/html;charset=UTF-8" import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>代理机构检索</title>
	<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap-chinese-r.js" type="text/javascript"></script>
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-chinese-r.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap-table.css" type="text/css" rel="stylesheet" />
	<script src="${ctx}/static/jquery/jquery.base64v1.1.js" type="text/javascript"></script>
	
	<!-- <script type = "text/javascript" src="${ctx}/static/jquery/jquery-1.10.2.js"></script> 
	<script type = "text/javascript" src="${ctx}/static/jquery/jquery-ui-1.10.4.js"></script>
	<link type="text/css" href="${ctx}/static/jquery/jquery-ui-1.10.4.css" rel="stylesheet" /> -->
	<script type="text/javascript">
	function getActionURL(val) {
		//c4w
		var lhref = location.href;
		var pos1 = lhref.indexOf('name='),pos2;
		var hf1,hf2,pid;
		if(pos1!=-1){
			var dy;
			pos2 = lhref.indexOf('&',pos1);
			if(pos2==-1){
				pos2 = lhref.lastIndexOf('#');
			}
			if(pos2==-1){
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = '';
				dy = lhref.substring(pos1+3);
			}else{
				hf1=lhref.substring(0,pos1)+'name=';
				hf2 = lhref.substring(pos2);
				dy =  lhref.substring(pos1+3,pos2);
			}
			dy = decodeURI(dy);
		}else{
			if(location.search && location.search!='')
				hf1=lhref+'&name=';
			else
				hf1=lhref+'?name=';
			pos2 = lhref.lastIndexOf('#');
			if(pos2==-1){
				hf2 = '';
			}else{
				hf2 = lhref.substring(pos2);
			}
		}
		//c4w end
		return hf1+val+hf2;
	};
	
	function jdnf(){
		var rs1 = $("#nf1").val();
		var rs2 = $("#nf2").val();
		if(rs1 == '' || rs2 == ''){
			alert("请输入数字");
			return false;
		}
		if(isNaN(rs1) || isNaN(rs2)){
			   alert("请输入数字");
			   return false;
		}
		var href = window.location.href;
		var search = window.location.search;
		if(search == ""){
			if(href.indexOf('?') > 0){
				window.location.href =  href + "sl_jdnf=" + rs1 + "-" + rs2;
			}else{
				window.location.href =  href + "?sl_jdnf=" + rs1 + "-" + rs2;
			}
		}else{
			window.location.href =  href + "&sl_jdnf=" + rs1 + "-" + rs2;
		}
		
	}
	
	function getName(){
		var rs1 = $.trim($("#leader").val());
		var rs2 = $.trim($("#examiner").val());
		var href = window.location.href;
		var search = window.location.search;
		if(href.indexOf("?") == -1){
			window.location.href =  href + "?sl_leaderName=" + rs1 + "&" + "sl_examinerName=" + rs2;
		}else{
			if(href.indexOf("sl_leaderName") != -1 && href.indexOf("sl_examinerName") != -1){
				var s = search.substring(1);
				var str = s.split("&");
				var addr = "?";
				for(i=0;i<str.length ;i++ ){
					var temp = str[i].split("=");
					if(temp[0] == "sl_leaderName"){
						temp[1] = rs1;
					}
					if(temp[0] == "sl_examinerName"){
						temp[1] = rs2;
					}
					addr = addr + temp[0] + "=" + temp[1] + "&"
				}
				window.location.href = href.substring(0,href.substring("?")) + addr.substring(0,addr.lastIndexOf("&")); 
			}else{
				window.location.href = href + "&sl_leaderName=" + rs1 + "&sl_examinerName=" + rs2; 
			}
			
		}
	}
	
	function getDLName(){
		var rs1 = $.trim($("#dljgs").val());
		var rs2 = $.trim($("#dlrs").val());
		var href = window.location.href;
		var search = window.location.search;
		if(href.indexOf("?") == -1){
			window.location.href =  href + "?sl_dljgs=" + rs1 + "&" + "sl_dlrs=" + rs2;
		}else{
			if(href.indexOf("sl_dljgs") != -1 && href.indexOf("sl_dlrs") != -1){
				var s = search.substring(1);
				var str = s.split("&");
				var addr = "?";
				for(i=0;i<str.length ;i++ ){
					var temp = str[i].split("=");
					if(temp[0] == "sl_dljgs"){
						temp[1] = rs1;
					}
					if(temp[0] == "sl_dlrs"){
						temp[1] = rs2;
					}
					addr = addr + temp[0] + "=" + temp[1] + "&"
				}
				window.location.href = href.substring(0,href.substring("?")) + addr.substring(0,addr.lastIndexOf("&")); 
			}else{
				window.location.href = href + "&sl_dljgs=" + rs1 + "&sl_dlrs=" + rs2; 
			}
			
		}
	}
	
	
	
	function initialParams(){
		
	    var ajlx = new Map();
	    ajlx.put("review","复审");
	    ajlx.put("invalid","无效");
	    var flsy_fs = new Map();
	    flsy_fs.put("first","第5、25条");
	    flsy_fs.put("second","第9条");
	    flsy_fs.put("third","第2条第三款");
	    flsy_fs.put("fourth","第20条第一款");
	    flsy_fs.put("fifth","第22条");
	    flsy_fs.put("sixth","第26条第三款");
	    flsy_fs.put("seventh","第四款");
	    flsy_fs.put("eighth","第五款");
	    flsy_fs.put("ninth","第31条第一款");
	    flsy_fs.put("tenth","第33条");
	    flsy_fs.put("eleventh","细则第20条第二款");
	    flsy_fs.put("twelfth","分案不满足细则第43条第一款");
	    
	    var flsy_wx = new Map();
	    flsy_wx.put("first","第2条");
	    flsy_wx.put("second","第5、25条");
	    flsy_wx.put("third","第9条");
	    flsy_wx.put("fourth","第20条第一款");
	    flsy_wx.put("fifth","第22条");
	    flsy_wx.put("sixth","第23条");
	    flsy_wx.put("seventh","第26条第三款");
	    flsy_wx.put("eighth","第四款");
	    flsy_wx.put("ninth","第27条第二款");
	    flsy_wx.put("tenth","第33条");
	    flsy_wx.put("eleventh","细则第20条第二款");
	    flsy_wx.put("twelfth","细则第43条第一款");
	    
	    var zllx = new Map();
	    zllx.put("0","发明");
	    zllx.put("1","实用新型");
	    zllx.put("2","外观设计");

	    var jsly = new Map();
	    jsly.put("0","机械");
	    jsly.put("1","电学");
	    jsly.put("2","化学");
	    
	    var jdjl_fs = new Map();
	    jdjl_fs.put("0","维持驳回");
	    jdjl_fs.put("1","撤回驳回");
	    
	    var jdjl_wx = new Map();
	    jdjl_wx.put("0","全部无效");
	    jdjl_wx.put("1","部分无效");
	    jdjl_wx.put("2","维持");
		
	    var param = "";
	    var search = window.location.search;
	    //{"type":["复审","无效"],
	    if(search != ""){
	    	var temp = search.substring(1);
	    	var keyvalue = temp.split("&");
	    	for(var i=0;i<keyvalue.length ;i++ ){
	    		var keys = keyvalue[i].split("=");
	    		if(keys[0] == "sl_ajlx"){
	    			var array = ajlx.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					// alert(ajlx.get(array[j]));
	    					 param +=  "\"type\":" + "[\"" + ajlx.get(array[j]) + "\"]" + ",";
	    				//alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_jdnf"){
	    			if(keys[1].split("-").length == 2){
	    				param +=  "\"juedingyear\":" + "["+"\"" + keys[1].split("-")[0] + "\","+ "\"" + keys[1].split("-")[1] + "\"" +"]" + ",";
	    			}
					if(keys[1].split("-").length == 1){
						param +=  "\"juedingyear\":" + "[\"" + keys[1] + "\"]" + ",";
	    			}
	    			
	    			//alert(param);
	    		}else if(keys[0] == "sl_flsy_fs"){
	    			var array = flsy_fs.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					 //alert(flsy_fs.get(array[j]));
	    					 param +=  "\"lawreason\":" + "[\"" + flsy_fs.get(array[j]) + "\"]" + ",";
	 	    				//alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_flsy_wx"){
	    			var array = flsy_wx.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					// alert(flsy_wx.get(array[j]));
	    				param +=  "\"lawreason\":" + "[\"" + flsy_wx.get(array[j]) + "\"]" + ",";
		 	    		//alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_zllx"){
	    			var array = zllx.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					 //alert(flsy_wx.get(array[j]));
	    					param +=  "\"patentkind\":" + "[\"" + zllx.get(array[j]) + "\"]" + ",";
	 		 	    		//alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_jsly"){
	    			var array = jsly.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					// alert(jsly.get(array[j]));
	    					param +=  "\"jishulinyu\":" + "[\"" + jsly.get(array[j]) + "\"]" + ",";
		 		 	    	//alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_jdjl_fs"){
	    			var array = jdjl_fs.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					// alert(jdjl_fs.get(array[j]));juedingjielun
	    					param +=  "\"juedingjielun\":" + "[\"" + jdjl_fs.get(array[j]) + "\"]" + ",";
			 		 	   // alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_jdjl_wx"){
	    			var array = jdjl_wx.keySet();
	    			 for(var j in array) {
	    				 if(array[j] == keys[1]){
	    					// alert(jdjl_wx.get(array[j]));
	    					 param +=  "\"juedingjielun\":" + "[\"" + jdjl_wx.get(array[j]) + "\"]" + ",";
				 		 	// alert(param);
	    				 }
	    			 }
	    		}else if(keys[0] == "sl_leaderName"){
	    			 param +=  "\"shenpanmaster\":" + "[\"" +decodeURI(keys[1]) + "\"]" + ",";
	    		}else if(keys[0] == "sl_examinerName"){
	    			 param +=  "\"zhushenperson\":" + "[\"" + decodeURI(keys[1]) + "\"]" + ",";
	    		}else if(keys[0] == "sl_dljgs"){
	    			 param +=  "\"agency\":" + "[\"" + decodeURI(keys[1]) + "\"]" + ",";
	    		}else if(keys[0] == "sl_dlrs"){
	    			 param +=  "\"agent\":" + "[\"" + decodeURI(keys[1]) + "\"]" + ",";
	    		}
	    	}
	    }
	    if(param != ""){
	    	 param +=  "\"pagenum\":" + "1" + "," + "\"rowcount\":"+ "10"+ ","+"\"FswxGuid\":"  +"\"\"" + ","+ "\"orderbydate\":" + "\"appdate\"" + ","+ "\"orderby\":" + "\"desc\"" + ",";
	    
	    }
	    //alert(param);
	    return param;
	}
	$(document).ready(function(){
		//检索名称带其他条件		
		$("#search_btn").click(function(e){
			e.preventDefault();
			window.location.href=getActionURL($("#ipt_name").val());
		});
		
		var ipt = $("#address");
		ipt.height(ipt.parent().height());
		if(${divFlag} == true){
			$("#nav").show();
		}else{
			$("#nav").hide();
		}
		if(!$("#nav").is(":hidden")){
			$("body,html").animate({
				   scrollTop:$("#scrollpos").offset().top  //让body的scrollTop等于pos的top，就实现了滚动
			},0);
		}
		
		var url = location.href;
		if(url.indexOf("/invalidreview") !=-1){
			$("#menuTab>ul>li").eq(4).attr("class","active");
		}
		
		//实现tab分页
		$('#searchTab a').click(function(e) {
	        e.preventDefault();//阻止a链接的跳转行为
	        $(this).tab('show');//显示当前选中的链接及关联的content
	    });
		
		var paras = initialParams();
		if(paras != ""){
			//alert('{'+paras.substring(0,paras.lastIndexOf(","))+'}');
			console.log('{'+paras.substring(0,paras.lastIndexOf(","))+'}');
			var s = $.base64.encode('{'+paras.substring(0,paras.lastIndexOf(","))+'}');	
			//var s = $.base64.encode('{"type":["复审","无效"],"titlekeyword":["辐射单元","螺旋钢管自动化2"],"juedingjielun":["维持原决定","维持驳回2"],"juedingyear":["2016","1999","2000","2013"],"lawreason":["专利法第22条第3款","专利法第26条"],"patentkind":["发明","新型","外观"],"jishulinyu":["M","C"],"shenpanmaster":["张","孙卓奇"],"zhushenperson":[],"keywords":["装置","辐射单元"],"agency":["有限公司"],"agent":["张","杨"],"rownum":0,"FswxGuid":"","orderbydate":"appdate","orderby":"desc"}');	
		
		$.ajax({
            type: "get",
            async: false,
            url: "http://103.41.55.29/FSWXSearchHandler/SearchListHandler.ashx?callback=fswx",
            data:{
  		        	 "FswxQuery":s.replace(/"+"/, "%2B")
  		         },
            dataType: "jsonp",
            jsonp: "callback",
            jsonpCallback:"fswx",
            success: function(json){
                for(var i = 0; i < json.FswxSearchCount; i++){
                	$("#newContent").append(json.Fswxitem[i].title);
                	$("#newContent").append("决定号：" + json.Fswxitem[i].juedingnum);
                	$("#newContent").append("决定日：" + json.Fswxitem[i].juedingdate);
                	$("#newContent").append("请求人：" + json.Fswxitem[i].wuxiaoOrfuShenrequestperson);
                	$("#newContent").append("决定结论：" + json.Fswxitem[i].juedingjielun);
                	$("#newContent").append("法律依据：" + json.Fswxitem[i].lawreason);
                	
                }
                
            },
            error: function(){
                alert('接口返回数据类型错误');
            }
        });
		
		}
		
	/*	$("#leader").autocomplete({
			maxRows:10,
			source:function(request,response){
				var name = $("#leader").val();
				//alert(name);
				$.ajax({
					type:"get",
					url:"leader",
					dataType:"json",
					data:{
						"name":name
					},
					success:function(data){
						alert(data.file);
						alert(data.length);
							
							response($.map(data,function(item){
								return{
									label:item.label,
									value:item.value
								};
							}));
					}
					
				});
			}
		
		});*/

		
	
	/*	var firstpage = '<a href = "#">首页</a>';
		var lastpage = '<a href = "#">尾页</a>';
		var  count = 89;
		var pagesize = 10;
		var pagecount = Math.ceil(count/pagesize);
		var currentpage ;
		$("#divpage").append(firstpage);
		for(var i = 1; i <= pagecount; i++){
			var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>'
			$("#divpage").append(page);
		}
		$("#divpage").append(lastpage); */
		
	});
	/*function check(num,count,pagesize){
		$("#divpage").empty();
		var firstpage = '<a href = "#">首页</a>';
		var lastpage = '<a href = "#">尾页</a>';
		$("#divpage").append(firstpage);
		var pagecount = Math.ceil(count/pagesize);
		if(pagecount - num >=9){
			for(var i = num; i <= num + 9; i++){
				var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
				$("#divpage").append(page);
			}
		}else{
			if(pagecount <= 10){
				for(var i = 1;i <= pagecount;i++){
					var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
					$("#divpage").append(page);
				}
			}else{
				for(var i = pagecount - 9;i <= pagecount;i++){
					var page = '<a href="javascript:check('+i+','+count+','+pagesize+')">第'+ i +'页    	</a>';
					$("#divpage").append(page);
				}
			}
			
		}
		$("#divpage").append(lastpage);
	} */
	
	
	
	function Map(){
		this.container = new Object();
	}


	Map.prototype.put = function(key, value){
	this.container[key] = value;
	}


	Map.prototype.get = function(key){
	return this.container[key];
	}
	
	Map.prototype.keySet = function() {
		var keyset = new Array();
		var count = 0;
		for (var key in this.container) {
		// 跳过object的extend函数
		if (key == 'extend') {
		continue;
		}
		keyset[count] = key;
		count++;
		}
		return keyset;
		}

	
</script>
	
</head>

<body>
	<c:if test="${not empty message}">
		<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close">×</button>${message}</div>
	</c:if>

<%
String[] sl_tags= new String[]{"案件类型","决定年份","法律适用","法律适用","专利类型","技术领域","决定结论","决定结论"};
String[] sl_names=new String[]{"sl_ajlx","sl_jdnf","sl_flsy_fs","sl_flsy_wx","sl_zllx","sl_jsly","sl_jdjl_fs","sl_jdjl_wx"};

String[] sp1 = new String[]{"复审","无效"};
String[] sv1 = new String[]{"review","invalid"};

String[] sp2 = new String[]{"2016","2015","2014","2013","2012"};
String[] sv2 = new String[]{"2016","2015","2014","2013","2012"};

String[] sp3 = new String[]{"第5、25条","第9条","第2条第三款","第20条第一款","第22条","第26条第三款","第四款","第五款","第31条第一款","第33条","细则第20条第二款","分案不满足细则第43条第一款"};
String[] sv3 = new String[]{"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth","eleventh","twelfth"};

String[] sp4 = new String[]{"第2条","第5、25条","第9条","第20条第一款","第22条","第23条","第26条第三款","第四款","第27条第二款","第33条","细则第20条第二款","细则第43条第一款"};
String[] sv4 = new String[]{"first","second","third","fourth","fifth","sixth","seventh","eighth","ninth","tenth","eleventh","twelfth"};

String[] sp5 = new String[]{"发明","实用新型","外观设计"};
String[] sv5 = new String[]{"0","1","2"};

String[] sp6 = new String[]{"机械","电学","化学"};
String[] sv6 = new String[]{"0","1","2"};

String[] sp7 = new String[]{"维持驳回","撤回驳回"};
String[] sv7 = new String[]{"0","1"};

String[] sp8 = new String[]{"全部无效","部分无效","维持"};
String[] sv8 = new String[]{"0","1","2"};



String[][] sp = new String[][]{sp1,sp2,sp3,sp4,sp5,sp6,sp7,sp8};
String[][] sv = new String[][]{sv1,sv2,sv3,sv4,sv5,sv6,sv7,sv8};

HashMap<String,String> fvp = new HashMap<String,String>();
for(int i=0; i<sl_names.length; i++){
		String[] v = sv[i];
		String[] p = sp[i];
		for(int j=0; j<v.length; j++){
			fvp.put(sl_names[i]+"."+v[j],p[j]);
		}
}
%>
<div id="scrollpos"></div>            
<nav class="navbar navbar-default" id = nav> <!-- 加 ID = nav 控制显隐性 -->
	<div class="navbar-header" style="margin-top:10px;">
	<div class="crumbs-nav-main clearfix">
			<div class="crumbs-nav-item">
				<div class="crumbs-first"><a href="${ctx}/dljg" style="font-family: '微软雅黑 Bold','微软雅黑';">全部结果</a></div>
			</div>
			<i class="crumbs-arrow">&gt;</i>
			<tags:slcrumb fld_tags="<%=sl_tags%>" fld_names="<%=sl_names%>" fvp="<%=fvp%>"/>
			</div>
		</div>
	
		<!--  <div class="navbar-header pull-right">
			<form class="navbar-form navbar-right" role="search" action="#">
			<div class="input-group">
				 <input placeholder="机构名称" type="text" name="name" class="form-control" id="ipt_name" value="<c:out value="${sname}"/>"> 
				  <span class="input-group-btn">
					<button type="submit" class="btn btn-default" id="search_btn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
			</div>
		    </form>
		 </div>  
	</div> -->
 </nav>
            
<div id="J_selector" class="selector">


<!-- <div class="J_selectorLine s-category">
	<div class="sl-wrap">
		<div class="sl-key"><strong>地区：</strong></div>
		<div class="sl-value" style="overflow:visible;margin-left:140px;">
	<div class="bs-chinese-region flat dropdown" data-min-level="1" data-max-level="2" data-def-val="[name=address]">
		<input type="text"  id="address" placeholder="选择你的地区" data-toggle="dropdown" readonly="">
		<div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<div>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#province" data-next="city" role="tab" data-toggle="tab">
							省份
						</a>
					</li>
					<li role="presentation">
						<a href="#city" data-next="district" role="tab" data-toggle="tab">
							城市
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="province">
						--
					</div>
					<div role="tabpanel" class="tab-pane" id="city">
						--
					</div>

				</div>
			</div>
		</div>
	</div>

		</div>
	</div>
</div>-->

<tags:invalidreview fld_tag="<%=sl_tags[0]%>" fld_name="<%=sl_names[0]%>" fld_sp="<%=sp1%>"  fld_sv="<%=sv1%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[1]%>" fld_name="<%=sl_names[1]%>" fld_sp="<%=sp2%>"  fld_sv="<%=sv2%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[2]%>" fld_name="<%=sl_names[2]%>" fld_sp="<%=sp3%>"  fld_sv="<%=sv3%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[3]%>" fld_name="<%=sl_names[3]%>" fld_sp="<%=sp4%>"  fld_sv="<%=sv4%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[4]%>" fld_name="<%=sl_names[4]%>" fld_sp="<%=sp5%>"  fld_sv="<%=sv5%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[5]%>" fld_name="<%=sl_names[5]%>" fld_sp="<%=sp6%>"  fld_sv="<%=sv6%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[6]%>" fld_name="<%=sl_names[6]%>" fld_sp="<%=sp7%>"  fld_sv="<%=sv7%>"/>
<tags:invalidreview fld_tag="<%=sl_tags[7]%>" fld_name="<%=sl_names[7]%>" fld_sp="<%=sp8%>"  fld_sv="<%=sv8%>"/>


<div class="J_selectorLine s-line">
	<div class="sl-wrap">
	<div class="sl-key"><span><strong>合议组成员：</span></strong></div>
	<div class="sl-value">
		<div class="sl-v-list">
			<ul class="J_valueList">
			组长：<input type = "text"  style="width: 100px ; height: 40px name = "" value = "" id = "leader"/>&nbsp;&nbsp;&nbsp;&nbsp;
			主审员：<input type = "text"  style="width: 100px ; height: 40px name = "" value = "" id = "examiner"/>
			<input type = "button" name = "" value = "确定" onclick = "getName()"/>
			</ul>
		</div>
	</div>
	</div>
</div>

<div class="J_selectorLine s-line">
	<div class="sl-wrap">
	<div class="sl-key"><span><strong>代理：</span></strong></div>
	<div class="sl-value">
		<div class="sl-v-list">
			<ul class="J_valueList">
			代理机构：<input type = "text" style="width: 100px ; height: 40px name = "" value = "" id = "dljgs"/>&nbsp;&nbsp;&nbsp;&nbsp;
			代理人：<input type = "text" style="width: 100px ; height: 40px name = "" value = "" id = "dlrs"/>
			<input type = "button" name = "" value = "确定" onclick = "getDLName()"/>
			</ul>
		</div>
	</div>
	</div>
</div>











</div>



  	
	<%-- <table id="contentTable" class="table table-striped table-bordered table-condensed" style="margin-top:10px;">
		<thead>
			<tr>
				<th><tags:sortth fld_tag="名称" fld_name="a.name"/></th>
				<th><tags:sortth fld_tag="请求人" fld_name="a.patitioner"/></th>
				<th><tags:sortth fld_tag="请求代理人" fld_name="a.requestAgent"/></th>
				<th><tags:sortth fld_tag="请求代理机构" fld_name="a.requestBroker"/></th>
				<th><tags:sortth fld_tag="专利代理人" fld_name="a.patentAgent"/></th>
				<th><tags:sortth fld_tag="专利代理人机构" fld_name="a.patentAgency"/></th>
				<th><tags:sortth fld_tag="法律依据" fld_name="a.legalBasis"/></th>
				<th><tags:sortth fld_tag="决定结果" fld_name="a.determineResult"/></th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${objs.content}" var="obj">
			<tr>
				<td>${obj.name}</td>
				<td><a href="${ctx}/dljg/detail/${obj.patitioner}">${obj.patitioner}</a></td>
				<td>${obj.requestAgent}</td>
				<td>${obj.requestBroker}</td>
				<td>${obj.patentAgent}</td>
				<td>${obj.patentAgency}</td>
				<td>${obj.legalBasis}</td>
				<td>${obj.determineResult}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<tags:pagination page="${objs}" paginationSize="5"/>--%>
	<div id = "newContent">
	
	
	</div>
	<!--<div id = "divpage">
	ssdf
	</div>-->
	
	
</body>
</html>
