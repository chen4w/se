<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<style type="text/css">
</style>

  <div class="alert alert-success fade in" id="alert_succ" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>提问成功</strong> 您的问题已发送给管理员，谢谢您的参与！
  </div>
  <div class="alert alert-warning fade in" id="alert_fail" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>提问失败</strong> 您的问题未正常发送给管理员。
  </div>

<div class="modal fade" id="dlg_zxxx" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">我要提问</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">标题:</label>
            <input type="text" class="form-control" id="ipt_title">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">描述:</label>
            <textarea class="form-control" id="ipt_content"></textarea>
          </div>
          <input type="hidden" id="jid" value="${jid}" />
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="btn_ok">确定</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading" style="background-color:#3B454D;">
  	<img class="img" style="width:20px;height:20px;float:left;" src="${ctx}/static/images/u130.png"/>
    <h3 class="panel-title" style="color:white;">在线咨询</h3>
  </div>
  <div class="panel-body">
    <div class="row">
	  	<div class="col-md-5">	
	  	
	  	<nav class="navbar navbar-default" style="background-color: transparent;background: transparent;border-color: transparent;">
			<button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#dlg_zxxx">我要提问</button>
			
			<tags:pagezx page="${objs}" paginationSize="5"/>
 		</nav>
		
		 
<div class="list-group">
	<c:forEach items="${objs.content}" var="obj" varStatus="loop">
		<c:choose>
	    <c:when test="${loop.index==0}">
	    	<a target="ifrm_remark" href="${ctx}/zxzx/detail/${obj.id}" class="list-group-item active">
	    </c:when>    
	    <c:otherwise>
	    	<a target="ifrm_remark" href="${ctx}/zxzx/detail/${obj.id}" class="list-group-item">
	    </c:otherwise>
</c:choose>
	    <h4 class="list-group-item-heading">${obj.title}</h4>
	    <p class="list-group-item-text">
	    <fmt:formatDate value="${obj.dtCreate}" pattern="yyyy-MM-dd HH:mm:ss"/><br>
		${obj.content}
	    </p>
	  </a>
	</c:forEach> 
</div>
		 
		 
		</div>
	  
  <iframe id="ifrm_remark" name="ifrm_remark" onload="iframeLoaded()" src="${ctx}/zxzx/detail/${pid}"></iframe>
	</div>
  </div>
</div>

<script type = "text/javascript">
function iframeLoaded() {
    var ifrm = document.getElementById('ifrm_remark');
    if(ifrm) {
          // here you can make the height, I delete it first, then I make it again
          ifrm.height = "";
          ifrm.height = ifrm.contentWindow.document.body.scrollHeight + "px";
    }   
}
   $(document).ready(function() {
	   $('#alert_succ').hide();
	  // $('#ifrm_remark').height( $('.row div').height());
	   $('#ifrm_remark').width( Math.floor($('.panel-body div').width()*7/12)-20);
	   $('#ifrm_remark').prop("frameborder", 0);
	    $('.list-group a').click(function(e) {
	        //e.preventDefault()

	        $that = $(this);

	        $that.parent().find('a').removeClass('active');
	        $that.addClass('active');
	    });
       //if submit button is clicked
       $('#btn_ok').click(function() {       
           //Get the data from all the fields
           var val_title = $('#ipt_title').val();
           var val_content = $('#ipt_content').val();
           var val_jid = $('#jid').val();

           jQuery.ajax({
               'type': 'POST',
               'url': '${ctx}/zxzx/create',
               'contentType': 'application/json',
               'data': JSON.stringify({'val_title':val_title,'val_content':val_content,'val_jid':val_jid}),
               'dataType': 'json'
           }).done(function() {
        	   $('#dlg_zxxx').modal('hide');
        	   $('#alert_succ').show();
           })
           .fail(function() {
        	   $('#dlg_zxxx').modal('hide');
        	   $('#alert_fail').show();
           });          
        });
   });     
</script>