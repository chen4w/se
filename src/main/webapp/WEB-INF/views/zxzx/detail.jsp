<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${ctx}/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-body">
  	 <h4 >${pobj.title}</h4>
  	 <p >
		 <fmt:formatDate value="${pobj.dtCreate}" pattern="yyyy-MM-dd HH:mm:ss"/><br>
		${pobj.content}
	 </p>
    
  </div>
<c:if test="${not empty objs}">
 <ul class="list-group">  
	<c:forEach items="${objs}" var="obj">
	    <li class="list-group-item">
	    <p class="list-group-item-text">
		<c:choose>
		    <c:when test="${obj.urole=='admin_jg'}">
		        <img style="width:32px;height:32px;" src="/static/images/u69.png"/>
		    </c:when>    
		    <c:when test="${obj.urole=='admin_xh'}">
		        <img style="width:32px;height:32px;" src="/static/images/u69.png"/>
		    </c:when>    
		    <c:otherwise>
		    	<img style="width:32px;height:32px;" src="/static/images/u73.png"/>
		    </c:otherwise>
		</c:choose>	    
		${obj.content}<br>
		${obj.uname}<br>
		 <fmt:formatDate value="${obj.dtCreate}" pattern="yyyy-MM-dd HH:mm:ss"/>
	    </p>
	    </li>
	</c:forEach>  
 </ul>	
</c:if>	
</div>

</body>