<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
<head>
<meta name="baidu-site-verification" content="xkRQBjUwBx" />
<title>欢迎使用全国专利代理公共服务平台</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<link type="image/x-icon" href="${ctx}/static/images/favicon.ico" rel="shortcut icon">
<link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/static/jquery-validation/1.11.1/validate.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/static/styles/default.css" type="text/css" rel="stylesheet" />
<script src="${ctx}/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery/jquery.placeholder.min.js" type="text/javascript"></script>
<script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery-validation/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctx}/static/jquery-validation/1.11.1/messages_bs_zh.js" type="text/javascript"></script>
</head>
<body>
<%@ include file="/WEB-INF/layouts/toolbar.jsp"%>

<!-- search -->
<div class="idxsecond-header"><!-- 解决屏幕分辨率大于最佳分辨率的背景颜色 -->
<div class="dljgcontainer" id="idxsecond-header"><!-- 解决屏幕分辨率小于最佳分辨率，滚动时背景颜色显示部分 -->
	<div id="idxcontainer" style="background: url(${ctx}/static/images/index_001.jpg) no-repeat center;height:681px;padding-top:80px;">	    
		<div style="background: url(${ctx}/static/images/logo_001.png) no-repeat center;height:238px;margin:0 auto 30px;"></div>
		 <!-- search -->
	    <div class="search-2016">
	    	<ul class="nav nav-tabs" id="searchTab">
			  <li class="active"><a href="#header-dljg" goto="dljg"><img class="index-search-img" src="${ctx}/static/images/index_101.png" />找代理机构</a><div class="nav-direc"><img style="margin-bottom: -3px;" src="${ctx}/static/images/index_0002.png" /></div></li>						  
			  <li><a href="#header-dlr" goto="dlr"><img class="index-search-img" src="${ctx}/static/images/index_102.png" />找代理人</a><div class="nav-direc"><img  style="margin-bottom: -3px;" src="${ctx}/static/images/index_0002.png" /></div></li>
			  <li><a href="#header-ssdlr" goto="ssdlr"><img class="index-search-img" src="${ctx}/static/images/index_103.png" />找诉讼代理人</a><div class="nav-direc"><img  style="margin-bottom: -3px;" src="${ctx}/static/images/index_0002.png" /></div></li>
			  <li><a href="#header-pj" goto="pj"><img class="index-search-img" src="${ctx}/static/images/index_104.png" />复审/无效决定</a><div class="nav-direc"><img  style="margin-bottom: -3px;" src="${ctx}/static/images/index_0002.png" /></div></li>
			  <li><a href="#header-service"><img class="index-search-img" src="${ctx}/static/images/index_105.png" />诉讼裁决</a><div class="nav-direc"><img  style="margin-bottom: -3px;" src="${ctx}/static/images/index_0002.png" /></div></li>		  
			</ul>
			<div class="tab-content">
			     <!-- 代理机构模块 -->
			    <div class="tab-pane active" id="header-dljg">
					<div id="dljgform">
						<div class="form">
							<input placeholder="输入代理机构名称，比如：北京市海淀区专利代理中心"  type="text" onkeydown="javascript:if(event.keyCode==13) search('dljg');" id="dljg" class="stext">
							<button onclick="search('dljg');return false;" class="button"><img class="btn-img" src="${ctx}/static/images/index_107.png" /></button>
							<button onclick="javascript:flsxBtnEvent();" class="lbutton"><img class="btn-img" src="${ctx}/static/images/index_109.png" />分类筛选</button>
						</div>
					</div>
					<div id="flsxform" style="display:none;">
						<div class="form">								
							<div id="flsxTab">
								 <img class="btn-img" width=24px height=24px src="${ctx}/static/images/index_109.png" />
								 <a onclick="javascript:fwlxCheckEvent();"><img class="flsxfwlx-img" src="${ctx}/static/images/index_110.png" />按服务类型</a>								
								 <a onclick="javascript:jslxCheckEvent();"><img class="flsxjslx-img" src="${ctx}/static/images/index_110.png" />按技术类型</a>							          									 
							</div>
							<div id="fwlxlist">
								<table class="table" id="table-nobordered" style="width:549px;margin:0 auto;margin-top:15px;"> 
						        <tr><td width=20%><a href="${ctx}/dljg?sl_fwlx=zlsq">专利申请</a></td> <td width=20%><a href="${ctx}/dljg?sl_fwlx=zlfs">专利复审</a></td><td width=20%><a href="${ctx}/dljg?sl_fwlx=zlwx">专利无效</a></td><td  width=20%><a href="${ctx}/dljg?sl_fwlx=zlxzss">专利行政诉讼</a></td><td  width=20%><a href="${ctx}/dljg?sl_fwlx=zlmsss">专利民事诉讼</a></td></tr>
						        <tr><td width=20%><a href="${ctx}/dljg?sl_fwlx=zlqsjf">专利权属纠纷</a></td> <td width=20%><a href="${ctx}/dljg?sl_fwlx=xzzfaj">行政执法案件</a></td><td width=20%><a href="${ctx}/dljg?sl_fwlx=zljs">专利检索</a></td><td  width=20%><a href="${ctx}/dljg?sl_fwlx=zlfxpy">专利分析评议</a></td><td  width=20%><a href="${ctx}/dljg?sl_fwlx=zlyj">专利预警</a></td></tr>
							    <tr><td width=20%><a href="${ctx}/dljg?sl_fwlx=zldh">专利导航</a></td><td width=20%><a href="${ctx}/dljg?sl_fwlx=zlyy">专利运营</a></td><td width=20%><a href="${ctx}/dljg?sl_fwlx=qyzscqgb">知识产权贯标</a></td><td  width=20%>&nbsp;</td><td  width=20%>&nbsp;</td></tr>
						        </table>	
					        </div>	
					        <div id="jslxlist">
					        	<table class="table" id="table-nobordered" style="width:549px;margin:0 auto;margin-top:15px;"> 
							        <tr>
							        	<td width=33%>
							        		<a href="javascript:showDiv('jslxpplayer3','jslxpplayer_head')">
		                                        <dl class="cate-item" style="margin-top:9px;">
		                                        	<dd><img width="38" height="32" src="/static/images/index_113.png"></dd>							                    
								                    <dd>国民经济产业分类</dd>                                  
								                </dl>
							                </a>
							            </td>
								        <td width=33%>
								        	<a href="javascript:showDiv('jslxpplayer1','jslxpplayer_head')">
								            	<dl class="cate-item" style="margin-top:9px;">
								            		<dd><div style="font-size:32px;height:32px;line-height:32px;font-family: 'Times New Roman Negreta', 'Times New Roman';">IPC</div></dd>
							                    	<dd>国际专利分类</dd>
							                    </dl>
							                </a>
							            </td>
	                                    <td width=33%>
	                                    	<a href="javascript:showDiv('jslxpplayer2','jslxpplayer_head')">
	                                        	<dl class="cate-item" style="margin-top:9px;">
	                                        		<dd><div style="font-size:26px;height:32px;line-height:32px;font-family: 'Times New Roman Negreta', 'Times New Roman';">LOCARNO</div></dd>
							                    	<dd>洛迦诺分类</dd>
							                    </dl>
							                </a>
							            </td>
							        </tr>
						        </table>	
					        </div>																		
						</div>							
					</div>
				</div>
				 <!-- 代理人模块 -->
			    <div class="tab-pane" id="header-dlr">
					<div class="form">
						<input placeholder="输入代理人姓名关键词" type="text" onkeydown="javascript:if(event.keyCode==13) search('dlr');" id="dlr" class="text">
						<button onclick="search('dlr');return false;" class="button"><img class="btn-img" src="${ctx}/static/images/index_107.png" /></button>
					</div>
				</div>
				 <!-- 诉讼代理人模块 -->
			    <div class="tab-pane" id="header-ssdlr">
					<div class="form">
					    <input placeholder="输入诉讼代理人姓名关键词" type="text" onkeydown="javascript:if(event.keyCode==13) search('ssdlr');" id="ssdlr" class="text">
						<button onclick="search('ssdlr');return false;" class="button"><img class="btn-img" src="${ctx}/static/images/index_107.png" /></button>												
					</div>
				</div>
				 <!-- 复审/无效决定模块 -->
			     <div class="tab-pane" id="header-pj">
					<div class="form">
					    <div class="text" style="text-align:center;font-size:20px;margin:0 auto;"><img width=30px height=30px class="btn-img" style="margin-bottom: 5px;" src="${ctx}/static/images/index_0001.png" />&nbsp;&nbsp;当前栏目尚未开放，敬请期待</div>						
						<!--<input placeholder="输入复审/无效决定名称" type="text" onkeydown="javascript:if(event.keyCode==13) search('key');" id="key" class="text">-->
					</div>
				</div>

				 <!-- 诉讼裁决-->
			    <div class="tab-pane" id="header-service">
					<div class="form">
					    <div class="text" style="text-align:center;font-size:20px;margin:0 auto;"><img width=30px height=30px class="btn-img" style="margin-bottom: 5px;" src="${ctx}/static/images/index_0001.png" />&nbsp;&nbsp;当前栏目尚未开放，敬请期待</div>
						<!--<input placeholder="输入诉讼裁决名称" type="text" onkeydown="javascript:if(event.keyCode==13) search('key');" id="key" class="text">-->
					</div>
				</div>					
			</div>
	   </div>
	</div>
</div>
</div>
<!-- content -->
<div class="first_cnt" >
	<div class="dljgcontainer" id="first_cnt">
	      <div class="first_cnt_text">您还需要了解哪些资讯？</div>
	      <div class="first_cnt_tab">
	      <ul class="first_cnt_tab_ul">
	            <li>
	   	        <div class="p-img"><a href="${ctx}/news?lm=专利知识介绍" ><img width="128" height="128" src="${ctx}/static/images/index_201.png" /></a></div>
	                 <div class="p-info"><div class="p-name">专利知识介绍</div></div>
	            </li>
	            <li>
	 			  <div class="p-img"><a href="${ctx}/news?lm=专利委托辅导"><img width="128" height="128" src="${ctx}/static/images/index_202.png"/></a></div>
	                <div class="p-info"><div class="p-name">专利委托辅导</div></div>
	            </li>
	            <li>
	 			<div class="p-img"><a href="${ctx}/news?lm=专利办理流程" ><img width="128" height="128" src="${ctx}/static/images/index_203.png"/></a></div>
	                <div class="p-info"><div class="p-name">专利办理流程</div></div>
	            </li>
	            <li>
	 				<div class="p-img"><a href="${ctx}/news?lm=公益服务" ><img width="128" height="128" src="${ctx}/static/images/index_206.png"/></a></div>
	                <div class="p-info"><div class="p-name">公益服务</div></div>
	            </li>
	           <li>
	  				<div class="p-img"><a href="${ctx}/news?lm=海外维权" ><img width="128" height="128" src="${ctx}/static/images/index_204.png"/></a></div>
	                 <div class="p-info"><div class="p-name">海外维权</div></div>
	           </li>
	           <li>
	  				<div class="p-img"><a href="${ctx}/news?lm=研究报告" ><img width="128" height="128" src="${ctx}/static/images/index_205.png"/></a></div>
	                 <div class="p-info"><div class="p-name">研究报告</div></div>
	          </li>                     
		</ul>
	 </div>
	</div>
</div>
<div class="second_cnt" >
	<div class="dljgcontainer" id="second_cnt">
	      <div class="second_cnt_text">我们能提供哪些服务？</div>
	      <div class="second_cnt_tab">
	        <ul class="second_cnt_tab_ul">
			  <li>
			      <div class="p-img"><img width="64" height="64" src="${ctx}/static/images/index_301.png" /></div>
	              <div class="p-info"><div class="p-name">专业的在线咨询</div></div>
	          </li>
	          <li>
			      <div class="p-img"><img width="64" height="64" src="${ctx}/static/images/index_302.png"/></div>
	              <div class="p-info"><div class="p-name">安全的服务交易</div></div>
	          </li>
	          <li>
			      <div class="p-img"><img width="64" height="64" src="${ctx}/static/images/index_303.png"/></div>
	              <div class="p-info"><div class="p-name">全面的信息检索</div></div>
	          </li>
			</ul>
		  </div>
	</div>
</div>

<!--弹出层--显示技术类型列表明细数据 分类方式1：ipc；2：洛迦诺  3:国民经济 开始-->
<!--  国际专利分类(IPC) 弹出层 -->
<div id="jslxpplayer1" class="header_alertDiv">
	<!-- 关闭按钮-->
	<button class="close header_close" data-dismiss="alert" onclick="closeDiv('jslxpplayer1');">×</button>
	<!-- 内容 -->
	<div class="header_alertDiv_content">
		<div id="jsfltitle" style="float:left; display:inline;">国际专利分类(IPC)</div>
		<div style="width:816px; float:left;">
			<div class="input-group" style='width:360px; padding-top:2px; float:right;'>
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" id="ipcBtn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
				<input placeholder="内容筛选" type="text" name="name" class="form-control" id="ipc_name" onkeydown="javascript:if(event.keyCode==13) searchJsfl(1,this.value);" >
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" name="clearBtn"><i class="glyphicon glyphicon-refresh"></i></button>
				</span>
			</div>
		</div>
		<div id="jsflsubdir">
			<table class="table" id="table-nobordered"> 
	        <tbody id="jsflsubdir1">${ipcsub1}</tbody>
	        </table>
		</div>
		<div id="jsflleafdir">
		  <table class="table" id="table-nobordered" > 
	       <tbody id="jsflleafcnt1">${obj1.ipcA}</tbody> 	   
	      </table>
		</div>		
	</div>
</div>
<!-- 外观设计国际分类(Lacarno) 弹出层 -->
<div id="jslxpplayer2" class="header_alertDiv">
	<!-- 关闭按钮-->
	<button class="close header_close" data-dismiss="alert" onclick="closeDiv('jslxpplayer2');" title="关闭">×</button>
	<!-- 内容 -->
	<div class="header_alertDiv_content" style="min-height:350px;">
		<div id="jsfltitle" style="float:left; display:inline;">外观设计国际分类(Lacarno)</div>
		<div style="width:596px; float:left;">	
			<div class="input-group" style='width:360px; padding-top:2px; float:right;'>
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" id="lacarnoBtn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
				<input placeholder="内容筛选" type="text" name="name" class="form-control" id="lacarno_name"  onkeydown="javascript:if(event.keyCode==13) searchJsfl(2,this.value);">
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" name="clearBtn"><i class="glyphicon glyphicon-refresh"></i></button>
				</span>
			</div>
		</div>
		<div id="jsflleafdir">
		  <table class="table" id="table-nobordered" > 
	        <tbody id="jsflleafcnt2">${jsflleaf2}</tbody> 	    
	      </table>
		</div>		
	</div>
</div>
<!-- 国民经济产业分类 弹出层 -->
<div id="jslxpplayer3" class="header_alertDiv">
	<!-- 关闭按钮-->
	<button class="close header_close" data-dismiss="alert" onclick="closeDiv('jslxpplayer3');">×</button>
	<!-- 内容 -->
	<div class="header_alertDiv_content">
		<div id="jsfltitle" style="float:left; display:inline;">国民经济产业分类</div>
		<div style="width:816px; float:left;">
			<div class="input-group" style='width:360px; padding-top:2px; float:right;'>
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" id="ecomBtn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
				<input placeholder="内容筛选" type="text" name="name" class="form-control" id="ecom_name"  onkeydown="javascript:if(event.keyCode==13) searchJsfl(3,this.value);">
				<span class="input-group-btn">
					<button type="button" class="btn btn-default" name="clearBtn"><i class="glyphicon glyphicon-refresh"></i></button>
				</span>
			</div>
		</div>
		<div id="scomsubdir">
			<table class="table" id="table-nobordered"> 
				<tbody id="jsflsubdir3">${jsflsub3}</tbody>
	        </table>
		</div>
		<div id="jsflleafdir">
		  <table class="table" id="table-nobordered" >
		   <tbody id="jsflleafcnt3">${obj2.ecomA}</tbody> 	               
	      </table>
		</div>		
	</div>
</div>
 
<!-- IPC叶节点内容 -->
<input type="hidden" id="ipcA" value="${obj1.ipcA}" />
<input type="hidden" id="ipcB" value="${obj1.ipcB}" />
<input type="hidden" id="ipcC" value="${obj1.ipcC}" />
<input type="hidden" id="ipcD" value="${obj1.ipcD}" />
<input type="hidden" id="ipcE" value="${obj1.ipcE}" />
<input type="hidden" id="ipcF" value="${obj1.ipcF}" />
<input type="hidden" id="ipcH" value="${obj1.ipcH}" />
<input type="hidden" id="ipcG" value="${obj1.ipcG}" />
<!-- 国民经济叶节点内容 -->
<input type="hidden" id="ecomA" value="${obj2.ecomA}" />
<input type="hidden" id="ecomB" value="${obj2.ecomB}" />
<input type="hidden" id="ecomC" value="${obj2.ecomC}" />
<input type="hidden" id="ecomD" value="${obj2.ecomD}" />
<input type="hidden" id="ecomE" value="${obj2.ecomE}" />
<input type="hidden" id="ecomF" value="${obj2.ecomF}" />
<input type="hidden" id="ecomH" value="${obj2.ecomH}" />
<input type="hidden" id="ecomG" value="${obj2.ecomG}" />
<input type="hidden" id="ecomH" value="${obj2.ecomH}" />
<input type="hidden" id="ecomI" value="${obj2.ecomI}" />
<input type="hidden" id="ecomJ" value="${obj2.ecomJ}" />
<input type="hidden" id="ecomK" value="${obj2.ecomK}" />
<input type="hidden" id="ecomL" value="${obj2.ecomL}" />
<input type="hidden" id="ecomM" value="${obj2.ecomM}" />
<input type="hidden" id="ecomN" value="${obj2.ecomN}" />
<input type="hidden" id="ecomO" value="${obj2.ecomO}" />
<input type="hidden" id="ecomP" value="${obj2.ecomP}" />
<input type="hidden" id="ecomQ" value="${obj2.ecomQ}" />
<input type="hidden" id="ecomR" value="${obj2.ecomR}" />
<input type="hidden" id="ecomS" value="${obj2.ecomS}" />
<input type="hidden" id="ecomT" value="${obj2.ecomT}" />

<!--弹出层--显示技术类型列表明细数据  结束-->
<!-- footer -->
<%@ include file="/WEB-INF/layouts/footer.jsp"%>
</body>
</html>
<script>
$(document).ready(function() {
	$('input, textarea').placeholder();
	
	//实现tab分页
	$('#searchTab a').hover(function(e) {
        e.preventDefault();//阻止a链接的跳转行为       
        $(this).tab('show');//显示当前选中的链接及关联的content
        $(this).focus();//
        
        //tab分页切换事件触发header区块背景图变动
        var hrefId = $(this).attr("href");
        if(hrefId.indexOf("dljg") !=-1){
        	$('#dljgform').css('display','');
        	$('#flsxform').css('display','none');
        	$("#idxcontainer").css("background","url(${ctx}/static/images/index_001.jpg) no-repeat center");
        }else if(hrefId.indexOf("-dlr") !=-1){
        	$("#idxcontainer").css("background","url(${ctx}/static/images/index_002.png) no-repeat center");
        }else if(hrefId.indexOf("ssdlr") !=-1){
        	$("#idxcontainer").css("background","url(${ctx}/static/images/index_003.jpg) no-repeat center");
        }else if(hrefId.indexOf("pj") !=-1){
        	$("#idxcontainer").css("background","url(${ctx}/static/images/index_004.jpg) no-repeat center");
        }else if(hrefId.indexOf("service") !=-1){
        	$("#idxcontainer").css("background","url(${ctx}/static/images/index_005.jpg) no-repeat center");
        }        
    });
	$('#searchTab a').click(function(e) {
        e.preventDefault();//阻止a链接的跳转行为
        
        if ($(this).attr("goto") == "dljg")
        	location.href = "${ctx}/dljg";
        else if ($(this).attr("goto") == "dlr")
            location.href = "${ctx}/dlr";
        else if ($(this).attr("goto") == "ssdlr")
            location.href = "${ctx}/lawsuitsdlr";
        else if ($(this).attr("goto") == "pj")
            location.href = "${ctx}/invalidreview";
	}); 
	
	//add by chenlh 20160519 begin
	//技术类型--分类列表页增加检索功能。
	$('#ipcBtn').click(function(e) {
        var content = $.trim($("#ipc_name").val());
        if (content == "")
        	$("#jsflsubdir1 a").css("border","none");
        searchJsfl(1,content);
	});
	
	$('#lacarnoBtn').click(function(e) {
		var content = $.trim($("#lacarno_name").val());
		searchJsfl(2,content);
	});
	
	$('#ecomBtn').click(function(e) {
		var content = $.trim($("#ecom_name").val());
		if (content == "")
			$("#jsflsubdir3 a").css("border","none");
		searchJsfl(3,content);
	});
	
	$("button[name='clearBtn']").click(function(e) {
		var inputName = $(this).parent().prev("input").attr("id");
		//console.log(inputName);
		if (inputName == "ipc_name") {
			$("#ipc_name").val("");
			$("#jsflsubdir1 a").css("border","none");
			searchJsfl(1,"");
		} else if (inputName == "lacarno_name") {
			$("#lacarno_name").val("");
			searchJsfl(2,"");
		} else if (inputName == "ecom_name") {
			$("#ecom_name").val("");
			$("#jsflsubdir3 a").css("border","none");
			searchJsfl(3,"");
		}
	});
});

//分类筛选按钮事件
function flsxBtnEvent(){
	$('#dljgform').css('display','none');
	$('#flsxform').css('display','');
	$('#fwlxlist').css('display','');
	$('#jslxlist').css('display','none');
	$('.flsxfwlx-img').css('display','');
	$('.flsxjslx-img').css('display','none');
	return false;
}
//勾选按服务类型事件
function fwlxCheckEvent(){
	$('.flsxfwlx-img').css('display','');
	$('.flsxjslx-img').css('display','none');
	$('#fwlxlist').css('display','');	
	$('#jslxlist').css('display','none');
	return false;
}
//勾选按技术类型事件
function jslxCheckEvent(){
	$('.flsxfwlx-img').css('display','none');
	$('.flsxjslx-img').css('display','');
	$('#fwlxlist').css('display','none');	
	$('#jslxlist').css('display','');
	return false;
}
//显示技术分类IPC叶节点内容
function jsflleafcnt1(content){
	var cnt = $("#"+content).val();
	$('#jsflleafcnt1').html(cnt);
}
//显示技术分类国民经济叶节点内容
function jsflleafcnt3(content){
	var cnt = $("#"+content).val();
	$('#jsflleafcnt3').html(cnt);
}
//“检索”按钮事件
function search(id){
    var name = $("#"+id).val();//#gotopagenum是文本框的id属性
    if (name != null) {
    	name = $.trim(name);
    	if(id == "dljg") {
    		if (name != "")
    			location.href = "${ctx}/dljg?name="+name;//代理机构检索页
    		else
    			location.href = "${ctx}/dljg";
    	}
    	else if(id == "dlr") {
    		if (name != "")
    			location.href = "${ctx}/dlr?name="+name;//代理人检索页
    		else
    			location.href = "${ctx}/dlr";
    	}
    	else if(id == "ssdlr") {
    		if (name != "")
    			location.href = "${ctx}/lawsuitsdlr?name="+name;//诉讼代理人检索页
    		else
    			location.href = "${ctx}/lawsuitsdlr";
    	}
    }
}
function showDiv(divlayer,mou_head)
{
	var Idiv     = document.getElementById(divlayer);
	//var mou_head = document.getElementById(mou_head);
	Idiv.style.display = "block";
	//以下部分要将弹出层居中显示
	
	Idiv.style.left=(document.documentElement.clientWidth-Idiv.clientWidth)/2+document.documentElement.scrollLeft+"px";
	Idiv.style.top =(document.documentElement.clientHeight-Idiv.clientHeight)/2+document.documentElement.scrollTop+"px";
	 
	//以下部分使整个页面至灰不可点击
	var procbg = document.createElement("div"); //首先创建一个div
	procbg.setAttribute("id","mybg"); //定义该div的id
	procbg.style.background = "#586EA7";
	procbg.style.width = "100%";
	procbg.style.height = "100%";
	procbg.style.position = "fixed";
	procbg.style.top = "0";
	procbg.style.left = "0";
	procbg.style.zIndex = "500";
	procbg.style.opacity = "0.6";
	procbg.style.filter = "Alpha(opacity=70)";
	//背景层加入页面
	document.body.appendChild(procbg);
	//document.body.style.overflow = "hidden"; //取消滚动条
	if(divlayer == 'jslxpplayer1'){
		$("#jsflsubdir1 a:eq(0)").focus();
	}else if(divlayer == 'jslxpplayer3'){
		$("#jsflsubdir3 a:eq(0)").focus();
	}
	
}
function closeDiv(divlayer) //关闭弹出层
{
	var Idiv=document.getElementById(divlayer);
	Idiv.style.display="none";
	document.body.style.overflow = "auto"; //恢复页面滚动条
	var body = document.getElementsByTagName("body");
	var mybg = document.getElementById("mybg");
	body[0].removeChild(mybg);
}
//add by chenlh 20160519 begin
//技术类型--分类列表页增加检索功能。
function searchJsfl(flfs,content){
	var ctx = '${ctx}';
	 $.ajax({
        type:"post",
        url:"${ctx}/index/searchJsfl",
        data:{ctx: ctx,flfs: flfs,content: content},   
        async:false,
        dataType : 'json',  
        success:function(data){
           if(flfs == 1){
        	   var objIndex = 0;
        	   $("#ipcA").val(data.obj1.ipcA);
        	   $("#ipcB").val(data.obj1.ipcB);
        	   $("#ipcC").val(data.obj1.ipcC);
        	   $("#ipcD").val(data.obj1.ipcD);
        	   $("#ipcE").val(data.obj1.ipcE);
        	   $("#ipcF").val(data.obj1.ipcF);
        	   $("#ipcG").val(data.obj1.ipcG);
        	   $("#ipcH").val(data.obj1.ipcH);  
        	   if(data.obj1.ipcA  != ""){
        		   $("#jsflsubdir1 a:eq(0)").focus();
        		   if (content != "")
        			   $("#jsflsubdir1 a:eq(0)").css("border-left","5px solid crimson");
        		   else
        			   $("#jsflsubdir1 a").css("border","none");
            	   $('#jsflleafcnt1').html(data.obj1.ipcA);            	   
        	   }else if(data.obj1.ipcB  != ""){
        		   $("#jsflsubdir1 a:eq(1)").focus();
        		   $("#jsflsubdir1 a:eq(1)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcB);            	   
        	   }else if(data.obj1.ipcC  != ""){
        		   $("#jsflsubdir1 a:eq(2)").focus();
        		   $("#jsflsubdir1 a:eq(2)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcC);            	   
        	   }else if(data.obj1.ipcD  != ""){
        		   $("#jsflsubdir1 a:eq(3)").focus();
        		   $("#jsflsubdir1 a:eq(3)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcD);            	   
        	   }else if(data.obj1.ipcE  != ""){
        		   $("#jsflsubdir1 a:eq(4)").focus();
        		   $("#jsflsubdir1 a:eq(4)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcE);            	   
        	   }else if(data.obj1.ipcF  != ""){
        		   $("#jsflsubdir1 a:eq(5)").focus();
        		   $("#jsflsubdir1 a:eq(5)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcF);            	   
        	   }else if(data.obj1.ipcG  != ""){
        		   $("#jsflsubdir1 a:eq(6)").focus();
        		   $("#jsflsubdir1 a:eq(6)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcG);            	   
        	   }else if(data.obj1.ipcH  != ""){
        		   $("#jsflsubdir1 a:eq(7)").focus();
        		   $("#jsflsubdir1 a:eq(7)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt1').html(data.obj1.ipcH);            	   
        	   }else{
        		   $("#jsflsubdir1 a:eq(0)").focus();
            	   $('#jsflleafcnt1').html(data.obj1.ipcA); 
        	   }
        	   
           }else if(flfs == 2){
        	   $("#jsflleafcnt2").html(data.jsflleaf2);
           }else if(flfs == 3){
        	   $("#ecomA").val(data.obj2.ecomA);
        	   $("#ecomB").val(data.obj2.ecomB);
        	   $("#ecomC").val(data.obj2.ecomC);
        	   $("#ecomD").val(data.obj2.ecomD);
        	   $("#ecomE").val(data.obj2.ecomE);
        	   $("#ecomF").val(data.obj2.ecomF);
        	   $("#ecomG").val(data.obj2.ecomG);
        	   $("#ecomH").val(data.obj2.ecomH);       	            	   
        	   $("#ecomI").val(data.obj2.ecomI);
        	   $("#ecomJ").val(data.obj2.ecomJ);
        	   $("#ecomK").val(data.obj2.ecomK);
        	   $("#ecomL").val(data.obj2.ecomL);
        	   $("#ecomM").val(data.obj2.ecomM);
        	   $("#ecomN").val(data.obj2.ecomN);
        	   $("#ecomO").val(data.obj2.ecomO);
        	   $("#ecomP").val(data.obj2.ecomP);
        	   $("#ecomQ").val(data.obj2.ecomQ);
        	   $("#ecomR").val(data.obj2.ecomR);
        	   $("#ecomS").val(data.obj2.ecomS);
        	   $("#ecomT").val(data.obj2.ecomT);
        	   if(data.obj2.ecomA  != ""){
        		   $("#jsflsubdir3 a:eq(0)").focus();
        		   if (content != "")
        			   $("#jsflsubdir3 a:eq(0)").css("border-left","5px solid crimson");
        		   else
        			   $("#jsflsubdir3 a").css("border","none");
        		   $('#jsflleafcnt3').html(data.obj2.ecomA);            	   
        	   }else if(data.obj2.ecomB  != ""){
        		   $("#jsflsubdir3 a:eq(1)").focus();
        		   $("#jsflsubdir3 a:eq(1)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomB);            	   
        	   }else if(data.obj2.ecomC  != ""){
        		   $("#jsflsubdir3 a:eq(2)").focus();
        		   $("#jsflsubdir3 a:eq(2)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomC);            	   
        	   }else if(data.obj2.ecomD  != ""){
        		   $("#jsflsubdir3 a:eq(3)").focus();
        		   $("#jsflsubdir3 a:eq(3)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomD);            	   
        	   }else if(data.obj2.ecomE  != ""){
        		   $("#jsflsubdir3 a:eq(4)").focus();
        		   $("#jsflsubdir3 a:eq(4)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomE);            	   
        	   }else if(data.obj2.ecomF  != ""){
        		   $("#jsflsubdir3 a:eq(5)").focus();
        		   $("#jsflsubdir3 a:eq(5)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomF);            	   
        	   }else if(data.obj2.ecomG  != ""){
        		   $("#jsflsubdir3 a:eq(6)").focus();
        		   $("#jsflsubdir3 a:eq(6)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomG);            	   
        	   }else if(data.obj2.ecomH  != ""){
        		   $("#jsflsubdir3 a:eq(7)").focus();
        		   $("#jsflsubdir3 a:eq(7)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomH);            	   
        	   }else if(data.obj2.ecomI  != ""){
        		   $("#jsflsubdir3 a:eq(8)").focus();
        		   $("#jsflsubdir3 a:eq(8)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomI);            	   
        	   }else if(data.obj2.ecomJ  != ""){
        		   $("#jsflsubdir3 a:eq(9)").focus();
        		   $("#jsflsubdir3 a:eq(9)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomJ);            	   
        	   }else if(data.obj2.ecomK  != ""){
        		   $("#jsflsubdir3 a:eq(10)").focus();
        		   $("#jsflsubdir3 a:eq(10)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomK);            	   
        	   }else if(data.obj2.ecomL  != ""){
        		   $("#jsflsubdir3 a:eq(11)").focus();
        		   $("#jsflsubdir3 a:eq(11)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomL);            	   
        	   }else if(data.obj2.ecomM  != ""){
        		   $("#jsflsubdir3 a:eq(12)").focus();
        		   $("#jsflsubdir3 a:eq(12)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomM);            	   
        	   }else if(data.obj2.ecomN  != ""){
        		   $("#jsflsubdir3 a:eq(13)").focus();
        		   $("#jsflsubdir3 a:eq(13)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomN);            	   
        	   }else if(data.obj2.ecomO  != ""){
        		   $("#jsflsubdir3 a:eq(14)").focus();
        		   $("#jsflsubdir3 a:eq(14)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomO);            	   
        	   }else if(data.obj2.ecomP  != ""){
        		   $("#jsflsubdir3 a:eq(15)").focus();
        		   $("#jsflsubdir3 a:eq(15)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomP);            	   
        	   }else if(data.obj2.ecomQ  != ""){
        		   $("#jsflsubdir3 a:eq(16)").focus();
        		   $("#jsflsubdir3 a:eq(16)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomQ);            	   
        	   }else if(data.obj2.ecomR  != ""){
        		   $("#jsflsubdir3 a:eq(17)").focus();
        		   $("#jsflsubdir3 a:eq(17)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomR);            	   
        	   }else if(data.obj2.ecomS  != ""){
        		   $("#jsflsubdir3 a:eq(18)").focus();
        		   $("#jsflsubdir3 a:eq(18)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomS);            	   
        	   }else if(data.obj2.ecomT  != ""){
        		   $("#jsflsubdir3 a:eq(19)").focus();
        		   $("#jsflsubdir3 a:eq(19)").css("border-left","5px solid crimson");
            	   $('#jsflleafcnt3').html(data.obj2.ecomT);            	   
        	   }else{
        		   $("#jsflsubdir3 a:eq(0)").focus();
        		   $('#jsflleafcnt3').html(data.obj2.ecomA);
        	   }        	   
           }           
        },
        error:function(e){
       	    alert("内容筛选失败");
        }                 
    });
}
</script>
