<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>代理人详情</title>
</head>

<body>
	<div style="background-color: black; color: white;">欢迎使用中华全国专利代理公共服务平台</div>

	<form id="inputForm" action="${ctx}/task/${action}" method="post" class="form-horizontal">
		
	<input type="hidden" name="id" value="${dlr.id}"/>
	
	<fieldset>
	<legend><small>您当前位置：专利代理人-代理人信息详览</small></legend>
	
	<div class="control-group">
			<p>个人简介---------------------------------------------------------</p>
				<table for="task_title" class="table table-striped table-bordered table-condensed">
					<tr>
						<td>姓名</td>
						<td>${dlr.xm}</td>
						<td>性别</td>
						<td>${dlr.xb}</td>
						<td>技术领域</td>
						<td>${dlr.jsly}</td>
					</tr>
					<tr>
						<td>代理人资格证号</td>
						<td>${dlr.zgzh}</td>
						<td>代理人职业证号</td>
						<td>${dlr.zyzh}</td>
						<td>代理人资格</td>
						<td></td>
					</tr>
					<tr>
						<td>工作外语</td>
						<td></td>
						<td>工作经验</td>
						<td>${dlr.dtGzjy}</td>
						<td>最高学历</td>
						<td>${dlr.zgxl}</td>
					</tr>
					<tr>
						<td>所在城市</td>
						<td>${dlr.szcs}</td>
						<td>是否有海外留学经验</td>
						<td>${dlr.bhwlx}</td>
						<td>服务机构</td>
						<td>${dlr.pid}</td>
					</tr>
					<tr>
						<td>惩戒记录</td>
						<td>${dlr.bcjjl}</td>
						<td>职称</td>
						<td>${dlr.zc}</td>
					</tr>
				</table>

			
				<p>服务类型---------------------------------------------------------</p>
				<table for="task_title" class="table table-striped table-bordered table-condensed">
					<tr>
						<td>国内撰写：</td>
						<td></td>
					</tr>
					<tr>
						<td>国内/国外申请：</td>
						<td></td>
					</tr>
				</table>
			
				<p>个人履历---------------------------------------------------------</p>
				<table for="task_title" class="table table-striped table-bordered table-condensed">
					<tr>
						<td>工作经历：</td>
						<td>${dlr.gzjl}</td>
					</tr>
					<tr>
						<td>教育经历：</td>
						<td>${dlr.jyjl}</td>
					</tr>
					<tr>
						<td>培训经历：</td>
						<td>${dlr.pxjl}</td>
					</tr>
					<tr>
						<td>出版物：</td>
						<td></td>
					</tr>
					<tr>
						<td>学术研究：</td>
						<td>${dlr.xsyj}</td>
					</tr>
					<tr>
						<td>项目类业务：</td>
						<td>${dlr.xmlyw}</td>
					</tr>
				</table>
			
				<p>其他---------------------------------------------------------</p>
				<table for="task_title" class="table table-striped table-bordered table-condensed">
					<tr>
						<td>个人荣誉：</td>
						<td>${dlr.grry}</td>
					</tr>
					<tr>
						<td>年度考核：</td>
						<td>${dlr.ndkh}</td>
					</tr>
				</table>
			</div>
			
			<div class="form-actions">
				<input id="cancel_btn" class="btn" type="button" value="返回"
					onclick="history.back()" />
			</div>
	</fieldset>
		
	</form>
	
	<script>
		$(document).ready(function() {
			//聚焦第一个输入框
			$("#task_title").focus();
			//为inputForm注册validate函数
			$("#inputForm").validate();
		});
	</script>
</body>
</html>