<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
request.setAttribute("wrap_n", "\n");
%>
<html>
<head>
	<title>代理人详细</title>
    <script src="${ctx}/static/echarts3/echarts.min.js" type="text/javascript"></script>
</head>
<body>
<form id="inputForm" action="${ctx}/details/${action}" method="post" class="form-horizontal">
<input type="hidden" name="id" value="${obj.id}"/>
<!-- div class="bigtitle">代理人详细</div -->
<div style="overflow:hidden;padding-top:5px;">
      <div class="smalltitle" style=""><img class="title-img" src="${ctx}/static/images/dlr_001.png" />个人简介</div>
      <div class="div-padding">
            <table class="table" id="table-nobordered"> 
	        <tr><td width=15%>姓名：</td> <td width=35%>${obj.xm}</td><td width=15%>性别：</td><td width=35%>${obj.xb}</td></tr>
	        <tr><td>资格证号：</td> <td>${obj.zgzh}</td><td>执业证号：</td><td>${obj.zyzh}</td></tr>
	        <tr><td>资质：</td> <td>${dlrzg}</td><td>工作经验：</td><td>${gzjy}年</td></tr>
	        <tr><td>工作外语：</td> <td>${obj.wynl}</td><td>所在城市：</td><td>${dy}</td></tr> 
	        <tr><td>代理人协会职务：</td> <td>${dlrxhzc}</td><td>其他社会职务：</td><td>${qtshzw}</td></tr> 
	        <tr><td>所在机构：</td> <td><a href="${ctx}/dljg/detail/${pid}">${fwmc}</a></td><td>惩戒记录：</td>
	        	<c:choose>
					<c:when test="${jcjl=='有'}">
					  <td><a href="${ctx}${url_dlr_attach}${obj.xm}.rar">${jcjl}</a></td>
					</c:when>
					<c:otherwise>
				      <td>${jcjl}</td>
				    </c:otherwise>
				</c:choose>
	        </tr> 
	        <tr><td>所学专业：</td> <td>${obj.sxzy}</td><td>年度考核：</td><td>${obj.ndkh}</td></tr>   
	       </table>
      </div>	               
</div>
<div style="margin-top:15px;">
      <div class="smalltitle" style=""><img class="title-img" src="${ctx}/static/images/dlr_002.png" />累计代理案件数量</div>
      <div class="div-padding">
      		<!-- 0426隐藏内容
            <div>
	            <table class="table" id="table-nobordered"> 
		        <tr><td>授权量：</td> <td>${finalMap.sql}</td><td>&nbsp;</td><td>&nbsp;</td></tr>
		        <tr><td>平均权利要求项数：</td><td>${finalMap.qls}</td><td>平均说明书页数：</td><td>${finalMap.sms}</td></tr>
		        <tr><td>专利复审：</td> 
		        <td>
		        	<a href="#" data-toggle="modal" data-target="#fswx_modal" onclick="$('#fswx_iframe').attr('src','${ctx}/fswx/dlr/${obj.id}/1')">${thirdMap.fs}</a>
		        </td>
		        <td>专利无效：</td>
		        <td>
		        	<a href="#" data-toggle="modal" data-target="#fswx_modal" onclick="$('#fswx_iframe').attr('src','${ctx}/fswx/dlr/${obj.id}/2')">${thirdMap.wx}</a>
		        </td></tr>
		        <tr><td>专利行政诉讼：</td>
		        <td>
		        	<a href="#" onclick="loginZCB(1,1,'${obj.xm}')">${secondMap.msajsl}</a>
		        </td>
		        <td>专利民事诉讼：</td>
		        <td>
		        	<a href="#" onclick="loginZCB(1,2,'${obj.xm}')">${secondMap.xzajsl}</a>
		        </td></tr>  
		       </table>
	       </div -->
	       <div>
	            <table class="table" id="table-nobordered">
		        	<tr>
		        		<td>平均权利要求项数：</td>
		        		<td>${finalMap.qls}</td>
		        		<td>平均说明书页数：</td>
		        		<td>${finalMap.sms}</td>
		        	</tr>
		        	<tr>
		        		<td><span class="glyphicon glyphicon-bookmark" style="cursor:help;" data-toggle="tooltip" title="点击数字可查看决定正文"></span>专利复审：</td>
		        		<td><a href="#" data-toggle="modal" data-target="#fswx_modal" onclick="$('#fswx_iframe').attr('src','${ctx}/fswx/dlr/${obj.id}/1')">${thirdMap.fs}</a></td>
		        		<td><span class="glyphicon glyphicon-bookmark" style="cursor:help;" data-toggle="tooltip" title="点击数字可查看决定正文"></span>专利无效：</td>
		        		<td><a href="#" data-toggle="modal" data-target="#fswx_modal" onclick="$('#fswx_iframe').attr('src','${ctx}/fswx/dlr/${obj.id}/2')">${thirdMap.wx}</a></td>
		        	</tr>
		        	<tr>
		        		<td><span class="glyphicon glyphicon-bookmark" style="cursor:help;" data-toggle="tooltip" title="点击数字可查看裁决文书"></span>专利行政诉讼：</td>
		        		<td><a href="#" onclick="loginZCB(1,1,'${obj.xm}')">${secondMap.msajsl}</a></td>
		        		<td><span class="glyphicon glyphicon-bookmark" style="cursor:help;" data-toggle="tooltip" title="点击数字可查看裁决文书"></span>专利民事诉讼：</td>
		        		<td><a href="#" onclick="loginZCB(1,2,'${obj.xm}')">${secondMap.xzajsl}</a></td>
		        	</tr>  
		       </table>
	       </div>
	       <div style="width:1%;float:left;">
	       		<!-- div style="padding-top:2px; cursor:help;"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="bottom" title="点击图表可查看专利申请详情"></span></div -->
	       </div>
	       <div style="width:49%;float:left;">
	            <div id="secondfwqchart" style="width:400px;height:300px;margin:15px auto;"></div>
	       </div>
	       <div style="width:1%;float:left;">
	       		<div style="padding-top:17px; padding-left:55px; cursor:help;"><span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="bottom" title="点击图表可查看专利申请详情"></span></div>
	       </div>
	       <div style="width:49%;float:right;">
	       		<div id="firstfwqchart" style="width:400px;height:300px;margin:15px auto;"></div>
	       </div>
	       <!-- div style="width:50%;">
		       <table class="table table-bordered table-striped"> 
		       <tbody>					
		        <tr><td>&nbsp;</td><td>发明</td> <td>实用新型</td><td>外观设计</td></tr> 			       
		        <tr><td>国内</td><td>${finalMap.fmgn}</td> <td>${finalMap.syxxgn}</td><td>${finalMap.wggn}</td></tr>
		        <tr><td>海外</td><td>${finalMap.fmgw}</td> <td>${finalMap.syxxgw}</td><td>${finalMap.wggw}</td></tr>
		        <tr><td>港澳台</td><td>${finalMap.fmgat}</td> <td>${finalMap.syxxgat}</td><td>${finalMap.wggat}</td></tr>		        		         
		        </tbody> 
		       </table>								       
	       </div -->
      </div>	               
</div>
<div id="third-dlr">
      <div style="float:left;width:33%;">
	      <div class="smalltitle"><img class="title-img" src="${ctx}/static/images/dlr_003.png" />个人荣誉</div>
	      <div class="div-padding">
			<c:choose>
	            <c:when test="${!empty ljrw}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	代理人协会高层次及领军人才：</div>
		            <div class="content">${ljrw}</div>
		            <hr />
	            </c:when>
	            <c:when test="${!empty gzjrc}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	国知局高层次、领军及专家库人才：</div>
		            <div class="content">${gzjrc}</div>
	            	<hr />
	            </c:when>
	            <c:when test="${!empty gjjlqk}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	获得与知识产权有关的国家级奖励情况：</div>
		            <div class="content">${gjjlqk}</div>
	            	<hr />
	            </c:when>
	            <c:when test="${!empty sbjjlqk}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	获得与知识产权有关的省部级奖励情况：</div>
		            <div class="content">${sbjjlqk}</div>
	            </c:when>
	        </c:choose>        
	      </div>
      </div>
      <div style="float:left;width:33%;margin-left:1%;">	
	      <div class="smalltitle" style=""><img class="title-img" src="${ctx}/static/images/dlr_004.png" />学术成果</div>
	      <div class="div-padding">
	      	<c:choose>
	            <c:when test="${!empty fbxslw}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	发表学术论文情况：</div>
		            <div class="content"><c:out value="${fn:replace(fbxslw, wrap_n, '<br/>')}" escapeXml="false"/></div>
		            <hr />
	            </c:when>
	            <c:when test="${!empty cbxszz}">
		            <div class="input-large"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	出版学术专著情况：</div>
		            <div class="content"><c:out value="${fn:replace(cbxszz, wrap_n, '<br/>')}" escapeXml="false"/></div>
		        </c:when>
		    </c:choose>
	      </div>	
      </div>
      <div style="float:right;width:32%;margin-left:1%;">
	      <div class="smalltitle" style=""><img class="title-img" src="${ctx}/static/images/dlr_005.png" />其他</div>
	      <div class="div-padding">
	      	<c:choose>
	            <c:when test="${!empty cdzdxm}">
		            <div class="input-large" style="margin-right: -20px;"><img class="title-img" src="${ctx}/static/images/dlr_006.png" style="margin-right: 10px;"/>
		            	承担国知局、协会、省局、重大项目情况：</div>
		            <div class="content">${cdzdxm}</div>
	            </c:when>
	        </c:choose>
	      </div>	
	  </div>
</div>
</form>
<script>
	$(document).ready(function() {
		//1.柱状图
		//基于准备好的dom，初始化echarts实例
		var secondfwqchart = echarts.init(document.getElementById('secondfwqchart'));

        // 指定图表的配置项和数据
        opt_secondfwqchart = {
        		title : {
    		        text: '近五年专利申请代理量 ',
    		        subtext: '依据申请日统计'
    		    },
        	    tooltip : {
        	        trigger: 'axis',
        	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
        	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        	        }
        	    },
        	    legend: {
        	        data:['国内', '海外','港澳台'],
        	        orient: 'vertical',
        	        right: 0
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '20%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : ['发明','实用新型','外观设计']
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'国内',
        	            type:'bar',
        	            data:['${finalMap.fmgn}', '${finalMap.syxxgn}', '${finalMap.wggn}']
        	        },
        	        {
        	            name:'海外',
        	            type:'bar',
        	            data:['${finalMap.fmgw}', '${finalMap.syxxgw}', '${finalMap.wggw}']
        	        },
        	        {
        	            name:'港澳台',
        	            type:'bar',
        	            data:['${finalMap.fmgat}', '${finalMap.syxxgat}', '${finalMap.wggat}']
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        secondfwqchart.setOption(opt_secondfwqchart);
        
        
        var firstfwqchart = echarts.init(document.getElementById('firstfwqchart'));

        // 指定图表的配置项和数据
        opt_firstfwqchart = {
        		title : {
        			text: '近五年专利申请授权量 ',
    		        subtext: '依据授权公告日统计    【点击图表可查看专利申请详情】'
    		    },
        	    tooltip : {
        	        trigger: 'item'
        	    },
        	    legend: {
        	        data:['发明', '实用新型','外观设计'],
        	        orient: 'vertical',
        	        right: 0
        	    },
        	    grid: {
        	        left: '3%',
        	        right: '23%',
        	        bottom: '3%',
        	        containLabel: true
        	    },
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : []
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'发明',
        	            type:'bar',
        	            data:['${finalMap.fmsql}']
        	        },
        	        {
        	            name:'实用新型',
        	            type:'bar',
        	            data:['${finalMap.syxxsql}']
        	        },
        	        {
        	            name:'外观设计',
        	            type:'bar',
        	            data:['${finalMap.wgsql}']
        	        }
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        firstfwqchart.setOption(opt_firstfwqchart);
        //------------------------ 结束------
      
		//聚焦第一个输入框
		//$("#task_title").focus();
        
        // add by zhangqiuyi 20160527 begin
        firstfwqchart.on('click', function(params) {
	    	var lx = params.seriesIndex;
	    	if (lx == "0")
	    		lx = "FM";
	    	else if (lx == "1")
	    		lx = "XX";
	    	else if (lx == "2")
	    		lx = "WG";
	    	var gd = "20110101>20151231";
	    	var at = $.trim('${obj.xm}');
	    	var ag = $.trim('${jgdm}');
			if (at && ag)
				window.open('http://www.patentstar.com.cn/My/frmCnTbSearch4dlr.aspx?gd=' + gd + '&ag=' + ag + '&at=' + encodeURI(encodeURI(at)) + '&lx=' + lx);	
	    });
        
        $("[data-toggle='tooltip']").tooltip();
	// end
	});
//add by wsx 20160420 begin
function loginZCB(type,casetype,content){
	 $.ajax({
        type:"post",
        url:"${ctx}/zcb",
        dataType : 'json',  
        data:{type:type,casetype:casetype,content:content},   
        async:false,
        success:function(e){
           if(e.status=='succ'){
           	window.open(e.url,'_blank');
           }else{
           	alert(e.msg);
           	window.open(e.url,'_blank');
           }
        },error:function(e){
       	    //alert(1);
        }                 
    });
}
//add by wsx 20160420 end
</script>
<!-- 查看复审无效 add by wsx 20160419 begin -->
<div class="modal fade" id="fswx_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">复审/无效</h4>
      </div>
      <div class="modal-body">
        <iframe id="fswx_iframe" src="" width="570" height="500" frameborder="no" scrolling="no"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- button type="button" class="btn btn-primary">Save changes</button-->
      </div>
    </div>
  </div>
</div>
<!-- 查看复审无效 add by wsx 20160419 end -->

</body>
</html>
