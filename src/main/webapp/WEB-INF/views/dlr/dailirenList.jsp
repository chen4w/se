<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html>

<head>
	<title>专利代理人-搜索页面</title>
</head>

<body>

  	<c:if test="${not empty message}">
		<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close">×</button>${message}</div>
	</c:if>
	<div id="J_selector" class="selector">
		<div class="J_selectorLine s-category">
			<div class="sl-wrap">
				<div class="sl-key"><strong>地域：</strong></div>
				<div class="sl-value" style="overflow:visible;margin-left:140px;">
				<div class="bs-chinese-region flat dropdown" data-min-level="1" data-max-level="3" data-def-val="[name=address]">
					<input type="text"  id="address" placeholder="选择你的地区" data-toggle="dropdown" readonly="">
					<input type="hidden" class="form-control" name="address" value="110100">
					<div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<div>
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active">
									<a href="#province" data-next="city" role="tab" data-toggle="tab">
										省份
									</a>
								</li>
								<li role="presentation">
									<a href="#city" data-next="district" role="tab" data-toggle="tab">
										城市
									</a>
								</li>
								<li role="presentation">
									<a href="#district" data-next="street" role="tab" data-toggle="tab">
										县区
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="province">
									--
								</div>
								<div role="tabpanel" class="tab-pane" id="city">
									--
								</div>
								<div role="tabpanel" class="tab-pane" id="district">
									--
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			
		 <% 
		 	String[] sl_tags= new String[]{"执业年限","资质","协会职务","工作语言","业务类型","服务类型","代理量","平均权利要求项数","平均说明书页数","专利类型","技术领域"  };
			String[] sl_names=new String[]{"sl_zynx","sl_zz","sl_xhzw","sl_gzyy","sl_ywlx","sl_fwlx","sl_dll" ,"sl_pjqlyqxs" ,"sl_pjsmsys","sl_zllx","sl_jsly" };
			
			String[] sp1 = new String[]{"3年以下","4-5年","5-10年","11-15年","16-20年","16-20年以上"};
			String[] sv1 = new String[]{"3lt","4-5","5-10","11-15","16-20","20gt"};
			
			String[] sp2 = new String[]{"行政诉讼代理人","民事诉讼代理人","领军人物","高层次人才"};
			String[] sv2 = new String[]{"1","2","3","4"};
			
			String[] sp3 = new String[]{"会长","理事","常务理事","会员"};
			String[] sv3 = new String[]{"1","2","3","4"};
			
			String[] sp4 = new String[]{"英语","日语","韩语","德语","法语","其他"};
			String[] sv4 = new String[]{"1","2","3","4","5","6"};
			
			String[] sp5 = new String[]{"国内撰写","海外申请","PCT申请","港澳台申请"};
			String[] sv5 = new String[]{"1","2","3","4"};
			
			String[] sp6 = new String[]{"专利申请","专利复审","专利无效","专利行政诉讼","专利民事诉讼","专利权属纠纷","专利检索","专利分析","专利运营","专利导航","专利分析评议","企业知识产权贯标认证/资讯"};
			String[] sv6 = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12"};
			
			String[] sp7 = new String[]{"50件一下","51-100件","101-200件","201-500件","501件以上"};
			String[] sv7 = new String[]{"50lt","51-100","101-200","201-500","501gt"};
			
			String[] sp8 = new String[]{"10项以下","10项以上"};
			String[] sv8 = new String[]{"10lt","10gt"};
			
			String[] sp9 = new String[]{"5页以下","6-10页","11-15页","16-20页","21页以上"};
			String[] sv9 = new String[]{"5lt","6-10","11-15","16-20","21gt"};
			
			String[] sp10 = new String[]{"发明","实用新型","外观设计"};
			String[] sv10 = new String[]{"1","2","3"};
			
			String[] sp11 = new String[]{"机械","电子通讯","化学.生物.医"};
			String[] sv11 = new String[]{"1","2","3"};
			
			
			String[][] sp = new String[][]{sp1,sp2,sp3,sp4,sp5,sp6,sp7,sp8,sp9,sp10,sp11};
			String[][] sv = new String[][]{sv1,sv2,sv3,sv4,sv5,sv6,sv7,sv8,sv9,sv10,sv11};
			
			HashMap<String,String> fvp = new HashMap<String,String>();
			for(int i=0; i<sl_names.length; i++){
				String[] v = sv[i];
				String[] p = sp[i];
				for(int j=0; j<v.length; j++){
					fvp.put(sl_names[i]+"."+v[j],p[j]);
				}
			}
		 %>
		</div>

			 <tags:sl fld_tag="<%=sl_tags[0]%>"  fld_name="<%=sl_names[0]%>"   fld_sp="<%=sp1%>"  fld_sv="<%=sv1%>"/>
			 <tags:sl fld_tag="<%=sl_tags[1]%>"  fld_name="<%=sl_names[1]%>"   fld_sp="<%=sp2%>"  fld_sv="<%=sv2%>"/>
			 <tags:sl fld_tag="<%=sl_tags[2]%>"  fld_name="<%=sl_names[2]%>"   fld_sp="<%=sp3%>"  fld_sv="<%=sv3%>"/>
			 <tags:sl fld_tag="<%=sl_tags[3]%>"  fld_name="<%=sl_names[3]%>"   fld_sp="<%=sp4%>"  fld_sv="<%=sv4%>"/>
			 <tags:sl fld_tag="<%=sl_tags[4]%>"  fld_name="<%=sl_names[4]%>"   fld_sp="<%=sp5%>"  fld_sv="<%=sv5%>"/>
			 <tags:sl fld_tag="<%=sl_tags[5]%>"  fld_name="<%=sl_names[5]%>"   fld_sp="<%=sp6%>"  fld_sv="<%=sv6%>"/>
			 <tags:sl fld_tag="<%=sl_tags[6]%>"  fld_name="<%=sl_names[6]%>"   fld_sp="<%=sp7%>"  fld_sv="<%=sv7%>"/>
			 <tags:sl fld_tag="<%=sl_tags[7]%>"  fld_name="<%=sl_names[7]%>"   fld_sp="<%=sp8%>"  fld_sv="<%=sv8%>"/>
			 <tags:sl fld_tag="<%=sl_tags[8]%>"  fld_name="<%=sl_names[8]%>"   fld_sp="<%=sp9%>"  fld_sv="<%=sv9%>"/>
			 <tags:sl fld_tag="<%=sl_tags[9]%>"  fld_name="<%=sl_names[9]%>"   fld_sp="<%=sp10%>" fld_sv="<%=sv10%>"/>
			 <tags:sl fld_tag="<%=sl_tags[10]%>" fld_name="<%=sl_names[10]%>"  fld_sp="<%=sp11%>" fld_sv="<%=sv11%>"/>

	</div>
	<nav class="navbar navbar-default">
<div class="container-fluid">
	<div class="navbar-header" style="margin-top:10px;">
	<div class="crumbs-nav-main clearfix">
			<div class="crumbs-nav-item">
				<div class="crumbs-first"><a href="?">全部结果</a></div>
			</div>
			<i class="crumbs-arrow">&gt;</i>
			<tags:slcrumb fld_tags="<%=sl_tags%>" fld_names="<%=sl_names%>" fvp="<%=fvp%>"/>
			</div>
		</div>
	</div>
			<form class="navbar-form navbar-right" role="search" action="#">
			<div class="input-group">
				 <input placeholder="机构名称" type="text" name="search_LIKE_mc" class="form-control" value="${param.search_LIKE_mc}"> 
				  <span class="input-group-btn">
					<button type="submit" class="btn btn-default" id="search_btn"><i class="glyphicon glyphicon-search"></i></button>
				</span>
			</div><!-- /input-group -->
		    </form>
		</div>
	 </nav>
	 
	 
	 <table id="contentTable" class="table table-striped table-bordered table-condensed" style="margin-top:10px;">
		<thead>
		<tr>
			<!--  <th>
			<tags:sortth fld_tag="机构代码" fld_name="jgdm"/>
			</th>
			<th>
			<tags:sortth fld_tag="机构名称" fld_name="mc"/>
			</th>
			<th>代理量</th>
			<th>设立年限</th>
			<th>代理人人数</th>
			<th>地域</th>
			<th>技术领域</th>-->
				<th>姓名</th>
				<th>经验(年)</th>
				<th>工作外语</th>
				<th>所属代理机构</th>
				<th>所在城市</th>
				<th>民事/刑事诉讼资格</th>
				<th>技术领域</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${objs.content}" var="obj">
			<tr>
			<td><a href="${ctx}/dlr/dlrForm/${obj.id}">${obj.xm}</a></td>
					<td>${obj.dtGzjy}</td>
					<td></td>
					<td>${obj.pid}</td>
					<td>${obj.szcs}</td>
					<td></td>
					<td>${obj.jsly}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<tags:pagination page="${sd}" paginationSize="5"/>
</body>
</html>