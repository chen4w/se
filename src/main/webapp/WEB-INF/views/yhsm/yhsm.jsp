<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
<title>用户声明</title>
</head>
<body>
<div class="container-fluid">
<div class="row" style="padding-top:10px;">
<div class="col-md-12">
<p style="text-align:center">
    <strong><span style="font-size:21px;font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;">用户须知</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">欢迎您（指用户）与进入由代理机构和北京源创云网络科技有限公司为您提供的相关服务，请您仔细阅读用户须知，同意本并接受全部条款，您点击“同意”按钮后，本协议即构成对双方有约束力的法律文件。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">一、用户条款内容</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本须知内容包括正文及所有已经发布的或将来可能发布的各类规则。所有规则为本须知不可分割的组成部分，与须知正文具有同等法律效力。除另行明确声明外，所提供的服务均受本约束。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">您应当在使用此服务之前认真阅读全部内容。如您有任何疑问，应向我们咨询。但无论您事实上是否在使用我们的服务之前认真阅读了本内容，只要您使用此服务，则本协议即对您产生约束，届时您不能以未阅读本内容或者未获得对您问询的解答等理由，主张本协议无效，或要求撤销本协议。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">3.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">您承诺接受并遵守本约定。如果您违反本约定的内容，您应立即停止注册程序或停止使用此服务。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">4.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">我们有权根据需要不定时地制订、修改本协议及各类规则，并以网站公示的方式进行公告，不再另行通知，用户可随时查阅最新版服务协议。变更后的协议和规则一经在网站公布后，立即自动生效。如您不同意相关变更内容，应当提出并立即停止使用此服务。如您继续使用， 即表示您对修改的内容无异议并接受。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">5.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本协议适用于我们提供的各种服务，但当用户使用某一特定服务时，如该服务另有单独的服务条款、指引或规则，用户应遵守本服务条款及随时公布的与该服务相关的服务条款、指引或规则等。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">您在使用电商服务过程中，所产生的应纳税赋，以及一切硬件、软件、服务及其它方面的费用，均由您独自承担。</span></strong>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">二、服务内容</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">我们为用户提供“创意提交”、“出证”、“申请专利”等服务。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">创意提交”功能指；用户可以通过将其原创作品进行上传或者记录，作品类型包括但不限于：文字、图片、音频、视频等格式。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">3.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">“出证”服务指用户可以实现对原创成果的见证和认证。我们可以提供数字签名、时间戳等技术手段等，将原创成果的完成过程和最终结果固化下来，具有唯一不变性。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">4.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">“申请专利”服务指用户可以向创意宝提交申请专利的需求，经创意宝确认后交由源创云认可的第三方为客户提供相关服务。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">三、服务规范</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">用户在使用该服务过程中，您承诺遵守以下约定：<br/> a) 所有行为均遵守国家法律、法规等规范性文件及我们的各项规则的规定和要求，不违背社会公共利益或公共道德，不损害他人的合法权益，不违反本协议及相关规则。您如果违反前述承诺，产生任何法律后果的，您应以自己的名义独立承担所有的法律责任，并确保源创云公司免于因此产生任何损失。<br/> b) 不使用任何装置、软件或例行程序干预或试图干预我们功能的正常运作或正在进行的任何交易、活动。您不得采取任何将导致不合理的庞大数据负载加诸我们网络设备的行动。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2.</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">您了解并同意：<br/> a）通过电子邮件、客户端、网页、或其他合法方式发送相关服务信息及服务内容。<br/> b) 我们有权对您是否违反上述承诺做出单方认定，并根据单方认定结果适用规则予以处理或终止向您提供服务，且无须征得您的同意或提前通知予您。<br/> c) 经国家行政或司法机关的生效法律文书确认您存在违法或侵权行为，或者根据自身的判断，认为您的行为涉嫌违反本协议和/或规则的条款或涉嫌违反法律法规的规定的，则我们有权在网站上公示您涉嫌违法或违约行为及我们已对您采取的措施。<br/> d) 对于您在此发布的涉嫌违法或涉嫌侵犯他人合法权利或违反本协议和/或规则的信息，我们有权不经通知您即予以删除，且按照规则的规定进行处理。<br/> e) 对于您在此实施的行为，包括您未在此实施但已经对我们和用户产生影响的行为，我们有权单方认定您行为的性质及是否构成对本协议和/或规则的违反，并据此作出相应处理。您应自行保存与您行为有关的全部证据，并应对无法提供充分证据而承担相应的责任。<br/> f) 对于您涉嫌违反承诺的行为对任意第三方造成损害的，您均应当以自己的名义独立承担所有的法律责任，并应确保我们免于因此产生损失或增加费用。<br/> g) 如您涉嫌违反有关法律或者本协议之规定，使我们遭受任何损失，或受到任何第三方的索赔，或受到任何行政管理部门的处罚，您应当赔偿我们，因此造成的损失及（或）发生的费用，包括合理的律师费用。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">四、行为规则</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、用户必须严格遵守《中华人民共和国民法通则》、《中华人民共和国保守国家秘密法》、《中华人民共和国著作权法》、《中华人民共和国专利法》、《中华人民共和国商标法》、《中华人民共和国反不正当竞争法》、《中华人民共和国计算机信息系统安全保护条例》、《计算机软件保护条例》、《互联网著作权行政保护办法》等法律法规的规定，不得实施任何违法行为或者影响我们利益的行为。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、用户不得利用我们上传、发布和传播法律法规禁止的内容，包括但不限于以下信息：<br/> a）违反宪法所确定的基本原则的；<br/> b）危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；<br/> c）损害国家荣誉和利益的；<br/> d）煽动民族仇恨、民族歧视、破坏民族团结的；<br/> e）破坏国家宗教政策，宣扬邪教和封建迷信的；<br/> f）散布谣言，扰乱社会秩序，破坏社会稳定的；<br/> g）煽动非法集会、结社、游行、静坐、示威、聚众扰乱社会秩序的；<br/> h）发表对于政府和法律进行破坏言论的；<br/> i）侵犯他人肖像权、姓名权、名誉权、隐私权或其他人身权利的。<br/> j）散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；<br/> k）侮辱或者诽谤他人，侵害他人合法权利的；<br/> l）含有虚假、有害、胁迫、侵害他人隐私、骚扰、侵害、中伤、粗俗、猥亵、或其他道德上令人反感的内容；</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">3</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、<strong>用户必须保证，在这里上传、发布或传播的内容未侵犯其他任何第三方之合法权益，否则将承担由此带来的一切法律责任。</strong></span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">4</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、<strong>用户不得将任何内部资料、机密资料、涉及他人隐私资料或侵犯任何人的专利权、商标权、著作权、商业秘密或其他专属权利之内容加以上传、发布或传播。</strong></span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">5</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、若第三方认为用户在创意宝中上传、发布或传播的内容侵犯其合法权益，且向创意宝发出合理通知的，<strong>则我们有权移除涉嫌侵权内容。</strong></span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">6</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、用户不得为任何非法目的而利用网络服务系统，从事包括但不限于以下活动： a)未经允许，进入计算机信息网络或者使用计算机信息网络资源的； b)未经允许，对计算机信息网络功能进行删除、修改或者增加的； c)未经允许，对进入计算机信息网络中存储、处理或者传输的数据和应用程序进行删除、修改或者增加的； d)故意制作、传播计算机病毒等破坏性程序的； e)其他危害计算机信息网络安全的行为。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">7</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、用户不得在此上传、发布广告函件、促销等内容。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">8</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、<strong>用户应承担一切因自己上传、发布或传播信息不当导致的民事、行政或刑事法律责任。</strong></span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">9</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、<strong>任何用户在使用过程中发现创意宝上存在违反国家法律法规或者侵犯了第三方的合法权益的内容，请及时向我们举报或反馈，我们将依法处理。</strong></span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">六、隐私权保护规则</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们对用户存储在这里的创意不做任何审查，工作人员也无法查看用户存储的任何内容，但是我们对用户发布的内容，有权进行必要的监督和审核。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们保证不对外公开用户信息，但下列情况除外：<br/> a）事先获得用户的明确授权；<br/> b）依据本协议条款或者您与源创云之间其他服务协议、合同等规定可以提供；<br/> c）我们为向您提供产品、服务、信息而向第三方提供的，包括通过第三方向您提供产品、服务、咨询的；<br/> d）政府主管部门或司法行为之要求；<br/> e）为使用户或他人的人身权利和财产权利免受重大损害。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">3</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、 在不透露单个用户隐私资料的前提下，创意宝有权对整个用户数据库进行分析和利用。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">七、安全规则</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">1</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们采取严格的技术措施和管理措施来保障用户数据的保密性和安全性。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">2</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们不含有任何旨在破坏用户数据和获取用户隐私信息的恶意代码，不含有任何跟踪、监视用户的功能代码，不会收集用户文件、文档等信息，不会泄漏用户隐私。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">3</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们制定了相关安全保护措施，来确保不超越目的和范围收集用户信息。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">4</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们工作人员不会向用户索取任何账号和密码资料。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">5</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">、我们因下列状况无法正常运作，使您无法使用各项服务时，我们不承担损害赔偿责任，该状况包括但不限于：<br/> a)系统停机维护期间。<br/> b)电信设备出现故障不能进行数据传输的。<br/> c)因台风、地震、海啸、洪水、停电、战争、恐怖袭击等不可抗力之因素，造成本公司系统障碍不能执行业务的。<br/> d)由于黑客攻击、电信部门技术调整或故障、网站升级、银行方面的问题等原因而造成的服务中断或者延迟。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">八、免责声明</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">一) 我们无法对非法或未经授权使用账号及密码的行为作出甄别，使用者成功登陆我们这里后的处分行为即认定为该注册用户的意思表示行为。因用户保管不当、私自许可他人使用等行为造成的损失由用户自行承担，我们不承担任何责任。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">二) 软件（含网站）经过详细的测试，但无法保证与所有的软硬件系统完全兼容，也无法保证本软件（或网站）完全没有错误。如果出现不兼容及软件错误的情况，用户可拨打客户服务电话将情况报告创意宝，获得技术支持。如果无法解决所遇问题，用户可以删除本软件或停止使用电商服务。对因使用或不能使用本软件所产生的损害及风险。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">三) 网络服务存在因上述不可抗力、计算机病毒、黑客攻击、系统不稳定、互联网、通信线路原因等造成的服务中断或不能满足用户要求的风险。承诺尽商业上的合理努力保障系统安全和数据安全，但无法对网络服务的及时性、安全性、准确性提供任何保证，对于因不可抗力、网络状况、通信线路、第三方网站或管理部门的要求以及其他非创意宝过错原因导致的用户数据损失、丢失或服务停止等不承担任何责任。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">四) 为了能够给用户提供更加高效快捷的服务，平台将定期或不定期对软件、服务器、网络或其他相关设备进行改版、升级、检修和维护等，对因此而导致的服务中断，我们不承担任何责任。我们保留不经事先通知而为改版、升级、检修和维护目的暂停本服务的权利。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">五) 用户对其上传内容或者发表的内容独立承担法律责任。为用户上传、发布和传播的内容提供存储空间，不对任何有关信息内容的真实性、适用性和合法性承担责任。任何经由我们提供的服务上传、发布和传播的文档、图片、视频、应用程序或其他资料，无论系公开还是私下传送，均由上传者承担法律责任。用户在这里发布侵犯他人知识产权或其他合法权益的内容，我们有权予以删除，并保留移交司法机关处理的权利。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">六) 为了保障公司业务发展和调整的自主权，我们拥有随时自行修改或中断软件授权而不需通知用户的权利，如有必要，修改或中断会以通告形式公布于网站重要页面上。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">七) 我们代理人不保证能够连续不间断地提供上述服务。想去有权于任何时间暂时或永久修改或终止本服务（或其任何部分），而无论其通知与否，我们对用户和任何第三人均无需承担任何责任。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">(</span><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">八) 用户使用“出证”服务所提取的证据，相关证书无论因何种原因未被司法机关、行政机关、仲裁机构等部门采信的，创意宝不承担任何法律责任。</span>
</p>
<p>
    <span style="font-size:16px">&nbsp;</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <strong><span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">九、法律效力及争议解决</span></strong>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本协议的签署、成立、效力、解释、履行、修订和终止以及争议解决均应适用中华人民共和国法律。</span>
</p>
<p style="text-indent:24px;line-height:26px">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">本协议全部或部分无效的，本争议解决条款依然有效</span><a name="_GoBack"></a>
</p>
<p style="text-indent: 24px; line-height: 26px;">
    <span style="font-family:&#39;微软雅黑&#39;,&#39;sans-serif&#39;;color:#333333">因本协议发生任何纠纷，双方应协商一致解决；协商不成的，双方均可向北京服务机构所地的人民法院提起诉讼。</span>
</p>
</div>
</div>
</div>
</body>
</html>