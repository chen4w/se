<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
%>
<html>
<head>
	<title>${obj.title}</title>
</head>

<body>
<div class="container bs-docs-container">

<div class="article-wrap">
<h1 class="page-header">${obj.title}</h1>

<div class="article-author">
    <!--
    <span class="author-name">${obj.author}</span>
    <i class="icon icon-line"></i>
    <span class="article-time">${obj.dtCreate}</span>
    <span class="article-share">收藏12</span>
    <span class="article-pl">评论4</span>
     -->
    <ul class="nav navbar-nav">
         <li>${obj.author}&nbsp;&nbsp;</li>
         <li>${obj.dtCreate}&nbsp;&nbsp;</li>
         <li class="dropdown">
            <span class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;">&nbsp;&nbsp;我要分享<b class="caret"></b></span>
            <ul class="dropdown-menu" id="s_article">
               <li><a href="" class="s_qqweibo" >腾讯微博</a></li>
               <li><a href="" class="s_qzone" >QQ空间</a></li>
               <li><a href="" class="s_qq" >QQ好友</a></li>
               <li><a href="" class="s_weibo" >新浪微博</a></li>
               <li id="shareWx" ><a href="javascript:void(0)" class="s_wx">微信好友</a><div class="share-weixin"></div></li>
            </ul>
         </li>
      </ul>
</div>
<div style="clear:both;"></div>
<div class="article-content-wrap">
<p>
${obj.content}
</p>

<c:forEach items="${robjs}" var="ro">
	<c:choose>
		<c:when test="${ro.suffix =='jpg' || ro.suffix =='png' || ro.suffix =='jpeg' }">
			<figure class="figure">
			  <img src="/fopen/${ro.path}" class="img-responsive center-block" >
			  <figcaption style="text-align:center;color:rgb(153, 153, 153);">${ro.title}</figcaption>
			</figure>	
			<p><br></p>
			<p>${ro.content}</p>
		</c:when>
	</c:choose>
</c:forEach> 
</div>

<c:forEach items="${robjs}" var="ro">
	<c:if test="${ro.suffix !='jpg' && ro.suffix !='png' && ro.suffix !='jpeg' }">
	<div class="cy-section-inner">                             
		<a target="_blank" href="/fopen/${ro.path}">
		<div class="cy-inner-title">${ro.title}</div>
		</a>
	</div>
	</c:if>
</c:forEach>


<c:if test="${not empty obj.tag}">
<c:set var="tags" value="${fn:split(obj.tag,';')}" />
<div class="tag-box ">
    <ul class="transition">
    <c:forEach items="${tags}" var="tag">
     	<li class="transition">${tag}</li>
    </c:forEach>
    </ul>
</div>
</c:if>

</div>
</div>

<img id="wximg-buffer" style="display: none;" src="${ctx}/static/images/favicon.ico" >
<script src="${ctx}/static/jquery/share.js"></script>
<script src="${ctx}/static/jquery/jquery.qrcode-0.12.0.min.js" ></script>
<script>
    var rqturl = '<%=basePath%>';
    $(function(){    	
        var $config = {
		    url: location.href, // 网址
		    site: '', // 来源(QQ空间会用到）, 默认读取head标签：<meta name="site" content="http://overtrue" />
		    title: '${obj.title}', // 标题，默认读取 document.title
		    description: '', // 描述, 默认读取head标签：<meta name="description" content="PHP弱类型的实现原理分析" />
		    image: rqturl + $(".article-content-wrap").find('img:first').attr('src'), // 图片, 默认取网页中第一个img标签
		    target: '_blank' //打开方式
		};
		$("#s_article").share($config);
    });
</script>
</body>
</html>
