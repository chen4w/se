<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<title>${obj.title}</title>
</head>

<body>
<h1>${obj.title}</h1>
<p>${obj.content}</p>
</body>
</html>
