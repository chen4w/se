<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
  <head>
    <title>404</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link href="${ctx}/static/styles/404.css" type="text/css" rel="stylesheet"/>
  </head>
  <body>
    <div id="base" class="">

      <!-- Unnamed (Image) -->
      <div id="u0" class="ax_image">
        <img id="u0_img" class="img " src="${ctx}/static/images/404_u0.jpg"/>
        <!-- Unnamed () -->
        <div id="u1" class="text">
          <p><span></span></p>
        </div>
      </div>
      
      <!-- Unnamed (Image) -->
      <div id="u80" class="ax_image">
        <img id="u80_img" class="img " src="${ctx}/static/images/404_u80.png"/>
        <!-- Unnamed () -->
        <div id="u81" class="text">
          <p><span></span></p>
        </div>
      </div>

      <!-- Unnamed (形状) -->
      <div id="u82" class="ax_h1">
        <img id="u82_img" class="img " src="${ctx}/static/images/transparent.gif"/>
        
        <div id="u83" class="text">
          <p><span>您所查询的页面暂时无法连接</span></p><p><span>请浏览其他页面</span></p>
        </div>
      </div>
    </div>
  </body>
</html>
