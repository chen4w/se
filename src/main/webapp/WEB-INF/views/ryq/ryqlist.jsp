<%@ page contentType="text/html;charset=UTF-8" import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>代理机构检索</title>
	<link type="image/x-icon" href="${ctx}/static/images/favicon.ico" rel="shortcut icon">
    <link href="${ctx}/static/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />

    <link href="${ctx}/static/jquery-validation/1.11.1/validate.css" type="text/css" rel="stylesheet" />
    <link href="${ctx}/static/styles/default.css" type="text/css" rel="stylesheet" />
    <script src="${ctx}/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="${ctx}/static/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>


    <script src="${ctx}/static/jquery-validation/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
    <script src="${ctx}/static/jquery-validation/1.11.1/messages_bs_zh.js" type="text/javascript"></script>

</head>
<body>	 
	 <table id="contentTable" class="table table-striped table-bordered" style="margin-top:3px;">
		<thead>
		<tr>
			<th>姓名</th>
			<th>执业年限</th>
			<th>工作语言</th>
			<th>所在城市</th>
			<th>所学专业</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${objs.content}" var="obj">
			<tr>
			<td><a href="${ctx}/dlr/detail/${obj.id}" target="_blank">${obj.xm}</a></td>
			<td>${obj.nx}</td>
			<td>${obj.wynl}</td>
			<td>${obj.dy}</td>
			<td>${obj.sxzy}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<tags:pagination page="${objs}" paginationSize="8"/>
</body>
</html>