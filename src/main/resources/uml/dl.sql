SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS attach;
DROP TABLE IF EXISTS bookmark;
DROP TABLE IF EXISTS d_dy;
DROP TABLE IF EXISTS d_jslyfl;
DROP TABLE IF EXISTS s_dlr;
DROP TABLE IF EXISTS e_dlr;
DROP TABLE IF EXISTS r_dljg_kh;
DROP TABLE IF EXISTS s_dljg;
DROP TABLE IF EXISTS e_dljg;
DROP TABLE IF EXISTS e_dljg_sh;
DROP TABLE IF EXISTS e_dlr_sh;
DROP TABLE IF EXISTS e_fswx;
DROP TABLE IF EXISTS e_kh;
DROP TABLE IF EXISTS l_oper;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS s_dljg_ajsl;
DROP TABLE IF EXISTS s_dljg_fswx;
DROP TABLE IF EXISTS s_dlr_ajsl;
DROP TABLE IF EXISTS s_dlr_fswx;
DROP TABLE IF EXISTS s_fswx;
DROP TABLE IF EXISTS s_jsly;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS u_user;




/* Create Tables */

-- 资讯类或其他类文章
CREATE TABLE article
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 编辑用户id
	uid int COMMENT '编辑用户id',
	-- 上级文章
	pid int COMMENT '上级文章',
	-- 题名
	title varchar(500) COMMENT '题名',
	-- 短题名
	stitle varchar(200) COMMENT '短题名',
	-- 分类
	stype varchar(200) COMMENT '分类',
	-- 内容
	content varchar(4000) COMMENT '内容',
	-- 作者
	author varchar(100) COMMENT '作者',
	-- 发布到哪些栏目
	postto varchar(500) COMMENT '发布到哪些栏目',
	-- 直接链接到此网址
	href varchar(500) COMMENT '直接链接到此网址',
	-- 纯文本
	plain varchar(4000) COMMENT '纯文本',
	-- 是否置顶
	btop int COMMENT '是否置顶',
	-- 内容标签
	tag varchar(200) COMMENT '内容标签',
	-- 状态：0预览 1发布 2置顶
	status int COMMENT '状态：0预览 1发布 2置顶',
	-- 建立时间
	dt_create datetime COMMENT '建立时间',
	PRIMARY KEY (id)
) COMMENT = '资讯类或其他类文章';


-- 文件附件
CREATE TABLE attach
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 编辑用户id
	uid int COMMENT '编辑用户id',
	-- 题名
	title varchar(200) COMMENT '题名',
	-- 附件文本描述
	content varchar(3000) COMMENT '附件文本描述',
	-- 分类
	purpose varchar(500) COMMENT '分类',
	-- 文件大小
	fsize int COMMENT '文件大小',
	-- 文件原名称
	fname varchar(500) COMMENT '文件原名称',
	-- 后缀
	suffix varchar(10) COMMENT '后缀',
	path varchar(500),
	-- 附件所属的实体类名
	ename varchar(100) COMMENT '附件所属的实体类名',
	-- 附件所属的实体id
	eid int COMMENT '附件所属的实体id',
	-- 相关附件排序
	ord int COMMENT '相关附件排序',
	-- 发布日期
	dt_create datetime COMMENT '发布日期',
	PRIMARY KEY (id)
) COMMENT = '文件附件';


CREATE TABLE bookmark
(
	-- 主键id
	id int NOT NULL COMMENT '主键id',
	-- 用户id
	uid int COMMENT '用户id',
	-- 网页题名
	title varchar(500) COMMENT '网页题名',
	-- 网址
	url varchar(500) COMMENT '网址',
	-- 建立时间
	dt_create datetime COMMENT '建立时间',
	PRIMARY KEY (id)
);


CREATE TABLE d_dy
(
	-- 主键
	id int NOT NULL AUTO_INCREMENT COMMENT '主键',
	-- 地域编码
	bm varchar(50) COMMENT '地域编码',
	-- 中文名称
	zwmc varchar(100) COMMENT '中文名称',
	PRIMARY KEY (id)
);


CREATE TABLE d_jslyfl
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 分类方式1：ipc；2：洛迦诺
	flfs int COMMENT '分类方式1：ipc；2：洛迦诺',
	-- 分类号
	flh varchar(20) COMMENT '分类号',
	-- 名称
	mc varchar(100) COMMENT '名称',
	PRIMARY KEY (id)
);


-- 代理机构基本信息
CREATE TABLE e_dljg
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 资格证号
	mc varchar(100) NOT NULL COMMENT '资格证号',
	-- 机构代码
	jgdm varchar(100) COMMENT '机构代码',
	-- 负责人
	fzr varchar(100) COMMENT '负责人',
	-- 设立时间
	dt_slsj datetime COMMENT '设立时间',
	-- 网址
	wz varchar(100) COMMENT '网址',
	-- 奖惩状态
	jczt int COMMENT '奖惩状态',
	-- 地址
	dz varchar(100) COMMENT '地址',
	-- 经营理念
	jyln varchar(500) COMMENT '经营理念',
	-- 公司介绍
	gsjs varchar(1100) COMMENT '公司介绍',
	-- 公司规模
	gsgm varchar(100) COMMENT '公司规模',
	-- 电话
	dh varchar(100) COMMENT '电话',
	-- 公司性质
	gsxz varchar(100) COMMENT '公司性质',
	-- 电子信箱
	dzxx varchar(100) COMMENT '电子信箱',
	-- 公司荣誉-时间
	gsry_sj datetime COMMENT '公司荣誉-时间',
	-- 公司荣誉-证书
	gsry_zs varchar(200) COMMENT '公司荣誉-证书',
	-- 公司荣誉-发证单位
	gsry_fzdw varchar(100) COMMENT '公司荣誉-发证单位',
	-- 公益申请_发明_机械
	gysq_fm_jx int COMMENT '公益申请_发明_机械',
	-- 公益申请_发明_电子通讯
	gysq_fm_dztx int COMMENT '公益申请_发明_电子通讯',
	-- 公益申请_发明_化学
	gysq_fm_hx int COMMENT '公益申请_发明_化学',
	-- 公益申请_实用新型_机械
	gysq_syxx_jx int COMMENT '公益申请_实用新型_机械',
	-- 公益申请_实用新型_电子通讯
	gysq_syxx_dztx int COMMENT '公益申请_实用新型_电子通讯',
	-- 公益申请_实用新型_化学
	gysq_syxx_hx int COMMENT '公益申请_实用新型_化学',
	-- 公益申请_外观
	gysq_wg int COMMENT '公益申请_外观',
	-- 代理人人数
	dlrrs int COMMENT '代理人人数',
	-- 地域
	dy varchar(100) COMMENT '地域',
	-- 律师人数
	lsrs int COMMENT '律师人数',
	-- 服务-专利申请
	zlsq int COMMENT '服务-专利申请',
	-- 专利复审0false 1true
	zlfs int COMMENT '专利复审0false 1true',
	-- 服务-专利无效宣告0false1ture
	zlwx int COMMENT '服务-专利无效宣告0false1ture',
	-- 服务类型-行政诉讼0false1true
	xzss int COMMENT '服务类型-行政诉讼0false1true',
	-- 服务类型-民事诉讼0false1true
	msss int COMMENT '服务类型-民事诉讼0false1true',
	-- 专利权属纠纷
	zlqsjf int COMMENT '专利权属纠纷',
	-- 行政执法案件
	xzzfaj int COMMENT '行政执法案件',
	-- 专利检索
	zljs int COMMENT '专利检索',
	-- 专利预警
	zlyj int COMMENT '专利预警',
	-- 专利导航
	zldh int COMMENT '专利导航',
	-- 专利分析评价
	zlfxpj int COMMENT '专利分析评价',
	-- 专利运营
	zlyy int COMMENT '专利运营',
	-- 知识产权贯标
	zscqgb int COMMENT '知识产权贯标',
	-- 用于存e_dljg_sh的主键值。
	eid int COMMENT '用于存e_dljg_sh的主键值。',
	-- 联系人
	lxr varchar(100) COMMENT '联系人',
	-- 联系电话
	lxdh varchar(50) COMMENT '联系电话',
	-- 电商标记：1，可以提供电商服务；0，不能提供电商服务
	ds int DEFAULT 0 COMMENT '电商标记：1，可以提供电商服务；0，不能提供电商服务',
	PRIMARY KEY (id)
) COMMENT = '代理机构基本信息';


-- 代理机构基本信息_审核表
CREATE TABLE e_dljg_sh
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 资格证号
	mc varchar(100) NOT NULL COMMENT '资格证号',
	-- 机构代码
	jgdm varchar(100) COMMENT '机构代码',
	-- 负责人
	fzr varchar(100) COMMENT '负责人',
	-- 设立时间
	dt_slsj datetime COMMENT '设立时间',
	-- 网址
	wz varchar(100) COMMENT '网址',
	-- 奖惩状态
	jczt int COMMENT '奖惩状态',
	-- 地址
	dz varchar(100) COMMENT '地址',
	-- 经营理念
	jyln varchar(500) COMMENT '经营理念',
	-- 公司介绍
	gsjs varchar(1100) COMMENT '公司介绍',
	-- 公司规模
	gsgm varchar(100) COMMENT '公司规模',
	-- 电话
	dh varchar(100) COMMENT '电话',
	-- 公司性质
	gsxz varchar(100) COMMENT '公司性质',
	-- 电子信箱
	dzxx varchar(100) COMMENT '电子信箱',
	-- 公司荣誉-时间
	gsry_sj datetime COMMENT '公司荣誉-时间',
	-- 公司荣誉-证书
	gsry_zs varchar(200) COMMENT '公司荣誉-证书',
	-- 公司荣誉-发证单位
	gsry_fzdw varchar(100) COMMENT '公司荣誉-发证单位',
	-- 公益申请_发明_机械
	gysq_fm_jx int COMMENT '公益申请_发明_机械',
	-- 公益申请_发明_电子通讯
	gysq_fm_dztx int COMMENT '公益申请_发明_电子通讯',
	-- 公益申请_发明_化学
	gysq_fm_hx int COMMENT '公益申请_发明_化学',
	-- 公益申请_实用新型_机械
	gysq_syxx_jx int COMMENT '公益申请_实用新型_机械',
	-- 公益申请_实用新型_电子通讯
	gysq_syxx_dztx int COMMENT '公益申请_实用新型_电子通讯',
	-- 公益申请_实用新型_化学
	gysq_syxx_hx int COMMENT '公益申请_实用新型_化学',
	-- 公益申请_外观
	gysq_wg int COMMENT '公益申请_外观',
	-- 代理人人数
	dlrrs int COMMENT '代理人人数',
	-- 地域
	dy varchar(100) COMMENT '地域',
	-- 律师人数
	lsrs int COMMENT '律师人数',
	-- 审核状态0,未提交审核;1，正在审核;2,审核通过;3，审核未通过
	shzt int COMMENT '审核状态0,未提交审核;1，正在审核;2,审核通过;3，审核未通过',
	-- 备注
	bz varchar(2000) COMMENT '备注',
	-- 服务类型-专利申请0false1true
	zlsq int COMMENT '服务类型-专利申请0false1true',
	-- 服务类型-专利复审0false1true
	zlfs int COMMENT '服务类型-专利复审0false1true',
	-- 服务类型-专利无效宣告0false1true
	zlwx int COMMENT '服务类型-专利无效宣告0false1true',
	-- 服务类型-行政诉讼0false1true
	xzss int COMMENT '服务类型-行政诉讼0false1true',
	-- 专利类型-民事诉讼0false1true
	msss int COMMENT '专利类型-民事诉讼0false1true',
	-- 专利权属纠纷
	zlqsjf int COMMENT '专利权属纠纷',
	-- 行政执法案件
	xzzfaj int COMMENT '行政执法案件',
	-- 专利检索
	zljs int COMMENT '专利检索',
	-- 专利预警
	zlyj int COMMENT '专利预警',
	-- 专利导航
	zldh int COMMENT '专利导航',
	-- 专利分析评价
	zlfxpj int COMMENT '专利分析评价',
	-- 专利运营
	zlyy int COMMENT '专利运营',
	-- 知识产权贯标
	zscqgb int COMMENT '知识产权贯标',
	-- 联系人
	lxr varchar(100) COMMENT '联系人',
	-- 联系电话
	lxdh varchar(50) COMMENT '联系电话',
	-- 电商标记：1，可以提供电商服务；0，不能提供电商标记
	ds int DEFAULT 0 COMMENT '电商标记：1，可以提供电商服务；0，不能提供电商标记',
	PRIMARY KEY (id)
) COMMENT = '代理机构基本信息_审核表';


-- 代理人基本信息
CREATE TABLE e_dlr
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 代理人服务机构id
	pid int COMMENT '代理人服务机构id',
	-- 姓名
	xm varchar(100) COMMENT '姓名',
	-- 性别
	xb varchar(100) COMMENT '性别',
	-- 技术领域
	jsly varchar(100) COMMENT '技术领域',
	-- 资格证号
	zgzh varchar(100) COMMENT '资格证号',
	-- 执业证号
	zyzh varchar(100) COMMENT '执业证号',
	-- 外语能力 
	wynl varchar(100) COMMENT '外语能力 ',
	-- 工作起始时间--工作经验
	dt_gzjy datetime COMMENT '工作起始时间--工作经验',
	-- 最高学历
	zgxl varchar(100) COMMENT '最高学历',
	-- 所在城市
	szcs varchar(100) COMMENT '所在城市',
	-- 是否有海外留学经历:0、否；1、是
	bhwlx int COMMENT '是否有海外留学经历:0、否；1、是',
	-- 惩戒记录:0、无；1、有
	bcjjl int COMMENT '惩戒记录:0、无；1、有',
	-- 代理人协会职称
	dlrxhzc varchar(100) COMMENT '代理人协会职称',
	-- 服务类型
	fwlx varchar(1000) COMMENT '服务类型',
	-- 个人履历-工作经历
	gzjl varchar(1000) COMMENT '个人履历-工作经历',
	-- 教育经历
	jyjl varchar(1000) COMMENT '教育经历',
	-- 培训经历
	pxjl varchar(1000) COMMENT '培训经历',
	-- 学术研究
	xsyj varchar(1000) COMMENT '学术研究',
	-- 项目类业务
	xmlyw varchar(1000) COMMENT '项目类业务',
	-- 个人荣誉-国知局专家库高层次人才
	gzjrc varchar(1000) COMMENT '个人荣誉-国知局专家库高层次人才',
	-- 年度考核
	ndkh varchar(100) COMMENT '年度考核',
	-- 0专利代理人   1行政诉讼代理人  2民事诉讼代理人   3律师(双执业证)
	zz int COMMENT '0专利代理人   1行政诉讼代理人  2民事诉讼代理人   3律师(双执业证)',
	-- 其他社会职务
	qtshzw varchar(2000) COMMENT '其他社会职务',
	-- 专业
	zy varchar(20) COMMENT '专业',
	-- 发表学术论文情况
	fbxslw varchar(2000) COMMENT '发表学术论文情况',
	-- 出版学术专著情况
	cbxszz varchar(2000) COMMENT '出版学术专著情况',
	-- 承担国知局、协会、省局、重大项目情况
	cdzdxm varchar(2000) COMMENT '承担国知局、协会、省局、重大项目情况',
	-- 专利申请：0、false;1、true
	zlsq int COMMENT '专利申请：0、false;1、true',
	-- 专利复审：0、false;1、true
	zlfs int COMMENT '专利复审：0、false;1、true',
	-- 专利无效宣告：0、false;1、true
	zlxgwx int COMMENT '专利无效宣告：0、false;1、true',
	-- 行政诉讼：0、false;1、true
	xzss int COMMENT '行政诉讼：0、false;1、true',
	-- 民事诉讼：0、false;1、true
	msss int COMMENT '民事诉讼：0、false;1、true',
	-- 专利权属纠纷
	zlqsjf int COMMENT '专利权属纠纷',
	-- 行政执法案件
	xzzfaj int COMMENT '行政执法案件',
	-- 专利检索
	zljs int COMMENT '专利检索',
	-- 专利预警
	zlyj int COMMENT '专利预警',
	-- 专利导航
	zldh int COMMENT '专利导航',
	-- 专利分析评价
	zlfxpj int COMMENT '专利分析评价',
	-- 专利运营
	zlyy int COMMENT '专利运营',
	-- 知识产权贯标
	zscqgb int COMMENT '知识产权贯标',
	-- 所学专业
	sxzy varchar(50) COMMENT '所学专业',
	-- 协会领军高层次人才0,什么都不是；1，高层；2，领军
	ljrw int COMMENT '协会领军高层次人才0,什么都不是；1，高层；2，领军',
	-- 获得与知识产权有关的国家奖励情况
	gjjlqk varchar(1000) COMMENT '获得与知识产权有关的国家奖励情况',
	-- 获得与知识产权有关的省部级奖励情况
	sbjjlqk varchar(1000) COMMENT '获得与知识产权有关的省部级奖励情况',
	PRIMARY KEY (id)
) COMMENT = '代理人基本信息';


-- 代理人基本信息
CREATE TABLE e_dlr_sh
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 代理人服务机构id
	pid int COMMENT '代理人服务机构id',
	-- 姓名
	xm varchar(100) COMMENT '姓名',
	-- 性别
	xb varchar(100) COMMENT '性别',
	-- 技术领域
	jsly varchar(100) COMMENT '技术领域',
	-- 资格证号
	zgzh varchar(100) COMMENT '资格证号',
	-- 执业证号
	zyzh varchar(100) COMMENT '执业证号',
	-- 外语能力 
	wynl varchar(100) COMMENT '外语能力 ',
	-- 工作起始时间--工作经验
	dt_gzjy datetime COMMENT '工作起始时间--工作经验',
	-- 最高学历
	zgxl varchar(100) COMMENT '最高学历',
	-- 所在城市
	szcs varchar(100) COMMENT '所在城市',
	-- 是否有海外留学经历:0、否；1、是
	bhwlx int COMMENT '是否有海外留学经历:0、否；1、是',
	-- 惩戒记录:0、无；1、有
	bcjjl int COMMENT '惩戒记录:0、无；1、有',
	-- 代理人协会职称
	dlrxhzc varchar(100) COMMENT '代理人协会职称',
	-- 服务类型
	fwlx varchar(1000) COMMENT '服务类型',
	-- 个人履历-工作经历
	gzjl varchar(1000) COMMENT '个人履历-工作经历',
	-- 教育经历
	jyjl varchar(1000) COMMENT '教育经历',
	-- 培训经历
	pxjl varchar(1000) COMMENT '培训经历',
	-- 学术研究
	xsyj varchar(1000) COMMENT '学术研究',
	-- 项目类业务
	xmlyw varchar(1000) COMMENT '项目类业务',
	-- 个人荣誉-国知局专家库高层次人才
	gzjrc varchar(1000) COMMENT '个人荣誉-国知局专家库高层次人才',
	-- 年度考核
	ndkh varchar(100) COMMENT '年度考核',
	-- 0专利代理人   1行政诉讼代理人  2民事诉讼代理人   3律师(双执业证)
	zz int COMMENT '0专利代理人   1行政诉讼代理人  2民事诉讼代理人   3律师(双执业证)',
	-- 其他社会职务
	qtshzw varchar(2000) COMMENT '其他社会职务',
	-- 专业
	zy varchar(20) COMMENT '专业',
	-- 发表学术论文情况
	fbxslw varchar(2000) COMMENT '发表学术论文情况',
	-- 出版学术专著情况
	cbxszz varchar(2000) COMMENT '出版学术专著情况',
	-- 承担国知局、协会、省局、重大项目情况
	cdzdxm varchar(2000) COMMENT '承担国知局、协会、省局、重大项目情况',
	-- 专利申请：0、false;1、true
	zlsq int COMMENT '专利申请：0、false;1、true',
	-- 专利复审：0、false;1、true
	zlfs int COMMENT '专利复审：0、false;1、true',
	-- 专利无效宣告：0、false;1、true
	zlxgwx int COMMENT '专利无效宣告：0、false;1、true',
	-- 行政诉讼：0、false;1、true
	xzss int COMMENT '行政诉讼：0、false;1、true',
	-- 民事诉讼：0、false;1、true
	msss int COMMENT '民事诉讼：0、false;1、true',
	-- 专利权属纠纷
	zlqsjf int COMMENT '专利权属纠纷',
	-- 行政执法案件
	xzzfaj int COMMENT '行政执法案件',
	-- 专利检索
	zljs int COMMENT '专利检索',
	-- 专利预警
	zlyj int COMMENT '专利预警',
	-- 专利导航
	zldh int COMMENT '专利导航',
	-- 专利分析评价
	zlfxpj int COMMENT '专利分析评价',
	-- 专利运营
	zlyy int COMMENT '专利运营',
	-- 知识产权贯标
	zscqgb int COMMENT '知识产权贯标',
	-- 所学专业
	sxzy varchar(50) COMMENT '所学专业',
	-- 协会领军高层次人才0,什么都不是；1，高层；2，领军
	ljrw int COMMENT '协会领军高层次人才0,什么都不是；1，高层；2，领军',
	-- 获得与知识产权有关的国家奖励情况
	gjjlqk varchar(1000) COMMENT '获得与知识产权有关的国家奖励情况',
	-- 获得与知识产权有关的省部级奖励情况
	sbjjlqk varchar(1000) COMMENT '获得与知识产权有关的省部级奖励情况',
	-- 审核状态0,未提交审核;1，正在审核;2,审核通过;3，审核未通过
	shzt int COMMENT '审核状态0,未提交审核;1，正在审核;2,审核通过;3，审核未通过',
	-- 备注
	bz varchar(1000) COMMENT '备注',
	PRIMARY KEY (id)
) COMMENT = '代理人基本信息';


-- 复审无效基础数据
CREATE TABLE e_fswx
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向s_fswx的id
	pid int COMMENT '指向s_fswx的id',
	-- 决定号
	jdh varchar(50) COMMENT '决定号',
	-- 决定日
	dt_jdr datetime COMMENT '决定日',
	-- 申请号
	sqh varchar(100) COMMENT '申请号',
	-- 申请日
	dt_sqr datetime COMMENT '申请日',
	-- 申请人
	sqr varchar(50) COMMENT '申请人',
	-- 公开号
	gkh varchar(50) COMMENT '公开号',
	-- 公开日
	dt_gkr datetime COMMENT '公开日',
	-- 发明名称
	fmmc varchar(1000) COMMENT '发明名称',
	-- 分类号
	flh varchar(100) COMMENT '分类号',
	-- 复审申请人
	fssqr varchar(500) COMMENT '复审申请人',
	-- 无效申请人
	wxsqr varchar(500) COMMENT '无效申请人',
	-- 组长姓名
	zzxm varchar(100) COMMENT '组长姓名',
	-- 主审员姓名
	zsyxm varchar(100) COMMENT '主审员姓名',
	-- 参审员姓名
	csyxm varchar(100) COMMENT '参审员姓名',
	-- 公告号
	ggh varchar(100) COMMENT '公告号',
	-- 公告日
	dt_ggr datetime COMMENT '公告日',
	-- 状态：0不可用，1可用
	zt int DEFAULT 1 COMMENT '状态：0不可用，1可用',
	-- 1复审2无效
	fswx int COMMENT '1复审2无效',
	PRIMARY KEY (id)
) COMMENT = '复审无效基础数据';


-- 客户机构
CREATE TABLE e_kh
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 地域
	dy varchar(100) COMMENT '地域',
	-- 客户数量
	sl int COMMENT '客户数量',
	-- 指向代理机构主键
	pid int COMMENT '指向代理机构主键',
	-- 分类：0，全部；1，国内；2，国外；3，港澳台；4，错误数据
	gnw int COMMENT '分类：0，全部；1，国内；2，国外；3，港澳台；4，错误数据',
	PRIMARY KEY (id)
) COMMENT = '客户机构';


-- 操作审计表，也可用于数据增量更新依据
CREATE TABLE l_oper
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 用户id
	uid int COMMENT '用户id',
	-- 来访ip地址
	ipaddr varchar(100) COMMENT '来访ip地址',
	-- 实体名
	ename varchar(100) COMMENT '实体名',
	-- 实体序号
	eid int COMMENT '实体序号',
	-- 操作动作：ADD/UPDATE/DELETE/
	oper varchar(10) COMMENT '操作动作：ADD/UPDATE/DELETE/',
	-- json格式操作数据内容描述
	content varchar(4000) COMMENT 'json格式操作数据内容描述',
	-- 创建时间
	dt_create datetime COMMENT '创建时间',
	PRIMARY KEY (id)
) COMMENT = '操作审计表，也可用于数据增量更新依据';


-- 用户留言
CREATE TABLE message
(
	-- 主键序号
	id int NOT NULL AUTO_INCREMENT COMMENT '主键序号',
	-- 父id
	pid int COMMENT '父id',
	-- 用户id
	uid int COMMENT '用户id',
	-- 题名
	title varchar(500) COMMENT '题名',
	-- 内容
	content varchar(3000) COMMENT '内容',
	-- 留言时间
	dt_create datetime COMMENT '留言时间',
	-- 机构Id
	jid int COMMENT '机构Id',
	-- 回复内容
	reply varchar(3000) COMMENT '回复内容',
	-- 是否公开：0 否，1 是 
	bpublish int COMMENT '是否公开：0 否，1 是 ',
	-- 回复时间
	dt_reply datetime COMMENT '回复时间',
	-- 是否删除：1 是 ,0 否
	del int COMMENT '是否删除：1 是 ,0 否',
	-- 是否关闭问题:1 是 ,0 否
	close int COMMENT '是否关闭问题:1 是 ,0 否',
	PRIMARY KEY (id)
) COMMENT = '用户留言';


-- 代理机构-客户-排序关联
CREATE TABLE r_dljg_kh
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 机构id
	pid1 int COMMENT '机构id',
	-- 客户id
	pid2 int COMMENT '客户id',
	-- 代理量
	dll int COMMENT '代理量',
	-- 排序
	ord int COMMENT '排序',
	PRIMARY KEY (id)
) COMMENT = '代理机构-客户-排序关联';


-- 代理机构-代理量统计
CREATE TABLE s_dljg
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 指向代理机构
	pid int COMMENT '指向代理机构',
	-- 机构代码，对应统计数据的机构agencyCode
	-- 为防止统计数据中出现基础数据中没有的机构
	jgdm varchar(100) COMMENT '机构代码，对应统计数据的机构agencyCode
为防止统计数据中出现基础数据中没有的机构',
	-- 分类组合字符串
	-- 将4个维度的分类共6位合并到一个字段
	fl varchar(100) COMMENT '分类组合字符串
将4个维度的分类共6位合并到一个字段',
	-- 年度
	nd int COMMENT '年度',
	-- 数量
	sl int COMMENT '数量',
	PRIMARY KEY (id)
) COMMENT = '代理机构-代理量统计';


CREATE TABLE s_dljg_ajsl
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dljg表的主键
	pid int COMMENT '指向e_dljg表的主键',
	-- 机构代码
	jgdm int COMMENT '机构代码',
	-- 案件总量
	ajzl int COMMENT '案件总量',
	-- 民事案件数量
	msajsl int COMMENT '民事案件数量',
	-- 行政案件数量
	xzajsl int COMMENT '行政案件数量',
	PRIMARY KEY (id)
);


-- 代理机构复审无效数统计
CREATE TABLE s_dljg_fswx
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dlr表主键
	pid int COMMENT '指向e_dlr表主键',
	-- 统计维度：1复审数据，2无效宣告数据
	fl int COMMENT '统计维度：1复审数据，2无效宣告数据',
	-- 数量
	sl int DEFAULT 0 COMMENT '数量',
	PRIMARY KEY (id)
) COMMENT = '代理机构复审无效数统计';


-- 代理人--累计代理案件数量按类型累计
CREATE TABLE s_dlr
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 指向代理人
	pid int COMMENT '指向代理人',
	-- 所在机构机构代码
	jgdm varchar(100) COMMENT '所在机构机构代码',
	-- 代理人姓名
	xm varchar(100) COMMENT '代理人姓名',
	-- 4个维度统计分类合并为一个6位分类代码
	fl varchar(100) COMMENT '4个维度统计分类合并为一个6位分类代码',
	-- 数量
	sl int COMMENT '数量',
	-- 年度
	nd int COMMENT '年度',
	PRIMARY KEY (id)
) COMMENT = '代理人--累计代理案件数量按类型累计';


CREATE TABLE s_dlr_ajsl
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dlr表的主键
	pid int COMMENT '指向e_dlr表的主键',
	-- 所属代理机构的机构代码
	jgdm int COMMENT '所属代理机构的机构代码',
	-- 姓名
	xm varchar(50) COMMENT '姓名',
	-- 案件总量
	ajzl int COMMENT '案件总量',
	-- 民事案件数量
	msajsl int COMMENT '民事案件数量',
	-- 行政案件数量
	xzajsl int COMMENT '行政案件数量',
	PRIMARY KEY (id)
);


-- 代理人复审无效数统计
CREATE TABLE s_dlr_fswx
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dlr表主键
	pid int COMMENT '指向e_dlr表主键',
	-- 统计维度：1复审数据，2无效宣告数据
	fl int COMMENT '统计维度：1复审数据，2无效宣告数据',
	-- 数量
	sl int DEFAULT 0 COMMENT '数量',
	PRIMARY KEY (id)
) COMMENT = '代理人复审无效数统计';


-- 代理人复审无效数据
CREATE TABLE s_fswx
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dlr表的主键
	pid_dlr int COMMENT '指向e_dlr表的主键',
	-- 机构代码
	jgdm varchar(100) COMMENT '机构代码',
	-- 代理人姓名
	xm varchar(100) COMMENT '代理人姓名',
	-- 决定号
	jdh varchar(50) COMMENT '决定号',
	-- 申请号
	sqh varchar(50) COMMENT '申请号',
	-- 指向e_dljg主键
	pid_dljg int COMMENT '指向e_dljg主键',
	-- 分类：1，复审；2，无效宣告
	fl int DEFAULT 0 COMMENT '分类：1，复审；2，无效宣告',
	-- 代理人分类1请求人委托2专利权人委托
	dlrfl int COMMENT '代理人分类1请求人委托2专利权人委托',
	-- 决定日
	jdr datetime COMMENT '决定日',
	PRIMARY KEY (id)
) COMMENT = '代理人复审无效数据';


CREATE TABLE s_jsly
(
	-- 自增主键
	id int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	-- 指向e_dljg表的主键
	pid int COMMENT '指向e_dljg表的主键',
	-- 机构代码
	jgdm varchar(100) COMMENT '机构代码',
	-- 统计指标
	zb int COMMENT '统计指标',
	-- 专利类型
	zllx varchar(10) COMMENT '专利类型',
	-- 地域
	dy varchar(10) COMMENT '地域',
	-- 技术领域
	jsly varchar(20) COMMENT '技术领域',
	-- 数量
	sl int COMMENT '数量',
	PRIMARY KEY (id)
);


-- 文章标签
CREATE TABLE tag
(
	-- 主键序号
	id int NOT NULL AUTO_INCREMENT COMMENT '主键序号',
	-- 标签名称
	name varchar(500) COMMENT '标签名称',
	-- 标签热度
	heat int COMMENT '标签热度',
	PRIMARY KEY (id)
) COMMENT = '文章标签';


-- 用户信息表
CREATE TABLE u_user
(
	-- 主键id
	id int NOT NULL AUTO_INCREMENT COMMENT '主键id',
	-- 用户名
	name varchar(100) COMMENT '用户名',
	-- 用户登录名
	login_name varchar(100) COMMENT '用户登录名',
	-- 密码明文
	plain_password varchar(200) COMMENT '密码明文',
	-- 密码
	password
 varchar(100) COMMENT '密码',
	-- 密码盐
	salt varchar(100) COMMENT '密码盐',
	-- 角色
	roles varchar(200) COMMENT '角色',
	-- 邮件地址
	email varchar(50) COMMENT '邮件地址',
	-- 注册日期
	dt_register datetime COMMENT '注册日期',
	-- 邮件验证通过
	vemail int COMMENT '邮件验证通过',
	-- 用户状态
	status int COMMENT '用户状态',
	PRIMARY KEY (id)
) COMMENT = '用户信息表';



