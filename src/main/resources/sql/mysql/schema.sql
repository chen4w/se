drop table if exists e_task;
drop table if exists e_user;

create table e_task (
	id bigint auto_increment,
	title varchar(128) not null,
	description varchar(255),
	user_id bigint not null,
    primary key (id)
) engine=InnoDB;

create table e_user (
	id bigint auto_increment,
	login_name varchar(64) not null unique,
	name varchar(64) not null,
	password varchar(255) not null,
	salt varchar(64) not null,
	roles varchar(255) not null,
	register_date timestamp not null,
	email varchar(255) COMMENT '邮箱',
	status int COMMENT '状态：启用/禁用',
	ssjg int COMMENT '所属机构',
	dt_lastlogin  datetime COMMENT '最近登陆时间',
	sex char(1),
	dt_birthday  datetime,
	adress varchar(255),
	phone varchar(255),
	primary key (id)
) engine=InnoDB;