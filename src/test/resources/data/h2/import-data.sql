
--delete from e_task;
insert into e_task (id, title, description, user_id) values(1, 'Study PlayFramework 2.0','http://www.playframework.org/', 2);
insert into e_task (id, title, description, user_id) values(2, 'Study Grails 2.0','http://www.grails.org/', 2);
insert into e_task (id, title, description, user_id) values(3, 'Try SpringFuse','http://www.springfuse.com/', 2);
insert into e_task (id, title, description, user_id) values(4, 'Try Spring Roo','http://www.springsource.org/spring-roo', 2);
insert into e_task (id, title, description, user_id) values(5, 'Release SpringSide 4.0','As soon as posibble.', 2);

--delete from e_user
--insert into e_user (id, login_name, name, password, salt, roles, register_date) values(1,'admin','Admin','691b14d79bf0fa2215f155235df5e670b64394cc','7efbd59d9741d34f','admin','2012-06-04 01:00:00');
--insert into e_user (id, login_name, name, password, salt, roles, register_date) values(2,'user','Calvin','2488aa0c31c624687bd9928e0a5d29e7d1ed520b','6d65d24122c30500','user','2012-06-04 02:00:00');

insert into e_user (id, login_name, name, password, salt, roles, register_date,email,status,ssjg,dt_lastlogin,birthday) values(1,'admin','admin','cdcbe336a276279fd0d9e38e6ea0143ec6f47286','c5326d5d183a5574','admin','2012-06-04 01:00:00','smile@126.coml',1,0,'2015-06-04 01:00:00','2015-06-04 01:00:00');
insert into e_user (id, login_name, name, password, salt, roles, register_date,email,status,ssjg,dt_lastlogin,birthday) values(2,'admin2','admin2','a308e45fe5e82c5dd736af9b7fbb279958bb28cc','7f79d7ab0a4dbce2','admin_xh','2012-06-04 01:00:00','Dave@126.coml',1,0,'2015-06-04 01:00:00','2015-06-04 01:00:00');
insert into e_user (id, login_name, name, password, salt, roles, register_date,email,status,ssjg,dt_lastlogin,birthday) values(3,'admin3','admin3','75f2390b102f4a51eccb749a3eb69ed8d9b1a308','47759fd86a38ba3d','admin_jg','2016-03-16 02:00:00','crazy03@126.coml',1,1,'2016-03-16 01:00:00','2015-06-04 01:00:00');
insert into e_user (id, login_name, name, password, salt, roles, register_date,email,status,ssjg,dt_lastlogin,birthday) values(4,'user','Calvin','56bdedc3ca955057b2210aa69b94d3a58986e743','a08530e4781ca357','user','2012-06-04 02:00:00','insane@126.coml',1,0,'2016-03-04 01:00:00','2015-06-04 01:00:00');
