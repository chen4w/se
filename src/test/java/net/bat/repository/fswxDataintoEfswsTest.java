package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDlr;
import net.bat.entity.EDlrBak;
import net.bat.entity.EFswx;
import net.bat.entity.SDljg;
import net.bat.web.dljg.DljgIdService;
import net.bat.web.dljg.DljgService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class fswxDataintoEfswsTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	@Autowired
	private DljgIdService dljgids;
	/**
	 * excel导入复审无效数据intoE_FSWS表
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		int count=0;
		System.out.println(Runtime.getRuntime().maxMemory()); 
		System.out.println("输入文件地址：（例如：E:/代理机构总量.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
        
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }    
	     	SimpleDateFormat sim=new SimpleDateFormat("yyyyMMdd");
	    	//Date d=null;
	        List<String[]> list= ReadFile.readExcel(file);  
	        System.out.println(list.size());
	        for (int i = 0; i < list.size(); i++) {
	        	EFswx eo= new EFswx();
	        	if(Integer.parseInt(list.get(i)[4])==1)
	        	{
	        		eo.setFswx(1);
	        	}
	        	if(Integer.parseInt(list.get(i)[5])==1)
	        	{
	        		eo.setFswx(2);
	        	}
	         
	        	eo.setZt(Integer.parseInt(list.get(i)[1]));
	        	eo.setJdh(list.get(i)[2]);
	        	if(list.get(i)[3]!=null)
	        	{
	        		eo.setDtJdr(sim.parse(list.get(i)[3]));
	        		}
	        	eo.setSqh(list.get(i)[6]);
	        	if(list.get(i)[7]!=null)
	        	{
	        		eo.setDtSqr(sim.parse(list.get(i)[7]));	
	        	}
	        	eo.setSqr(list.get(i)[8]);
	        	eo.setGkh(list.get(i)[10]);
	        	if(list.get(i)[11]!=null)
	        	{
	        		eo.setDtGkr(sim.parse(list.get(i)[11]));
	        	}
	        	eo.setFmmc(list.get(i)[12]);
	        	eo.setFlh(list.get(i)[13]);
	        	eo.setFssqr(list.get(i)[14]);
	  
	        	try {
					eo.setWxsqr(list.get(i)[15]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					eo.setWxsqr("");
				}
	        	try {
					eo.setZzxm(list.get(i)[16]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					eo.setZzxm("");
				}
	        	try {
					eo.setZsyxm(list.get(i)[17]);
				} catch (Exception e) {
					eo.setZsyxm("");
				}
	        	try {
					eo.setCsyxm(list.get(i)[18]);
				} catch (Exception e) {
					eo.setCsyxm("");
				}
	        	try {
					eo.setGgh(list.get(i)[20]);
				} catch (Exception e) {
					eo.setGgh("");
				}
	        	try {
					if(list.get(i)[21]!=null)
					{
						eo.setDtGgr(sim.parse(list.get(i)[21]));
					}
				} catch (Exception e) {
					eo.setDtGgr(null);
				}
	        	dao.save(eo);
	        	count++;
			}
	     
	     System.out.println(count);
	        
	}

	
	
}
