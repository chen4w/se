package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDlr;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class DlrBaseDataTest1 extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	
	/**
	 * excel导入代理基础数据
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		
		
		StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		System.out.println("输入文件1地址：（例如：E:/专利代理人信息for信息中心（20160328）.xls）");
		//E:/专利代理人信息（样例）.xls
		String file1Path = sc.nextLine();
		System.out.println("输入文件2地址：（例如：E:/领军和高人联系簿.xlsx）");
		//E:/专利代理人信息（样例）.xls
		String file2Path = sc.nextLine();
        
		 File file1 = new File(file1Path);  
		 File file2 = new File(file2Path);  
	        if(!file1.exists()){  
	            System.out.println("1文件不存在");  
	            return;  
	        }  
	        if(!file2.exists()){  
	            System.out.println("2文件不存在");  
	            return;  
	        } 
	        List<String[]> list= ReadFile.readExcel(file1); 
	        List<String[]> list2= ReadFile.readExcel(file2);  
	        System.out.println(list.size());
	        for (int i = 0; i < list.size(); i++) {
	        	EDlr eo = new EDlr();
	        	eo.setPid(Integer.valueOf(list.get(i)[0]));
	        	eo.setXm(list.get(i)[1]);
	        	eo.setXb(list.get(i)[2]);
	        	eo.setSxzy(list.get(i)[3]);
	        	eo.setSzcs(list.get(i)[4]);
	        	eo.setZgzh(list.get(i)[5]);
	        	eo.setZyzh(list.get(i)[6]);
	        	eo.setLjrw(getState(list2,list.get(i)[5]));
	        	
	        	SimpleDateFormat sim=new SimpleDateFormat("yyyy年MM月dd日");
	        	if(list.get(i).length==9){//有的数据excel中一行并不是9个单元格。不足9个数据肯定不全，需要判断
	        	if(list.get(i)[8]!=null&&list.get(i)[8]!="")//判断首次执业时间是否为空
	        	{
	        		System.out.println(list.get(i)[8]);
	        		if(list.get(i)[8].indexOf("年")>0&&list.get(i)[8].indexOf("月")>0&&list.get(i)[8].indexOf("日")>0&&list.get(i)[8].length()<12)//判断首次执业时间是否为年月日形式。
	        		{   
	        			Date d=sim.parse(list.get(i)[8]);
	        			eo.setDtGzjy(d);
	        		}
	        		else
	        		{
	        			sb.append("姓名为:"+list.get(i)[1]+"(机构代码为"+list.get(i)[0]+")的代理人首次执业时间格式无法解析，需手工录入...@");
	        		}
	        	}
	        	}else
	        	{
	        		sb.append("姓名为:"+list.get(i)[1]+"(机构代码为"+list.get(i)[0]+")的代理人数据格式无法解析，需手工录入...@");
	        	}
	        	dao.save(eo);
			}
	        String str= sb.toString();
	         for(int  i=0;i<str.split("@").length;i++)
	         {
	        	 System.out.println(str.split("@")[i]);
	         }
	        
	}

	
	private int getState(List<String[]> ls2,String zgzh){
		for (int i = 0; i < ls2.size(); i++) {
			if((ls2.get(i)[2]).equals(zgzh)){
				System.out.println(ls2.get(i)[6]);
				
				if(ls2.get(i)[6].equals("√")){
					return 1;
				}else{
					return 0;
				}
			}
		}
		
		return 0;
	}
	
	
}
