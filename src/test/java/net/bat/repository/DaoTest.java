package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Page;

import net.bat.dao.QueryResult;
import net.bat.dao.UserDAO;
import net.bat.entity.EDljg;
import net.bat.entity.SDljg;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class DaoTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;

	/**
	 * excel导入数据到代理机构表
	 * @throws Exception
	 */
	public void searchDljg() throws Exception {	
		List<Object[]> ls = dao.query(0,5,
				"from EDljg a, SDljg b where b.jgdm=a.jgdm  and b.fl='100000' and b.sl>10 order by b.sl",
				null,null);
		if(ls.size()==0)
			return;
		Object[] r= ls.get(0);
		EDljg r0=(EDljg)r[0];
		SDljg r1=(SDljg)r[1];
		System.out.println(r0.getMc());
		System.out.println(r1.getSl());
	}
	@Test
	public void getDljgList() throws Exception {
		//String selectjpql = "select a.jgdm, a.mc, b.sl, a.dtSlsj, a.dlrrs, a.dy from EDljg a, SDljg b";
		String selectjpql = "select new net.bat.dto.EDljgDTO(a.jgdm, a.mc, b.sl, a.dtSlsj, a.dlrrs, a.dy)  from EDljg a, SDljg b";
		
		String selectjpql_cout="select count(*) from EDljg a, SDljg b";
		String wherejpql = "a.jgdm=b.jgdm  and b.fl=(?1)";
		int firstindex=11;
		int maxresult =20;
		List<Object> queryParams = new ArrayList<Object>();
		queryParams.add("100002");
		String orderby="b.sl desc";
		QueryResult<Map> qr = dao.getScrollData(selectjpql, selectjpql_cout, 
				firstindex, maxresult, wherejpql, queryParams.toArray(), orderby);
		
		//Page p = new PageImpl(qr.getResultlist(), searchable.getPage(), qr.getTotalrecord());
		
		System.out.println(qr.getTotalrecord());
	}
		
	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(i+"    ___________________");
			
		}
	}
	
}
