package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDlr;
import net.bat.web.dljg.DljgIdService;
import net.bat.web.dljg.DljgService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class DljgBaseDataTestlh extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	@Autowired
	private DljgIdService dljgids;
	/**
	 * excel导入代理基础数据
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		System.out.println("输入文件地址：（例如：E:/专利代理人信息.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
        
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }    
	        List<String[]> list= ReadFile.readExcel(file);  
	        System.out.println(list.size());
	        for (int i = 0; i < list.size(); i++) {
	        	EDlr eo = new EDlr();
	        	if(list.get(i)[0]!=null&&!"".equals(list.get(i)[0])){
	        	if(dljgids.getDljgBj(Integer.valueOf(list.get(i)[0]).toString())!=null){
	        		eo.setPid(Integer.valueOf(dljgids.getDljgBj(Integer.valueOf(list.get(i)[0]).toString()).getId().toString()));
	        	}else{	        	
	        		sb.append("姓名为:"+list.get(i)[1]+"(机构代码为"+list.get(i)[0]+")的代理人的机构代码在数据库中不存在，需手工录入...@");	        		
	        		continue;
	        	}
	        	}else{
	        		sb.append("第"+i+"行"+"的机构代码为空，请检查...@");
	        	}
	        	
	        	eo.setXm(list.get(i)[1]);
	        	eo.setXb(list.get(i)[2]);
	        	eo.setSxzy(list.get(i)[3]);
	        	eo.setSzcs(list.get(i)[4]);
	        	eo.setZgzh(list.get(i)[5]);
	        	eo.setZyzh(list.get(i)[6]);
	        	SimpleDateFormat sim=new SimpleDateFormat("yyyy年MM月dd日");
	        	if(list.get(i).length==9){//有的数据excel中一行并不是9个单元格。不足9个数据肯定不全，需要判断
	        	if(list.get(i)[8]!=null&&list.get(i)[8]!="")//判断首次执业时间是否为空
	        	{
	        		//System.out.println(list.get(i)[8]);
	        		if(list.get(i)[8].indexOf("年")>0&&list.get(i)[8].indexOf("月")>0&&list.get(i)[8].indexOf("日")>0&&list.get(i)[8].length()<12)//判断首次执业时间是否为年月日形式。
	        		{   
	        			Date d=sim.parse(list.get(i)[8]);
	        			eo.setDtGzjy(d);
	        		}
	        		else
	        		{
	        			sb.append("姓名为:"+list.get(i)[1]+"(机构代码为"+list.get(i)[0]+")的代理人首次执业时间格式无法解析，需手工录入...@");
	        		}
	        	}
	        	}else
	        	{
	        		sb.append("姓名为:"+list.get(i)[1]+"(机构代码为"+list.get(i)[0]+")的代理人数据格式无法解析，需手工录入...@");
	        	}
	        	dao.save(eo);
			}
	        String str= sb.toString();
	         for(int  i=0;i<str.split("@").length;i++)
	         {
	        	 System.out.println(str.split("@")[i]);
	         }
	        
	}

	
	
}
