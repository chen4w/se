package net.bat.repository;

import static org.assertj.core.api.Assertions.*;
import net.bat.dao.UserDAO;
import net.bat.entity.Task;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class UserDaoTest extends SpringTransactionalTestCase {

	@Autowired
	private UserDAO dao;

	@Test
	public void findTasksByUserId() throws Exception {
		Task task = dao.find(Task.class, 3L);
		assertThat(task).isNotNull();
	}
}
