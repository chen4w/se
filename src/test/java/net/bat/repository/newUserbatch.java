/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package net.bat.repository;


import java.io.File;
import java.util.List;
import java.util.Scanner;
import net.bat.entity.User;
import net.bat.service.account.AccountService;
import net.bat.web.dljg.DljgIdService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.security.utils.Digests;
import org.springside.modules.test.spring.SpringTransactionalTestCase;
import org.springside.modules.utils.Encodes;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class newUserbatch extends SpringTransactionalTestCase {
	private static final int SALT_SIZE = 8;
	public static final int HASH_INTERATIONS = 1024;
	@Autowired
	private UserDao userDao;
	@Autowired
	private AccountService as;
	@Autowired
	private DljgIdService dljgids;
	@Test
	@Rollback(false)
	public void BuildUser() throws Exception {
		
	 	StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		System.out.println("输入代理机构基本信息文件地址：（例如：E:/代理机构信息.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }    
	        List<String[]> list= ReadFile.readExcel(file);  
	        for(int i = 0; i < list.size(); i++) { 
	          
	        	 User u= new User();
	        	// u.setName(list.get(i)[0]);
	         	 u.setName(list.get(i)[0]);
	         	 u.setLoginName(list.get(i)[0]);
	        //	 u.setPassword("123456");
	        //	 u.setPlainPassword("123456");
	             u.setRoles("admin_jg");
	         	// u.setSsjg(dljgids.getDljgBj(list.get(i)[0]).getId().intValue());
	        	 byte[] salt = Digests.generateSalt(SALT_SIZE);
	     	   	 u.setSalt(Encodes.encodeHex(salt));
                 byte[] hashPassword = Digests.sha1("123456".getBytes(), salt, HASH_INTERATIONS);
	     		 u.setPassword(Encodes.encodeHex(hashPassword));
	             userDao.save(u);
	            
		
		
	
	        }}
}