package net.bat.repository;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDljg;
import net.bat.entity.EDlr;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class DljgBaseDataTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;

	/**
	 * excel导入代理机构基础数据
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {
		StringBuffer sb = new StringBuffer();// 用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);// 获取键盘，以便输入地址
		System.out.println("输入文件地址：（例如：E:/专利代理人信息.xls）");
		// E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();

		File file = new File(filePath);
		if (!file.exists()) {
			System.out.println("文件不存在");
			return;
		}
		List<String[]> list = ReadFile.readExcel(file);
		System.out.println(list.size());
		for (int i = 0; i < list.size(); i++) {
			EDljg ed = new EDljg();
			ed.setJgdm(list.get(i)[0]);
			ed.setMc(list.get(i)[1]);
			ed.setGsxz(list.get(i)[2]);
			// ed.setDtSlsj(list.get(i)[3]);
			ed.setDz(list.get(i)[4]);
			ed.setFzr(list.get(i)[5]);
			ed.setDh(list.get(i)[6] + ";" + list.get(i)[7]);
			ed.setDzxx(list.get(i)[8]);
			ed.setWz(list.get(i)[9]);
		    
		  //得到地址,通过省或市，拆分得到地域
		    String dy=list.get(i)[4];
		    if(dy.contains("省")){
		    	String[] dyy=dy.split("省");
		    	String area=dyy[0];
		    	ed.setDy(area);
		    }
		    if(dy.contains("市")&&!dy.contains("省")){
		    	String[] dyy=dy.split("市");
		    	String area=dyy[0];
		    	ed.setDy(area);
		    	//&&!dy.contains("省")&&!dy.contains("市")
		    } if(dy.contains("自治区")){
		    	String[] dyy=dy.split("自治区");
		    	String area=dyy[0];
		    	ed.setDy(area);
		    }

			
			

			System.out.println(list.get(i)[3]);
			SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
			// if(list.get(i).length==9){//有的数据excel中一行并不是9个单元格。不足9个数据肯定不全，需要判断
			if (list.get(i)[3] != null && list.get(i)[3] != "")// 判断首次执业时间是否为空
			{
				try {
					sim.setLenient(false);// 设置为严格检验
					sim.parse(list.get(i)[3]);
					Date d = sim.parse(list.get(i)[3]);
					ed.setDtSlsj(d);
				} catch (ParseException e) {
					sb.append("名称为:" + list.get(i)[1] + "(机构代码为"
							+ list.get(i)[0] + ")的代理机构设立时间时间格式无法解析，需手工录入...@");
					// 如果throw
					// java.text.ParseException或者NullPointerException，就说明格式不对
				}
			}
			dao.save(ed);
		}
		String str = sb.toString();
		for (int i = 0; i < str.split("@").length; i++) {
			System.out.println(str.split("@")[i]);
		}

	}

}
