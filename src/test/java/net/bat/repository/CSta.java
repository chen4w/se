package net.bat.repository;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class CSta extends SpringTransactionalTestCase {
	@Test
	public void parseAgentSta() throws Exception {
		String fpath = "/users/chen4w/desktop/AgentStatistics.txt";
		int len_col = 10;

		Date dt_start = new Date();
		File file = new File(fpath);
		Scanner sc = new Scanner(file);
		sc.nextLine();

		int[] cols = new int[len_col];
		// 最近一次机构代码
		int last_ac = 0;
		int total_ac = 0;
		int total = 0;

		int total_name = 0;
		String name, last_name = null;
		// Type PatentType Region Class Year Number

		while (sc.hasNextLine()) {
			sc.useDelimiter("\\s+");
			for (int i = 0; i < len_col; i++) {
				if (i == 2) {
					sc.next();
				} else if (i == 3) {
					name = sc.next();
					if ((last_name == null) || !last_name.equals(name)) {
						total_name++;
						System.out.println(total_name + ":" + name);
					}
					last_name = name;
				} else {
					cols[i] = sc.nextInt();
					// 保持机构代码，用于判断机构是否下一个机构
					if (i == 1) {
						if (last_ac != cols[i]) {
							total_ac++;
							System.out.println(total_ac + ":" + cols[i]);
						}
						last_ac = cols[i];
					}
				}
			}
			total++;
		}
		long span = new Date().getTime() - dt_start.getTime();
		System.out.println("total_ac:" + total_ac + "  total:" + total + "  span:" + span + "ms");
	}

	public void parseAgencySta() throws Exception {
		String fpath = "/users/chen4w/desktop/AgencyStatistics.txt";
		int len_col = 9;

		Date dt_start = new Date();
		File file = new File(fpath);
		Scanner sc = new Scanner(file);
		sc.nextLine();

		int[] cols = new int[len_col];
		// 最近一次机构代码
		int last_ac = 0;
		int total_ac = 0;
		int total = 0;
		// Type PatentType Region Class Year Number

		while (sc.hasNextLine()) {
			sc.useDelimiter("\\s+");
			for (int i = 0; i < len_col; i++) {
				if (i == 2) {
					sc.next();
				} else {
					cols[i] = sc.nextInt();
					// 保持机构代码，用于判断机构是否下一个机构
					if (i == 1) {
						if (last_ac != cols[i]) {
							total_ac++;
							System.out.println(total_ac + ":" + cols[i]);
						}
						last_ac = cols[i];
					}
				}
			}
			total++;
		}
		long span = new Date().getTime() - dt_start.getTime();
		System.out.println("total_ac:" + total_ac + "  total:" + total + "  span:" + span + "ms");
	}
}
