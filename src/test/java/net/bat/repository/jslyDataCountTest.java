package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.DJslyfl;
import net.bat.entity.EDlr;
import net.bat.entity.EDlrBak;
import net.bat.entity.SDljg;
import net.bat.entity.SDlr;
import net.bat.entity.SJsly;
import net.bat.web.dljg.DljgIdService;
import net.bat.web.dljg.DljgService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class jslyDataCountTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	@Autowired
	private DljgIdService dljgids;
	/**
	 * excel导入代理机构按技术领域分类统计信息/技术领域分类信息导入字典表/国民经济产业分类
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		int count=0;
		System.out.println("输入文件地址：（例如：E:/代理机构总量.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
        
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }    
	        System.out.println("开始读取excel！");
	        List<String[]> list= ReadFile.readExcel(file);  
	        System.out.println("excel大小为"+list.size()+"条");
	        for (int i = 0; i < list.size(); i++) {
	       
	        /*	 DJslyfl  eo= new DJslyfl();
	         * eo.setFlfs(1);
	            eo.setFlh(list.get(i)[0]+list.get(i)[1]);
	            eo.setMc(list.get(i)[2]);//导入ipc分类*/
	         	/*eo.setFlfs(2);
                eo.setFlh(list.get(i)[0]);
                eo.setMc(list.get(i)[1]);//导入ipc分类 
                
            */ 
	        /*	DJslyfl  eo= new DJslyfl();
	        	eo.setFlfs(3);
	        	if(list.get(i)[1]==null)
	        	{
	        		 eo.setFlh(list.get(i)[0]);
	        	}else
	        	{
                eo.setFlh(list.get(i)[0]+list.get(i)[1]);
	        	}
                eo.setMc(list.get(i)[2]);//导入国民经济产业分类
                dao.save(eo);*/
	           SJsly sj= new SJsly();
	           sj.setJgdm(list.get(i)[1]);
	           sj.setZb(Integer.parseInt(list.get(i)[3]));
	           sj.setZllx(list.get(i)[4]);
	           sj.setDy(list.get(i)[5]);
	           sj.setJsly(list.get(i)[6]);
	           sj.setSl(Integer.parseInt(list.get(i)[7]));
	           dao.save(sj);
	        	count++; 
			}
	     
	     System.out.println(count);
	        
	}

	
	
}
