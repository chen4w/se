package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDljg;
import net.bat.entity.EDlr;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class EDlrDaoTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;

	/**
	 * excel导入数据到代理人表
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		/*EDlr edlr = new EDlr();
		edlr.setXm("郭靖");
		dao.save(edlr);*/
		
		//代理机构导入excel
		Scanner sc = new Scanner(System.in);

		System.out.println("输入文件地址：（例如：E:/专利代理人信息.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
		
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }  
	        ReadFile rf = new ReadFile();  
	        List<String[]> list= ReadFile.readExcel(file);  
	        System.out.println(list.size());
	       // String[] strs = null;
	        for (int i = 0; i < list.size(); i++) {
	        	EDlr eo = new EDlr();
	        	eo.setPid(Integer.valueOf(list.get(i)[0]));
	        	eo.setXm(list.get(i)[1]);
	        	eo.setXb(list.get(i)[2]);
	        	eo.setSzcs(list.get(i)[4]);
	        	eo.setZgzh(list.get(i)[5]);
	        	eo.setZyzh(list.get(i)[6]);
	        	SimpleDateFormat sim=new SimpleDateFormat("yyyy年MM月dd日");
	        	String str=list.get(i)[9];
	        	System.out.println(str);
	        	Date d=sim.parse(str);
	        	eo.setDtGzjy(d);
	        	dao.save(eo);
			}
	}
	
	
	
	/**
	 * 数据仿真：填充伪数据到代理人表
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertData() throws Exception {
		String[] xms={"韩信","韩愈","赵飞燕","赵合德","杨玉环","西施","貂蝉","王昭君","司马相如","卓文君"};
		String[] xbs={"男","女"};
		//所在省市
		String[] szcss={"11","12","13","14","15","16","17","18","19","20"};
		//资格证号
		String[] zgzhs={"1111111","1111112","1111113","1111114","1111115","1111116","1111117","1111118","1111119","1111120"};
		//执业证号
		String[] zyzhs={"1111111111","1111111112","1111111113","1111111114","1111111115","1111111116","1111111117","1111111118","1111111119","1111111120"};
		for (int i = 0; i <10000; i++) {
			System.out.println(i+"    ___________");
			Date randomDate=randomDate("1991-01-01","2016-01-01"); 
			int rand=(int)(Math.random()*1000)+11001;
			int randxm=(int)(Math.random()*10);
			int randxb=(int)(Math.random()*2);
			int randszcs=(int)(Math.random()*10);
			int randzgzh=(int)(Math.random()*10);
			int randzyzh=(int)(Math.random()*10);
			//int pid=11003+i;
			EDlr eo = new EDlr();
        	eo.setPid(rand);
        	eo.setXm(xms[randxm]);
        	eo.setXb(xbs[randxb]);
        	eo.setSzcs(szcss[randszcs]);
        	eo.setZgzh(zgzhs[randzgzh]);
        	eo.setZyzh(zyzhs[randzyzh]);

        	eo.setDtGzjy(randomDate);
			dao.save(eo);
		}
		
		/*Map rm = new HashMap<String, Object>();
		rm.put("mc", "代理机构111");
		dao.add(EDljg.class, rm);
		
		EDljg eo = new EDljg();
		eo.setMc("代理机构222");
		dao.save(eo);

		long total = dao.getCount(EDljg.class);
		System.out.println(total);*/
		
	}
	


	//java生成某个时间段内的随机时间（先定义一个时间段，之后随机生成符合条件的时间）：
	//Date randomDate=randomDate("2010-09-20","2010-09-21"); 
	/** 
	* 生成随机时间 
	* @param beginDate 
	* @param endDate 
	* @return 
	*/ 
	private static Date randomDate(String beginDate,String  endDate ){  
	try {  
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
	Date start = format.parse(beginDate);//构造开始日期  
	Date end = format.parse(endDate);//构造结束日期  
	//getTime()表示返回自 1970 年 1 月 1 日 00:00:00 GMT 以来此 Date 对象表示的毫秒数。  
	if(start.getTime() >= end.getTime()){  
	return null;  
	}  
	long date = random(start.getTime(),end.getTime());  
	return new Date(date);  
	} catch (Exception e) {  
	e.printStackTrace();  
	}  
	return null;  
	}  

	private static long random(long begin,long end){  
		long rtn = begin + (long)(Math.random() * (end - begin));  
		//如果返回的是开始时间和结束时间，则递归调用本函数查找随机值  
		if(rtn == begin || rtn == end){  
		return random(begin,end);  
		}  
		return rtn;  
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(i+"    ___________________");
			
		}
	}
	
}
