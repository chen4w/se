package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDlr;
import net.bat.entity.EDlrBak;
import net.bat.entity.EFswx;
import net.bat.entity.SDljg;
import net.bat.entity.SFswx;
import net.bat.web.dljg.DljgIdService;
import net.bat.web.dljg.DljgService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class fswxDataintoSfswsTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	@Autowired
	private DljgIdService dljgids;

	/**
	 * excel导入复审无效数据intoS_FSWS表
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {
		StringBuffer sb = new StringBuffer();// 用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);// 获取键盘，以便输入地址
		int count = 0;
		System.out.println("输入文件地址：（例如：E:/代理机构总量.xls）");
		// E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();

		File file = new File(filePath);
		if (!file.exists()) {
			System.out.println("文件不存在");
			return;
		}
		SimpleDateFormat sim = new SimpleDateFormat("yyyyMMdd");
		// Date d=null;
		List<String[]> list = ReadFile.readExcel(file);
		System.out.println(list.size());
		for (int i = 0; i < list.size(); i++) {
		
 
 
			/*	try {
					String[] dlr = list.get(i)[6].split(" +");
 
						for (int j = 0; j < dlr.length; j++) {
							SFswx eo = new SFswx();
							eo.setJdh(list.get(i)[1]);
							eo.setJgdm(list.get(i)[4]);
							eo.setXm(dlr[j]);
							if(list.get(i)[3].indexOf("审")>0)
							{
								eo.setFl(1);
							}
							if(list.get(i)[3].indexOf("效")>0)
							{
								eo.setFl(2);
							}
							try {
								eo.setJdr(sim.parse(list.get(i)[2]));
							} catch (Exception e) {
							 
							}
							eo.setDlrfl(1);
							dao.save(eo);
						 
					}
				} catch (Exception e) {
					SFswx eo = new SFswx();
					eo.setJdh(list.get(i)[1]);
					eo.setJgdm(list.get(i)[4]);
					if(list.get(i)[3].indexOf("审")>0)
					{
						eo.setFl(1);
					}
					if(list.get(i)[3].indexOf("效")>0)
					{
						eo.setFl(2);
					}
					try {
						eo.setJdr(sim.parse(list.get(i)[2]));
					} catch (Exception e1) {
					 
					}
					//eo.setXm(dlr[j]);
					eo.setDlrfl(1);
					dao.save(eo);
				} */
			 
	 		try {
				String[] dlr = list.get(i)[9].split(" +");

					for (int j = 0; j < dlr.length; j++) {
						SFswx eo = new SFswx();
						eo.setJdh(list.get(i)[1]);
						eo.setJgdm(list.get(i)[7]);
						eo.setXm(dlr[j]);
						eo.setDlrfl(2);
						if(list.get(i)[3].indexOf("审")>0)
						{
							eo.setFl(1);
						}
						if(list.get(i)[3].indexOf("效")>0)
						{
							eo.setFl(2);
						}
						try {
							eo.setJdr(sim.parse(list.get(i)[2]));
						} catch (Exception e) {
						 
						}
						dao.save(eo);
					 
				}
			} catch (Exception e) {
			
				try {
					SFswx eo = new SFswx();
					eo.setJdh(list.get(i)[1]);
					eo.setJgdm(list.get(i)[7]);
					eo.setDlrfl(2);
					if(list.get(i)[3].indexOf("审")>0)
					{
						eo.setFl(1);
					}
					if(list.get(i)[3].indexOf("效")>0)
					{
						eo.setFl(2);
					}
					try {
						eo.setJdr(sim.parse(list.get(i)[2]));
					} catch (Exception e2) {
					 
					}
					dao.save(eo);
				} catch (Exception e1) {
				 
				}
				//eo.setXm(dlr[j]);
				
		 
 
		}

		//System.out.println(count);

	}
	}}
 
