package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDlr;
import net.bat.entity.EDlrBak;
import net.bat.entity.SDljg;
import net.bat.entity.SDlr;
import net.bat.web.dljg.DljgIdService;
import net.bat.web.dljg.DljgService;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class dlrDataCount extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;
	@Autowired
	private DljgIdService dljgids;
	/**
	 * excel导入代理人统计数据
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		StringBuffer sb= new StringBuffer();//用来保存没有录入的信息。
		Scanner sc = new Scanner(System.in);//获取键盘，以便输入地址
		int count=0;
		System.out.println("输入文件地址：（例如：E:/代理机构总量.xls）");
		//E:/专利代理人信息（样例）.xls
		String filePath = sc.nextLine();
        
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }    
	        System.out.println("开始读取excel！");
	        List<String[]> list= ReadFile.readExcel(file);  
	        System.out.println("excel大小为"+list.size()+"条");
	        for (int i = 0; i < list.size(); i++) {
	        	SDlr eo = new SDlr();
	        	eo.setJgdm(list.get(i)[1]);
	      
	        	eo.setXm(list.get(i)[3]);
	          	eo.setFl(list.get(i)[4]+list.get(i)[5]+list.get(i)[6]+list.get(i)[7]);
	          	eo.setNd(Integer.parseInt(list.get(i)[8]));
	        	eo.setSl(Integer.parseInt(list.get(i)[9]));
	        	dao.save(eo);
	        	count++;
			}
	     
	     System.out.println(count);
	        
	}

	
	
}
