package net.bat.repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import net.bat.dao.UserDAO;
import net.bat.entity.EDljg;
import net.bat.entity.EDlr;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class EDljgDaoTest extends SpringTransactionalTestCase {
	@Autowired
	private UserDAO dao;

	/**
	 * excel导入数据到代理机构表
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertDataByExcel() throws Exception {	
		//代理机构导入excel
		Scanner sc = new Scanner(System.in);

		System.out.println("输入文件地址：（例如：E:/01.xlsx）");
		String filePath = sc.nextLine();
		
		 File file = new File(filePath);  
	        if(!file.exists()){  
	            System.out.println("文件不存在");  
	            return;  
	        }  
	        ReadFile rf = new ReadFile();  
	        List<String[]> list= rf.readExcel(file);  
	        System.out.println(list.size());
	        String[] strs = null;
	        for (int i = 0; i < list.size(); i++) {
	        	EDljg eo = new EDljg();
	        	eo.setJgdm(list.get(i)[0]);
	        	eo.setMc(list.get(i)[2]);
	        	eo.setDz(list.get(i)[7]);
	        	eo.setFzr(list.get(i)[8]);
	        	String slsj=list.get(i)[5];//设立时间
	        	SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd");
	        	String str=slsj;
	        	Date d=sim.parse(str);
	        	//System.out.println(d);
	        	eo.setDtSlsj(d);
	        	
	        	dao.save(eo);
			}
	}
	
	
	/**
	 * 数据仿真：填充伪数据到代理机构表
	 * @throws Exception
	 */
	@Test
	@Rollback(false)
	public void insertData() throws Exception {
		String[] mcs={"北京卓爱普知识产权代理有限公司","北京轻创知识产权代理有限公司","北翔知识产权代理公司","信慧永光知识产权代理公司","德琦知识产权代理公司","北京商专知识产权代理有限公司 ","青松知识产权代理事务所","北京汇泽知识产权代理有限公司","康信知识产权代理公司","北京三环知识产权代理公司",
				"北京展翼知识产权代理事务所","北翔知识产权代理有限公司接待","英赛嘉华知识产权代理公司","律城同业知识产权代理公司","北京理利仁知识产权代理有限公司","北京英创嘉友知识产权代理事务所","燕真了(北京)知识产权代理事务所有限公司","北京汇泽诚信知识产权代理有限公司","北京方亚清源知识产权代理事务所","北京汇信合知识产权代理有限公司湖州分公司"};
		String[] dzs={"北京市海淀区知春路1号学院国际大厦1812","北京市海淀区知春路7号致真大厦A座14层1404-1405室","学院路35号世宁大厦9层908","知春路9号坤讯大厦11层1106","知春路1号学院国际大厦7层0705","知春路1号学院国际大厦901室","知春路9号坤讯大厦3楼301室","北京市海淀区知春路锦秋国际大厦18层","知春路甲48-1号盈都大厦A座15层","北四环中路238号柏彦大厦1701A",
				"知春路22号院4国鹏大厦写字楼4F423","学院路35号世宁大厦903","知春路甲48-1号盈都大厦A座19层","知春路甲48-2号盈都大厦B座16层","北京市海淀区中关村南四街东华合创大厦8层","北京市海淀区中关村南四街东华合创大厦9层","北四环西路9号银谷大厦2104号室","中关村东路66号世纪科贸大厦2号楼11层1206","北京市海淀区中关村东路66号世纪科贸大厦C座11层","中关村东路66号世纪科贸大厦C座12层"};
		String[] fzrs={"郭靖","黄蓉","康熙","雍正","乾隆","嘉庆","道光","咸丰","同治","光绪"};
		
		for (int i = 0; i <1000; i++) {
			System.out.println(i+"    ___________");
			Date randomDate=randomDate("1991-01-01","2016-01-01"); 
			int rand=(int)(Math.random()*20);
			int randfzr=(int)(Math.random()*10);
			int jgdm=11001+i;
			EDljg eo = new EDljg();
			eo.setJgdm(String.valueOf(jgdm));
			eo.setMc(mcs[rand]+i);
			eo.setDz(dzs[rand]);
			eo.setFzr(fzrs[randfzr]);
			eo.setDtSlsj(randomDate);
			dao.save(eo);
		}
		
		/*Map rm = new HashMap<String, Object>();
		rm.put("mc", "代理机构111");
		dao.add(EDljg.class, rm);
		
		EDljg eo = new EDljg();
		eo.setMc("代理机构222");
		dao.save(eo);

		long total = dao.getCount(EDljg.class);
		System.out.println(total);*/
		
	}
	


	//java生成某个时间段内的随机时间（先定义一个时间段，之后随机生成符合条件的时间）：
	//Date randomDate=randomDate("2010-09-20","2010-09-21"); 
	/** 
	* 生成随机时间 
	* @param beginDate 
	* @param endDate 
	* @return 
	*/ 
	private static Date randomDate(String beginDate,String  endDate ){  
	try {  
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
	Date start = format.parse(beginDate);//构造开始日期  
	Date end = format.parse(endDate);//构造结束日期  
	//getTime()表示返回自 1970 年 1 月 1 日 00:00:00 GMT 以来此 Date 对象表示的毫秒数。  
	if(start.getTime() >= end.getTime()){  
	return null;  
	}  
	long date = random(start.getTime(),end.getTime());  
	return new Date(date);  
	} catch (Exception e) {  
	e.printStackTrace();  
	}  
	return null;  
	}  

	private static long random(long begin,long end){  
		long rtn = begin + (long)(Math.random() * (end - begin));  
		//如果返回的是开始时间和结束时间，则递归调用本函数查找随机值  
		if(rtn == begin || rtn == end){  
		return random(begin,end);  
		}  
		return rtn;  
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(i+"    ___________________");
			
		}
	}
	
}
